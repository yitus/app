package yitus.web.rest;

import yitus.service.AidantService;
import yitus.web.rest.errors.BadRequestAlertException;
import yitus.service.dto.AidantDTO;
import yitus.service.dto.AidantCriteria;
import yitus.service.AidantQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link yitus.domain.Aidant}.
 */
@RestController
@RequestMapping("/api")
public class AidantResource {

    private final Logger log = LoggerFactory.getLogger(AidantResource.class);

    private static final String ENTITY_NAME = "aidant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AidantService aidantService;

    private final AidantQueryService aidantQueryService;

    public AidantResource(AidantService aidantService, AidantQueryService aidantQueryService) {
        this.aidantService = aidantService;
        this.aidantQueryService = aidantQueryService;
    }

    /**
     * {@code POST  /aidants} : Create a new aidant.
     *
     * @param aidantDTO the aidantDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aidantDTO, or with status {@code 400 (Bad Request)} if the aidant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/aidants")
    public ResponseEntity<AidantDTO> createAidant(@Valid @RequestBody AidantDTO aidantDTO) throws URISyntaxException {
        log.debug("REST request to save Aidant : {}", aidantDTO);
        if (aidantDTO.getId() != null) {
            throw new BadRequestAlertException("A new aidant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AidantDTO result = aidantService.save(aidantDTO);
        return ResponseEntity.created(new URI("/api/aidants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aidants} : Updates an existing aidant.
     *
     * @param aidantDTO the aidantDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aidantDTO,
     * or with status {@code 400 (Bad Request)} if the aidantDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aidantDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/aidants")
    public ResponseEntity<AidantDTO> updateAidant(@Valid @RequestBody AidantDTO aidantDTO) throws URISyntaxException {
        log.debug("REST request to update Aidant : {}", aidantDTO);
        if (aidantDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AidantDTO result = aidantService.save(aidantDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aidantDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /aidants} : get all the aidants.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aidants in body.
     */
    @GetMapping("/aidants")
    public ResponseEntity<List<AidantDTO>> getAllAidants(AidantCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Aidants by criteria: {}", criteria);
        Page<AidantDTO> page = aidantQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aidants/count} : count all the aidants.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/aidants/count")
    public ResponseEntity<Long> countAidants(AidantCriteria criteria) {
        log.debug("REST request to count Aidants by criteria: {}", criteria);
        return ResponseEntity.ok().body(aidantQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /aidants/:id} : get the "id" aidant.
     *
     * @param id the id of the aidantDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aidantDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/aidants/{id}")
    public ResponseEntity<AidantDTO> getAidant(@PathVariable Long id) {
        log.debug("REST request to get Aidant : {}", id);
        Optional<AidantDTO> aidantDTO = aidantService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aidantDTO);
    }

    /**
     * {@code DELETE  /aidants/:id} : delete the "id" aidant.
     *
     * @param id the id of the aidantDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/aidants/{id}")
    public ResponseEntity<Void> deleteAidant(@PathVariable Long id) {
        log.debug("REST request to delete Aidant : {}", id);
        aidantService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
