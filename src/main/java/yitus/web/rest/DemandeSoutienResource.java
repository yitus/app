package yitus.web.rest;

import yitus.service.DemandeSoutienService;
import yitus.web.rest.errors.BadRequestAlertException;
import yitus.service.dto.DemandeSoutienDTO;
import yitus.service.dto.DemandeSoutienCriteria;
import yitus.service.DemandeSoutienQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link yitus.domain.DemandeSoutien}.
 */
@RestController
@RequestMapping("/api")
public class DemandeSoutienResource {

    private final Logger log = LoggerFactory.getLogger(DemandeSoutienResource.class);

    private static final String ENTITY_NAME = "demandeSoutien";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DemandeSoutienService demandeSoutienService;

    private final DemandeSoutienQueryService demandeSoutienQueryService;

    public DemandeSoutienResource(DemandeSoutienService demandeSoutienService, DemandeSoutienQueryService demandeSoutienQueryService) {
        this.demandeSoutienService = demandeSoutienService;
        this.demandeSoutienQueryService = demandeSoutienQueryService;
    }

    /**
     * {@code POST  /demande-soutiens} : Create a new demandeSoutien.
     *
     * @param demandeSoutienDTO the demandeSoutienDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new demandeSoutienDTO, or with status {@code 400 (Bad Request)} if the demandeSoutien has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/demande-soutiens")
    public ResponseEntity<DemandeSoutienDTO> createDemandeSoutien(@Valid @RequestBody DemandeSoutienDTO demandeSoutienDTO) throws URISyntaxException {
        log.debug("REST request to save DemandeSoutien : {}", demandeSoutienDTO);
        if (demandeSoutienDTO.getId() != null) {
            throw new BadRequestAlertException("A new demandeSoutien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DemandeSoutienDTO result = demandeSoutienService.save(demandeSoutienDTO);
        return ResponseEntity.created(new URI("/api/demande-soutiens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /demande-soutiens} : Updates an existing demandeSoutien.
     *
     * @param demandeSoutienDTO the demandeSoutienDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated demandeSoutienDTO,
     * or with status {@code 400 (Bad Request)} if the demandeSoutienDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the demandeSoutienDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/demande-soutiens")
    public ResponseEntity<DemandeSoutienDTO> updateDemandeSoutien(@Valid @RequestBody DemandeSoutienDTO demandeSoutienDTO) throws URISyntaxException {
        log.debug("REST request to update DemandeSoutien : {}", demandeSoutienDTO);
        if (demandeSoutienDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DemandeSoutienDTO result = demandeSoutienService.save(demandeSoutienDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, demandeSoutienDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /demande-soutiens} : get all the demandeSoutiens.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of demandeSoutiens in body.
     */
    @GetMapping("/demande-soutiens")
    public ResponseEntity<List<DemandeSoutienDTO>> getAllDemandeSoutiens(DemandeSoutienCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DemandeSoutiens by criteria: {}", criteria);
        Page<DemandeSoutienDTO> page = demandeSoutienQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /demande-soutiens/count} : count all the demandeSoutiens.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/demande-soutiens/count")
    public ResponseEntity<Long> countDemandeSoutiens(DemandeSoutienCriteria criteria) {
        log.debug("REST request to count DemandeSoutiens by criteria: {}", criteria);
        return ResponseEntity.ok().body(demandeSoutienQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /demande-soutiens/:id} : get the "id" demandeSoutien.
     *
     * @param id the id of the demandeSoutienDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the demandeSoutienDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/demande-soutiens/{id}")
    public ResponseEntity<DemandeSoutienDTO> getDemandeSoutien(@PathVariable Long id) {
        log.debug("REST request to get DemandeSoutien : {}", id);
        Optional<DemandeSoutienDTO> demandeSoutienDTO = demandeSoutienService.findOne(id);
        return ResponseUtil.wrapOrNotFound(demandeSoutienDTO);
    }

    /**
     * {@code DELETE  /demande-soutiens/:id} : delete the "id" demandeSoutien.
     *
     * @param id the id of the demandeSoutienDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/demande-soutiens/{id}")
    public ResponseEntity<Void> deleteDemandeSoutien(@PathVariable Long id) {
        log.debug("REST request to delete DemandeSoutien : {}", id);
        demandeSoutienService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
