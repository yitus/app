package yitus.web.rest;

import yitus.service.ChefEtablissementService;
import yitus.web.rest.errors.BadRequestAlertException;
import yitus.service.dto.ChefEtablissementDTO;
import yitus.service.dto.ChefEtablissementCriteria;
import yitus.service.ChefEtablissementQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link yitus.domain.ChefEtablissement}.
 */
@RestController
@RequestMapping("/api")
public class ChefEtablissementResource {

    private final Logger log = LoggerFactory.getLogger(ChefEtablissementResource.class);

    private static final String ENTITY_NAME = "chefEtablissement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChefEtablissementService chefEtablissementService;

    private final ChefEtablissementQueryService chefEtablissementQueryService;

    public ChefEtablissementResource(ChefEtablissementService chefEtablissementService, ChefEtablissementQueryService chefEtablissementQueryService) {
        this.chefEtablissementService = chefEtablissementService;
        this.chefEtablissementQueryService = chefEtablissementQueryService;
    }

    /**
     * {@code POST  /chef-etablissements} : Create a new chefEtablissement.
     *
     * @param chefEtablissementDTO the chefEtablissementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chefEtablissementDTO, or with status {@code 400 (Bad Request)} if the chefEtablissement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chef-etablissements")
    public ResponseEntity<ChefEtablissementDTO> createChefEtablissement(@RequestBody ChefEtablissementDTO chefEtablissementDTO) throws URISyntaxException {
        log.debug("REST request to save ChefEtablissement : {}", chefEtablissementDTO);
        if (chefEtablissementDTO.getId() != null) {
            throw new BadRequestAlertException("A new chefEtablissement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChefEtablissementDTO result = chefEtablissementService.save(chefEtablissementDTO);
        return ResponseEntity.created(new URI("/api/chef-etablissements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chef-etablissements} : Updates an existing chefEtablissement.
     *
     * @param chefEtablissementDTO the chefEtablissementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chefEtablissementDTO,
     * or with status {@code 400 (Bad Request)} if the chefEtablissementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chefEtablissementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chef-etablissements")
    public ResponseEntity<ChefEtablissementDTO> updateChefEtablissement(@RequestBody ChefEtablissementDTO chefEtablissementDTO) throws URISyntaxException {
        log.debug("REST request to update ChefEtablissement : {}", chefEtablissementDTO);
        if (chefEtablissementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChefEtablissementDTO result = chefEtablissementService.save(chefEtablissementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chefEtablissementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /chef-etablissements} : get all the chefEtablissements.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chefEtablissements in body.
     */
    @GetMapping("/chef-etablissements")
    public ResponseEntity<List<ChefEtablissementDTO>> getAllChefEtablissements(ChefEtablissementCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChefEtablissements by criteria: {}", criteria);
        Page<ChefEtablissementDTO> page = chefEtablissementQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chef-etablissements/count} : count all the chefEtablissements.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chef-etablissements/count")
    public ResponseEntity<Long> countChefEtablissements(ChefEtablissementCriteria criteria) {
        log.debug("REST request to count ChefEtablissements by criteria: {}", criteria);
        return ResponseEntity.ok().body(chefEtablissementQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chef-etablissements/:id} : get the "id" chefEtablissement.
     *
     * @param id the id of the chefEtablissementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chefEtablissementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chef-etablissements/{id}")
    public ResponseEntity<ChefEtablissementDTO> getChefEtablissement(@PathVariable Long id) {
        log.debug("REST request to get ChefEtablissement : {}", id);
        Optional<ChefEtablissementDTO> chefEtablissementDTO = chefEtablissementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chefEtablissementDTO);
    }

    /**
     * {@code DELETE  /chef-etablissements/:id} : delete the "id" chefEtablissement.
     *
     * @param id the id of the chefEtablissementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chef-etablissements/{id}")
    public ResponseEntity<Void> deleteChefEtablissement(@PathVariable Long id) {
        log.debug("REST request to delete ChefEtablissement : {}", id);
        chefEtablissementService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
