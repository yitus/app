package yitus.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import yitus.domain.Referent;
import yitus.domain.*; // for static metamodels
import yitus.repository.ReferentRepository;
import yitus.service.dto.ReferentCriteria;
import yitus.service.dto.ReferentDTO;
import yitus.service.mapper.ReferentMapper;

/**
 * Service for executing complex queries for {@link Referent} entities in the database.
 * The main input is a {@link ReferentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReferentDTO} or a {@link Page} of {@link ReferentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReferentQueryService extends QueryService<Referent> {

    private final Logger log = LoggerFactory.getLogger(ReferentQueryService.class);

    private final ReferentRepository referentRepository;

    private final ReferentMapper referentMapper;

    public ReferentQueryService(ReferentRepository referentRepository, ReferentMapper referentMapper) {
        this.referentRepository = referentRepository;
        this.referentMapper = referentMapper;
    }

    /**
     * Return a {@link List} of {@link ReferentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReferentDTO> findByCriteria(ReferentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Referent> specification = createSpecification(criteria);
        return referentMapper.toDto(referentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ReferentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReferentDTO> findByCriteria(ReferentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Referent> specification = createSpecification(criteria);
        return referentRepository.findAll(specification, page)
            .map(referentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReferentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Referent> specification = createSpecification(criteria);
        return referentRepository.count(specification);
    }

    /**
     * Function to convert {@link ReferentCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Referent> createSpecification(ReferentCriteria criteria) {
        Specification<Referent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Referent_.id));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Referent_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), Referent_.dateModification));
            }
            if (criteria.getDateArchivage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateArchivage(), Referent_.dateArchivage));
            }
            if (criteria.getUtilisateurId() != null) {
                specification = specification.and(buildSpecification(criteria.getUtilisateurId(),
                    root -> root.join(Referent_.utilisateur, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(Referent_.contact, JoinType.LEFT).get(Contact_.id)));
            }
            if (criteria.getPhotoId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhotoId(),
                    root -> root.join(Referent_.photo, JoinType.LEFT).get(Image_.id)));
            }
            if (criteria.getEtablissementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtablissementId(),
                    root -> root.join(Referent_.etablissement, JoinType.LEFT).get(Etablissement_.id)));
            }
        }
        return specification;
    }
}
