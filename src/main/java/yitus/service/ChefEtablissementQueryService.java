package yitus.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import yitus.domain.ChefEtablissement;
import yitus.domain.*; // for static metamodels
import yitus.repository.ChefEtablissementRepository;
import yitus.service.dto.ChefEtablissementCriteria;
import yitus.service.dto.ChefEtablissementDTO;
import yitus.service.mapper.ChefEtablissementMapper;

/**
 * Service for executing complex queries for {@link ChefEtablissement} entities in the database.
 * The main input is a {@link ChefEtablissementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChefEtablissementDTO} or a {@link Page} of {@link ChefEtablissementDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChefEtablissementQueryService extends QueryService<ChefEtablissement> {

    private final Logger log = LoggerFactory.getLogger(ChefEtablissementQueryService.class);

    private final ChefEtablissementRepository chefEtablissementRepository;

    private final ChefEtablissementMapper chefEtablissementMapper;

    public ChefEtablissementQueryService(ChefEtablissementRepository chefEtablissementRepository, ChefEtablissementMapper chefEtablissementMapper) {
        this.chefEtablissementRepository = chefEtablissementRepository;
        this.chefEtablissementMapper = chefEtablissementMapper;
    }

    /**
     * Return a {@link List} of {@link ChefEtablissementDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChefEtablissementDTO> findByCriteria(ChefEtablissementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChefEtablissement> specification = createSpecification(criteria);
        return chefEtablissementMapper.toDto(chefEtablissementRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChefEtablissementDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChefEtablissementDTO> findByCriteria(ChefEtablissementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChefEtablissement> specification = createSpecification(criteria);
        return chefEtablissementRepository.findAll(specification, page)
            .map(chefEtablissementMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChefEtablissementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChefEtablissement> specification = createSpecification(criteria);
        return chefEtablissementRepository.count(specification);
    }

    /**
     * Function to convert {@link ChefEtablissementCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChefEtablissement> createSpecification(ChefEtablissementCriteria criteria) {
        Specification<ChefEtablissement> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChefEtablissement_.id));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), ChefEtablissement_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), ChefEtablissement_.dateModification));
            }
            if (criteria.getDateArchivage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateArchivage(), ChefEtablissement_.dateArchivage));
            }
            if (criteria.getUtilisateurId() != null) {
                specification = specification.and(buildSpecification(criteria.getUtilisateurId(),
                    root -> root.join(ChefEtablissement_.utilisateur, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getPhotoId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhotoId(),
                    root -> root.join(ChefEtablissement_.photo, JoinType.LEFT).get(Image_.id)));
            }
            if (criteria.getEtablissementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtablissementId(),
                    root -> root.join(ChefEtablissement_.etablissement, JoinType.LEFT).get(Etablissement_.id)));
            }
        }
        return specification;
    }
}
