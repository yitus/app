package yitus.service.impl;

import yitus.service.AidantService;
import yitus.domain.Aidant;
import yitus.repository.AidantRepository;
import yitus.service.dto.AidantDTO;
import yitus.service.mapper.AidantMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Aidant}.
 */
@Service
@Transactional
public class AidantServiceImpl implements AidantService {

    private final Logger log = LoggerFactory.getLogger(AidantServiceImpl.class);

    private final AidantRepository aidantRepository;

    private final AidantMapper aidantMapper;

    public AidantServiceImpl(AidantRepository aidantRepository, AidantMapper aidantMapper) {
        this.aidantRepository = aidantRepository;
        this.aidantMapper = aidantMapper;
    }

    /**
     * Save a aidant.
     *
     * @param aidantDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AidantDTO save(AidantDTO aidantDTO) {
        log.debug("Request to save Aidant : {}", aidantDTO);
        Aidant aidant = aidantMapper.toEntity(aidantDTO);
        aidant = aidantRepository.save(aidant);
        return aidantMapper.toDto(aidant);
    }

    /**
     * Get all the aidants.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AidantDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Aidants");
        return aidantRepository.findAll(pageable)
            .map(aidantMapper::toDto);
    }

    /**
     * Get one aidant by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AidantDTO> findOne(Long id) {
        log.debug("Request to get Aidant : {}", id);
        return aidantRepository.findById(id)
            .map(aidantMapper::toDto);
    }

    /**
     * Delete the aidant by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Aidant : {}", id);
        aidantRepository.deleteById(id);
    }
}
