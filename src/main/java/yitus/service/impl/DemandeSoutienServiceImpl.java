package yitus.service.impl;

import yitus.service.DemandeSoutienService;
import yitus.domain.DemandeSoutien;
import yitus.repository.DemandeSoutienRepository;
import yitus.service.dto.DemandeSoutienDTO;
import yitus.service.mapper.DemandeSoutienMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DemandeSoutien}.
 */
@Service
@Transactional
public class DemandeSoutienServiceImpl implements DemandeSoutienService {

    private final Logger log = LoggerFactory.getLogger(DemandeSoutienServiceImpl.class);

    private final DemandeSoutienRepository demandeSoutienRepository;

    private final DemandeSoutienMapper demandeSoutienMapper;

    public DemandeSoutienServiceImpl(DemandeSoutienRepository demandeSoutienRepository, DemandeSoutienMapper demandeSoutienMapper) {
        this.demandeSoutienRepository = demandeSoutienRepository;
        this.demandeSoutienMapper = demandeSoutienMapper;
    }

    /**
     * Save a demandeSoutien.
     *
     * @param demandeSoutienDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DemandeSoutienDTO save(DemandeSoutienDTO demandeSoutienDTO) {
        log.debug("Request to save DemandeSoutien : {}", demandeSoutienDTO);
        DemandeSoutien demandeSoutien = demandeSoutienMapper.toEntity(demandeSoutienDTO);
        demandeSoutien = demandeSoutienRepository.save(demandeSoutien);
        return demandeSoutienMapper.toDto(demandeSoutien);
    }

    /**
     * Get all the demandeSoutiens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DemandeSoutienDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DemandeSoutiens");
        return demandeSoutienRepository.findAll(pageable)
            .map(demandeSoutienMapper::toDto);
    }

    /**
     * Get one demandeSoutien by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DemandeSoutienDTO> findOne(Long id) {
        log.debug("Request to get DemandeSoutien : {}", id);
        return demandeSoutienRepository.findById(id)
            .map(demandeSoutienMapper::toDto);
    }

    /**
     * Delete the demandeSoutien by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DemandeSoutien : {}", id);
        demandeSoutienRepository.deleteById(id);
    }
}
