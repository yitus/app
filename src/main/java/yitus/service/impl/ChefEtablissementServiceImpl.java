package yitus.service.impl;

import yitus.service.ChefEtablissementService;
import yitus.domain.ChefEtablissement;
import yitus.repository.ChefEtablissementRepository;
import yitus.service.dto.ChefEtablissementDTO;
import yitus.service.mapper.ChefEtablissementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link ChefEtablissement}.
 */
@Service
@Transactional
public class ChefEtablissementServiceImpl implements ChefEtablissementService {

    private final Logger log = LoggerFactory.getLogger(ChefEtablissementServiceImpl.class);

    private final ChefEtablissementRepository chefEtablissementRepository;

    private final ChefEtablissementMapper chefEtablissementMapper;

    public ChefEtablissementServiceImpl(ChefEtablissementRepository chefEtablissementRepository, ChefEtablissementMapper chefEtablissementMapper) {
        this.chefEtablissementRepository = chefEtablissementRepository;
        this.chefEtablissementMapper = chefEtablissementMapper;
    }

    /**
     * Save a chefEtablissement.
     *
     * @param chefEtablissementDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChefEtablissementDTO save(ChefEtablissementDTO chefEtablissementDTO) {
        log.debug("Request to save ChefEtablissement : {}", chefEtablissementDTO);
        ChefEtablissement chefEtablissement = chefEtablissementMapper.toEntity(chefEtablissementDTO);
        chefEtablissement = chefEtablissementRepository.save(chefEtablissement);
        return chefEtablissementMapper.toDto(chefEtablissement);
    }

    /**
     * Get all the chefEtablissements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChefEtablissementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChefEtablissements");
        return chefEtablissementRepository.findAll(pageable)
            .map(chefEtablissementMapper::toDto);
    }


    /**
     *  Get all the chefEtablissements where Etablissement is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<ChefEtablissementDTO> findAllWhereEtablissementIsNull() {
        log.debug("Request to get all chefEtablissements where Etablissement is null");
        return StreamSupport
            .stream(chefEtablissementRepository.findAll().spliterator(), false)
            .filter(chefEtablissement -> chefEtablissement.getEtablissement() == null)
            .map(chefEtablissementMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one chefEtablissement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChefEtablissementDTO> findOne(Long id) {
        log.debug("Request to get ChefEtablissement : {}", id);
        return chefEtablissementRepository.findById(id)
            .map(chefEtablissementMapper::toDto);
    }

    /**
     * Delete the chefEtablissement by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChefEtablissement : {}", id);
        chefEtablissementRepository.deleteById(id);
    }
}
