package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.EvenementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Evenement} and its DTO {@link EvenementDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface EvenementMapper extends EntityMapper<EvenementDTO, Evenement> {

    @Mapping(source = "creePar.id", target = "creeParId")
    @Mapping(source = "creePar.login", target = "creeParLogin")
    EvenementDTO toDto(Evenement evenement);

    @Mapping(source = "creeParId", target = "creePar")
    Evenement toEntity(EvenementDTO evenementDTO);

    default Evenement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Evenement evenement = new Evenement();
        evenement.setId(id);
        return evenement;
    }
}
