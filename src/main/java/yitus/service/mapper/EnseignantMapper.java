package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.EnseignantDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Enseignant} and its DTO {@link EnseignantDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ContactMapper.class, ImageMapper.class, EtablissementMapper.class})
public interface EnseignantMapper extends EntityMapper<EnseignantDTO, Enseignant> {

    @Mapping(source = "utilisateur.id", target = "utilisateurId")
    @Mapping(source = "utilisateur.login", target = "utilisateurLogin")
    @Mapping(source = "contact.id", target = "contactId")
    @Mapping(source = "photo.id", target = "photoId")
    @Mapping(source = "etablissement.id", target = "etablissementId")
    @Mapping(source = "etablissement.nom", target = "etablissementNom")
    EnseignantDTO toDto(Enseignant enseignant);

    @Mapping(source = "utilisateurId", target = "utilisateur")
    @Mapping(source = "contactId", target = "contact")
    @Mapping(source = "photoId", target = "photo")
    @Mapping(source = "etablissementId", target = "etablissement")
    Enseignant toEntity(EnseignantDTO enseignantDTO);

    default Enseignant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Enseignant enseignant = new Enseignant();
        enseignant.setId(id);
        return enseignant;
    }
}
