package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.NotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Notification} and its DTO {@link NotificationDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface NotificationMapper extends EntityMapper<NotificationDTO, Notification> {

    @Mapping(source = "destineA.id", target = "destineAId")
    @Mapping(source = "destineA.login", target = "destineALogin")
    @Mapping(source = "creePar.id", target = "creeParId")
    @Mapping(source = "creePar.login", target = "creeParLogin")
    NotificationDTO toDto(Notification notification);

    @Mapping(source = "destineAId", target = "destineA")
    @Mapping(source = "creeParId", target = "creePar")
    Notification toEntity(NotificationDTO notificationDTO);

    default Notification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Notification notification = new Notification();
        notification.setId(id);
        return notification;
    }
}
