package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.ChefEtablissementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChefEtablissement} and its DTO {@link ChefEtablissementDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ImageMapper.class})
public interface ChefEtablissementMapper extends EntityMapper<ChefEtablissementDTO, ChefEtablissement> {

    @Mapping(source = "utilisateur.id", target = "utilisateurId")
    @Mapping(source = "utilisateur.login", target = "utilisateurLogin")
    @Mapping(source = "photo.id", target = "photoId")
    ChefEtablissementDTO toDto(ChefEtablissement chefEtablissement);

    @Mapping(source = "utilisateurId", target = "utilisateur")
    @Mapping(source = "photoId", target = "photo")
    @Mapping(target = "etablissement", ignore = true)
    ChefEtablissement toEntity(ChefEtablissementDTO chefEtablissementDTO);

    default ChefEtablissement fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChefEtablissement chefEtablissement = new ChefEtablissement();
        chefEtablissement.setId(id);
        return chefEtablissement;
    }
}
