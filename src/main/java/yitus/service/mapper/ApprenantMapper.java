package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.ApprenantDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Apprenant} and its DTO {@link ApprenantDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ContactMapper.class, ImageMapper.class, EtablissementMapper.class})
public interface ApprenantMapper extends EntityMapper<ApprenantDTO, Apprenant> {

    @Mapping(source = "utilisateur.id", target = "utilisateurId")
    @Mapping(source = "utilisateur.login", target = "utilisateurLogin")
    @Mapping(source = "contact.id", target = "contactId")
    @Mapping(source = "photo.id", target = "photoId")
    @Mapping(source = "etablissement.id", target = "etablissementId")
    @Mapping(source = "etablissement.nom", target = "etablissementNom")
    ApprenantDTO toDto(Apprenant apprenant);

    @Mapping(source = "utilisateurId", target = "utilisateur")
    @Mapping(source = "contactId", target = "contact")
    @Mapping(source = "photoId", target = "photo")
    @Mapping(source = "etablissementId", target = "etablissement")
    Apprenant toEntity(ApprenantDTO apprenantDTO);

    default Apprenant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Apprenant apprenant = new Apprenant();
        apprenant.setId(id);
        return apprenant;
    }
}
