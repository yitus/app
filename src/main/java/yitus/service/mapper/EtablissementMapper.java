package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.EtablissementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Etablissement} and its DTO {@link EtablissementDTO}.
 */
@Mapper(componentModel = "spring", uses = {ChefEtablissementMapper.class, ImageMapper.class})
public interface EtablissementMapper extends EntityMapper<EtablissementDTO, Etablissement> {

    @Mapping(source = "chef.id", target = "chefId")
    @Mapping(source = "photo.id", target = "photoId")
    EtablissementDTO toDto(Etablissement etablissement);

    @Mapping(source = "chefId", target = "chef")
    @Mapping(source = "photoId", target = "photo")
    Etablissement toEntity(EtablissementDTO etablissementDTO);

    default Etablissement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Etablissement etablissement = new Etablissement();
        etablissement.setId(id);
        return etablissement;
    }
}
