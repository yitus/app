package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.ReferentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Referent} and its DTO {@link ReferentDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ContactMapper.class, ImageMapper.class, EtablissementMapper.class})
public interface ReferentMapper extends EntityMapper<ReferentDTO, Referent> {

    @Mapping(source = "utilisateur.id", target = "utilisateurId")
    @Mapping(source = "utilisateur.login", target = "utilisateurLogin")
    @Mapping(source = "contact.id", target = "contactId")
    @Mapping(source = "photo.id", target = "photoId")
    @Mapping(source = "etablissement.id", target = "etablissementId")
    @Mapping(source = "etablissement.nom", target = "etablissementNom")
    ReferentDTO toDto(Referent referent);

    @Mapping(source = "utilisateurId", target = "utilisateur")
    @Mapping(source = "contactId", target = "contact")
    @Mapping(source = "photoId", target = "photo")
    @Mapping(source = "etablissementId", target = "etablissement")
    Referent toEntity(ReferentDTO referentDTO);

    default Referent fromId(Long id) {
        if (id == null) {
            return null;
        }
        Referent referent = new Referent();
        referent.setId(id);
        return referent;
    }
}
