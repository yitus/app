package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.DemandeSoutienDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DemandeSoutien} and its DTO {@link DemandeSoutienDTO}.
 */
@Mapper(componentModel = "spring", uses = {ApprenantMapper.class, AidantMapper.class, EnseignantMapper.class, ReferentMapper.class})
public interface DemandeSoutienMapper extends EntityMapper<DemandeSoutienDTO, DemandeSoutien> {

    @Mapping(source = "apprenant.id", target = "apprenantId")
    @Mapping(source = "aidant.id", target = "aidantId")
    @Mapping(source = "enseignant.id", target = "enseignantId")
    @Mapping(source = "referent.id", target = "referentId")
    DemandeSoutienDTO toDto(DemandeSoutien demandeSoutien);

    @Mapping(source = "apprenantId", target = "apprenant")
    @Mapping(source = "aidantId", target = "aidant")
    @Mapping(source = "enseignantId", target = "enseignant")
    @Mapping(source = "referentId", target = "referent")
    DemandeSoutien toEntity(DemandeSoutienDTO demandeSoutienDTO);

    default DemandeSoutien fromId(Long id) {
        if (id == null) {
            return null;
        }
        DemandeSoutien demandeSoutien = new DemandeSoutien();
        demandeSoutien.setId(id);
        return demandeSoutien;
    }
}
