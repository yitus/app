package yitus.service.mapper;


import yitus.domain.*;
import yitus.service.dto.AidantDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Aidant} and its DTO {@link AidantDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ContactMapper.class, ImageMapper.class, EtablissementMapper.class})
public interface AidantMapper extends EntityMapper<AidantDTO, Aidant> {

    @Mapping(source = "utilisateur.id", target = "utilisateurId")
    @Mapping(source = "utilisateur.login", target = "utilisateurLogin")
    @Mapping(source = "contact.id", target = "contactId")
    @Mapping(source = "photo.id", target = "photoId")
    @Mapping(source = "etablissement.id", target = "etablissementId")
    @Mapping(source = "etablissement.nom", target = "etablissementNom")
    AidantDTO toDto(Aidant aidant);

    @Mapping(source = "utilisateurId", target = "utilisateur")
    @Mapping(source = "contactId", target = "contact")
    @Mapping(source = "photoId", target = "photo")
    @Mapping(source = "etablissementId", target = "etablissement")
    Aidant toEntity(AidantDTO aidantDTO);

    default Aidant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Aidant aidant = new Aidant();
        aidant.setId(id);
        return aidant;
    }
}
