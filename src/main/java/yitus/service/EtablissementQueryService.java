package yitus.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import yitus.domain.Etablissement;
import yitus.domain.*; // for static metamodels
import yitus.repository.EtablissementRepository;
import yitus.service.dto.EtablissementCriteria;
import yitus.service.dto.EtablissementDTO;
import yitus.service.mapper.EtablissementMapper;

/**
 * Service for executing complex queries for {@link Etablissement} entities in the database.
 * The main input is a {@link EtablissementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EtablissementDTO} or a {@link Page} of {@link EtablissementDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EtablissementQueryService extends QueryService<Etablissement> {

    private final Logger log = LoggerFactory.getLogger(EtablissementQueryService.class);

    private final EtablissementRepository etablissementRepository;

    private final EtablissementMapper etablissementMapper;

    public EtablissementQueryService(EtablissementRepository etablissementRepository, EtablissementMapper etablissementMapper) {
        this.etablissementRepository = etablissementRepository;
        this.etablissementMapper = etablissementMapper;
    }

    /**
     * Return a {@link List} of {@link EtablissementDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EtablissementDTO> findByCriteria(EtablissementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Etablissement> specification = createSpecification(criteria);
        return etablissementMapper.toDto(etablissementRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EtablissementDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EtablissementDTO> findByCriteria(EtablissementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Etablissement> specification = createSpecification(criteria);
        return etablissementRepository.findAll(specification, page)
            .map(etablissementMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EtablissementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Etablissement> specification = createSpecification(criteria);
        return etablissementRepository.count(specification);
    }

    /**
     * Function to convert {@link EtablissementCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Etablissement> createSpecification(EtablissementCriteria criteria) {
        Specification<Etablissement> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Etablissement_.id));
            }
            if (criteria.getTypeEtablissement() != null) {
                specification = specification.and(buildSpecification(criteria.getTypeEtablissement(), Etablissement_.typeEtablissement));
            }
            if (criteria.getNom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNom(), Etablissement_.nom));
            }
            if (criteria.getSiteweb() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSiteweb(), Etablissement_.siteweb));
            }
            if (criteria.getAdresse() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAdresse(), Etablissement_.adresse));
            }
            if (criteria.getVille() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVille(), Etablissement_.ville));
            }
            if (criteria.getCodePostal() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodePostal(), Etablissement_.codePostal));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Etablissement_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), Etablissement_.dateModification));
            }
            if (criteria.getDateArchivage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateArchivage(), Etablissement_.dateArchivage));
            }
            if (criteria.getChefId() != null) {
                specification = specification.and(buildSpecification(criteria.getChefId(),
                    root -> root.join(Etablissement_.chef, JoinType.LEFT).get(ChefEtablissement_.id)));
            }
            if (criteria.getPhotoId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhotoId(),
                    root -> root.join(Etablissement_.photo, JoinType.LEFT).get(Image_.id)));
            }
        }
        return specification;
    }
}
