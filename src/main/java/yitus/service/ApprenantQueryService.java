package yitus.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import yitus.domain.Apprenant;
import yitus.domain.*; // for static metamodels
import yitus.repository.ApprenantRepository;
import yitus.service.dto.ApprenantCriteria;
import yitus.service.dto.ApprenantDTO;
import yitus.service.mapper.ApprenantMapper;

/**
 * Service for executing complex queries for {@link Apprenant} entities in the database.
 * The main input is a {@link ApprenantCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ApprenantDTO} or a {@link Page} of {@link ApprenantDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ApprenantQueryService extends QueryService<Apprenant> {

    private final Logger log = LoggerFactory.getLogger(ApprenantQueryService.class);

    private final ApprenantRepository apprenantRepository;

    private final ApprenantMapper apprenantMapper;

    public ApprenantQueryService(ApprenantRepository apprenantRepository, ApprenantMapper apprenantMapper) {
        this.apprenantRepository = apprenantRepository;
        this.apprenantMapper = apprenantMapper;
    }

    /**
     * Return a {@link List} of {@link ApprenantDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ApprenantDTO> findByCriteria(ApprenantCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Apprenant> specification = createSpecification(criteria);
        return apprenantMapper.toDto(apprenantRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ApprenantDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ApprenantDTO> findByCriteria(ApprenantCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Apprenant> specification = createSpecification(criteria);
        return apprenantRepository.findAll(specification, page)
            .map(apprenantMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ApprenantCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Apprenant> specification = createSpecification(criteria);
        return apprenantRepository.count(specification);
    }

    /**
     * Function to convert {@link ApprenantCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Apprenant> createSpecification(ApprenantCriteria criteria) {
        Specification<Apprenant> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Apprenant_.id));
            }
            if (criteria.getNiveau() != null) {
                specification = specification.and(buildSpecification(criteria.getNiveau(), Apprenant_.niveau));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Apprenant_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), Apprenant_.dateModification));
            }
            if (criteria.getDateArchivage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateArchivage(), Apprenant_.dateArchivage));
            }
            if (criteria.getUtilisateurId() != null) {
                specification = specification.and(buildSpecification(criteria.getUtilisateurId(),
                    root -> root.join(Apprenant_.utilisateur, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(Apprenant_.contact, JoinType.LEFT).get(Contact_.id)));
            }
            if (criteria.getPhotoId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhotoId(),
                    root -> root.join(Apprenant_.photo, JoinType.LEFT).get(Image_.id)));
            }
            if (criteria.getEtablissementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtablissementId(),
                    root -> root.join(Apprenant_.etablissement, JoinType.LEFT).get(Etablissement_.id)));
            }
        }
        return specification;
    }
}
