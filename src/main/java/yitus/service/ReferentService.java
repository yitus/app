package yitus.service;

import yitus.service.dto.ReferentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link yitus.domain.Referent}.
 */
public interface ReferentService {

    /**
     * Save a referent.
     *
     * @param referentDTO the entity to save.
     * @return the persisted entity.
     */
    ReferentDTO save(ReferentDTO referentDTO);

    /**
     * Get all the referents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ReferentDTO> findAll(Pageable pageable);

    /**
     * Get the "id" referent.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ReferentDTO> findOne(Long id);

    /**
     * Delete the "id" referent.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
