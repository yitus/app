package yitus.service;

import yitus.service.dto.ApprenantDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link yitus.domain.Apprenant}.
 */
public interface ApprenantService {

    /**
     * Save a apprenant.
     *
     * @param apprenantDTO the entity to save.
     * @return the persisted entity.
     */
    ApprenantDTO save(ApprenantDTO apprenantDTO);

    /**
     * Get all the apprenants.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ApprenantDTO> findAll(Pageable pageable);

    /**
     * Get the "id" apprenant.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApprenantDTO> findOne(Long id);

    /**
     * Delete the "id" apprenant.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
