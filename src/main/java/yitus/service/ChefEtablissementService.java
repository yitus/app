package yitus.service;

import yitus.service.dto.ChefEtablissementDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link yitus.domain.ChefEtablissement}.
 */
public interface ChefEtablissementService {

    /**
     * Save a chefEtablissement.
     *
     * @param chefEtablissementDTO the entity to save.
     * @return the persisted entity.
     */
    ChefEtablissementDTO save(ChefEtablissementDTO chefEtablissementDTO);

    /**
     * Get all the chefEtablissements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChefEtablissementDTO> findAll(Pageable pageable);
    /**
     * Get all the ChefEtablissementDTO where Etablissement is {@code null}.
     *
     * @return the list of entities.
     */
    List<ChefEtablissementDTO> findAllWhereEtablissementIsNull();

    /**
     * Get the "id" chefEtablissement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChefEtablissementDTO> findOne(Long id);

    /**
     * Delete the "id" chefEtablissement.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
