package yitus.service;

import yitus.service.dto.AidantDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link yitus.domain.Aidant}.
 */
public interface AidantService {

    /**
     * Save a aidant.
     *
     * @param aidantDTO the entity to save.
     * @return the persisted entity.
     */
    AidantDTO save(AidantDTO aidantDTO);

    /**
     * Get all the aidants.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AidantDTO> findAll(Pageable pageable);

    /**
     * Get the "id" aidant.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AidantDTO> findOne(Long id);

    /**
     * Delete the "id" aidant.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
