package yitus.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import yitus.domain.Aidant;
import yitus.domain.*; // for static metamodels
import yitus.repository.AidantRepository;
import yitus.service.dto.AidantCriteria;
import yitus.service.dto.AidantDTO;
import yitus.service.mapper.AidantMapper;

/**
 * Service for executing complex queries for {@link Aidant} entities in the database.
 * The main input is a {@link AidantCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AidantDTO} or a {@link Page} of {@link AidantDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AidantQueryService extends QueryService<Aidant> {

    private final Logger log = LoggerFactory.getLogger(AidantQueryService.class);

    private final AidantRepository aidantRepository;

    private final AidantMapper aidantMapper;

    public AidantQueryService(AidantRepository aidantRepository, AidantMapper aidantMapper) {
        this.aidantRepository = aidantRepository;
        this.aidantMapper = aidantMapper;
    }

    /**
     * Return a {@link List} of {@link AidantDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AidantDTO> findByCriteria(AidantCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Aidant> specification = createSpecification(criteria);
        return aidantMapper.toDto(aidantRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AidantDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AidantDTO> findByCriteria(AidantCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Aidant> specification = createSpecification(criteria);
        return aidantRepository.findAll(specification, page)
            .map(aidantMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AidantCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Aidant> specification = createSpecification(criteria);
        return aidantRepository.count(specification);
    }

    /**
     * Function to convert {@link AidantCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Aidant> createSpecification(AidantCriteria criteria) {
        Specification<Aidant> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Aidant_.id));
            }
            if (criteria.getCursusActuel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCursusActuel(), Aidant_.cursusActuel));
            }
            if (criteria.getNombreAnneesPostBAC() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNombreAnneesPostBAC(), Aidant_.nombreAnneesPostBAC));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Aidant_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), Aidant_.dateModification));
            }
            if (criteria.getDateArchivage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateArchivage(), Aidant_.dateArchivage));
            }
            if (criteria.getUtilisateurId() != null) {
                specification = specification.and(buildSpecification(criteria.getUtilisateurId(),
                    root -> root.join(Aidant_.utilisateur, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(Aidant_.contact, JoinType.LEFT).get(Contact_.id)));
            }
            if (criteria.getPhotoId() != null) {
                specification = specification.and(buildSpecification(criteria.getPhotoId(),
                    root -> root.join(Aidant_.photo, JoinType.LEFT).get(Image_.id)));
            }
            if (criteria.getEtablissementId() != null) {
                specification = specification.and(buildSpecification(criteria.getEtablissementId(),
                    root -> root.join(Aidant_.etablissement, JoinType.LEFT).get(Etablissement_.id)));
            }
        }
        return specification;
    }
}
