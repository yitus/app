package yitus.service;

import yitus.service.dto.DemandeSoutienDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link yitus.domain.DemandeSoutien}.
 */
public interface DemandeSoutienService {

    /**
     * Save a demandeSoutien.
     *
     * @param demandeSoutienDTO the entity to save.
     * @return the persisted entity.
     */
    DemandeSoutienDTO save(DemandeSoutienDTO demandeSoutienDTO);

    /**
     * Get all the demandeSoutiens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DemandeSoutienDTO> findAll(Pageable pageable);

    /**
     * Get the "id" demandeSoutien.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DemandeSoutienDTO> findOne(Long id);

    /**
     * Delete the "id" demandeSoutien.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
