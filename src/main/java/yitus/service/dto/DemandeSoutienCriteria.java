package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import yitus.domain.enumeration.TypeStatut;
import yitus.domain.enumeration.TypeNiveau;
import yitus.domain.enumeration.TypeMatiere;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link yitus.domain.DemandeSoutien} entity. This class is used
 * in {@link yitus.web.rest.DemandeSoutienResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /demande-soutiens?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DemandeSoutienCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeStatut
     */
    public static class TypeStatutFilter extends Filter<TypeStatut> {

        public TypeStatutFilter() {
        }

        public TypeStatutFilter(TypeStatutFilter filter) {
            super(filter);
        }

        @Override
        public TypeStatutFilter copy() {
            return new TypeStatutFilter(this);
        }

    }
    /**
     * Class for filtering TypeNiveau
     */
    public static class TypeNiveauFilter extends Filter<TypeNiveau> {

        public TypeNiveauFilter() {
        }

        public TypeNiveauFilter(TypeNiveauFilter filter) {
            super(filter);
        }

        @Override
        public TypeNiveauFilter copy() {
            return new TypeNiveauFilter(this);
        }

    }
    /**
     * Class for filtering TypeMatiere
     */
    public static class TypeMatiereFilter extends Filter<TypeMatiere> {

        public TypeMatiereFilter() {
        }

        public TypeMatiereFilter(TypeMatiereFilter filter) {
            super(filter);
        }

        @Override
        public TypeMatiereFilter copy() {
            return new TypeMatiereFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TypeStatutFilter statut;

    private StringFilter etiquettes;

    private TypeNiveauFilter niveau;

    private TypeMatiereFilter matiere;

    private StringFilter horairesDisponibilites;

    private InstantFilter dateRendezVous;

    private IntegerFilter noteApprenant;

    private IntegerFilter noteAidant;

    private InstantFilter dateLimite;

    private InstantFilter dateCreation;

    private InstantFilter dateModification;

    private InstantFilter dateArchivage;

    private LongFilter apprenantId;

    private LongFilter aidantId;

    private LongFilter enseignantId;

    private LongFilter referentId;

    public DemandeSoutienCriteria() {
    }

    public DemandeSoutienCriteria(DemandeSoutienCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.statut = other.statut == null ? null : other.statut.copy();
        this.etiquettes = other.etiquettes == null ? null : other.etiquettes.copy();
        this.niveau = other.niveau == null ? null : other.niveau.copy();
        this.matiere = other.matiere == null ? null : other.matiere.copy();
        this.horairesDisponibilites = other.horairesDisponibilites == null ? null : other.horairesDisponibilites.copy();
        this.dateRendezVous = other.dateRendezVous == null ? null : other.dateRendezVous.copy();
        this.noteApprenant = other.noteApprenant == null ? null : other.noteApprenant.copy();
        this.noteAidant = other.noteAidant == null ? null : other.noteAidant.copy();
        this.dateLimite = other.dateLimite == null ? null : other.dateLimite.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.dateModification = other.dateModification == null ? null : other.dateModification.copy();
        this.dateArchivage = other.dateArchivage == null ? null : other.dateArchivage.copy();
        this.apprenantId = other.apprenantId == null ? null : other.apprenantId.copy();
        this.aidantId = other.aidantId == null ? null : other.aidantId.copy();
        this.enseignantId = other.enseignantId == null ? null : other.enseignantId.copy();
        this.referentId = other.referentId == null ? null : other.referentId.copy();
    }

    @Override
    public DemandeSoutienCriteria copy() {
        return new DemandeSoutienCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TypeStatutFilter getStatut() {
        return statut;
    }

    public void setStatut(TypeStatutFilter statut) {
        this.statut = statut;
    }

    public StringFilter getEtiquettes() {
        return etiquettes;
    }

    public void setEtiquettes(StringFilter etiquettes) {
        this.etiquettes = etiquettes;
    }

    public TypeNiveauFilter getNiveau() {
        return niveau;
    }

    public void setNiveau(TypeNiveauFilter niveau) {
        this.niveau = niveau;
    }

    public TypeMatiereFilter getMatiere() {
        return matiere;
    }

    public void setMatiere(TypeMatiereFilter matiere) {
        this.matiere = matiere;
    }

    public StringFilter getHorairesDisponibilites() {
        return horairesDisponibilites;
    }

    public void setHorairesDisponibilites(StringFilter horairesDisponibilites) {
        this.horairesDisponibilites = horairesDisponibilites;
    }

    public InstantFilter getDateRendezVous() {
        return dateRendezVous;
    }

    public void setDateRendezVous(InstantFilter dateRendezVous) {
        this.dateRendezVous = dateRendezVous;
    }

    public IntegerFilter getNoteApprenant() {
        return noteApprenant;
    }

    public void setNoteApprenant(IntegerFilter noteApprenant) {
        this.noteApprenant = noteApprenant;
    }

    public IntegerFilter getNoteAidant() {
        return noteAidant;
    }

    public void setNoteAidant(IntegerFilter noteAidant) {
        this.noteAidant = noteAidant;
    }

    public InstantFilter getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(InstantFilter dateLimite) {
        this.dateLimite = dateLimite;
    }

    public InstantFilter getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(InstantFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public InstantFilter getDateModification() {
        return dateModification;
    }

    public void setDateModification(InstantFilter dateModification) {
        this.dateModification = dateModification;
    }

    public InstantFilter getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(InstantFilter dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public LongFilter getApprenantId() {
        return apprenantId;
    }

    public void setApprenantId(LongFilter apprenantId) {
        this.apprenantId = apprenantId;
    }

    public LongFilter getAidantId() {
        return aidantId;
    }

    public void setAidantId(LongFilter aidantId) {
        this.aidantId = aidantId;
    }

    public LongFilter getEnseignantId() {
        return enseignantId;
    }

    public void setEnseignantId(LongFilter enseignantId) {
        this.enseignantId = enseignantId;
    }

    public LongFilter getReferentId() {
        return referentId;
    }

    public void setReferentId(LongFilter referentId) {
        this.referentId = referentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DemandeSoutienCriteria that = (DemandeSoutienCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(statut, that.statut) &&
            Objects.equals(etiquettes, that.etiquettes) &&
            Objects.equals(niveau, that.niveau) &&
            Objects.equals(matiere, that.matiere) &&
            Objects.equals(horairesDisponibilites, that.horairesDisponibilites) &&
            Objects.equals(dateRendezVous, that.dateRendezVous) &&
            Objects.equals(noteApprenant, that.noteApprenant) &&
            Objects.equals(noteAidant, that.noteAidant) &&
            Objects.equals(dateLimite, that.dateLimite) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(dateModification, that.dateModification) &&
            Objects.equals(dateArchivage, that.dateArchivage) &&
            Objects.equals(apprenantId, that.apprenantId) &&
            Objects.equals(aidantId, that.aidantId) &&
            Objects.equals(enseignantId, that.enseignantId) &&
            Objects.equals(referentId, that.referentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        statut,
        etiquettes,
        niveau,
        matiere,
        horairesDisponibilites,
        dateRendezVous,
        noteApprenant,
        noteAidant,
        dateLimite,
        dateCreation,
        dateModification,
        dateArchivage,
        apprenantId,
        aidantId,
        enseignantId,
        referentId
        );
    }

    @Override
    public String toString() {
        return "DemandeSoutienCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (statut != null ? "statut=" + statut + ", " : "") +
                (etiquettes != null ? "etiquettes=" + etiquettes + ", " : "") +
                (niveau != null ? "niveau=" + niveau + ", " : "") +
                (matiere != null ? "matiere=" + matiere + ", " : "") +
                (horairesDisponibilites != null ? "horairesDisponibilites=" + horairesDisponibilites + ", " : "") +
                (dateRendezVous != null ? "dateRendezVous=" + dateRendezVous + ", " : "") +
                (noteApprenant != null ? "noteApprenant=" + noteApprenant + ", " : "") +
                (noteAidant != null ? "noteAidant=" + noteAidant + ", " : "") +
                (dateLimite != null ? "dateLimite=" + dateLimite + ", " : "") +
                (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
                (dateModification != null ? "dateModification=" + dateModification + ", " : "") +
                (dateArchivage != null ? "dateArchivage=" + dateArchivage + ", " : "") +
                (apprenantId != null ? "apprenantId=" + apprenantId + ", " : "") +
                (aidantId != null ? "aidantId=" + aidantId + ", " : "") +
                (enseignantId != null ? "enseignantId=" + enseignantId + ", " : "") +
                (referentId != null ? "referentId=" + referentId + ", " : "") +
            "}";
    }

}
