package yitus.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import yitus.domain.enumeration.TypeEtablissement;

/**
 * A DTO for the {@link yitus.domain.Etablissement} entity.
 */
public class EtablissementDTO implements Serializable {
    
    private Long id;

    /**
     * Type de l'établissement
     */
    @NotNull
    @ApiModelProperty(value = "Type de l'établissement", required = true)
    private TypeEtablissement typeEtablissement;

    @NotNull
    private String nom;

    /**
     * URL du site web de l'établissement
     */
    @NotNull
    @ApiModelProperty(value = "URL du site web de l'établissement", required = true)
    private String siteweb;

    @NotNull
    private String adresse;

    /**
     * Ville de l'établissement
     */
    @NotNull
    @ApiModelProperty(value = "Ville de l'établissement", required = true)
    private String ville;

    @NotNull
    private String codePostal;

    /**
     * Date de la première création
     */
    @ApiModelProperty(value = "Date de la première création")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @ApiModelProperty(value = "Date de dernière modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long chefId;

    private Long photoId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeEtablissement getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(TypeEtablissement typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSiteweb() {
        return siteweb;
    }

    public void setSiteweb(String siteweb) {
        this.siteweb = siteweb;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getChefId() {
        return chefId;
    }

    public void setChefId(Long chefEtablissementId) {
        this.chefId = chefEtablissementId;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long imageId) {
        this.photoId = imageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EtablissementDTO etablissementDTO = (EtablissementDTO) o;
        if (etablissementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), etablissementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EtablissementDTO{" +
            "id=" + getId() +
            ", typeEtablissement='" + getTypeEtablissement() + "'" +
            ", nom='" + getNom() + "'" +
            ", siteweb='" + getSiteweb() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", ville='" + getVille() + "'" +
            ", codePostal='" + getCodePostal() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", chefId=" + getChefId() +
            ", photoId=" + getPhotoId() +
            "}";
    }
}
