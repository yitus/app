package yitus.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import yitus.domain.enumeration.TypeStatut;
import yitus.domain.enumeration.TypeNiveau;
import yitus.domain.enumeration.TypeMatiere;

/**
 * A DTO for the {@link yitus.domain.DemandeSoutien} entity.
 */
@ApiModel(description = "Demande de soutien")
public class DemandeSoutienDTO implements Serializable {
    
    private Long id;

    /**
     * Statut de la demande
     */
    @NotNull
    @ApiModelProperty(value = "Statut de la demande", required = true)
    private TypeStatut statut;

    /**
     * Etiquettes ou mots-clé de la demande
     */
    @Size(max = 2000)
    @Pattern(regexp = "^(([a-z]+,)*[a-z]+)?$")
    @ApiModelProperty(value = "Etiquettes ou mots-clé de la demande")
    private String etiquettes;

    /**
     * Niveau demandé
     */
    @NotNull
    @ApiModelProperty(value = "Niveau demandé", required = true)
    private TypeNiveau niveau;

    /**
     * Matiére concernée
     */
    @NotNull
    @ApiModelProperty(value = "Matiére concernée", required = true)
    private TypeMatiere matiere;

    /**
     * Detail de la demande de soutien (rich text)
     */
    
    @ApiModelProperty(value = "Detail de la demande de soutien (rich text)", required = true)
    @Lob
    private String detail;

    /**
     * Detail de la demande de soutien
     */
    @NotNull
    @ApiModelProperty(value = "Detail de la demande de soutien", required = true)
    private String horairesDisponibilites;

    /**
     * Date de la prise de rendez-vous pour la demande de soutien
     */
    @ApiModelProperty(value = "Date de la prise de rendez-vous pour la demande de soutien")
    private Instant dateRendezVous;

    /**
     * Note de l'apprenant sur l'aidant
     */
    @Min(value = 1)
    @Max(value = 10)
    @ApiModelProperty(value = "Note de l'apprenant sur l'aidant")
    private Integer noteApprenant;

    /**
     * Note de l'aidant sur l'apprenant
     */
    @Min(value = 1)
    @Max(value = 10)
    @ApiModelProperty(value = "Note de l'aidant sur l'apprenant")
    private Integer noteAidant;

    /**
     * Commentaire de l'apprenant sur l'aidant (rich text)
     */
    @ApiModelProperty(value = "Commentaire de l'apprenant sur l'aidant (rich text)")
    @Lob
    private String commentaireApprenant;

    /**
     * Commentaire de l'aidant sur l'apprenant (rich text)
     */
    @ApiModelProperty(value = "Commentaire de l'aidant sur l'apprenant (rich text)")
    @Lob
    private String commentaireAidant;

    /**
     * Commentaire de l'enseignant (rich text)
     */
    @ApiModelProperty(value = "Commentaire de l'enseignant (rich text)")
    @Lob
    private String commentaireEnseignant;

    /**
     * Commentaire du référent (rich text)
     */
    @ApiModelProperty(value = "Commentaire du référent (rich text)")
    @Lob
    private String commentaireReferent;

    /**
     * Date limite de la demande de soutien
     */
    @NotNull
    @ApiModelProperty(value = "Date limite de la demande de soutien", required = true)
    private Instant dateLimite;

    /**
     * Date de la première création
     */
    @ApiModelProperty(value = "Date de la première création")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @ApiModelProperty(value = "Date de dernière modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long apprenantId;

    private Long aidantId;

    private Long enseignantId;

    private Long referentId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStatut getStatut() {
        return statut;
    }

    public void setStatut(TypeStatut statut) {
        this.statut = statut;
    }

    public String getEtiquettes() {
        return etiquettes;
    }

    public void setEtiquettes(String etiquettes) {
        this.etiquettes = etiquettes;
    }

    public TypeNiveau getNiveau() {
        return niveau;
    }

    public void setNiveau(TypeNiveau niveau) {
        this.niveau = niveau;
    }

    public TypeMatiere getMatiere() {
        return matiere;
    }

    public void setMatiere(TypeMatiere matiere) {
        this.matiere = matiere;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getHorairesDisponibilites() {
        return horairesDisponibilites;
    }

    public void setHorairesDisponibilites(String horairesDisponibilites) {
        this.horairesDisponibilites = horairesDisponibilites;
    }

    public Instant getDateRendezVous() {
        return dateRendezVous;
    }

    public void setDateRendezVous(Instant dateRendezVous) {
        this.dateRendezVous = dateRendezVous;
    }

    public Integer getNoteApprenant() {
        return noteApprenant;
    }

    public void setNoteApprenant(Integer noteApprenant) {
        this.noteApprenant = noteApprenant;
    }

    public Integer getNoteAidant() {
        return noteAidant;
    }

    public void setNoteAidant(Integer noteAidant) {
        this.noteAidant = noteAidant;
    }

    public String getCommentaireApprenant() {
        return commentaireApprenant;
    }

    public void setCommentaireApprenant(String commentaireApprenant) {
        this.commentaireApprenant = commentaireApprenant;
    }

    public String getCommentaireAidant() {
        return commentaireAidant;
    }

    public void setCommentaireAidant(String commentaireAidant) {
        this.commentaireAidant = commentaireAidant;
    }

    public String getCommentaireEnseignant() {
        return commentaireEnseignant;
    }

    public void setCommentaireEnseignant(String commentaireEnseignant) {
        this.commentaireEnseignant = commentaireEnseignant;
    }

    public String getCommentaireReferent() {
        return commentaireReferent;
    }

    public void setCommentaireReferent(String commentaireReferent) {
        this.commentaireReferent = commentaireReferent;
    }

    public Instant getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(Instant dateLimite) {
        this.dateLimite = dateLimite;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getApprenantId() {
        return apprenantId;
    }

    public void setApprenantId(Long apprenantId) {
        this.apprenantId = apprenantId;
    }

    public Long getAidantId() {
        return aidantId;
    }

    public void setAidantId(Long aidantId) {
        this.aidantId = aidantId;
    }

    public Long getEnseignantId() {
        return enseignantId;
    }

    public void setEnseignantId(Long enseignantId) {
        this.enseignantId = enseignantId;
    }

    public Long getReferentId() {
        return referentId;
    }

    public void setReferentId(Long referentId) {
        this.referentId = referentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DemandeSoutienDTO demandeSoutienDTO = (DemandeSoutienDTO) o;
        if (demandeSoutienDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), demandeSoutienDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DemandeSoutienDTO{" +
            "id=" + getId() +
            ", statut='" + getStatut() + "'" +
            ", etiquettes='" + getEtiquettes() + "'" +
            ", niveau='" + getNiveau() + "'" +
            ", matiere='" + getMatiere() + "'" +
            ", detail='" + getDetail() + "'" +
            ", horairesDisponibilites='" + getHorairesDisponibilites() + "'" +
            ", dateRendezVous='" + getDateRendezVous() + "'" +
            ", noteApprenant=" + getNoteApprenant() +
            ", noteAidant=" + getNoteAidant() +
            ", commentaireApprenant='" + getCommentaireApprenant() + "'" +
            ", commentaireAidant='" + getCommentaireAidant() + "'" +
            ", commentaireEnseignant='" + getCommentaireEnseignant() + "'" +
            ", commentaireReferent='" + getCommentaireReferent() + "'" +
            ", dateLimite='" + getDateLimite() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", apprenantId=" + getApprenantId() +
            ", aidantId=" + getAidantId() +
            ", enseignantId=" + getEnseignantId() +
            ", referentId=" + getReferentId() +
            "}";
    }
}
