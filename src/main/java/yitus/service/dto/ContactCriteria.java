package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link yitus.domain.Contact} entity. This class is used
 * in {@link yitus.web.rest.ContactResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contacts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter badges;

    private StringFilter login;

    private StringFilter nom;

    private StringFilter prenom;

    private StringFilter email;

    private StringFilter telephone;

    private StringFilter ville;

    private StringFilter gravatar;

    private StringFilter linkedin;

    private StringFilter google;

    private StringFilter facebook;

    private StringFilter skype;

    private StringFilter discord;

    private StringFilter zoom;

    private StringFilter whereby;

    private StringFilter mumble;

    private StringFilter tox;

    private StringFilter matrix;

    public ContactCriteria() {
    }

    public ContactCriteria(ContactCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.badges = other.badges == null ? null : other.badges.copy();
        this.login = other.login == null ? null : other.login.copy();
        this.nom = other.nom == null ? null : other.nom.copy();
        this.prenom = other.prenom == null ? null : other.prenom.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.telephone = other.telephone == null ? null : other.telephone.copy();
        this.ville = other.ville == null ? null : other.ville.copy();
        this.gravatar = other.gravatar == null ? null : other.gravatar.copy();
        this.linkedin = other.linkedin == null ? null : other.linkedin.copy();
        this.google = other.google == null ? null : other.google.copy();
        this.facebook = other.facebook == null ? null : other.facebook.copy();
        this.skype = other.skype == null ? null : other.skype.copy();
        this.discord = other.discord == null ? null : other.discord.copy();
        this.zoom = other.zoom == null ? null : other.zoom.copy();
        this.whereby = other.whereby == null ? null : other.whereby.copy();
        this.mumble = other.mumble == null ? null : other.mumble.copy();
        this.tox = other.tox == null ? null : other.tox.copy();
        this.matrix = other.matrix == null ? null : other.matrix.copy();
    }

    @Override
    public ContactCriteria copy() {
        return new ContactCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getBadges() {
        return badges;
    }

    public void setBadges(StringFilter badges) {
        this.badges = badges;
    }

    public StringFilter getLogin() {
        return login;
    }

    public void setLogin(StringFilter login) {
        this.login = login;
    }

    public StringFilter getNom() {
        return nom;
    }

    public void setNom(StringFilter nom) {
        this.nom = nom;
    }

    public StringFilter getPrenom() {
        return prenom;
    }

    public void setPrenom(StringFilter prenom) {
        this.prenom = prenom;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getTelephone() {
        return telephone;
    }

    public void setTelephone(StringFilter telephone) {
        this.telephone = telephone;
    }

    public StringFilter getVille() {
        return ville;
    }

    public void setVille(StringFilter ville) {
        this.ville = ville;
    }

    public StringFilter getGravatar() {
        return gravatar;
    }

    public void setGravatar(StringFilter gravatar) {
        this.gravatar = gravatar;
    }

    public StringFilter getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(StringFilter linkedin) {
        this.linkedin = linkedin;
    }

    public StringFilter getGoogle() {
        return google;
    }

    public void setGoogle(StringFilter google) {
        this.google = google;
    }

    public StringFilter getFacebook() {
        return facebook;
    }

    public void setFacebook(StringFilter facebook) {
        this.facebook = facebook;
    }

    public StringFilter getSkype() {
        return skype;
    }

    public void setSkype(StringFilter skype) {
        this.skype = skype;
    }

    public StringFilter getDiscord() {
        return discord;
    }

    public void setDiscord(StringFilter discord) {
        this.discord = discord;
    }

    public StringFilter getZoom() {
        return zoom;
    }

    public void setZoom(StringFilter zoom) {
        this.zoom = zoom;
    }

    public StringFilter getWhereby() {
        return whereby;
    }

    public void setWhereby(StringFilter whereby) {
        this.whereby = whereby;
    }

    public StringFilter getMumble() {
        return mumble;
    }

    public void setMumble(StringFilter mumble) {
        this.mumble = mumble;
    }

    public StringFilter getTox() {
        return tox;
    }

    public void setTox(StringFilter tox) {
        this.tox = tox;
    }

    public StringFilter getMatrix() {
        return matrix;
    }

    public void setMatrix(StringFilter matrix) {
        this.matrix = matrix;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactCriteria that = (ContactCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(badges, that.badges) &&
            Objects.equals(login, that.login) &&
            Objects.equals(nom, that.nom) &&
            Objects.equals(prenom, that.prenom) &&
            Objects.equals(email, that.email) &&
            Objects.equals(telephone, that.telephone) &&
            Objects.equals(ville, that.ville) &&
            Objects.equals(gravatar, that.gravatar) &&
            Objects.equals(linkedin, that.linkedin) &&
            Objects.equals(google, that.google) &&
            Objects.equals(facebook, that.facebook) &&
            Objects.equals(skype, that.skype) &&
            Objects.equals(discord, that.discord) &&
            Objects.equals(zoom, that.zoom) &&
            Objects.equals(whereby, that.whereby) &&
            Objects.equals(mumble, that.mumble) &&
            Objects.equals(tox, that.tox) &&
            Objects.equals(matrix, that.matrix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        badges,
        login,
        nom,
        prenom,
        email,
        telephone,
        ville,
        gravatar,
        linkedin,
        google,
        facebook,
        skype,
        discord,
        zoom,
        whereby,
        mumble,
        tox,
        matrix
        );
    }

    @Override
    public String toString() {
        return "ContactCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (badges != null ? "badges=" + badges + ", " : "") +
                (login != null ? "login=" + login + ", " : "") +
                (nom != null ? "nom=" + nom + ", " : "") +
                (prenom != null ? "prenom=" + prenom + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (telephone != null ? "telephone=" + telephone + ", " : "") +
                (ville != null ? "ville=" + ville + ", " : "") +
                (gravatar != null ? "gravatar=" + gravatar + ", " : "") +
                (linkedin != null ? "linkedin=" + linkedin + ", " : "") +
                (google != null ? "google=" + google + ", " : "") +
                (facebook != null ? "facebook=" + facebook + ", " : "") +
                (skype != null ? "skype=" + skype + ", " : "") +
                (discord != null ? "discord=" + discord + ", " : "") +
                (zoom != null ? "zoom=" + zoom + ", " : "") +
                (whereby != null ? "whereby=" + whereby + ", " : "") +
                (mumble != null ? "mumble=" + mumble + ", " : "") +
                (tox != null ? "tox=" + tox + ", " : "") +
                (matrix != null ? "matrix=" + matrix + ", " : "") +
            "}";
    }

}
