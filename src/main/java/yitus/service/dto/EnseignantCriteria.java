package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import yitus.domain.enumeration.TypeMatiere;
import yitus.domain.enumeration.TypeMatiere;
import yitus.domain.enumeration.TypeMatiere;
import yitus.domain.enumeration.TypeMatiere;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link yitus.domain.Enseignant} entity. This class is used
 * in {@link yitus.web.rest.EnseignantResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /enseignants?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EnseignantCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeMatiere
     */
    public static class TypeMatiereFilter extends Filter<TypeMatiere> {

        public TypeMatiereFilter() {
        }

        public TypeMatiereFilter(TypeMatiereFilter filter) {
            super(filter);
        }

        @Override
        public TypeMatiereFilter copy() {
            return new TypeMatiereFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TypeMatiereFilter matiere1;

    private TypeMatiereFilter matiere2;

    private TypeMatiereFilter matiere3;

    private TypeMatiereFilter matiere4;

    private InstantFilter dateCreation;

    private InstantFilter dateModification;

    private InstantFilter dateArchivage;

    private LongFilter utilisateurId;

    private LongFilter contactId;

    private LongFilter photoId;

    private LongFilter etablissementId;

    public EnseignantCriteria() {
    }

    public EnseignantCriteria(EnseignantCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.matiere1 = other.matiere1 == null ? null : other.matiere1.copy();
        this.matiere2 = other.matiere2 == null ? null : other.matiere2.copy();
        this.matiere3 = other.matiere3 == null ? null : other.matiere3.copy();
        this.matiere4 = other.matiere4 == null ? null : other.matiere4.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.dateModification = other.dateModification == null ? null : other.dateModification.copy();
        this.dateArchivage = other.dateArchivage == null ? null : other.dateArchivage.copy();
        this.utilisateurId = other.utilisateurId == null ? null : other.utilisateurId.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
        this.photoId = other.photoId == null ? null : other.photoId.copy();
        this.etablissementId = other.etablissementId == null ? null : other.etablissementId.copy();
    }

    @Override
    public EnseignantCriteria copy() {
        return new EnseignantCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TypeMatiereFilter getMatiere1() {
        return matiere1;
    }

    public void setMatiere1(TypeMatiereFilter matiere1) {
        this.matiere1 = matiere1;
    }

    public TypeMatiereFilter getMatiere2() {
        return matiere2;
    }

    public void setMatiere2(TypeMatiereFilter matiere2) {
        this.matiere2 = matiere2;
    }

    public TypeMatiereFilter getMatiere3() {
        return matiere3;
    }

    public void setMatiere3(TypeMatiereFilter matiere3) {
        this.matiere3 = matiere3;
    }

    public TypeMatiereFilter getMatiere4() {
        return matiere4;
    }

    public void setMatiere4(TypeMatiereFilter matiere4) {
        this.matiere4 = matiere4;
    }

    public InstantFilter getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(InstantFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public InstantFilter getDateModification() {
        return dateModification;
    }

    public void setDateModification(InstantFilter dateModification) {
        this.dateModification = dateModification;
    }

    public InstantFilter getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(InstantFilter dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public LongFilter getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(LongFilter utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }

    public LongFilter getPhotoId() {
        return photoId;
    }

    public void setPhotoId(LongFilter photoId) {
        this.photoId = photoId;
    }

    public LongFilter getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(LongFilter etablissementId) {
        this.etablissementId = etablissementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EnseignantCriteria that = (EnseignantCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(matiere1, that.matiere1) &&
            Objects.equals(matiere2, that.matiere2) &&
            Objects.equals(matiere3, that.matiere3) &&
            Objects.equals(matiere4, that.matiere4) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(dateModification, that.dateModification) &&
            Objects.equals(dateArchivage, that.dateArchivage) &&
            Objects.equals(utilisateurId, that.utilisateurId) &&
            Objects.equals(contactId, that.contactId) &&
            Objects.equals(photoId, that.photoId) &&
            Objects.equals(etablissementId, that.etablissementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        matiere1,
        matiere2,
        matiere3,
        matiere4,
        dateCreation,
        dateModification,
        dateArchivage,
        utilisateurId,
        contactId,
        photoId,
        etablissementId
        );
    }

    @Override
    public String toString() {
        return "EnseignantCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (matiere1 != null ? "matiere1=" + matiere1 + ", " : "") +
                (matiere2 != null ? "matiere2=" + matiere2 + ", " : "") +
                (matiere3 != null ? "matiere3=" + matiere3 + ", " : "") +
                (matiere4 != null ? "matiere4=" + matiere4 + ", " : "") +
                (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
                (dateModification != null ? "dateModification=" + dateModification + ", " : "") +
                (dateArchivage != null ? "dateArchivage=" + dateArchivage + ", " : "") +
                (utilisateurId != null ? "utilisateurId=" + utilisateurId + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
                (photoId != null ? "photoId=" + photoId + ", " : "") +
                (etablissementId != null ? "etablissementId=" + etablissementId + ", " : "") +
            "}";
    }

}
