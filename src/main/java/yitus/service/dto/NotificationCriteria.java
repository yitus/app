package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import yitus.domain.enumeration.TypeStatusNotification;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link yitus.domain.Notification} entity. This class is used
 * in {@link yitus.web.rest.NotificationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /notifications?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NotificationCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeStatusNotification
     */
    public static class TypeStatusNotificationFilter extends Filter<TypeStatusNotification> {

        public TypeStatusNotificationFilter() {
        }

        public TypeStatusNotificationFilter(TypeStatusNotificationFilter filter) {
            super(filter);
        }

        @Override
        public TypeStatusNotificationFilter copy() {
            return new TypeStatusNotificationFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TypeStatusNotificationFilter statusNotification;

    private StringFilter message;

    private InstantFilter dateCreation;

    private InstantFilter dateLecture;

    private InstantFilter dateArchivage;

    private LongFilter destineAId;

    private LongFilter creeParId;

    public NotificationCriteria() {
    }

    public NotificationCriteria(NotificationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.statusNotification = other.statusNotification == null ? null : other.statusNotification.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.dateLecture = other.dateLecture == null ? null : other.dateLecture.copy();
        this.dateArchivage = other.dateArchivage == null ? null : other.dateArchivage.copy();
        this.destineAId = other.destineAId == null ? null : other.destineAId.copy();
        this.creeParId = other.creeParId == null ? null : other.creeParId.copy();
    }

    @Override
    public NotificationCriteria copy() {
        return new NotificationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TypeStatusNotificationFilter getStatusNotification() {
        return statusNotification;
    }

    public void setStatusNotification(TypeStatusNotificationFilter statusNotification) {
        this.statusNotification = statusNotification;
    }

    public StringFilter getMessage() {
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public InstantFilter getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(InstantFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public InstantFilter getDateLecture() {
        return dateLecture;
    }

    public void setDateLecture(InstantFilter dateLecture) {
        this.dateLecture = dateLecture;
    }

    public InstantFilter getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(InstantFilter dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public LongFilter getDestineAId() {
        return destineAId;
    }

    public void setDestineAId(LongFilter destineAId) {
        this.destineAId = destineAId;
    }

    public LongFilter getCreeParId() {
        return creeParId;
    }

    public void setCreeParId(LongFilter creeParId) {
        this.creeParId = creeParId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NotificationCriteria that = (NotificationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(statusNotification, that.statusNotification) &&
            Objects.equals(message, that.message) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(dateLecture, that.dateLecture) &&
            Objects.equals(dateArchivage, that.dateArchivage) &&
            Objects.equals(destineAId, that.destineAId) &&
            Objects.equals(creeParId, that.creeParId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        statusNotification,
        message,
        dateCreation,
        dateLecture,
        dateArchivage,
        destineAId,
        creeParId
        );
    }

    @Override
    public String toString() {
        return "NotificationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (statusNotification != null ? "statusNotification=" + statusNotification + ", " : "") +
                (message != null ? "message=" + message + ", " : "") +
                (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
                (dateLecture != null ? "dateLecture=" + dateLecture + ", " : "") +
                (dateArchivage != null ? "dateArchivage=" + dateArchivage + ", " : "") +
                (destineAId != null ? "destineAId=" + destineAId + ", " : "") +
                (creeParId != null ? "creeParId=" + creeParId + ", " : "") +
            "}";
    }

}
