package yitus.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link yitus.domain.Contact} entity.
 */
@ApiModel(description = "Entité Contact : ne sert qu'a générer un DTO")
public class ContactDTO implements Serializable {
    
    private Long id;

    /**
     * Badge du contact
     */
    @Size(max = 2000)
    @Pattern(regexp = "^(([a-z]+,)*[a-z]+)?$")
    @ApiModelProperty(value = "Badge du contact")
    private String badges;

    /**
     * Image à ajouter (champ utilisé pour le chargement des photos des utilisateurs)
     */
    
    @ApiModelProperty(value = "Image à ajouter (champ utilisé pour le chargement des photos des utilisateurs)")
    @Lob
    private byte[] ajoutContenuImage;

    private String ajoutContenuImageContentType;
    /**
     * login/pseudo du contact  (le login de l'entité User)
     */
    @ApiModelProperty(value = "login/pseudo du contact  (le login de l'entité User)")
    private String login;

    /**
     * Nom du contact (le même que l'entité User)
     */
    @ApiModelProperty(value = "Nom du contact (le même que l'entité User)")
    private String nom;

    /**
     * Prénom du contact  (le même que l'entité User)
     */
    @ApiModelProperty(value = "Prénom du contact  (le même que l'entité User)")
    private String prenom;

    /**
     * Email du contact (le même que l'entité User)
     */
    @NotNull
    @ApiModelProperty(value = "Email du contact (le même que l'entité User)", required = true)
    private String email;

    /**
     * Téléphone du contact
     */
    @Size(min = 10, max = 10)
    @Pattern(regexp = "0[0-9]{9}")
    @ApiModelProperty(value = "Téléphone du contact")
    private String telephone;

    /**
     * Ville du contact
     */
    @ApiModelProperty(value = "Ville du contact")
    private String ville;

    /**
     * Gravatar (http:
     */
    @ApiModelProperty(value = "Gravatar (http:")
    private String gravatar;

    /**
     * Identifiant Linkedin
     */
    @ApiModelProperty(value = "Identifiant Linkedin")
    private String linkedin;

    /**
     * Identifiant google
     */
    @ApiModelProperty(value = "Identifiant google")
    private String google;

    /**
     * Identifiant facebook
     */
    @ApiModelProperty(value = "Identifiant facebook")
    private String facebook;

    /**
     * Identifiant Skype
     */
    @ApiModelProperty(value = "Identifiant Skype")
    private String skype;

    /**
     * Identifiant Discord
     */
    @ApiModelProperty(value = "Identifiant Discord")
    private String discord;

    /**
     * Identifiant Zoom
     */
    @ApiModelProperty(value = "Identifiant Zoom")
    private String zoom;

    /**
     * Identifiant whereby
     */
    @ApiModelProperty(value = "Identifiant whereby")
    private String whereby;

    /**
     * Identifiant mumble
     */
    @ApiModelProperty(value = "Identifiant mumble")
    private String mumble;

    /**
     * Identifiant tox
     */
    @ApiModelProperty(value = "Identifiant tox")
    private String tox;

    /**
     * Identifiant matrix
     */
    @ApiModelProperty(value = "Identifiant matrix")
    private String matrix;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBadges() {
        return badges;
    }

    public void setBadges(String badges) {
        this.badges = badges;
    }

    public byte[] getAjoutContenuImage() {
        return ajoutContenuImage;
    }

    public void setAjoutContenuImage(byte[] ajoutContenuImage) {
        this.ajoutContenuImage = ajoutContenuImage;
    }

    public String getAjoutContenuImageContentType() {
        return ajoutContenuImageContentType;
    }

    public void setAjoutContenuImageContentType(String ajoutContenuImageContentType) {
        this.ajoutContenuImageContentType = ajoutContenuImageContentType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getGravatar() {
        return gravatar;
    }

    public void setGravatar(String gravatar) {
        this.gravatar = gravatar;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getDiscord() {
        return discord;
    }

    public void setDiscord(String discord) {
        this.discord = discord;
    }

    public String getZoom() {
        return zoom;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    public String getWhereby() {
        return whereby;
    }

    public void setWhereby(String whereby) {
        this.whereby = whereby;
    }

    public String getMumble() {
        return mumble;
    }

    public void setMumble(String mumble) {
        this.mumble = mumble;
    }

    public String getTox() {
        return tox;
    }

    public void setTox(String tox) {
        this.tox = tox;
    }

    public String getMatrix() {
        return matrix;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactDTO contactDTO = (ContactDTO) o;
        if (contactDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contactDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContactDTO{" +
            "id=" + getId() +
            ", badges='" + getBadges() + "'" +
            ", ajoutContenuImage='" + getAjoutContenuImage() + "'" +
            ", login='" + getLogin() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", ville='" + getVille() + "'" +
            ", gravatar='" + getGravatar() + "'" +
            ", linkedin='" + getLinkedin() + "'" +
            ", google='" + getGoogle() + "'" +
            ", facebook='" + getFacebook() + "'" +
            ", skype='" + getSkype() + "'" +
            ", discord='" + getDiscord() + "'" +
            ", zoom='" + getZoom() + "'" +
            ", whereby='" + getWhereby() + "'" +
            ", mumble='" + getMumble() + "'" +
            ", tox='" + getTox() + "'" +
            ", matrix='" + getMatrix() + "'" +
            "}";
    }
}
