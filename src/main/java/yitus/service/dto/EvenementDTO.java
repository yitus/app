package yitus.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import yitus.domain.enumeration.TypeStatusEvenement;
import yitus.domain.enumeration.TypeOperation;
import yitus.domain.enumeration.TypeEntite;

/**
 * A DTO for the {@link yitus.domain.Evenement} entity.
 */
@ApiModel(description = "Evenement")
public class EvenementDTO implements Serializable {
    
    private Long id;

    /**
     * status de l'événement
     */
    @NotNull
    @ApiModelProperty(value = "status de l'événement", required = true)
    private TypeStatusEvenement statusEvenement;

    /**
     * Date de l'évenement
     */
    @NotNull
    @ApiModelProperty(value = "Date de l'évenement", required = true)
    private Instant dateEvenement;

    /**
     * Operation
     */
    @NotNull
    @ApiModelProperty(value = "Operation", required = true)
    private TypeOperation operation;

    /**
     * Entité
     */
    @NotNull
    @ApiModelProperty(value = "Entité", required = true)
    private TypeEntite entite;

    /**
     * Id de l'entité
     */
    @ApiModelProperty(value = "Id de l'entité")
    private Integer idEntite;

    /**
     * Message
     */
    @Size(max = 4000)
    @ApiModelProperty(value = "Message")
    private String message;


    private Long creeParId;

    private String creeParLogin;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStatusEvenement getStatusEvenement() {
        return statusEvenement;
    }

    public void setStatusEvenement(TypeStatusEvenement statusEvenement) {
        this.statusEvenement = statusEvenement;
    }

    public Instant getDateEvenement() {
        return dateEvenement;
    }

    public void setDateEvenement(Instant dateEvenement) {
        this.dateEvenement = dateEvenement;
    }

    public TypeOperation getOperation() {
        return operation;
    }

    public void setOperation(TypeOperation operation) {
        this.operation = operation;
    }

    public TypeEntite getEntite() {
        return entite;
    }

    public void setEntite(TypeEntite entite) {
        this.entite = entite;
    }

    public Integer getIdEntite() {
        return idEntite;
    }

    public void setIdEntite(Integer idEntite) {
        this.idEntite = idEntite;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getCreeParId() {
        return creeParId;
    }

    public void setCreeParId(Long userId) {
        this.creeParId = userId;
    }

    public String getCreeParLogin() {
        return creeParLogin;
    }

    public void setCreeParLogin(String userLogin) {
        this.creeParLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EvenementDTO evenementDTO = (EvenementDTO) o;
        if (evenementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evenementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvenementDTO{" +
            "id=" + getId() +
            ", statusEvenement='" + getStatusEvenement() + "'" +
            ", dateEvenement='" + getDateEvenement() + "'" +
            ", operation='" + getOperation() + "'" +
            ", entite='" + getEntite() + "'" +
            ", idEntite=" + getIdEntite() +
            ", message='" + getMessage() + "'" +
            ", creeParId=" + getCreeParId() +
            ", creeParLogin='" + getCreeParLogin() + "'" +
            "}";
    }
}
