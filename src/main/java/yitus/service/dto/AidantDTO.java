package yitus.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link yitus.domain.Aidant} entity.
 */
public class AidantDTO implements Serializable {
    
    private Long id;

    /**
     * cursus actuel de l'apprenant (ie Cycle ingénieur 4ème Info filière Info)
     */
    @NotNull
    @ApiModelProperty(value = "cursus actuel de l'apprenant (ie Cycle ingénieur 4ème Info filière Info)", required = true)
    private String cursusActuel;

    /**
     * nombre d'année post BAC
     */
    @Min(value = 1)
    @Max(value = 8)
    @ApiModelProperty(value = "nombre d'année post BAC")
    private Integer nombreAnneesPostBAC;

    /**
     * Date de la première création
     */
    @ApiModelProperty(value = "Date de la première création")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @ApiModelProperty(value = "Date de dernière modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long utilisateurId;

    private String utilisateurLogin;

    private Long contactId;

    private Long photoId;

    private Long etablissementId;

    private String etablissementNom;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCursusActuel() {
        return cursusActuel;
    }

    public void setCursusActuel(String cursusActuel) {
        this.cursusActuel = cursusActuel;
    }

    public Integer getNombreAnneesPostBAC() {
        return nombreAnneesPostBAC;
    }

    public void setNombreAnneesPostBAC(Integer nombreAnneesPostBAC) {
        this.nombreAnneesPostBAC = nombreAnneesPostBAC;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(Long userId) {
        this.utilisateurId = userId;
    }

    public String getUtilisateurLogin() {
        return utilisateurLogin;
    }

    public void setUtilisateurLogin(String userLogin) {
        this.utilisateurLogin = userLogin;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long imageId) {
        this.photoId = imageId;
    }

    public Long getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(Long etablissementId) {
        this.etablissementId = etablissementId;
    }

    public String getEtablissementNom() {
        return etablissementNom;
    }

    public void setEtablissementNom(String etablissementNom) {
        this.etablissementNom = etablissementNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AidantDTO aidantDTO = (AidantDTO) o;
        if (aidantDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aidantDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AidantDTO{" +
            "id=" + getId() +
            ", cursusActuel='" + getCursusActuel() + "'" +
            ", nombreAnneesPostBAC=" + getNombreAnneesPostBAC() +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", utilisateurId=" + getUtilisateurId() +
            ", utilisateurLogin='" + getUtilisateurLogin() + "'" +
            ", contactId=" + getContactId() +
            ", photoId=" + getPhotoId() +
            ", etablissementId=" + getEtablissementId() +
            ", etablissementNom='" + getEtablissementNom() + "'" +
            "}";
    }
}
