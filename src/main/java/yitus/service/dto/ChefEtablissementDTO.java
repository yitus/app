package yitus.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link yitus.domain.ChefEtablissement} entity.
 */
public class ChefEtablissementDTO implements Serializable {
    
    private Long id;

    /**
     * Date de la première création
     */
    @ApiModelProperty(value = "Date de la première création")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @ApiModelProperty(value = "Date de dernière modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long utilisateurId;

    private String utilisateurLogin;

    private Long photoId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(Long userId) {
        this.utilisateurId = userId;
    }

    public String getUtilisateurLogin() {
        return utilisateurLogin;
    }

    public void setUtilisateurLogin(String userLogin) {
        this.utilisateurLogin = userLogin;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long imageId) {
        this.photoId = imageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChefEtablissementDTO chefEtablissementDTO = (ChefEtablissementDTO) o;
        if (chefEtablissementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chefEtablissementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChefEtablissementDTO{" +
            "id=" + getId() +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", utilisateurId=" + getUtilisateurId() +
            ", utilisateurLogin='" + getUtilisateurLogin() + "'" +
            ", photoId=" + getPhotoId() +
            "}";
    }
}
