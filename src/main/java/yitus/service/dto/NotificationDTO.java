package yitus.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import yitus.domain.enumeration.TypeStatusNotification;

/**
 * A DTO for the {@link yitus.domain.Notification} entity.
 */
@ApiModel(description = "Notification")
public class NotificationDTO implements Serializable {
    
    private Long id;

    /**
     * status de la notification
     */
    @NotNull
    @ApiModelProperty(value = "status de la notification", required = true)
    private TypeStatusNotification statusNotification;

    /**
     * Message
     */
    @Size(max = 4000)
    @ApiModelProperty(value = "Message")
    private String message;

    /**
     * Date de la création
     */
    @NotNull
    @ApiModelProperty(value = "Date de la création", required = true)
    private Instant dateCreation;

    /**
     * Date de lecture
     */
    @ApiModelProperty(value = "Date de lecture")
    private Instant dateLecture;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long destineAId;

    private String destineALogin;

    private Long creeParId;

    private String creeParLogin;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStatusNotification getStatusNotification() {
        return statusNotification;
    }

    public void setStatusNotification(TypeStatusNotification statusNotification) {
        this.statusNotification = statusNotification;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateLecture() {
        return dateLecture;
    }

    public void setDateLecture(Instant dateLecture) {
        this.dateLecture = dateLecture;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getDestineAId() {
        return destineAId;
    }

    public void setDestineAId(Long userId) {
        this.destineAId = userId;
    }

    public String getDestineALogin() {
        return destineALogin;
    }

    public void setDestineALogin(String userLogin) {
        this.destineALogin = userLogin;
    }

    public Long getCreeParId() {
        return creeParId;
    }

    public void setCreeParId(Long userId) {
        this.creeParId = userId;
    }

    public String getCreeParLogin() {
        return creeParLogin;
    }

    public void setCreeParLogin(String userLogin) {
        this.creeParLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NotificationDTO notificationDTO = (NotificationDTO) o;
        if (notificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), notificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NotificationDTO{" +
            "id=" + getId() +
            ", statusNotification='" + getStatusNotification() + "'" +
            ", message='" + getMessage() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateLecture='" + getDateLecture() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", destineAId=" + getDestineAId() +
            ", destineALogin='" + getDestineALogin() + "'" +
            ", creeParId=" + getCreeParId() +
            ", creeParLogin='" + getCreeParLogin() + "'" +
            "}";
    }
}
