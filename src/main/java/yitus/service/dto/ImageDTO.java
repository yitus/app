package yitus.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link yitus.domain.Image} entity.
 */
@ApiModel(description = "Entité Image pour les photos des différents types d'utilisateurs et l'établissement")
public class ImageDTO implements Serializable {
    
    private Long id;

    /**
     * Contenu de l'image
     */
    
    @ApiModelProperty(value = "Contenu de l'image", required = true)
    @Lob
    private byte[] contenu;

    private String contenuContentType;
    @Size(min = 40, max = 40)
    @Pattern(regexp = "[a-fA-F0-9]{40}")
    private String contenuSha1;

    /**
     * Vignette de l'image
     */
    
    @ApiModelProperty(value = "Vignette de l'image", required = true)
    @Lob
    private byte[] vignette;

    private String vignetteContentType;
    @Size(min = 40, max = 40)
    @Pattern(regexp = "[a-fA-F0-9]{40}")
    private String vignetteSha1;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getContenu() {
        return contenu;
    }

    public void setContenu(byte[] contenu) {
        this.contenu = contenu;
    }

    public String getContenuContentType() {
        return contenuContentType;
    }

    public void setContenuContentType(String contenuContentType) {
        this.contenuContentType = contenuContentType;
    }

    public String getContenuSha1() {
        return contenuSha1;
    }

    public void setContenuSha1(String contenuSha1) {
        this.contenuSha1 = contenuSha1;
    }

    public byte[] getVignette() {
        return vignette;
    }

    public void setVignette(byte[] vignette) {
        this.vignette = vignette;
    }

    public String getVignetteContentType() {
        return vignetteContentType;
    }

    public void setVignetteContentType(String vignetteContentType) {
        this.vignetteContentType = vignetteContentType;
    }

    public String getVignetteSha1() {
        return vignetteSha1;
    }

    public void setVignetteSha1(String vignetteSha1) {
        this.vignetteSha1 = vignetteSha1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImageDTO imageDTO = (ImageDTO) o;
        if (imageDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), imageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ImageDTO{" +
            "id=" + getId() +
            ", contenu='" + getContenu() + "'" +
            ", contenuSha1='" + getContenuSha1() + "'" +
            ", vignette='" + getVignette() + "'" +
            ", vignetteSha1='" + getVignetteSha1() + "'" +
            "}";
    }
}
