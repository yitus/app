package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import yitus.domain.enumeration.TypeStatusEvenement;
import yitus.domain.enumeration.TypeOperation;
import yitus.domain.enumeration.TypeEntite;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link yitus.domain.Evenement} entity. This class is used
 * in {@link yitus.web.rest.EvenementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /evenements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EvenementCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeStatusEvenement
     */
    public static class TypeStatusEvenementFilter extends Filter<TypeStatusEvenement> {

        public TypeStatusEvenementFilter() {
        }

        public TypeStatusEvenementFilter(TypeStatusEvenementFilter filter) {
            super(filter);
        }

        @Override
        public TypeStatusEvenementFilter copy() {
            return new TypeStatusEvenementFilter(this);
        }

    }
    /**
     * Class for filtering TypeOperation
     */
    public static class TypeOperationFilter extends Filter<TypeOperation> {

        public TypeOperationFilter() {
        }

        public TypeOperationFilter(TypeOperationFilter filter) {
            super(filter);
        }

        @Override
        public TypeOperationFilter copy() {
            return new TypeOperationFilter(this);
        }

    }
    /**
     * Class for filtering TypeEntite
     */
    public static class TypeEntiteFilter extends Filter<TypeEntite> {

        public TypeEntiteFilter() {
        }

        public TypeEntiteFilter(TypeEntiteFilter filter) {
            super(filter);
        }

        @Override
        public TypeEntiteFilter copy() {
            return new TypeEntiteFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TypeStatusEvenementFilter statusEvenement;

    private InstantFilter dateEvenement;

    private TypeOperationFilter operation;

    private TypeEntiteFilter entite;

    private IntegerFilter idEntite;

    private StringFilter message;

    private LongFilter creeParId;

    public EvenementCriteria() {
    }

    public EvenementCriteria(EvenementCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.statusEvenement = other.statusEvenement == null ? null : other.statusEvenement.copy();
        this.dateEvenement = other.dateEvenement == null ? null : other.dateEvenement.copy();
        this.operation = other.operation == null ? null : other.operation.copy();
        this.entite = other.entite == null ? null : other.entite.copy();
        this.idEntite = other.idEntite == null ? null : other.idEntite.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.creeParId = other.creeParId == null ? null : other.creeParId.copy();
    }

    @Override
    public EvenementCriteria copy() {
        return new EvenementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TypeStatusEvenementFilter getStatusEvenement() {
        return statusEvenement;
    }

    public void setStatusEvenement(TypeStatusEvenementFilter statusEvenement) {
        this.statusEvenement = statusEvenement;
    }

    public InstantFilter getDateEvenement() {
        return dateEvenement;
    }

    public void setDateEvenement(InstantFilter dateEvenement) {
        this.dateEvenement = dateEvenement;
    }

    public TypeOperationFilter getOperation() {
        return operation;
    }

    public void setOperation(TypeOperationFilter operation) {
        this.operation = operation;
    }

    public TypeEntiteFilter getEntite() {
        return entite;
    }

    public void setEntite(TypeEntiteFilter entite) {
        this.entite = entite;
    }

    public IntegerFilter getIdEntite() {
        return idEntite;
    }

    public void setIdEntite(IntegerFilter idEntite) {
        this.idEntite = idEntite;
    }

    public StringFilter getMessage() {
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public LongFilter getCreeParId() {
        return creeParId;
    }

    public void setCreeParId(LongFilter creeParId) {
        this.creeParId = creeParId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EvenementCriteria that = (EvenementCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(statusEvenement, that.statusEvenement) &&
            Objects.equals(dateEvenement, that.dateEvenement) &&
            Objects.equals(operation, that.operation) &&
            Objects.equals(entite, that.entite) &&
            Objects.equals(idEntite, that.idEntite) &&
            Objects.equals(message, that.message) &&
            Objects.equals(creeParId, that.creeParId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        statusEvenement,
        dateEvenement,
        operation,
        entite,
        idEntite,
        message,
        creeParId
        );
    }

    @Override
    public String toString() {
        return "EvenementCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (statusEvenement != null ? "statusEvenement=" + statusEvenement + ", " : "") +
                (dateEvenement != null ? "dateEvenement=" + dateEvenement + ", " : "") +
                (operation != null ? "operation=" + operation + ", " : "") +
                (entite != null ? "entite=" + entite + ", " : "") +
                (idEntite != null ? "idEntite=" + idEntite + ", " : "") +
                (message != null ? "message=" + message + ", " : "") +
                (creeParId != null ? "creeParId=" + creeParId + ", " : "") +
            "}";
    }

}
