package yitus.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import yitus.domain.enumeration.TypeNiveau;

/**
 * A DTO for the {@link yitus.domain.Apprenant} entity.
 */
public class ApprenantDTO implements Serializable {
    
    private Long id;

    /**
     * niveau actuel de l'apprenant
     */
    @NotNull
    @ApiModelProperty(value = "niveau actuel de l'apprenant", required = true)
    private TypeNiveau niveau;

    /**
     * Date de la première création
     */
    @ApiModelProperty(value = "Date de la première création")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @ApiModelProperty(value = "Date de dernière modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long utilisateurId;

    private String utilisateurLogin;

    private Long contactId;

    private Long photoId;

    private Long etablissementId;

    private String etablissementNom;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeNiveau getNiveau() {
        return niveau;
    }

    public void setNiveau(TypeNiveau niveau) {
        this.niveau = niveau;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(Long userId) {
        this.utilisateurId = userId;
    }

    public String getUtilisateurLogin() {
        return utilisateurLogin;
    }

    public void setUtilisateurLogin(String userLogin) {
        this.utilisateurLogin = userLogin;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long imageId) {
        this.photoId = imageId;
    }

    public Long getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(Long etablissementId) {
        this.etablissementId = etablissementId;
    }

    public String getEtablissementNom() {
        return etablissementNom;
    }

    public void setEtablissementNom(String etablissementNom) {
        this.etablissementNom = etablissementNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ApprenantDTO apprenantDTO = (ApprenantDTO) o;
        if (apprenantDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), apprenantDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ApprenantDTO{" +
            "id=" + getId() +
            ", niveau='" + getNiveau() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", utilisateurId=" + getUtilisateurId() +
            ", utilisateurLogin='" + getUtilisateurLogin() + "'" +
            ", contactId=" + getContactId() +
            ", photoId=" + getPhotoId() +
            ", etablissementId=" + getEtablissementId() +
            ", etablissementNom='" + getEtablissementNom() + "'" +
            "}";
    }
}
