package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import yitus.domain.enumeration.TypeEtablissement;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link yitus.domain.Etablissement} entity. This class is used
 * in {@link yitus.web.rest.EtablissementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /etablissements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EtablissementCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeEtablissement
     */
    public static class TypeEtablissementFilter extends Filter<TypeEtablissement> {

        public TypeEtablissementFilter() {
        }

        public TypeEtablissementFilter(TypeEtablissementFilter filter) {
            super(filter);
        }

        @Override
        public TypeEtablissementFilter copy() {
            return new TypeEtablissementFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TypeEtablissementFilter typeEtablissement;

    private StringFilter nom;

    private StringFilter siteweb;

    private StringFilter adresse;

    private StringFilter ville;

    private StringFilter codePostal;

    private InstantFilter dateCreation;

    private InstantFilter dateModification;

    private InstantFilter dateArchivage;

    private LongFilter chefId;

    private LongFilter photoId;

    public EtablissementCriteria() {
    }

    public EtablissementCriteria(EtablissementCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.typeEtablissement = other.typeEtablissement == null ? null : other.typeEtablissement.copy();
        this.nom = other.nom == null ? null : other.nom.copy();
        this.siteweb = other.siteweb == null ? null : other.siteweb.copy();
        this.adresse = other.adresse == null ? null : other.adresse.copy();
        this.ville = other.ville == null ? null : other.ville.copy();
        this.codePostal = other.codePostal == null ? null : other.codePostal.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.dateModification = other.dateModification == null ? null : other.dateModification.copy();
        this.dateArchivage = other.dateArchivage == null ? null : other.dateArchivage.copy();
        this.chefId = other.chefId == null ? null : other.chefId.copy();
        this.photoId = other.photoId == null ? null : other.photoId.copy();
    }

    @Override
    public EtablissementCriteria copy() {
        return new EtablissementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TypeEtablissementFilter getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(TypeEtablissementFilter typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public StringFilter getNom() {
        return nom;
    }

    public void setNom(StringFilter nom) {
        this.nom = nom;
    }

    public StringFilter getSiteweb() {
        return siteweb;
    }

    public void setSiteweb(StringFilter siteweb) {
        this.siteweb = siteweb;
    }

    public StringFilter getAdresse() {
        return adresse;
    }

    public void setAdresse(StringFilter adresse) {
        this.adresse = adresse;
    }

    public StringFilter getVille() {
        return ville;
    }

    public void setVille(StringFilter ville) {
        this.ville = ville;
    }

    public StringFilter getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(StringFilter codePostal) {
        this.codePostal = codePostal;
    }

    public InstantFilter getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(InstantFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public InstantFilter getDateModification() {
        return dateModification;
    }

    public void setDateModification(InstantFilter dateModification) {
        this.dateModification = dateModification;
    }

    public InstantFilter getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(InstantFilter dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public LongFilter getChefId() {
        return chefId;
    }

    public void setChefId(LongFilter chefId) {
        this.chefId = chefId;
    }

    public LongFilter getPhotoId() {
        return photoId;
    }

    public void setPhotoId(LongFilter photoId) {
        this.photoId = photoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EtablissementCriteria that = (EtablissementCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(typeEtablissement, that.typeEtablissement) &&
            Objects.equals(nom, that.nom) &&
            Objects.equals(siteweb, that.siteweb) &&
            Objects.equals(adresse, that.adresse) &&
            Objects.equals(ville, that.ville) &&
            Objects.equals(codePostal, that.codePostal) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(dateModification, that.dateModification) &&
            Objects.equals(dateArchivage, that.dateArchivage) &&
            Objects.equals(chefId, that.chefId) &&
            Objects.equals(photoId, that.photoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        typeEtablissement,
        nom,
        siteweb,
        adresse,
        ville,
        codePostal,
        dateCreation,
        dateModification,
        dateArchivage,
        chefId,
        photoId
        );
    }

    @Override
    public String toString() {
        return "EtablissementCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (typeEtablissement != null ? "typeEtablissement=" + typeEtablissement + ", " : "") +
                (nom != null ? "nom=" + nom + ", " : "") +
                (siteweb != null ? "siteweb=" + siteweb + ", " : "") +
                (adresse != null ? "adresse=" + adresse + ", " : "") +
                (ville != null ? "ville=" + ville + ", " : "") +
                (codePostal != null ? "codePostal=" + codePostal + ", " : "") +
                (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
                (dateModification != null ? "dateModification=" + dateModification + ", " : "") +
                (dateArchivage != null ? "dateArchivage=" + dateArchivage + ", " : "") +
                (chefId != null ? "chefId=" + chefId + ", " : "") +
                (photoId != null ? "photoId=" + photoId + ", " : "") +
            "}";
    }

}
