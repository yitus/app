package yitus.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import yitus.domain.enumeration.TypeMatiere;

/**
 * A DTO for the {@link yitus.domain.Enseignant} entity.
 */
public class EnseignantDTO implements Serializable {
    
    private Long id;

    /**
     * Matière enseignée
     */
    @NotNull
    @ApiModelProperty(value = "Matière enseignée", required = true)
    private TypeMatiere matiere1;

    /**
     * Matière enseignée
     */
    @ApiModelProperty(value = "Matière enseignée")
    private TypeMatiere matiere2;

    /**
     * Matière enseignée
     */
    @ApiModelProperty(value = "Matière enseignée")
    private TypeMatiere matiere3;

    /**
     * Matière enseignée
     */
    @ApiModelProperty(value = "Matière enseignée")
    private TypeMatiere matiere4;

    /**
     * Date de la première création
     */
    @ApiModelProperty(value = "Date de la première création")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @ApiModelProperty(value = "Date de dernière modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @ApiModelProperty(value = "Date d'archivage")
    private Instant dateArchivage;


    private Long utilisateurId;

    private String utilisateurLogin;

    private Long contactId;

    private Long photoId;

    private Long etablissementId;

    private String etablissementNom;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeMatiere getMatiere1() {
        return matiere1;
    }

    public void setMatiere1(TypeMatiere matiere1) {
        this.matiere1 = matiere1;
    }

    public TypeMatiere getMatiere2() {
        return matiere2;
    }

    public void setMatiere2(TypeMatiere matiere2) {
        this.matiere2 = matiere2;
    }

    public TypeMatiere getMatiere3() {
        return matiere3;
    }

    public void setMatiere3(TypeMatiere matiere3) {
        this.matiere3 = matiere3;
    }

    public TypeMatiere getMatiere4() {
        return matiere4;
    }

    public void setMatiere4(TypeMatiere matiere4) {
        this.matiere4 = matiere4;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Long getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(Long userId) {
        this.utilisateurId = userId;
    }

    public String getUtilisateurLogin() {
        return utilisateurLogin;
    }

    public void setUtilisateurLogin(String userLogin) {
        this.utilisateurLogin = userLogin;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long imageId) {
        this.photoId = imageId;
    }

    public Long getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(Long etablissementId) {
        this.etablissementId = etablissementId;
    }

    public String getEtablissementNom() {
        return etablissementNom;
    }

    public void setEtablissementNom(String etablissementNom) {
        this.etablissementNom = etablissementNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EnseignantDTO enseignantDTO = (EnseignantDTO) o;
        if (enseignantDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), enseignantDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EnseignantDTO{" +
            "id=" + getId() +
            ", matiere1='" + getMatiere1() + "'" +
            ", matiere2='" + getMatiere2() + "'" +
            ", matiere3='" + getMatiere3() + "'" +
            ", matiere4='" + getMatiere4() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            ", utilisateurId=" + getUtilisateurId() +
            ", utilisateurLogin='" + getUtilisateurLogin() + "'" +
            ", contactId=" + getContactId() +
            ", photoId=" + getPhotoId() +
            ", etablissementId=" + getEtablissementId() +
            ", etablissementNom='" + getEtablissementNom() + "'" +
            "}";
    }
}
