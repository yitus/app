package yitus.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import yitus.domain.enumeration.TypeNiveau;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link yitus.domain.Apprenant} entity. This class is used
 * in {@link yitus.web.rest.ApprenantResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /apprenants?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ApprenantCriteria implements Serializable, Criteria {
    /**
     * Class for filtering TypeNiveau
     */
    public static class TypeNiveauFilter extends Filter<TypeNiveau> {

        public TypeNiveauFilter() {
        }

        public TypeNiveauFilter(TypeNiveauFilter filter) {
            super(filter);
        }

        @Override
        public TypeNiveauFilter copy() {
            return new TypeNiveauFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private TypeNiveauFilter niveau;

    private InstantFilter dateCreation;

    private InstantFilter dateModification;

    private InstantFilter dateArchivage;

    private LongFilter utilisateurId;

    private LongFilter contactId;

    private LongFilter photoId;

    private LongFilter etablissementId;

    public ApprenantCriteria() {
    }

    public ApprenantCriteria(ApprenantCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.niveau = other.niveau == null ? null : other.niveau.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.dateModification = other.dateModification == null ? null : other.dateModification.copy();
        this.dateArchivage = other.dateArchivage == null ? null : other.dateArchivage.copy();
        this.utilisateurId = other.utilisateurId == null ? null : other.utilisateurId.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
        this.photoId = other.photoId == null ? null : other.photoId.copy();
        this.etablissementId = other.etablissementId == null ? null : other.etablissementId.copy();
    }

    @Override
    public ApprenantCriteria copy() {
        return new ApprenantCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public TypeNiveauFilter getNiveau() {
        return niveau;
    }

    public void setNiveau(TypeNiveauFilter niveau) {
        this.niveau = niveau;
    }

    public InstantFilter getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(InstantFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public InstantFilter getDateModification() {
        return dateModification;
    }

    public void setDateModification(InstantFilter dateModification) {
        this.dateModification = dateModification;
    }

    public InstantFilter getDateArchivage() {
        return dateArchivage;
    }

    public void setDateArchivage(InstantFilter dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public LongFilter getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(LongFilter utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }

    public LongFilter getPhotoId() {
        return photoId;
    }

    public void setPhotoId(LongFilter photoId) {
        this.photoId = photoId;
    }

    public LongFilter getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(LongFilter etablissementId) {
        this.etablissementId = etablissementId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ApprenantCriteria that = (ApprenantCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(niveau, that.niveau) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(dateModification, that.dateModification) &&
            Objects.equals(dateArchivage, that.dateArchivage) &&
            Objects.equals(utilisateurId, that.utilisateurId) &&
            Objects.equals(contactId, that.contactId) &&
            Objects.equals(photoId, that.photoId) &&
            Objects.equals(etablissementId, that.etablissementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        niveau,
        dateCreation,
        dateModification,
        dateArchivage,
        utilisateurId,
        contactId,
        photoId,
        etablissementId
        );
    }

    @Override
    public String toString() {
        return "ApprenantCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (niveau != null ? "niveau=" + niveau + ", " : "") +
                (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
                (dateModification != null ? "dateModification=" + dateModification + ", " : "") +
                (dateArchivage != null ? "dateArchivage=" + dateArchivage + ", " : "") +
                (utilisateurId != null ? "utilisateurId=" + utilisateurId + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
                (photoId != null ? "photoId=" + photoId + ", " : "") +
                (etablissementId != null ? "etablissementId=" + etablissementId + ", " : "") +
            "}";
    }

}
