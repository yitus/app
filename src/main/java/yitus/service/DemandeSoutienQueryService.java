package yitus.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import yitus.domain.DemandeSoutien;
import yitus.domain.*; // for static metamodels
import yitus.repository.DemandeSoutienRepository;
import yitus.service.dto.DemandeSoutienCriteria;
import yitus.service.dto.DemandeSoutienDTO;
import yitus.service.mapper.DemandeSoutienMapper;

/**
 * Service for executing complex queries for {@link DemandeSoutien} entities in the database.
 * The main input is a {@link DemandeSoutienCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DemandeSoutienDTO} or a {@link Page} of {@link DemandeSoutienDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DemandeSoutienQueryService extends QueryService<DemandeSoutien> {

    private final Logger log = LoggerFactory.getLogger(DemandeSoutienQueryService.class);

    private final DemandeSoutienRepository demandeSoutienRepository;

    private final DemandeSoutienMapper demandeSoutienMapper;

    public DemandeSoutienQueryService(DemandeSoutienRepository demandeSoutienRepository, DemandeSoutienMapper demandeSoutienMapper) {
        this.demandeSoutienRepository = demandeSoutienRepository;
        this.demandeSoutienMapper = demandeSoutienMapper;
    }

    /**
     * Return a {@link List} of {@link DemandeSoutienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DemandeSoutienDTO> findByCriteria(DemandeSoutienCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DemandeSoutien> specification = createSpecification(criteria);
        return demandeSoutienMapper.toDto(demandeSoutienRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DemandeSoutienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DemandeSoutienDTO> findByCriteria(DemandeSoutienCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DemandeSoutien> specification = createSpecification(criteria);
        return demandeSoutienRepository.findAll(specification, page)
            .map(demandeSoutienMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DemandeSoutienCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DemandeSoutien> specification = createSpecification(criteria);
        return demandeSoutienRepository.count(specification);
    }

    /**
     * Function to convert {@link DemandeSoutienCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DemandeSoutien> createSpecification(DemandeSoutienCriteria criteria) {
        Specification<DemandeSoutien> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DemandeSoutien_.id));
            }
            if (criteria.getStatut() != null) {
                specification = specification.and(buildSpecification(criteria.getStatut(), DemandeSoutien_.statut));
            }
            if (criteria.getEtiquettes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEtiquettes(), DemandeSoutien_.etiquettes));
            }
            if (criteria.getNiveau() != null) {
                specification = specification.and(buildSpecification(criteria.getNiveau(), DemandeSoutien_.niveau));
            }
            if (criteria.getMatiere() != null) {
                specification = specification.and(buildSpecification(criteria.getMatiere(), DemandeSoutien_.matiere));
            }
            if (criteria.getHorairesDisponibilites() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHorairesDisponibilites(), DemandeSoutien_.horairesDisponibilites));
            }
            if (criteria.getDateRendezVous() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateRendezVous(), DemandeSoutien_.dateRendezVous));
            }
            if (criteria.getNoteApprenant() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNoteApprenant(), DemandeSoutien_.noteApprenant));
            }
            if (criteria.getNoteAidant() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNoteAidant(), DemandeSoutien_.noteAidant));
            }
            if (criteria.getDateLimite() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateLimite(), DemandeSoutien_.dateLimite));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), DemandeSoutien_.dateCreation));
            }
            if (criteria.getDateModification() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateModification(), DemandeSoutien_.dateModification));
            }
            if (criteria.getDateArchivage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateArchivage(), DemandeSoutien_.dateArchivage));
            }
            if (criteria.getApprenantId() != null) {
                specification = specification.and(buildSpecification(criteria.getApprenantId(),
                    root -> root.join(DemandeSoutien_.apprenant, JoinType.LEFT).get(Apprenant_.id)));
            }
            if (criteria.getAidantId() != null) {
                specification = specification.and(buildSpecification(criteria.getAidantId(),
                    root -> root.join(DemandeSoutien_.aidant, JoinType.LEFT).get(Aidant_.id)));
            }
            if (criteria.getEnseignantId() != null) {
                specification = specification.and(buildSpecification(criteria.getEnseignantId(),
                    root -> root.join(DemandeSoutien_.enseignant, JoinType.LEFT).get(Enseignant_.id)));
            }
            if (criteria.getReferentId() != null) {
                specification = specification.and(buildSpecification(criteria.getReferentId(),
                    root -> root.join(DemandeSoutien_.referent, JoinType.LEFT).get(Referent_.id)));
            }
        }
        return specification;
    }
}
