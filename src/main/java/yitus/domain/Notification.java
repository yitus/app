package yitus.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

import yitus.domain.enumeration.TypeStatusNotification;

/**
 * Notification
 */
@Entity
@Table(name = "notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * status de la notification
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status_notification", nullable = false)
    private TypeStatusNotification statusNotification;

    /**
     * Message
     */
    @Size(max = 4000)
    @Column(name = "message", length = 4000)
    private String message;

    /**
     * Date de la création
     */
    @NotNull
    @Column(name = "date_creation", nullable = false)
    private Instant dateCreation;

    /**
     * Date de lecture
     */
    @Column(name = "date_lecture")
    private Instant dateLecture;

    /**
     * Date d'archivage
     */
    @Column(name = "date_archivage")
    private Instant dateArchivage;

    @ManyToOne
    @JsonIgnoreProperties("notifications")
    private User destineA;

    @ManyToOne
    @JsonIgnoreProperties("notifications")
    private User creePar;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStatusNotification getStatusNotification() {
        return statusNotification;
    }

    public Notification statusNotification(TypeStatusNotification statusNotification) {
        this.statusNotification = statusNotification;
        return this;
    }

    public void setStatusNotification(TypeStatusNotification statusNotification) {
        this.statusNotification = statusNotification;
    }

    public String getMessage() {
        return message;
    }

    public Notification message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public Notification dateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateLecture() {
        return dateLecture;
    }

    public Notification dateLecture(Instant dateLecture) {
        this.dateLecture = dateLecture;
        return this;
    }

    public void setDateLecture(Instant dateLecture) {
        this.dateLecture = dateLecture;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public Notification dateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
        return this;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public User getDestineA() {
        return destineA;
    }

    public Notification destineA(User user) {
        this.destineA = user;
        return this;
    }

    public void setDestineA(User user) {
        this.destineA = user;
    }

    public User getCreePar() {
        return creePar;
    }

    public Notification creePar(User user) {
        this.creePar = user;
        return this;
    }

    public void setCreePar(User user) {
        this.creePar = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        return id != null && id.equals(((Notification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Notification{" +
            "id=" + getId() +
            ", statusNotification='" + getStatusNotification() + "'" +
            ", message='" + getMessage() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateLecture='" + getDateLecture() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            "}";
    }
}
