package yitus.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entité Contact : ne sert qu'a générer un DTO
 */
@Entity
@Table(name = "contact")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Badge du contact
     */
    @Size(max = 2000)
    @Pattern(regexp = "^(([a-z]+,)*[a-z]+)?$")
    @Column(name = "badges", length = 2000)
    private String badges;

    /**
     * Image à ajouter (champ utilisé pour le chargement des photos des utilisateurs)
     */
    
    @Lob
    @Column(name = "ajout_contenu_image")
    private byte[] ajoutContenuImage;

    @Column(name = "ajout_contenu_image_content_type")
    private String ajoutContenuImageContentType;

    /**
     * login/pseudo du contact  (le login de l'entité User)
     */
    @Column(name = "login")
    private String login;

    /**
     * Nom du contact (le même que l'entité User)
     */
    @Column(name = "nom")
    private String nom;

    /**
     * Prénom du contact  (le même que l'entité User)
     */
    @Column(name = "prenom")
    private String prenom;

    /**
     * Email du contact (le même que l'entité User)
     */
    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    /**
     * Téléphone du contact
     */
    @Size(min = 10, max = 10)
    @Pattern(regexp = "0[0-9]{9}")
    @Column(name = "telephone", length = 10)
    private String telephone;

    /**
     * Ville du contact
     */
    @Column(name = "ville")
    private String ville;

    /**
     * Gravatar (http:
     */
    @Column(name = "gravatar")
    private String gravatar;

    /**
     * Identifiant Linkedin
     */
    @Column(name = "linkedin")
    private String linkedin;

    /**
     * Identifiant google
     */
    @Column(name = "google")
    private String google;

    /**
     * Identifiant facebook
     */
    @Column(name = "facebook")
    private String facebook;

    /**
     * Identifiant Skype
     */
    @Column(name = "skype")
    private String skype;

    /**
     * Identifiant Discord
     */
    @Column(name = "discord")
    private String discord;

    /**
     * Identifiant Zoom
     */
    @Column(name = "zoom")
    private String zoom;

    /**
     * Identifiant whereby
     */
    @Column(name = "whereby")
    private String whereby;

    /**
     * Identifiant mumble
     */
    @Column(name = "mumble")
    private String mumble;

    /**
     * Identifiant tox
     */
    @Column(name = "tox")
    private String tox;

    /**
     * Identifiant matrix
     */
    @Column(name = "matrix")
    private String matrix;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBadges() {
        return badges;
    }

    public Contact badges(String badges) {
        this.badges = badges;
        return this;
    }

    public void setBadges(String badges) {
        this.badges = badges;
    }

    public byte[] getAjoutContenuImage() {
        return ajoutContenuImage;
    }

    public Contact ajoutContenuImage(byte[] ajoutContenuImage) {
        this.ajoutContenuImage = ajoutContenuImage;
        return this;
    }

    public void setAjoutContenuImage(byte[] ajoutContenuImage) {
        this.ajoutContenuImage = ajoutContenuImage;
    }

    public String getAjoutContenuImageContentType() {
        return ajoutContenuImageContentType;
    }

    public Contact ajoutContenuImageContentType(String ajoutContenuImageContentType) {
        this.ajoutContenuImageContentType = ajoutContenuImageContentType;
        return this;
    }

    public void setAjoutContenuImageContentType(String ajoutContenuImageContentType) {
        this.ajoutContenuImageContentType = ajoutContenuImageContentType;
    }

    public String getLogin() {
        return login;
    }

    public Contact login(String login) {
        this.login = login;
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public Contact nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Contact prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public Contact email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public Contact telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getVille() {
        return ville;
    }

    public Contact ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getGravatar() {
        return gravatar;
    }

    public Contact gravatar(String gravatar) {
        this.gravatar = gravatar;
        return this;
    }

    public void setGravatar(String gravatar) {
        this.gravatar = gravatar;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public Contact linkedin(String linkedin) {
        this.linkedin = linkedin;
        return this;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getGoogle() {
        return google;
    }

    public Contact google(String google) {
        this.google = google;
        return this;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getFacebook() {
        return facebook;
    }

    public Contact facebook(String facebook) {
        this.facebook = facebook;
        return this;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getSkype() {
        return skype;
    }

    public Contact skype(String skype) {
        this.skype = skype;
        return this;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getDiscord() {
        return discord;
    }

    public Contact discord(String discord) {
        this.discord = discord;
        return this;
    }

    public void setDiscord(String discord) {
        this.discord = discord;
    }

    public String getZoom() {
        return zoom;
    }

    public Contact zoom(String zoom) {
        this.zoom = zoom;
        return this;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    public String getWhereby() {
        return whereby;
    }

    public Contact whereby(String whereby) {
        this.whereby = whereby;
        return this;
    }

    public void setWhereby(String whereby) {
        this.whereby = whereby;
    }

    public String getMumble() {
        return mumble;
    }

    public Contact mumble(String mumble) {
        this.mumble = mumble;
        return this;
    }

    public void setMumble(String mumble) {
        this.mumble = mumble;
    }

    public String getTox() {
        return tox;
    }

    public Contact tox(String tox) {
        this.tox = tox;
        return this;
    }

    public void setTox(String tox) {
        this.tox = tox;
    }

    public String getMatrix() {
        return matrix;
    }

    public Contact matrix(String matrix) {
        this.matrix = matrix;
        return this;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contact)) {
            return false;
        }
        return id != null && id.equals(((Contact) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Contact{" +
            "id=" + getId() +
            ", badges='" + getBadges() + "'" +
            ", ajoutContenuImage='" + getAjoutContenuImage() + "'" +
            ", ajoutContenuImageContentType='" + getAjoutContenuImageContentType() + "'" +
            ", login='" + getLogin() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", ville='" + getVille() + "'" +
            ", gravatar='" + getGravatar() + "'" +
            ", linkedin='" + getLinkedin() + "'" +
            ", google='" + getGoogle() + "'" +
            ", facebook='" + getFacebook() + "'" +
            ", skype='" + getSkype() + "'" +
            ", discord='" + getDiscord() + "'" +
            ", zoom='" + getZoom() + "'" +
            ", whereby='" + getWhereby() + "'" +
            ", mumble='" + getMumble() + "'" +
            ", tox='" + getTox() + "'" +
            ", matrix='" + getMatrix() + "'" +
            "}";
    }
}
