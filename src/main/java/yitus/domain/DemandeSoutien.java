package yitus.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

import yitus.domain.enumeration.TypeStatut;

import yitus.domain.enumeration.TypeNiveau;

import yitus.domain.enumeration.TypeMatiere;

/**
 * Demande de soutien
 */
@Entity
@Table(name = "demande_soutien")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DemandeSoutien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Statut de la demande
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "statut", nullable = false)
    private TypeStatut statut;

    /**
     * Etiquettes ou mots-clé de la demande
     */
    @Size(max = 2000)
    @Pattern(regexp = "^(([a-z]+,)*[a-z]+)?$")
    @Column(name = "etiquettes", length = 2000)
    private String etiquettes;

    /**
     * Niveau demandé
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "niveau", nullable = false)
    private TypeNiveau niveau;

    /**
     * Matiére concernée
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "matiere", nullable = false)
    private TypeMatiere matiere;

    /**
     * Detail de la demande de soutien (rich text)
     */
    
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "detail", nullable = false)
    private String detail;

    /**
     * Detail de la demande de soutien
     */
    @NotNull
    @Column(name = "horaires_disponibilites", nullable = false)
    private String horairesDisponibilites;

    /**
     * Date de la prise de rendez-vous pour la demande de soutien
     */
    @Column(name = "date_rendez_vous")
    private Instant dateRendezVous;

    /**
     * Note de l'apprenant sur l'aidant
     */
    @Min(value = 1)
    @Max(value = 10)
    @Column(name = "note_apprenant")
    private Integer noteApprenant;

    /**
     * Note de l'aidant sur l'apprenant
     */
    @Min(value = 1)
    @Max(value = 10)
    @Column(name = "note_aidant")
    private Integer noteAidant;

    /**
     * Commentaire de l'apprenant sur l'aidant (rich text)
     */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "commentaire_apprenant")
    private String commentaireApprenant;

    /**
     * Commentaire de l'aidant sur l'apprenant (rich text)
     */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "commentaire_aidant")
    private String commentaireAidant;

    /**
     * Commentaire de l'enseignant (rich text)
     */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "commentaire_enseignant")
    private String commentaireEnseignant;

    /**
     * Commentaire du référent (rich text)
     */
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "commentaire_referent")
    private String commentaireReferent;

    /**
     * Date limite de la demande de soutien
     */
    @NotNull
    @Column(name = "date_limite", nullable = false)
    private Instant dateLimite;

    /**
     * Date de la première création
     */
    @Column(name = "date_creation")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @Column(name = "date_modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @Column(name = "date_archivage")
    private Instant dateArchivage;

    @ManyToOne
    @JsonIgnoreProperties("demandeSoutiens")
    private Apprenant apprenant;

    @ManyToOne
    @JsonIgnoreProperties("demandeSoutiens")
    private Aidant aidant;

    @ManyToOne
    @JsonIgnoreProperties("demandeSoutiens")
    private Enseignant enseignant;

    @ManyToOne
    @JsonIgnoreProperties("demandeSoutiens")
    private Referent referent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStatut getStatut() {
        return statut;
    }

    public DemandeSoutien statut(TypeStatut statut) {
        this.statut = statut;
        return this;
    }

    public void setStatut(TypeStatut statut) {
        this.statut = statut;
    }

    public String getEtiquettes() {
        return etiquettes;
    }

    public DemandeSoutien etiquettes(String etiquettes) {
        this.etiquettes = etiquettes;
        return this;
    }

    public void setEtiquettes(String etiquettes) {
        this.etiquettes = etiquettes;
    }

    public TypeNiveau getNiveau() {
        return niveau;
    }

    public DemandeSoutien niveau(TypeNiveau niveau) {
        this.niveau = niveau;
        return this;
    }

    public void setNiveau(TypeNiveau niveau) {
        this.niveau = niveau;
    }

    public TypeMatiere getMatiere() {
        return matiere;
    }

    public DemandeSoutien matiere(TypeMatiere matiere) {
        this.matiere = matiere;
        return this;
    }

    public void setMatiere(TypeMatiere matiere) {
        this.matiere = matiere;
    }

    public String getDetail() {
        return detail;
    }

    public DemandeSoutien detail(String detail) {
        this.detail = detail;
        return this;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getHorairesDisponibilites() {
        return horairesDisponibilites;
    }

    public DemandeSoutien horairesDisponibilites(String horairesDisponibilites) {
        this.horairesDisponibilites = horairesDisponibilites;
        return this;
    }

    public void setHorairesDisponibilites(String horairesDisponibilites) {
        this.horairesDisponibilites = horairesDisponibilites;
    }

    public Instant getDateRendezVous() {
        return dateRendezVous;
    }

    public DemandeSoutien dateRendezVous(Instant dateRendezVous) {
        this.dateRendezVous = dateRendezVous;
        return this;
    }

    public void setDateRendezVous(Instant dateRendezVous) {
        this.dateRendezVous = dateRendezVous;
    }

    public Integer getNoteApprenant() {
        return noteApprenant;
    }

    public DemandeSoutien noteApprenant(Integer noteApprenant) {
        this.noteApprenant = noteApprenant;
        return this;
    }

    public void setNoteApprenant(Integer noteApprenant) {
        this.noteApprenant = noteApprenant;
    }

    public Integer getNoteAidant() {
        return noteAidant;
    }

    public DemandeSoutien noteAidant(Integer noteAidant) {
        this.noteAidant = noteAidant;
        return this;
    }

    public void setNoteAidant(Integer noteAidant) {
        this.noteAidant = noteAidant;
    }

    public String getCommentaireApprenant() {
        return commentaireApprenant;
    }

    public DemandeSoutien commentaireApprenant(String commentaireApprenant) {
        this.commentaireApprenant = commentaireApprenant;
        return this;
    }

    public void setCommentaireApprenant(String commentaireApprenant) {
        this.commentaireApprenant = commentaireApprenant;
    }

    public String getCommentaireAidant() {
        return commentaireAidant;
    }

    public DemandeSoutien commentaireAidant(String commentaireAidant) {
        this.commentaireAidant = commentaireAidant;
        return this;
    }

    public void setCommentaireAidant(String commentaireAidant) {
        this.commentaireAidant = commentaireAidant;
    }

    public String getCommentaireEnseignant() {
        return commentaireEnseignant;
    }

    public DemandeSoutien commentaireEnseignant(String commentaireEnseignant) {
        this.commentaireEnseignant = commentaireEnseignant;
        return this;
    }

    public void setCommentaireEnseignant(String commentaireEnseignant) {
        this.commentaireEnseignant = commentaireEnseignant;
    }

    public String getCommentaireReferent() {
        return commentaireReferent;
    }

    public DemandeSoutien commentaireReferent(String commentaireReferent) {
        this.commentaireReferent = commentaireReferent;
        return this;
    }

    public void setCommentaireReferent(String commentaireReferent) {
        this.commentaireReferent = commentaireReferent;
    }

    public Instant getDateLimite() {
        return dateLimite;
    }

    public DemandeSoutien dateLimite(Instant dateLimite) {
        this.dateLimite = dateLimite;
        return this;
    }

    public void setDateLimite(Instant dateLimite) {
        this.dateLimite = dateLimite;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public DemandeSoutien dateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public DemandeSoutien dateModification(Instant dateModification) {
        this.dateModification = dateModification;
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public DemandeSoutien dateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
        return this;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public Apprenant getApprenant() {
        return apprenant;
    }

    public DemandeSoutien apprenant(Apprenant apprenant) {
        this.apprenant = apprenant;
        return this;
    }

    public void setApprenant(Apprenant apprenant) {
        this.apprenant = apprenant;
    }

    public Aidant getAidant() {
        return aidant;
    }

    public DemandeSoutien aidant(Aidant aidant) {
        this.aidant = aidant;
        return this;
    }

    public void setAidant(Aidant aidant) {
        this.aidant = aidant;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public DemandeSoutien enseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
        return this;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public Referent getReferent() {
        return referent;
    }

    public DemandeSoutien referent(Referent referent) {
        this.referent = referent;
        return this;
    }

    public void setReferent(Referent referent) {
        this.referent = referent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DemandeSoutien)) {
            return false;
        }
        return id != null && id.equals(((DemandeSoutien) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DemandeSoutien{" +
            "id=" + getId() +
            ", statut='" + getStatut() + "'" +
            ", etiquettes='" + getEtiquettes() + "'" +
            ", niveau='" + getNiveau() + "'" +
            ", matiere='" + getMatiere() + "'" +
            ", detail='" + getDetail() + "'" +
            ", horairesDisponibilites='" + getHorairesDisponibilites() + "'" +
            ", dateRendezVous='" + getDateRendezVous() + "'" +
            ", noteApprenant=" + getNoteApprenant() +
            ", noteAidant=" + getNoteAidant() +
            ", commentaireApprenant='" + getCommentaireApprenant() + "'" +
            ", commentaireAidant='" + getCommentaireAidant() + "'" +
            ", commentaireEnseignant='" + getCommentaireEnseignant() + "'" +
            ", commentaireReferent='" + getCommentaireReferent() + "'" +
            ", dateLimite='" + getDateLimite() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            "}";
    }
}
