package yitus.domain.enumeration;

/**
 * The TypeMatiere enumeration.
 */
public enum TypeMatiere {
    MATHS, FRANCAIS, PHILOSOPHIE, PHYSIQUE, CHIMIE, BIOLOGIE, GEOLOGIE, MECANIQUE, ELECTRONIQUE, NSI, SNT
}
