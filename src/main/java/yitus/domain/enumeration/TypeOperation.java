package yitus.domain.enumeration;

/**
 * The TypeOperation enumeration.
 */
public enum TypeOperation {
    CREATION, MODIFICATION, SUPPRESSION, ARCHIVAGE, IMPORT, MAIL, AUTRE
}
