package yitus.domain.enumeration;

/**
 * The TypeEntite enumeration.
 */
public enum TypeEntite {
    AIDANT, APPRENANT, REFERENT, ENSEIGNANT, CHEF_ETABLISSEMENT, ETABLISSEMENT, IMAGE, AUTRE
}
