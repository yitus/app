package yitus.domain.enumeration;

/**
 * The TypeEtablissement enumeration.
 */
public enum TypeEtablissement {
    COLLEGE, LYCEE, LYCEE_PRO, UFR, ECOLE_INGENIEUR, ECOLE_DOCTORALE
}
