package yitus.domain.enumeration;

/**
 * The TypeStatut enumeration.
 */
public enum TypeStatut {
    EN_COURS, RDV_PROPOSE, RDV_CONFIRME, ANNULE, SOUTIEN_NON_EFFECTUE, SOUTIEN_EFFECTUE
}
