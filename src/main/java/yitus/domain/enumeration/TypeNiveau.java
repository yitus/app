package yitus.domain.enumeration;

/**
 * The TypeNiveau enumeration.
 */
public enum TypeNiveau {
    SIXIEME, CINQUIEME, QUATRIEME, TROISIEME, SECONDE, PREMIERE, TERMINALE
}
