package yitus.domain.enumeration;

/**
 * The TypeStatusEvenement enumeration.
 */
public enum TypeStatusEvenement {
    INFO, SUCCES, ALERTE, ECHEC
}
