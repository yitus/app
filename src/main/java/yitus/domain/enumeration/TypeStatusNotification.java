package yitus.domain.enumeration;

/**
 * The TypeStatusNotification enumeration.
 */
public enum TypeStatusNotification {
    INFO, ALERTE
}
