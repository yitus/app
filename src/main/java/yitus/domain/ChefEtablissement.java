package yitus.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A ChefEtablissement.
 */
@Entity
@Table(name = "chef_etablissement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChefEtablissement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Date de la première création
     */
    @Column(name = "date_creation")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @Column(name = "date_modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @Column(name = "date_archivage")
    private Instant dateArchivage;

    @OneToOne
    @JoinColumn(unique = true)
    private User utilisateur;

    @OneToOne
    @JoinColumn(unique = true)
    private Image photo;

    @OneToOne(mappedBy = "chef")
    @JsonIgnore
    private Etablissement etablissement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public ChefEtablissement dateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public ChefEtablissement dateModification(Instant dateModification) {
        this.dateModification = dateModification;
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public ChefEtablissement dateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
        return this;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public User getUtilisateur() {
        return utilisateur;
    }

    public ChefEtablissement utilisateur(User user) {
        this.utilisateur = user;
        return this;
    }

    public void setUtilisateur(User user) {
        this.utilisateur = user;
    }

    public Image getPhoto() {
        return photo;
    }

    public ChefEtablissement photo(Image image) {
        this.photo = image;
        return this;
    }

    public void setPhoto(Image image) {
        this.photo = image;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public ChefEtablissement etablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
        return this;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChefEtablissement)) {
            return false;
        }
        return id != null && id.equals(((ChefEtablissement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ChefEtablissement{" +
            "id=" + getId() +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            "}";
    }
}
