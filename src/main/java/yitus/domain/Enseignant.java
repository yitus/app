package yitus.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

import yitus.domain.enumeration.TypeMatiere;

/**
 * A Enseignant.
 */
@Entity
@Table(name = "enseignant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Enseignant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Matière enseignée
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "matiere_1", nullable = false)
    private TypeMatiere matiere1;

    /**
     * Matière enseignée
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "matiere_2")
    private TypeMatiere matiere2;

    /**
     * Matière enseignée
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "matiere_3")
    private TypeMatiere matiere3;

    /**
     * Matière enseignée
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "matiere_4")
    private TypeMatiere matiere4;

    /**
     * Date de la première création
     */
    @Column(name = "date_creation")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @Column(name = "date_modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @Column(name = "date_archivage")
    private Instant dateArchivage;

    @OneToOne
    @JoinColumn(unique = true)
    private User utilisateur;

    @OneToOne
    @JoinColumn(unique = true)
    private Contact contact;

    @OneToOne
    @JoinColumn(unique = true)
    private Image photo;

    @ManyToOne
    @JsonIgnoreProperties("enseignants")
    private Etablissement etablissement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeMatiere getMatiere1() {
        return matiere1;
    }

    public Enseignant matiere1(TypeMatiere matiere1) {
        this.matiere1 = matiere1;
        return this;
    }

    public void setMatiere1(TypeMatiere matiere1) {
        this.matiere1 = matiere1;
    }

    public TypeMatiere getMatiere2() {
        return matiere2;
    }

    public Enseignant matiere2(TypeMatiere matiere2) {
        this.matiere2 = matiere2;
        return this;
    }

    public void setMatiere2(TypeMatiere matiere2) {
        this.matiere2 = matiere2;
    }

    public TypeMatiere getMatiere3() {
        return matiere3;
    }

    public Enseignant matiere3(TypeMatiere matiere3) {
        this.matiere3 = matiere3;
        return this;
    }

    public void setMatiere3(TypeMatiere matiere3) {
        this.matiere3 = matiere3;
    }

    public TypeMatiere getMatiere4() {
        return matiere4;
    }

    public Enseignant matiere4(TypeMatiere matiere4) {
        this.matiere4 = matiere4;
        return this;
    }

    public void setMatiere4(TypeMatiere matiere4) {
        this.matiere4 = matiere4;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public Enseignant dateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public Enseignant dateModification(Instant dateModification) {
        this.dateModification = dateModification;
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public Enseignant dateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
        return this;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public User getUtilisateur() {
        return utilisateur;
    }

    public Enseignant utilisateur(User user) {
        this.utilisateur = user;
        return this;
    }

    public void setUtilisateur(User user) {
        this.utilisateur = user;
    }

    public Contact getContact() {
        return contact;
    }

    public Enseignant contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Image getPhoto() {
        return photo;
    }

    public Enseignant photo(Image image) {
        this.photo = image;
        return this;
    }

    public void setPhoto(Image image) {
        this.photo = image;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public Enseignant etablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
        return this;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Enseignant)) {
            return false;
        }
        return id != null && id.equals(((Enseignant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Enseignant{" +
            "id=" + getId() +
            ", matiere1='" + getMatiere1() + "'" +
            ", matiere2='" + getMatiere2() + "'" +
            ", matiere3='" + getMatiere3() + "'" +
            ", matiere4='" + getMatiere4() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            "}";
    }
}
