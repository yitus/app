package yitus.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

import yitus.domain.enumeration.TypeStatusEvenement;

import yitus.domain.enumeration.TypeOperation;

import yitus.domain.enumeration.TypeEntite;

/**
 * Evenement
 */
@Entity
@Table(name = "evenement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Evenement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * status de l'événement
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status_evenement", nullable = false)
    private TypeStatusEvenement statusEvenement;

    /**
     * Date de l'évenement
     */
    @NotNull
    @Column(name = "date_evenement", nullable = false)
    private Instant dateEvenement;

    /**
     * Operation
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operation", nullable = false)
    private TypeOperation operation;

    /**
     * Entité
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "entite", nullable = false)
    private TypeEntite entite;

    /**
     * Id de l'entité
     */
    @Column(name = "id_entite")
    private Integer idEntite;

    /**
     * Message
     */
    @Size(max = 4000)
    @Column(name = "message", length = 4000)
    private String message;

    @ManyToOne
    @JsonIgnoreProperties("evenements")
    private User creePar;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStatusEvenement getStatusEvenement() {
        return statusEvenement;
    }

    public Evenement statusEvenement(TypeStatusEvenement statusEvenement) {
        this.statusEvenement = statusEvenement;
        return this;
    }

    public void setStatusEvenement(TypeStatusEvenement statusEvenement) {
        this.statusEvenement = statusEvenement;
    }

    public Instant getDateEvenement() {
        return dateEvenement;
    }

    public Evenement dateEvenement(Instant dateEvenement) {
        this.dateEvenement = dateEvenement;
        return this;
    }

    public void setDateEvenement(Instant dateEvenement) {
        this.dateEvenement = dateEvenement;
    }

    public TypeOperation getOperation() {
        return operation;
    }

    public Evenement operation(TypeOperation operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(TypeOperation operation) {
        this.operation = operation;
    }

    public TypeEntite getEntite() {
        return entite;
    }

    public Evenement entite(TypeEntite entite) {
        this.entite = entite;
        return this;
    }

    public void setEntite(TypeEntite entite) {
        this.entite = entite;
    }

    public Integer getIdEntite() {
        return idEntite;
    }

    public Evenement idEntite(Integer idEntite) {
        this.idEntite = idEntite;
        return this;
    }

    public void setIdEntite(Integer idEntite) {
        this.idEntite = idEntite;
    }

    public String getMessage() {
        return message;
    }

    public Evenement message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getCreePar() {
        return creePar;
    }

    public Evenement creePar(User user) {
        this.creePar = user;
        return this;
    }

    public void setCreePar(User user) {
        this.creePar = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Evenement)) {
            return false;
        }
        return id != null && id.equals(((Evenement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Evenement{" +
            "id=" + getId() +
            ", statusEvenement='" + getStatusEvenement() + "'" +
            ", dateEvenement='" + getDateEvenement() + "'" +
            ", operation='" + getOperation() + "'" +
            ", entite='" + getEntite() + "'" +
            ", idEntite=" + getIdEntite() +
            ", message='" + getMessage() + "'" +
            "}";
    }
}
