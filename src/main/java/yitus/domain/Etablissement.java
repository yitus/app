package yitus.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

import yitus.domain.enumeration.TypeEtablissement;

/**
 * A Etablissement.
 */
@Entity
@Table(name = "etablissement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Etablissement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Type de l'établissement
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_etablissement", nullable = false)
    private TypeEtablissement typeEtablissement;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    /**
     * URL du site web de l'établissement
     */
    @NotNull
    @Column(name = "siteweb", nullable = false)
    private String siteweb;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    /**
     * Ville de l'établissement
     */
    @NotNull
    @Column(name = "ville", nullable = false)
    private String ville;

    @NotNull
    @Column(name = "code_postal", nullable = false)
    private String codePostal;

    /**
     * Date de la première création
     */
    @Column(name = "date_creation")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @Column(name = "date_modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @Column(name = "date_archivage")
    private Instant dateArchivage;

    @OneToOne
    @JoinColumn(unique = true)
    private ChefEtablissement chef;

    @OneToOne
    @JoinColumn(unique = true)
    private Image photo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeEtablissement getTypeEtablissement() {
        return typeEtablissement;
    }

    public Etablissement typeEtablissement(TypeEtablissement typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
        return this;
    }

    public void setTypeEtablissement(TypeEtablissement typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public String getNom() {
        return nom;
    }

    public Etablissement nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSiteweb() {
        return siteweb;
    }

    public Etablissement siteweb(String siteweb) {
        this.siteweb = siteweb;
        return this;
    }

    public void setSiteweb(String siteweb) {
        this.siteweb = siteweb;
    }

    public String getAdresse() {
        return adresse;
    }

    public Etablissement adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public Etablissement ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public Etablissement codePostal(String codePostal) {
        this.codePostal = codePostal;
        return this;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public Etablissement dateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public Etablissement dateModification(Instant dateModification) {
        this.dateModification = dateModification;
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public Etablissement dateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
        return this;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public ChefEtablissement getChef() {
        return chef;
    }

    public Etablissement chef(ChefEtablissement chefEtablissement) {
        this.chef = chefEtablissement;
        return this;
    }

    public void setChef(ChefEtablissement chefEtablissement) {
        this.chef = chefEtablissement;
    }

    public Image getPhoto() {
        return photo;
    }

    public Etablissement photo(Image image) {
        this.photo = image;
        return this;
    }

    public void setPhoto(Image image) {
        this.photo = image;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Etablissement)) {
            return false;
        }
        return id != null && id.equals(((Etablissement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Etablissement{" +
            "id=" + getId() +
            ", typeEtablissement='" + getTypeEtablissement() + "'" +
            ", nom='" + getNom() + "'" +
            ", siteweb='" + getSiteweb() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", ville='" + getVille() + "'" +
            ", codePostal='" + getCodePostal() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            "}";
    }
}
