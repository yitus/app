package yitus.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A Aidant.
 */
@Entity
@Table(name = "aidant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Aidant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * cursus actuel de l'apprenant (ie Cycle ingénieur 4ème Info filière Info)
     */
    @NotNull
    @Column(name = "cursus_actuel", nullable = false)
    private String cursusActuel;

    /**
     * nombre d'année post BAC
     */
    @Min(value = 1)
    @Max(value = 8)
    @Column(name = "nombre_annees_post_bac")
    private Integer nombreAnneesPostBAC;

    /**
     * Date de la première création
     */
    @Column(name = "date_creation")
    private Instant dateCreation;

    /**
     * Date de dernière modification
     */
    @Column(name = "date_modification")
    private Instant dateModification;

    /**
     * Date d'archivage
     */
    @Column(name = "date_archivage")
    private Instant dateArchivage;

    @OneToOne
    @JoinColumn(unique = true)
    private User utilisateur;

    @OneToOne
    @JoinColumn(unique = true)
    private Contact contact;

    @OneToOne
    @JoinColumn(unique = true)
    private Image photo;

    @ManyToOne
    @JsonIgnoreProperties("aidants")
    private Etablissement etablissement;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCursusActuel() {
        return cursusActuel;
    }

    public Aidant cursusActuel(String cursusActuel) {
        this.cursusActuel = cursusActuel;
        return this;
    }

    public void setCursusActuel(String cursusActuel) {
        this.cursusActuel = cursusActuel;
    }

    public Integer getNombreAnneesPostBAC() {
        return nombreAnneesPostBAC;
    }

    public Aidant nombreAnneesPostBAC(Integer nombreAnneesPostBAC) {
        this.nombreAnneesPostBAC = nombreAnneesPostBAC;
        return this;
    }

    public void setNombreAnneesPostBAC(Integer nombreAnneesPostBAC) {
        this.nombreAnneesPostBAC = nombreAnneesPostBAC;
    }

    public Instant getDateCreation() {
        return dateCreation;
    }

    public Aidant dateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(Instant dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Instant getDateModification() {
        return dateModification;
    }

    public Aidant dateModification(Instant dateModification) {
        this.dateModification = dateModification;
        return this;
    }

    public void setDateModification(Instant dateModification) {
        this.dateModification = dateModification;
    }

    public Instant getDateArchivage() {
        return dateArchivage;
    }

    public Aidant dateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
        return this;
    }

    public void setDateArchivage(Instant dateArchivage) {
        this.dateArchivage = dateArchivage;
    }

    public User getUtilisateur() {
        return utilisateur;
    }

    public Aidant utilisateur(User user) {
        this.utilisateur = user;
        return this;
    }

    public void setUtilisateur(User user) {
        this.utilisateur = user;
    }

    public Contact getContact() {
        return contact;
    }

    public Aidant contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Image getPhoto() {
        return photo;
    }

    public Aidant photo(Image image) {
        this.photo = image;
        return this;
    }

    public void setPhoto(Image image) {
        this.photo = image;
    }

    public Etablissement getEtablissement() {
        return etablissement;
    }

    public Aidant etablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
        return this;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Aidant)) {
            return false;
        }
        return id != null && id.equals(((Aidant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Aidant{" +
            "id=" + getId() +
            ", cursusActuel='" + getCursusActuel() + "'" +
            ", nombreAnneesPostBAC=" + getNombreAnneesPostBAC() +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateModification='" + getDateModification() + "'" +
            ", dateArchivage='" + getDateArchivage() + "'" +
            "}";
    }
}
