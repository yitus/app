package yitus.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Entité Image pour les photos des différents types d'utilisateurs et l'établissement
 */
@Entity
@Table(name = "image")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Contenu de l'image
     */
    
    @Lob
    @Column(name = "contenu", nullable = false)
    private byte[] contenu;

    @Column(name = "contenu_content_type", nullable = false)
    private String contenuContentType;

    @Size(min = 40, max = 40)
    @Pattern(regexp = "[a-fA-F0-9]{40}")
    @Column(name = "contenu_sha_1", length = 40)
    private String contenuSha1;

    /**
     * Vignette de l'image
     */
    
    @Lob
    @Column(name = "vignette", nullable = false)
    private byte[] vignette;

    @Column(name = "vignette_content_type", nullable = false)
    private String vignetteContentType;

    @Size(min = 40, max = 40)
    @Pattern(regexp = "[a-fA-F0-9]{40}")
    @Column(name = "vignette_sha_1", length = 40)
    private String vignetteSha1;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getContenu() {
        return contenu;
    }

    public Image contenu(byte[] contenu) {
        this.contenu = contenu;
        return this;
    }

    public void setContenu(byte[] contenu) {
        this.contenu = contenu;
    }

    public String getContenuContentType() {
        return contenuContentType;
    }

    public Image contenuContentType(String contenuContentType) {
        this.contenuContentType = contenuContentType;
        return this;
    }

    public void setContenuContentType(String contenuContentType) {
        this.contenuContentType = contenuContentType;
    }

    public String getContenuSha1() {
        return contenuSha1;
    }

    public Image contenuSha1(String contenuSha1) {
        this.contenuSha1 = contenuSha1;
        return this;
    }

    public void setContenuSha1(String contenuSha1) {
        this.contenuSha1 = contenuSha1;
    }

    public byte[] getVignette() {
        return vignette;
    }

    public Image vignette(byte[] vignette) {
        this.vignette = vignette;
        return this;
    }

    public void setVignette(byte[] vignette) {
        this.vignette = vignette;
    }

    public String getVignetteContentType() {
        return vignetteContentType;
    }

    public Image vignetteContentType(String vignetteContentType) {
        this.vignetteContentType = vignetteContentType;
        return this;
    }

    public void setVignetteContentType(String vignetteContentType) {
        this.vignetteContentType = vignetteContentType;
    }

    public String getVignetteSha1() {
        return vignetteSha1;
    }

    public Image vignetteSha1(String vignetteSha1) {
        this.vignetteSha1 = vignetteSha1;
        return this;
    }

    public void setVignetteSha1(String vignetteSha1) {
        this.vignetteSha1 = vignetteSha1;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Image)) {
            return false;
        }
        return id != null && id.equals(((Image) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
            ", contenu='" + getContenu() + "'" +
            ", contenuContentType='" + getContenuContentType() + "'" +
            ", contenuSha1='" + getContenuSha1() + "'" +
            ", vignette='" + getVignette() + "'" +
            ", vignetteContentType='" + getVignetteContentType() + "'" +
            ", vignetteSha1='" + getVignetteSha1() + "'" +
            "}";
    }
}
