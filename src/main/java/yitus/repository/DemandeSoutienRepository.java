package yitus.repository;

import yitus.domain.DemandeSoutien;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DemandeSoutien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DemandeSoutienRepository extends JpaRepository<DemandeSoutien, Long>, JpaSpecificationExecutor<DemandeSoutien> {
}
