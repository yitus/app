package yitus.repository;

import yitus.domain.Aidant;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Aidant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AidantRepository extends JpaRepository<Aidant, Long>, JpaSpecificationExecutor<Aidant> {
}
