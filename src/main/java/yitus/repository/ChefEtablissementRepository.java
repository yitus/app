package yitus.repository;

import yitus.domain.ChefEtablissement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ChefEtablissement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChefEtablissementRepository extends JpaRepository<ChefEtablissement, Long>, JpaSpecificationExecutor<ChefEtablissement> {
}
