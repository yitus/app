package yitus.repository;

import yitus.domain.Evenement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Evenement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvenementRepository extends JpaRepository<Evenement, Long>, JpaSpecificationExecutor<Evenement> {

    @Query("select evenement from Evenement evenement where evenement.creePar.login = ?#{principal.username}")
    List<Evenement> findByCreeParIsCurrentUser();
}
