import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IEtablissement, Etablissement } from 'app/shared/model/etablissement.model';
import { EtablissementService } from './etablissement.service';
import { IChefEtablissement } from 'app/shared/model/chef-etablissement.model';
import { ChefEtablissementService } from 'app/entities/chef-etablissement/chef-etablissement.service';
import { IImage } from 'app/shared/model/image.model';
import { ImageService } from 'app/entities/image/image.service';

type SelectableEntity = IChefEtablissement | IImage;

@Component({
  selector: 'jhi-etablissement-update',
  templateUrl: './etablissement-update.component.html'
})
export class EtablissementUpdateComponent implements OnInit {
  isSaving = false;
  chefs: IChefEtablissement[] = [];
  photos: IImage[] = [];

  editForm = this.fb.group({
    id: [],
    typeEtablissement: [null, [Validators.required]],
    nom: [null, [Validators.required]],
    siteweb: [null, [Validators.required]],
    adresse: [null, [Validators.required]],
    ville: [null, [Validators.required]],
    codePostal: [null, [Validators.required]],
    dateCreation: [],
    dateModification: [],
    dateArchivage: [],
    chefId: [],
    photoId: []
  });

  constructor(
    protected etablissementService: EtablissementService,
    protected chefEtablissementService: ChefEtablissementService,
    protected imageService: ImageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ etablissement }) => {
      if (!etablissement.id) {
        const today = moment().startOf('day');
        etablissement.dateCreation = today;
        etablissement.dateModification = today;
        etablissement.dateArchivage = today;
      }

      this.updateForm(etablissement);

      this.chefEtablissementService
        .query({ 'etablissementId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IChefEtablissement[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IChefEtablissement[]) => {
          if (!etablissement.chefId) {
            this.chefs = resBody;
          } else {
            this.chefEtablissementService
              .find(etablissement.chefId)
              .pipe(
                map((subRes: HttpResponse<IChefEtablissement>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IChefEtablissement[]) => (this.chefs = concatRes));
          }
        });

      this.imageService
        .query({ filter: 'etablissement-is-null' })
        .pipe(
          map((res: HttpResponse<IImage[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IImage[]) => {
          if (!etablissement.photoId) {
            this.photos = resBody;
          } else {
            this.imageService
              .find(etablissement.photoId)
              .pipe(
                map((subRes: HttpResponse<IImage>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IImage[]) => (this.photos = concatRes));
          }
        });
    });
  }

  updateForm(etablissement: IEtablissement): void {
    this.editForm.patchValue({
      id: etablissement.id,
      typeEtablissement: etablissement.typeEtablissement,
      nom: etablissement.nom,
      siteweb: etablissement.siteweb,
      adresse: etablissement.adresse,
      ville: etablissement.ville,
      codePostal: etablissement.codePostal,
      dateCreation: etablissement.dateCreation ? etablissement.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: etablissement.dateModification ? etablissement.dateModification.format(DATE_TIME_FORMAT) : null,
      dateArchivage: etablissement.dateArchivage ? etablissement.dateArchivage.format(DATE_TIME_FORMAT) : null,
      chefId: etablissement.chefId,
      photoId: etablissement.photoId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const etablissement = this.createFromForm();
    if (etablissement.id !== undefined) {
      this.subscribeToSaveResponse(this.etablissementService.update(etablissement));
    } else {
      this.subscribeToSaveResponse(this.etablissementService.create(etablissement));
    }
  }

  private createFromForm(): IEtablissement {
    return {
      ...new Etablissement(),
      id: this.editForm.get(['id'])!.value,
      typeEtablissement: this.editForm.get(['typeEtablissement'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      siteweb: this.editForm.get(['siteweb'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      ville: this.editForm.get(['ville'])!.value,
      codePostal: this.editForm.get(['codePostal'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? moment(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? moment(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateArchivage: this.editForm.get(['dateArchivage'])!.value
        ? moment(this.editForm.get(['dateArchivage'])!.value, DATE_TIME_FORMAT)
        : undefined,
      chefId: this.editForm.get(['chefId'])!.value,
      photoId: this.editForm.get(['photoId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEtablissement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
