import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEtablissement } from 'app/shared/model/etablissement.model';

type EntityResponseType = HttpResponse<IEtablissement>;
type EntityArrayResponseType = HttpResponse<IEtablissement[]>;

@Injectable({ providedIn: 'root' })
export class EtablissementService {
  public resourceUrl = SERVER_API_URL + 'api/etablissements';

  constructor(protected http: HttpClient) {}

  create(etablissement: IEtablissement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(etablissement);
    return this.http
      .post<IEtablissement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(etablissement: IEtablissement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(etablissement);
    return this.http
      .put<IEtablissement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEtablissement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEtablissement[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(etablissement: IEtablissement): IEtablissement {
    const copy: IEtablissement = Object.assign({}, etablissement, {
      dateCreation: etablissement.dateCreation && etablissement.dateCreation.isValid() ? etablissement.dateCreation.toJSON() : undefined,
      dateModification:
        etablissement.dateModification && etablissement.dateModification.isValid() ? etablissement.dateModification.toJSON() : undefined,
      dateArchivage: etablissement.dateArchivage && etablissement.dateArchivage.isValid() ? etablissement.dateArchivage.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? moment(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? moment(res.body.dateModification) : undefined;
      res.body.dateArchivage = res.body.dateArchivage ? moment(res.body.dateArchivage) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((etablissement: IEtablissement) => {
        etablissement.dateCreation = etablissement.dateCreation ? moment(etablissement.dateCreation) : undefined;
        etablissement.dateModification = etablissement.dateModification ? moment(etablissement.dateModification) : undefined;
        etablissement.dateArchivage = etablissement.dateArchivage ? moment(etablissement.dateArchivage) : undefined;
      });
    }
    return res;
  }
}
