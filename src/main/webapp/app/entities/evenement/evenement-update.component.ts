import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IEvenement, Evenement } from 'app/shared/model/evenement.model';
import { EvenementService } from './evenement.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-evenement-update',
  templateUrl: './evenement-update.component.html'
})
export class EvenementUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    statusEvenement: [null, [Validators.required]],
    dateEvenement: [null, [Validators.required]],
    operation: [null, [Validators.required]],
    entite: [null, [Validators.required]],
    idEntite: [],
    message: [null, [Validators.maxLength(4000)]],
    creeParId: []
  });

  constructor(
    protected evenementService: EvenementService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ evenement }) => {
      if (!evenement.id) {
        const today = moment().startOf('day');
        evenement.dateEvenement = today;
      }

      this.updateForm(evenement);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(evenement: IEvenement): void {
    this.editForm.patchValue({
      id: evenement.id,
      statusEvenement: evenement.statusEvenement,
      dateEvenement: evenement.dateEvenement ? evenement.dateEvenement.format(DATE_TIME_FORMAT) : null,
      operation: evenement.operation,
      entite: evenement.entite,
      idEntite: evenement.idEntite,
      message: evenement.message,
      creeParId: evenement.creeParId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const evenement = this.createFromForm();
    if (evenement.id !== undefined) {
      this.subscribeToSaveResponse(this.evenementService.update(evenement));
    } else {
      this.subscribeToSaveResponse(this.evenementService.create(evenement));
    }
  }

  private createFromForm(): IEvenement {
    return {
      ...new Evenement(),
      id: this.editForm.get(['id'])!.value,
      statusEvenement: this.editForm.get(['statusEvenement'])!.value,
      dateEvenement: this.editForm.get(['dateEvenement'])!.value
        ? moment(this.editForm.get(['dateEvenement'])!.value, DATE_TIME_FORMAT)
        : undefined,
      operation: this.editForm.get(['operation'])!.value,
      entite: this.editForm.get(['entite'])!.value,
      idEntite: this.editForm.get(['idEntite'])!.value,
      message: this.editForm.get(['message'])!.value,
      creeParId: this.editForm.get(['creeParId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEvenement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
