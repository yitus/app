import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IContact, Contact } from 'app/shared/model/contact.model';
import { ContactService } from './contact.service';
import { AlertError } from 'app/shared/alert/alert-error.model';

@Component({
  selector: 'jhi-contact-update',
  templateUrl: './contact-update.component.html'
})
export class ContactUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    badges: [null, [Validators.maxLength(2000), Validators.pattern('^(([a-z]+,)*[a-z]+)?$')]],
    ajoutContenuImage: [null, []],
    ajoutContenuImageContentType: [],
    login: [],
    nom: [],
    prenom: [],
    email: [null, [Validators.required]],
    telephone: [null, [Validators.minLength(10), Validators.maxLength(10), Validators.pattern('0[0-9]{9}')]],
    ville: [],
    gravatar: [],
    linkedin: [],
    google: [],
    facebook: [],
    skype: [],
    discord: [],
    zoom: [],
    whereby: [],
    mumble: [],
    tox: [],
    matrix: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected contactService: ContactService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ contact }) => {
      this.updateForm(contact);
    });
  }

  updateForm(contact: IContact): void {
    this.editForm.patchValue({
      id: contact.id,
      badges: contact.badges,
      ajoutContenuImage: contact.ajoutContenuImage,
      ajoutContenuImageContentType: contact.ajoutContenuImageContentType,
      login: contact.login,
      nom: contact.nom,
      prenom: contact.prenom,
      email: contact.email,
      telephone: contact.telephone,
      ville: contact.ville,
      gravatar: contact.gravatar,
      linkedin: contact.linkedin,
      google: contact.google,
      facebook: contact.facebook,
      skype: contact.skype,
      discord: contact.discord,
      zoom: contact.zoom,
      whereby: contact.whereby,
      mumble: contact.mumble,
      tox: contact.tox,
      matrix: contact.matrix
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('yitusApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const contact = this.createFromForm();
    if (contact.id !== undefined) {
      this.subscribeToSaveResponse(this.contactService.update(contact));
    } else {
      this.subscribeToSaveResponse(this.contactService.create(contact));
    }
  }

  private createFromForm(): IContact {
    return {
      ...new Contact(),
      id: this.editForm.get(['id'])!.value,
      badges: this.editForm.get(['badges'])!.value,
      ajoutContenuImageContentType: this.editForm.get(['ajoutContenuImageContentType'])!.value,
      ajoutContenuImage: this.editForm.get(['ajoutContenuImage'])!.value,
      login: this.editForm.get(['login'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      email: this.editForm.get(['email'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      ville: this.editForm.get(['ville'])!.value,
      gravatar: this.editForm.get(['gravatar'])!.value,
      linkedin: this.editForm.get(['linkedin'])!.value,
      google: this.editForm.get(['google'])!.value,
      facebook: this.editForm.get(['facebook'])!.value,
      skype: this.editForm.get(['skype'])!.value,
      discord: this.editForm.get(['discord'])!.value,
      zoom: this.editForm.get(['zoom'])!.value,
      whereby: this.editForm.get(['whereby'])!.value,
      mumble: this.editForm.get(['mumble'])!.value,
      tox: this.editForm.get(['tox'])!.value,
      matrix: this.editForm.get(['matrix'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContact>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
