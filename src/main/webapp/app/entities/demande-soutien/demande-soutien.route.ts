import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDemandeSoutien, DemandeSoutien } from 'app/shared/model/demande-soutien.model';
import { DemandeSoutienService } from './demande-soutien.service';
import { DemandeSoutienComponent } from './demande-soutien.component';
import { DemandeSoutienDetailComponent } from './demande-soutien-detail.component';
import { DemandeSoutienUpdateComponent } from './demande-soutien-update.component';

@Injectable({ providedIn: 'root' })
export class DemandeSoutienResolve implements Resolve<IDemandeSoutien> {
  constructor(private service: DemandeSoutienService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDemandeSoutien> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((demandeSoutien: HttpResponse<DemandeSoutien>) => {
          if (demandeSoutien.body) {
            return of(demandeSoutien.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DemandeSoutien());
  }
}

export const demandeSoutienRoute: Routes = [
  {
    path: '',
    component: DemandeSoutienComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'yitusApp.demandeSoutien.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DemandeSoutienDetailComponent,
    resolve: {
      demandeSoutien: DemandeSoutienResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.demandeSoutien.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DemandeSoutienUpdateComponent,
    resolve: {
      demandeSoutien: DemandeSoutienResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.demandeSoutien.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DemandeSoutienUpdateComponent,
    resolve: {
      demandeSoutien: DemandeSoutienResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.demandeSoutien.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
