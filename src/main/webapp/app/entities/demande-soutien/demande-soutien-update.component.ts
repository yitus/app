import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IDemandeSoutien, DemandeSoutien } from 'app/shared/model/demande-soutien.model';
import { DemandeSoutienService } from './demande-soutien.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IApprenant } from 'app/shared/model/apprenant.model';
import { ApprenantService } from 'app/entities/apprenant/apprenant.service';
import { IAidant } from 'app/shared/model/aidant.model';
import { AidantService } from 'app/entities/aidant/aidant.service';
import { IEnseignant } from 'app/shared/model/enseignant.model';
import { EnseignantService } from 'app/entities/enseignant/enseignant.service';
import { IReferent } from 'app/shared/model/referent.model';
import { ReferentService } from 'app/entities/referent/referent.service';

type SelectableEntity = IApprenant | IAidant | IEnseignant | IReferent;

@Component({
  selector: 'jhi-demande-soutien-update',
  templateUrl: './demande-soutien-update.component.html'
})
export class DemandeSoutienUpdateComponent implements OnInit {
  isSaving = false;
  apprenants: IApprenant[] = [];
  aidants: IAidant[] = [];
  enseignants: IEnseignant[] = [];
  referents: IReferent[] = [];

  editForm = this.fb.group({
    id: [],
    statut: [null, [Validators.required]],
    etiquettes: [null, [Validators.maxLength(2000), Validators.pattern('^(([a-z]+,)*[a-z]+)?$')]],
    niveau: [null, [Validators.required]],
    matiere: [null, [Validators.required]],
    detail: [null, [Validators.required]],
    horairesDisponibilites: [null, [Validators.required]],
    dateRendezVous: [],
    noteApprenant: [null, [Validators.min(1), Validators.max(10)]],
    noteAidant: [null, [Validators.min(1), Validators.max(10)]],
    commentaireApprenant: [],
    commentaireAidant: [],
    commentaireEnseignant: [],
    commentaireReferent: [],
    dateLimite: [null, [Validators.required]],
    dateCreation: [],
    dateModification: [],
    dateArchivage: [],
    apprenantId: [],
    aidantId: [],
    enseignantId: [],
    referentId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected demandeSoutienService: DemandeSoutienService,
    protected apprenantService: ApprenantService,
    protected aidantService: AidantService,
    protected enseignantService: EnseignantService,
    protected referentService: ReferentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demandeSoutien }) => {
      if (!demandeSoutien.id) {
        const today = moment().startOf('day');
        demandeSoutien.dateRendezVous = today;
        demandeSoutien.dateLimite = today;
        demandeSoutien.dateCreation = today;
        demandeSoutien.dateModification = today;
        demandeSoutien.dateArchivage = today;
      }

      this.updateForm(demandeSoutien);

      this.apprenantService.query().subscribe((res: HttpResponse<IApprenant[]>) => (this.apprenants = res.body || []));

      this.aidantService.query().subscribe((res: HttpResponse<IAidant[]>) => (this.aidants = res.body || []));

      this.enseignantService.query().subscribe((res: HttpResponse<IEnseignant[]>) => (this.enseignants = res.body || []));

      this.referentService.query().subscribe((res: HttpResponse<IReferent[]>) => (this.referents = res.body || []));
    });
  }

  updateForm(demandeSoutien: IDemandeSoutien): void {
    this.editForm.patchValue({
      id: demandeSoutien.id,
      statut: demandeSoutien.statut,
      etiquettes: demandeSoutien.etiquettes,
      niveau: demandeSoutien.niveau,
      matiere: demandeSoutien.matiere,
      detail: demandeSoutien.detail,
      horairesDisponibilites: demandeSoutien.horairesDisponibilites,
      dateRendezVous: demandeSoutien.dateRendezVous ? demandeSoutien.dateRendezVous.format(DATE_TIME_FORMAT) : null,
      noteApprenant: demandeSoutien.noteApprenant,
      noteAidant: demandeSoutien.noteAidant,
      commentaireApprenant: demandeSoutien.commentaireApprenant,
      commentaireAidant: demandeSoutien.commentaireAidant,
      commentaireEnseignant: demandeSoutien.commentaireEnseignant,
      commentaireReferent: demandeSoutien.commentaireReferent,
      dateLimite: demandeSoutien.dateLimite ? demandeSoutien.dateLimite.format(DATE_TIME_FORMAT) : null,
      dateCreation: demandeSoutien.dateCreation ? demandeSoutien.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: demandeSoutien.dateModification ? demandeSoutien.dateModification.format(DATE_TIME_FORMAT) : null,
      dateArchivage: demandeSoutien.dateArchivage ? demandeSoutien.dateArchivage.format(DATE_TIME_FORMAT) : null,
      apprenantId: demandeSoutien.apprenantId,
      aidantId: demandeSoutien.aidantId,
      enseignantId: demandeSoutien.enseignantId,
      referentId: demandeSoutien.referentId
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('yitusApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const demandeSoutien = this.createFromForm();
    if (demandeSoutien.id !== undefined) {
      this.subscribeToSaveResponse(this.demandeSoutienService.update(demandeSoutien));
    } else {
      this.subscribeToSaveResponse(this.demandeSoutienService.create(demandeSoutien));
    }
  }

  private createFromForm(): IDemandeSoutien {
    return {
      ...new DemandeSoutien(),
      id: this.editForm.get(['id'])!.value,
      statut: this.editForm.get(['statut'])!.value,
      etiquettes: this.editForm.get(['etiquettes'])!.value,
      niveau: this.editForm.get(['niveau'])!.value,
      matiere: this.editForm.get(['matiere'])!.value,
      detail: this.editForm.get(['detail'])!.value,
      horairesDisponibilites: this.editForm.get(['horairesDisponibilites'])!.value,
      dateRendezVous: this.editForm.get(['dateRendezVous'])!.value
        ? moment(this.editForm.get(['dateRendezVous'])!.value, DATE_TIME_FORMAT)
        : undefined,
      noteApprenant: this.editForm.get(['noteApprenant'])!.value,
      noteAidant: this.editForm.get(['noteAidant'])!.value,
      commentaireApprenant: this.editForm.get(['commentaireApprenant'])!.value,
      commentaireAidant: this.editForm.get(['commentaireAidant'])!.value,
      commentaireEnseignant: this.editForm.get(['commentaireEnseignant'])!.value,
      commentaireReferent: this.editForm.get(['commentaireReferent'])!.value,
      dateLimite: this.editForm.get(['dateLimite'])!.value ? moment(this.editForm.get(['dateLimite'])!.value, DATE_TIME_FORMAT) : undefined,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? moment(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? moment(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateArchivage: this.editForm.get(['dateArchivage'])!.value
        ? moment(this.editForm.get(['dateArchivage'])!.value, DATE_TIME_FORMAT)
        : undefined,
      apprenantId: this.editForm.get(['apprenantId'])!.value,
      aidantId: this.editForm.get(['aidantId'])!.value,
      enseignantId: this.editForm.get(['enseignantId'])!.value,
      referentId: this.editForm.get(['referentId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDemandeSoutien>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
