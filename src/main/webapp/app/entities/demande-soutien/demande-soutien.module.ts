import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { YitusSharedModule } from 'app/shared/shared.module';
import { DemandeSoutienComponent } from './demande-soutien.component';
import { DemandeSoutienDetailComponent } from './demande-soutien-detail.component';
import { DemandeSoutienUpdateComponent } from './demande-soutien-update.component';
import { DemandeSoutienDeleteDialogComponent } from './demande-soutien-delete-dialog.component';
import { demandeSoutienRoute } from './demande-soutien.route';

@NgModule({
  imports: [YitusSharedModule, RouterModule.forChild(demandeSoutienRoute)],
  declarations: [
    DemandeSoutienComponent,
    DemandeSoutienDetailComponent,
    DemandeSoutienUpdateComponent,
    DemandeSoutienDeleteDialogComponent
  ],
  entryComponents: [DemandeSoutienDeleteDialogComponent]
})
export class YitusDemandeSoutienModule {}
