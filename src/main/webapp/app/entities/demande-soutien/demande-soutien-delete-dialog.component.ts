import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDemandeSoutien } from 'app/shared/model/demande-soutien.model';
import { DemandeSoutienService } from './demande-soutien.service';

@Component({
  templateUrl: './demande-soutien-delete-dialog.component.html'
})
export class DemandeSoutienDeleteDialogComponent {
  demandeSoutien?: IDemandeSoutien;

  constructor(
    protected demandeSoutienService: DemandeSoutienService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.demandeSoutienService.delete(id).subscribe(() => {
      this.eventManager.broadcast('demandeSoutienListModification');
      this.activeModal.close();
    });
  }
}
