import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IDemandeSoutien } from 'app/shared/model/demande-soutien.model';

@Component({
  selector: 'jhi-demande-soutien-detail',
  templateUrl: './demande-soutien-detail.component.html'
})
export class DemandeSoutienDetailComponent implements OnInit {
  demandeSoutien: IDemandeSoutien | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demandeSoutien }) => (this.demandeSoutien = demandeSoutien));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
