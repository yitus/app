import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDemandeSoutien } from 'app/shared/model/demande-soutien.model';

type EntityResponseType = HttpResponse<IDemandeSoutien>;
type EntityArrayResponseType = HttpResponse<IDemandeSoutien[]>;

@Injectable({ providedIn: 'root' })
export class DemandeSoutienService {
  public resourceUrl = SERVER_API_URL + 'api/demande-soutiens';

  constructor(protected http: HttpClient) {}

  create(demandeSoutien: IDemandeSoutien): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeSoutien);
    return this.http
      .post<IDemandeSoutien>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(demandeSoutien: IDemandeSoutien): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeSoutien);
    return this.http
      .put<IDemandeSoutien>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDemandeSoutien>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDemandeSoutien[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(demandeSoutien: IDemandeSoutien): IDemandeSoutien {
    const copy: IDemandeSoutien = Object.assign({}, demandeSoutien, {
      dateRendezVous:
        demandeSoutien.dateRendezVous && demandeSoutien.dateRendezVous.isValid() ? demandeSoutien.dateRendezVous.toJSON() : undefined,
      dateLimite: demandeSoutien.dateLimite && demandeSoutien.dateLimite.isValid() ? demandeSoutien.dateLimite.toJSON() : undefined,
      dateCreation: demandeSoutien.dateCreation && demandeSoutien.dateCreation.isValid() ? demandeSoutien.dateCreation.toJSON() : undefined,
      dateModification:
        demandeSoutien.dateModification && demandeSoutien.dateModification.isValid() ? demandeSoutien.dateModification.toJSON() : undefined,
      dateArchivage:
        demandeSoutien.dateArchivage && demandeSoutien.dateArchivage.isValid() ? demandeSoutien.dateArchivage.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateRendezVous = res.body.dateRendezVous ? moment(res.body.dateRendezVous) : undefined;
      res.body.dateLimite = res.body.dateLimite ? moment(res.body.dateLimite) : undefined;
      res.body.dateCreation = res.body.dateCreation ? moment(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? moment(res.body.dateModification) : undefined;
      res.body.dateArchivage = res.body.dateArchivage ? moment(res.body.dateArchivage) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((demandeSoutien: IDemandeSoutien) => {
        demandeSoutien.dateRendezVous = demandeSoutien.dateRendezVous ? moment(demandeSoutien.dateRendezVous) : undefined;
        demandeSoutien.dateLimite = demandeSoutien.dateLimite ? moment(demandeSoutien.dateLimite) : undefined;
        demandeSoutien.dateCreation = demandeSoutien.dateCreation ? moment(demandeSoutien.dateCreation) : undefined;
        demandeSoutien.dateModification = demandeSoutien.dateModification ? moment(demandeSoutien.dateModification) : undefined;
        demandeSoutien.dateArchivage = demandeSoutien.dateArchivage ? moment(demandeSoutien.dateArchivage) : undefined;
      });
    }
    return res;
  }
}
