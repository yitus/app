import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'image',
        loadChildren: () => import('./image/image.module').then(m => m.YitusImageModule)
      },
      {
        path: 'contact',
        loadChildren: () => import('./contact/contact.module').then(m => m.YitusContactModule)
      },
      {
        path: 'etablissement',
        loadChildren: () => import('./etablissement/etablissement.module').then(m => m.YitusEtablissementModule)
      },
      {
        path: 'apprenant',
        loadChildren: () => import('./apprenant/apprenant.module').then(m => m.YitusApprenantModule)
      },
      {
        path: 'aidant',
        loadChildren: () => import('./aidant/aidant.module').then(m => m.YitusAidantModule)
      },
      {
        path: 'enseignant',
        loadChildren: () => import('./enseignant/enseignant.module').then(m => m.YitusEnseignantModule)
      },
      {
        path: 'referent',
        loadChildren: () => import('./referent/referent.module').then(m => m.YitusReferentModule)
      },
      {
        path: 'chef-etablissement',
        loadChildren: () => import('./chef-etablissement/chef-etablissement.module').then(m => m.YitusChefEtablissementModule)
      },
      {
        path: 'evenement',
        loadChildren: () => import('./evenement/evenement.module').then(m => m.YitusEvenementModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('./notification/notification.module').then(m => m.YitusNotificationModule)
      },
      {
        path: 'demande-soutien',
        loadChildren: () => import('./demande-soutien/demande-soutien.module').then(m => m.YitusDemandeSoutienModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class YitusEntityModule {}
