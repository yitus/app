import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IReferent } from 'app/shared/model/referent.model';

type EntityResponseType = HttpResponse<IReferent>;
type EntityArrayResponseType = HttpResponse<IReferent[]>;

@Injectable({ providedIn: 'root' })
export class ReferentService {
  public resourceUrl = SERVER_API_URL + 'api/referents';

  constructor(protected http: HttpClient) {}

  create(referent: IReferent): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(referent);
    return this.http
      .post<IReferent>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(referent: IReferent): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(referent);
    return this.http
      .put<IReferent>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IReferent>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IReferent[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(referent: IReferent): IReferent {
    const copy: IReferent = Object.assign({}, referent, {
      dateCreation: referent.dateCreation && referent.dateCreation.isValid() ? referent.dateCreation.toJSON() : undefined,
      dateModification: referent.dateModification && referent.dateModification.isValid() ? referent.dateModification.toJSON() : undefined,
      dateArchivage: referent.dateArchivage && referent.dateArchivage.isValid() ? referent.dateArchivage.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? moment(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? moment(res.body.dateModification) : undefined;
      res.body.dateArchivage = res.body.dateArchivage ? moment(res.body.dateArchivage) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((referent: IReferent) => {
        referent.dateCreation = referent.dateCreation ? moment(referent.dateCreation) : undefined;
        referent.dateModification = referent.dateModification ? moment(referent.dateModification) : undefined;
        referent.dateArchivage = referent.dateArchivage ? moment(referent.dateArchivage) : undefined;
      });
    }
    return res;
  }
}
