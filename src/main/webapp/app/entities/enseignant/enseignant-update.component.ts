import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IEnseignant, Enseignant } from 'app/shared/model/enseignant.model';
import { EnseignantService } from './enseignant.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IContact } from 'app/shared/model/contact.model';
import { ContactService } from 'app/entities/contact/contact.service';
import { IImage } from 'app/shared/model/image.model';
import { ImageService } from 'app/entities/image/image.service';
import { IEtablissement } from 'app/shared/model/etablissement.model';
import { EtablissementService } from 'app/entities/etablissement/etablissement.service';

type SelectableEntity = IUser | IContact | IImage | IEtablissement;

@Component({
  selector: 'jhi-enseignant-update',
  templateUrl: './enseignant-update.component.html'
})
export class EnseignantUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  contacts: IContact[] = [];
  photos: IImage[] = [];
  etablissements: IEtablissement[] = [];

  editForm = this.fb.group({
    id: [],
    matiere1: [null, [Validators.required]],
    matiere2: [],
    matiere3: [],
    matiere4: [],
    dateCreation: [],
    dateModification: [],
    dateArchivage: [],
    utilisateurId: [],
    contactId: [],
    photoId: [],
    etablissementId: []
  });

  constructor(
    protected enseignantService: EnseignantService,
    protected userService: UserService,
    protected contactService: ContactService,
    protected imageService: ImageService,
    protected etablissementService: EtablissementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ enseignant }) => {
      if (!enseignant.id) {
        const today = moment().startOf('day');
        enseignant.dateCreation = today;
        enseignant.dateModification = today;
        enseignant.dateArchivage = today;
      }

      this.updateForm(enseignant);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.contactService
        .query({ 'enseignantId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IContact[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IContact[]) => {
          if (!enseignant.contactId) {
            this.contacts = resBody;
          } else {
            this.contactService
              .find(enseignant.contactId)
              .pipe(
                map((subRes: HttpResponse<IContact>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IContact[]) => (this.contacts = concatRes));
          }
        });

      this.imageService
        .query({ filter: 'enseignant-is-null' })
        .pipe(
          map((res: HttpResponse<IImage[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IImage[]) => {
          if (!enseignant.photoId) {
            this.photos = resBody;
          } else {
            this.imageService
              .find(enseignant.photoId)
              .pipe(
                map((subRes: HttpResponse<IImage>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IImage[]) => (this.photos = concatRes));
          }
        });

      this.etablissementService.query().subscribe((res: HttpResponse<IEtablissement[]>) => (this.etablissements = res.body || []));
    });
  }

  updateForm(enseignant: IEnseignant): void {
    this.editForm.patchValue({
      id: enseignant.id,
      matiere1: enseignant.matiere1,
      matiere2: enseignant.matiere2,
      matiere3: enseignant.matiere3,
      matiere4: enseignant.matiere4,
      dateCreation: enseignant.dateCreation ? enseignant.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: enseignant.dateModification ? enseignant.dateModification.format(DATE_TIME_FORMAT) : null,
      dateArchivage: enseignant.dateArchivage ? enseignant.dateArchivage.format(DATE_TIME_FORMAT) : null,
      utilisateurId: enseignant.utilisateurId,
      contactId: enseignant.contactId,
      photoId: enseignant.photoId,
      etablissementId: enseignant.etablissementId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const enseignant = this.createFromForm();
    if (enseignant.id !== undefined) {
      this.subscribeToSaveResponse(this.enseignantService.update(enseignant));
    } else {
      this.subscribeToSaveResponse(this.enseignantService.create(enseignant));
    }
  }

  private createFromForm(): IEnseignant {
    return {
      ...new Enseignant(),
      id: this.editForm.get(['id'])!.value,
      matiere1: this.editForm.get(['matiere1'])!.value,
      matiere2: this.editForm.get(['matiere2'])!.value,
      matiere3: this.editForm.get(['matiere3'])!.value,
      matiere4: this.editForm.get(['matiere4'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? moment(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? moment(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateArchivage: this.editForm.get(['dateArchivage'])!.value
        ? moment(this.editForm.get(['dateArchivage'])!.value, DATE_TIME_FORMAT)
        : undefined,
      utilisateurId: this.editForm.get(['utilisateurId'])!.value,
      contactId: this.editForm.get(['contactId'])!.value,
      photoId: this.editForm.get(['photoId'])!.value,
      etablissementId: this.editForm.get(['etablissementId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEnseignant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
