import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { YitusSharedModule } from 'app/shared/shared.module';
import { ChefEtablissementComponent } from './chef-etablissement.component';
import { ChefEtablissementDetailComponent } from './chef-etablissement-detail.component';
import { ChefEtablissementUpdateComponent } from './chef-etablissement-update.component';
import { ChefEtablissementDeleteDialogComponent } from './chef-etablissement-delete-dialog.component';
import { chefEtablissementRoute } from './chef-etablissement.route';

@NgModule({
  imports: [YitusSharedModule, RouterModule.forChild(chefEtablissementRoute)],
  declarations: [
    ChefEtablissementComponent,
    ChefEtablissementDetailComponent,
    ChefEtablissementUpdateComponent,
    ChefEtablissementDeleteDialogComponent
  ],
  entryComponents: [ChefEtablissementDeleteDialogComponent]
})
export class YitusChefEtablissementModule {}
