import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IChefEtablissement, ChefEtablissement } from 'app/shared/model/chef-etablissement.model';
import { ChefEtablissementService } from './chef-etablissement.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IImage } from 'app/shared/model/image.model';
import { ImageService } from 'app/entities/image/image.service';

type SelectableEntity = IUser | IImage;

@Component({
  selector: 'jhi-chef-etablissement-update',
  templateUrl: './chef-etablissement-update.component.html'
})
export class ChefEtablissementUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  photos: IImage[] = [];

  editForm = this.fb.group({
    id: [],
    dateCreation: [],
    dateModification: [],
    dateArchivage: [],
    utilisateurId: [],
    photoId: []
  });

  constructor(
    protected chefEtablissementService: ChefEtablissementService,
    protected userService: UserService,
    protected imageService: ImageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chefEtablissement }) => {
      if (!chefEtablissement.id) {
        const today = moment().startOf('day');
        chefEtablissement.dateCreation = today;
        chefEtablissement.dateModification = today;
        chefEtablissement.dateArchivage = today;
      }

      this.updateForm(chefEtablissement);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.imageService
        .query({ filter: 'chefetablissement-is-null' })
        .pipe(
          map((res: HttpResponse<IImage[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IImage[]) => {
          if (!chefEtablissement.photoId) {
            this.photos = resBody;
          } else {
            this.imageService
              .find(chefEtablissement.photoId)
              .pipe(
                map((subRes: HttpResponse<IImage>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IImage[]) => (this.photos = concatRes));
          }
        });
    });
  }

  updateForm(chefEtablissement: IChefEtablissement): void {
    this.editForm.patchValue({
      id: chefEtablissement.id,
      dateCreation: chefEtablissement.dateCreation ? chefEtablissement.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: chefEtablissement.dateModification ? chefEtablissement.dateModification.format(DATE_TIME_FORMAT) : null,
      dateArchivage: chefEtablissement.dateArchivage ? chefEtablissement.dateArchivage.format(DATE_TIME_FORMAT) : null,
      utilisateurId: chefEtablissement.utilisateurId,
      photoId: chefEtablissement.photoId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const chefEtablissement = this.createFromForm();
    if (chefEtablissement.id !== undefined) {
      this.subscribeToSaveResponse(this.chefEtablissementService.update(chefEtablissement));
    } else {
      this.subscribeToSaveResponse(this.chefEtablissementService.create(chefEtablissement));
    }
  }

  private createFromForm(): IChefEtablissement {
    return {
      ...new ChefEtablissement(),
      id: this.editForm.get(['id'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? moment(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? moment(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateArchivage: this.editForm.get(['dateArchivage'])!.value
        ? moment(this.editForm.get(['dateArchivage'])!.value, DATE_TIME_FORMAT)
        : undefined,
      utilisateurId: this.editForm.get(['utilisateurId'])!.value,
      photoId: this.editForm.get(['photoId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChefEtablissement>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
