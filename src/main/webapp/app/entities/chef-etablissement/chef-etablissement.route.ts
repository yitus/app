import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IChefEtablissement, ChefEtablissement } from 'app/shared/model/chef-etablissement.model';
import { ChefEtablissementService } from './chef-etablissement.service';
import { ChefEtablissementComponent } from './chef-etablissement.component';
import { ChefEtablissementDetailComponent } from './chef-etablissement-detail.component';
import { ChefEtablissementUpdateComponent } from './chef-etablissement-update.component';

@Injectable({ providedIn: 'root' })
export class ChefEtablissementResolve implements Resolve<IChefEtablissement> {
  constructor(private service: ChefEtablissementService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChefEtablissement> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((chefEtablissement: HttpResponse<ChefEtablissement>) => {
          if (chefEtablissement.body) {
            return of(chefEtablissement.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChefEtablissement());
  }
}

export const chefEtablissementRoute: Routes = [
  {
    path: '',
    component: ChefEtablissementComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'yitusApp.chefEtablissement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ChefEtablissementDetailComponent,
    resolve: {
      chefEtablissement: ChefEtablissementResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.chefEtablissement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ChefEtablissementUpdateComponent,
    resolve: {
      chefEtablissement: ChefEtablissementResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.chefEtablissement.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ChefEtablissementUpdateComponent,
    resolve: {
      chefEtablissement: ChefEtablissementResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.chefEtablissement.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
