import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IChefEtablissement } from 'app/shared/model/chef-etablissement.model';

type EntityResponseType = HttpResponse<IChefEtablissement>;
type EntityArrayResponseType = HttpResponse<IChefEtablissement[]>;

@Injectable({ providedIn: 'root' })
export class ChefEtablissementService {
  public resourceUrl = SERVER_API_URL + 'api/chef-etablissements';

  constructor(protected http: HttpClient) {}

  create(chefEtablissement: IChefEtablissement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chefEtablissement);
    return this.http
      .post<IChefEtablissement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(chefEtablissement: IChefEtablissement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chefEtablissement);
    return this.http
      .put<IChefEtablissement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IChefEtablissement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IChefEtablissement[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(chefEtablissement: IChefEtablissement): IChefEtablissement {
    const copy: IChefEtablissement = Object.assign({}, chefEtablissement, {
      dateCreation:
        chefEtablissement.dateCreation && chefEtablissement.dateCreation.isValid() ? chefEtablissement.dateCreation.toJSON() : undefined,
      dateModification:
        chefEtablissement.dateModification && chefEtablissement.dateModification.isValid()
          ? chefEtablissement.dateModification.toJSON()
          : undefined,
      dateArchivage:
        chefEtablissement.dateArchivage && chefEtablissement.dateArchivage.isValid() ? chefEtablissement.dateArchivage.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? moment(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? moment(res.body.dateModification) : undefined;
      res.body.dateArchivage = res.body.dateArchivage ? moment(res.body.dateArchivage) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((chefEtablissement: IChefEtablissement) => {
        chefEtablissement.dateCreation = chefEtablissement.dateCreation ? moment(chefEtablissement.dateCreation) : undefined;
        chefEtablissement.dateModification = chefEtablissement.dateModification ? moment(chefEtablissement.dateModification) : undefined;
        chefEtablissement.dateArchivage = chefEtablissement.dateArchivage ? moment(chefEtablissement.dateArchivage) : undefined;
      });
    }
    return res;
  }
}
