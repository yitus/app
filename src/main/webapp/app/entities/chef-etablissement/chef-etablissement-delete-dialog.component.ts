import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IChefEtablissement } from 'app/shared/model/chef-etablissement.model';
import { ChefEtablissementService } from './chef-etablissement.service';

@Component({
  templateUrl: './chef-etablissement-delete-dialog.component.html'
})
export class ChefEtablissementDeleteDialogComponent {
  chefEtablissement?: IChefEtablissement;

  constructor(
    protected chefEtablissementService: ChefEtablissementService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.chefEtablissementService.delete(id).subscribe(() => {
      this.eventManager.broadcast('chefEtablissementListModification');
      this.activeModal.close();
    });
  }
}
