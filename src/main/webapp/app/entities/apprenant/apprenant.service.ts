import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IApprenant } from 'app/shared/model/apprenant.model';

type EntityResponseType = HttpResponse<IApprenant>;
type EntityArrayResponseType = HttpResponse<IApprenant[]>;

@Injectable({ providedIn: 'root' })
export class ApprenantService {
  public resourceUrl = SERVER_API_URL + 'api/apprenants';

  constructor(protected http: HttpClient) {}

  create(apprenant: IApprenant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(apprenant);
    return this.http
      .post<IApprenant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(apprenant: IApprenant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(apprenant);
    return this.http
      .put<IApprenant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IApprenant>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApprenant[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(apprenant: IApprenant): IApprenant {
    const copy: IApprenant = Object.assign({}, apprenant, {
      dateCreation: apprenant.dateCreation && apprenant.dateCreation.isValid() ? apprenant.dateCreation.toJSON() : undefined,
      dateModification:
        apprenant.dateModification && apprenant.dateModification.isValid() ? apprenant.dateModification.toJSON() : undefined,
      dateArchivage: apprenant.dateArchivage && apprenant.dateArchivage.isValid() ? apprenant.dateArchivage.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? moment(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? moment(res.body.dateModification) : undefined;
      res.body.dateArchivage = res.body.dateArchivage ? moment(res.body.dateArchivage) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((apprenant: IApprenant) => {
        apprenant.dateCreation = apprenant.dateCreation ? moment(apprenant.dateCreation) : undefined;
        apprenant.dateModification = apprenant.dateModification ? moment(apprenant.dateModification) : undefined;
        apprenant.dateArchivage = apprenant.dateArchivage ? moment(apprenant.dateArchivage) : undefined;
      });
    }
    return res;
  }
}
