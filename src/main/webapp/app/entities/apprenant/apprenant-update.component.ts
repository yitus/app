import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IApprenant, Apprenant } from 'app/shared/model/apprenant.model';
import { ApprenantService } from './apprenant.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IContact } from 'app/shared/model/contact.model';
import { ContactService } from 'app/entities/contact/contact.service';
import { IImage } from 'app/shared/model/image.model';
import { ImageService } from 'app/entities/image/image.service';
import { IEtablissement } from 'app/shared/model/etablissement.model';
import { EtablissementService } from 'app/entities/etablissement/etablissement.service';

type SelectableEntity = IUser | IContact | IImage | IEtablissement;

@Component({
  selector: 'jhi-apprenant-update',
  templateUrl: './apprenant-update.component.html'
})
export class ApprenantUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  contacts: IContact[] = [];
  photos: IImage[] = [];
  etablissements: IEtablissement[] = [];

  editForm = this.fb.group({
    id: [],
    niveau: [null, [Validators.required]],
    dateCreation: [],
    dateModification: [],
    dateArchivage: [],
    utilisateurId: [],
    contactId: [],
    photoId: [],
    etablissementId: []
  });

  constructor(
    protected apprenantService: ApprenantService,
    protected userService: UserService,
    protected contactService: ContactService,
    protected imageService: ImageService,
    protected etablissementService: EtablissementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ apprenant }) => {
      if (!apprenant.id) {
        const today = moment().startOf('day');
        apprenant.dateCreation = today;
        apprenant.dateModification = today;
        apprenant.dateArchivage = today;
      }

      this.updateForm(apprenant);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.contactService
        .query({ 'apprenantId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IContact[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IContact[]) => {
          if (!apprenant.contactId) {
            this.contacts = resBody;
          } else {
            this.contactService
              .find(apprenant.contactId)
              .pipe(
                map((subRes: HttpResponse<IContact>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IContact[]) => (this.contacts = concatRes));
          }
        });

      this.imageService
        .query({ filter: 'apprenant-is-null' })
        .pipe(
          map((res: HttpResponse<IImage[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IImage[]) => {
          if (!apprenant.photoId) {
            this.photos = resBody;
          } else {
            this.imageService
              .find(apprenant.photoId)
              .pipe(
                map((subRes: HttpResponse<IImage>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IImage[]) => (this.photos = concatRes));
          }
        });

      this.etablissementService.query().subscribe((res: HttpResponse<IEtablissement[]>) => (this.etablissements = res.body || []));
    });
  }

  updateForm(apprenant: IApprenant): void {
    this.editForm.patchValue({
      id: apprenant.id,
      niveau: apprenant.niveau,
      dateCreation: apprenant.dateCreation ? apprenant.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: apprenant.dateModification ? apprenant.dateModification.format(DATE_TIME_FORMAT) : null,
      dateArchivage: apprenant.dateArchivage ? apprenant.dateArchivage.format(DATE_TIME_FORMAT) : null,
      utilisateurId: apprenant.utilisateurId,
      contactId: apprenant.contactId,
      photoId: apprenant.photoId,
      etablissementId: apprenant.etablissementId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const apprenant = this.createFromForm();
    if (apprenant.id !== undefined) {
      this.subscribeToSaveResponse(this.apprenantService.update(apprenant));
    } else {
      this.subscribeToSaveResponse(this.apprenantService.create(apprenant));
    }
  }

  private createFromForm(): IApprenant {
    return {
      ...new Apprenant(),
      id: this.editForm.get(['id'])!.value,
      niveau: this.editForm.get(['niveau'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? moment(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? moment(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateArchivage: this.editForm.get(['dateArchivage'])!.value
        ? moment(this.editForm.get(['dateArchivage'])!.value, DATE_TIME_FORMAT)
        : undefined,
      utilisateurId: this.editForm.get(['utilisateurId'])!.value,
      contactId: this.editForm.get(['contactId'])!.value,
      photoId: this.editForm.get(['photoId'])!.value,
      etablissementId: this.editForm.get(['etablissementId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApprenant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
