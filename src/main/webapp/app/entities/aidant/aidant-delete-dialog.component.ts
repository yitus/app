import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAidant } from 'app/shared/model/aidant.model';
import { AidantService } from './aidant.service';

@Component({
  templateUrl: './aidant-delete-dialog.component.html'
})
export class AidantDeleteDialogComponent {
  aidant?: IAidant;

  constructor(protected aidantService: AidantService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.aidantService.delete(id).subscribe(() => {
      this.eventManager.broadcast('aidantListModification');
      this.activeModal.close();
    });
  }
}
