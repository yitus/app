import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAidant } from 'app/shared/model/aidant.model';

@Component({
  selector: 'jhi-aidant-detail',
  templateUrl: './aidant-detail.component.html'
})
export class AidantDetailComponent implements OnInit {
  aidant: IAidant | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ aidant }) => (this.aidant = aidant));
  }

  previousState(): void {
    window.history.back();
  }
}
