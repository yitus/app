import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAidant } from 'app/shared/model/aidant.model';

type EntityResponseType = HttpResponse<IAidant>;
type EntityArrayResponseType = HttpResponse<IAidant[]>;

@Injectable({ providedIn: 'root' })
export class AidantService {
  public resourceUrl = SERVER_API_URL + 'api/aidants';

  constructor(protected http: HttpClient) {}

  create(aidant: IAidant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(aidant);
    return this.http
      .post<IAidant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(aidant: IAidant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(aidant);
    return this.http
      .put<IAidant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAidant>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAidant[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(aidant: IAidant): IAidant {
    const copy: IAidant = Object.assign({}, aidant, {
      dateCreation: aidant.dateCreation && aidant.dateCreation.isValid() ? aidant.dateCreation.toJSON() : undefined,
      dateModification: aidant.dateModification && aidant.dateModification.isValid() ? aidant.dateModification.toJSON() : undefined,
      dateArchivage: aidant.dateArchivage && aidant.dateArchivage.isValid() ? aidant.dateArchivage.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreation = res.body.dateCreation ? moment(res.body.dateCreation) : undefined;
      res.body.dateModification = res.body.dateModification ? moment(res.body.dateModification) : undefined;
      res.body.dateArchivage = res.body.dateArchivage ? moment(res.body.dateArchivage) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((aidant: IAidant) => {
        aidant.dateCreation = aidant.dateCreation ? moment(aidant.dateCreation) : undefined;
        aidant.dateModification = aidant.dateModification ? moment(aidant.dateModification) : undefined;
        aidant.dateArchivage = aidant.dateArchivage ? moment(aidant.dateArchivage) : undefined;
      });
    }
    return res;
  }
}
