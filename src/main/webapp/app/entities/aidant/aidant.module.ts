import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { YitusSharedModule } from 'app/shared/shared.module';
import { AidantComponent } from './aidant.component';
import { AidantDetailComponent } from './aidant-detail.component';
import { AidantUpdateComponent } from './aidant-update.component';
import { AidantDeleteDialogComponent } from './aidant-delete-dialog.component';
import { aidantRoute } from './aidant.route';

@NgModule({
  imports: [YitusSharedModule, RouterModule.forChild(aidantRoute)],
  declarations: [AidantComponent, AidantDetailComponent, AidantUpdateComponent, AidantDeleteDialogComponent],
  entryComponents: [AidantDeleteDialogComponent]
})
export class YitusAidantModule {}
