import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAidant, Aidant } from 'app/shared/model/aidant.model';
import { AidantService } from './aidant.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IContact } from 'app/shared/model/contact.model';
import { ContactService } from 'app/entities/contact/contact.service';
import { IImage } from 'app/shared/model/image.model';
import { ImageService } from 'app/entities/image/image.service';
import { IEtablissement } from 'app/shared/model/etablissement.model';
import { EtablissementService } from 'app/entities/etablissement/etablissement.service';

type SelectableEntity = IUser | IContact | IImage | IEtablissement;

@Component({
  selector: 'jhi-aidant-update',
  templateUrl: './aidant-update.component.html'
})
export class AidantUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  contacts: IContact[] = [];
  photos: IImage[] = [];
  etablissements: IEtablissement[] = [];

  editForm = this.fb.group({
    id: [],
    cursusActuel: [null, [Validators.required]],
    nombreAnneesPostBAC: [null, [Validators.min(1), Validators.max(8)]],
    dateCreation: [],
    dateModification: [],
    dateArchivage: [],
    utilisateurId: [],
    contactId: [],
    photoId: [],
    etablissementId: []
  });

  constructor(
    protected aidantService: AidantService,
    protected userService: UserService,
    protected contactService: ContactService,
    protected imageService: ImageService,
    protected etablissementService: EtablissementService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ aidant }) => {
      if (!aidant.id) {
        const today = moment().startOf('day');
        aidant.dateCreation = today;
        aidant.dateModification = today;
        aidant.dateArchivage = today;
      }

      this.updateForm(aidant);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.contactService
        .query({ 'aidantId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IContact[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IContact[]) => {
          if (!aidant.contactId) {
            this.contacts = resBody;
          } else {
            this.contactService
              .find(aidant.contactId)
              .pipe(
                map((subRes: HttpResponse<IContact>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IContact[]) => (this.contacts = concatRes));
          }
        });

      this.imageService
        .query({ filter: 'aidant-is-null' })
        .pipe(
          map((res: HttpResponse<IImage[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IImage[]) => {
          if (!aidant.photoId) {
            this.photos = resBody;
          } else {
            this.imageService
              .find(aidant.photoId)
              .pipe(
                map((subRes: HttpResponse<IImage>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IImage[]) => (this.photos = concatRes));
          }
        });

      this.etablissementService.query().subscribe((res: HttpResponse<IEtablissement[]>) => (this.etablissements = res.body || []));
    });
  }

  updateForm(aidant: IAidant): void {
    this.editForm.patchValue({
      id: aidant.id,
      cursusActuel: aidant.cursusActuel,
      nombreAnneesPostBAC: aidant.nombreAnneesPostBAC,
      dateCreation: aidant.dateCreation ? aidant.dateCreation.format(DATE_TIME_FORMAT) : null,
      dateModification: aidant.dateModification ? aidant.dateModification.format(DATE_TIME_FORMAT) : null,
      dateArchivage: aidant.dateArchivage ? aidant.dateArchivage.format(DATE_TIME_FORMAT) : null,
      utilisateurId: aidant.utilisateurId,
      contactId: aidant.contactId,
      photoId: aidant.photoId,
      etablissementId: aidant.etablissementId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const aidant = this.createFromForm();
    if (aidant.id !== undefined) {
      this.subscribeToSaveResponse(this.aidantService.update(aidant));
    } else {
      this.subscribeToSaveResponse(this.aidantService.create(aidant));
    }
  }

  private createFromForm(): IAidant {
    return {
      ...new Aidant(),
      id: this.editForm.get(['id'])!.value,
      cursusActuel: this.editForm.get(['cursusActuel'])!.value,
      nombreAnneesPostBAC: this.editForm.get(['nombreAnneesPostBAC'])!.value,
      dateCreation: this.editForm.get(['dateCreation'])!.value
        ? moment(this.editForm.get(['dateCreation'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateModification: this.editForm.get(['dateModification'])!.value
        ? moment(this.editForm.get(['dateModification'])!.value, DATE_TIME_FORMAT)
        : undefined,
      dateArchivage: this.editForm.get(['dateArchivage'])!.value
        ? moment(this.editForm.get(['dateArchivage'])!.value, DATE_TIME_FORMAT)
        : undefined,
      utilisateurId: this.editForm.get(['utilisateurId'])!.value,
      contactId: this.editForm.get(['contactId'])!.value,
      photoId: this.editForm.get(['photoId'])!.value,
      etablissementId: this.editForm.get(['etablissementId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAidant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
