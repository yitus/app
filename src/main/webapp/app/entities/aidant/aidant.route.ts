import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAidant, Aidant } from 'app/shared/model/aidant.model';
import { AidantService } from './aidant.service';
import { AidantComponent } from './aidant.component';
import { AidantDetailComponent } from './aidant-detail.component';
import { AidantUpdateComponent } from './aidant-update.component';

@Injectable({ providedIn: 'root' })
export class AidantResolve implements Resolve<IAidant> {
  constructor(private service: AidantService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAidant> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((aidant: HttpResponse<Aidant>) => {
          if (aidant.body) {
            return of(aidant.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Aidant());
  }
}

export const aidantRoute: Routes = [
  {
    path: '',
    component: AidantComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'yitusApp.aidant.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AidantDetailComponent,
    resolve: {
      aidant: AidantResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.aidant.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AidantUpdateComponent,
    resolve: {
      aidant: AidantResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.aidant.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AidantUpdateComponent,
    resolve: {
      aidant: AidantResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'yitusApp.aidant.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
