import { Moment } from 'moment';

export interface IReferent {
  id?: number;
  dateCreation?: Moment;
  dateModification?: Moment;
  dateArchivage?: Moment;
  utilisateurLogin?: string;
  utilisateurId?: number;
  contactId?: number;
  photoId?: number;
  etablissementNom?: string;
  etablissementId?: number;
}

export class Referent implements IReferent {
  constructor(
    public id?: number,
    public dateCreation?: Moment,
    public dateModification?: Moment,
    public dateArchivage?: Moment,
    public utilisateurLogin?: string,
    public utilisateurId?: number,
    public contactId?: number,
    public photoId?: number,
    public etablissementNom?: string,
    public etablissementId?: number
  ) {}
}
