import { Moment } from 'moment';
import { TypeNiveau } from 'app/shared/model/enumerations/type-niveau.model';

export interface IApprenant {
  id?: number;
  niveau?: TypeNiveau;
  dateCreation?: Moment;
  dateModification?: Moment;
  dateArchivage?: Moment;
  utilisateurLogin?: string;
  utilisateurId?: number;
  contactId?: number;
  photoId?: number;
  etablissementNom?: string;
  etablissementId?: number;
}

export class Apprenant implements IApprenant {
  constructor(
    public id?: number,
    public niveau?: TypeNiveau,
    public dateCreation?: Moment,
    public dateModification?: Moment,
    public dateArchivage?: Moment,
    public utilisateurLogin?: string,
    public utilisateurId?: number,
    public contactId?: number,
    public photoId?: number,
    public etablissementNom?: string,
    public etablissementId?: number
  ) {}
}
