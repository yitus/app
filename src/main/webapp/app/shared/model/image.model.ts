export interface IImage {
  id?: number;
  contenuContentType?: string;
  contenu?: any;
  contenuSha1?: string;
  vignetteContentType?: string;
  vignette?: any;
  vignetteSha1?: string;
}

export class Image implements IImage {
  constructor(
    public id?: number,
    public contenuContentType?: string,
    public contenu?: any,
    public contenuSha1?: string,
    public vignetteContentType?: string,
    public vignette?: any,
    public vignetteSha1?: string
  ) {}
}
