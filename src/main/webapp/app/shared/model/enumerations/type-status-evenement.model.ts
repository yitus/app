export const enum TypeStatusEvenement {
  INFO = 'INFO',
  SUCCES = 'SUCCES',
  ALERTE = 'ALERTE',
  ECHEC = 'ECHEC'
}
