import { Moment } from 'moment';
import { TypeStatusEvenement } from 'app/shared/model/enumerations/type-status-evenement.model';
import { TypeOperation } from 'app/shared/model/enumerations/type-operation.model';
import { TypeEntite } from 'app/shared/model/enumerations/type-entite.model';

export interface IEvenement {
  id?: number;
  statusEvenement?: TypeStatusEvenement;
  dateEvenement?: Moment;
  operation?: TypeOperation;
  entite?: TypeEntite;
  idEntite?: number;
  message?: string;
  creeParLogin?: string;
  creeParId?: number;
}

export class Evenement implements IEvenement {
  constructor(
    public id?: number,
    public statusEvenement?: TypeStatusEvenement,
    public dateEvenement?: Moment,
    public operation?: TypeOperation,
    public entite?: TypeEntite,
    public idEntite?: number,
    public message?: string,
    public creeParLogin?: string,
    public creeParId?: number
  ) {}
}
