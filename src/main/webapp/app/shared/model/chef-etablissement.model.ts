import { Moment } from 'moment';

export interface IChefEtablissement {
  id?: number;
  dateCreation?: Moment;
  dateModification?: Moment;
  dateArchivage?: Moment;
  utilisateurLogin?: string;
  utilisateurId?: number;
  photoId?: number;
  etablissementId?: number;
}

export class ChefEtablissement implements IChefEtablissement {
  constructor(
    public id?: number,
    public dateCreation?: Moment,
    public dateModification?: Moment,
    public dateArchivage?: Moment,
    public utilisateurLogin?: string,
    public utilisateurId?: number,
    public photoId?: number,
    public etablissementId?: number
  ) {}
}
