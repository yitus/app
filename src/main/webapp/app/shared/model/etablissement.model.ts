import { Moment } from 'moment';
import { TypeEtablissement } from 'app/shared/model/enumerations/type-etablissement.model';

export interface IEtablissement {
  id?: number;
  typeEtablissement?: TypeEtablissement;
  nom?: string;
  siteweb?: string;
  adresse?: string;
  ville?: string;
  codePostal?: string;
  dateCreation?: Moment;
  dateModification?: Moment;
  dateArchivage?: Moment;
  chefId?: number;
  photoId?: number;
}

export class Etablissement implements IEtablissement {
  constructor(
    public id?: number,
    public typeEtablissement?: TypeEtablissement,
    public nom?: string,
    public siteweb?: string,
    public adresse?: string,
    public ville?: string,
    public codePostal?: string,
    public dateCreation?: Moment,
    public dateModification?: Moment,
    public dateArchivage?: Moment,
    public chefId?: number,
    public photoId?: number
  ) {}
}
