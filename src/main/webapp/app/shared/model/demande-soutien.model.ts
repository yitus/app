import { Moment } from 'moment';
import { TypeStatut } from 'app/shared/model/enumerations/type-statut.model';
import { TypeNiveau } from 'app/shared/model/enumerations/type-niveau.model';
import { TypeMatiere } from 'app/shared/model/enumerations/type-matiere.model';

export interface IDemandeSoutien {
  id?: number;
  statut?: TypeStatut;
  etiquettes?: string;
  niveau?: TypeNiveau;
  matiere?: TypeMatiere;
  detail?: any;
  horairesDisponibilites?: string;
  dateRendezVous?: Moment;
  noteApprenant?: number;
  noteAidant?: number;
  commentaireApprenant?: any;
  commentaireAidant?: any;
  commentaireEnseignant?: any;
  commentaireReferent?: any;
  dateLimite?: Moment;
  dateCreation?: Moment;
  dateModification?: Moment;
  dateArchivage?: Moment;
  apprenantId?: number;
  aidantId?: number;
  enseignantId?: number;
  referentId?: number;
}

export class DemandeSoutien implements IDemandeSoutien {
  constructor(
    public id?: number,
    public statut?: TypeStatut,
    public etiquettes?: string,
    public niveau?: TypeNiveau,
    public matiere?: TypeMatiere,
    public detail?: any,
    public horairesDisponibilites?: string,
    public dateRendezVous?: Moment,
    public noteApprenant?: number,
    public noteAidant?: number,
    public commentaireApprenant?: any,
    public commentaireAidant?: any,
    public commentaireEnseignant?: any,
    public commentaireReferent?: any,
    public dateLimite?: Moment,
    public dateCreation?: Moment,
    public dateModification?: Moment,
    public dateArchivage?: Moment,
    public apprenantId?: number,
    public aidantId?: number,
    public enseignantId?: number,
    public referentId?: number
  ) {}
}
