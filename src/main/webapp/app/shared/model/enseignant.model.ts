import { Moment } from 'moment';
import { TypeMatiere } from 'app/shared/model/enumerations/type-matiere.model';

export interface IEnseignant {
  id?: number;
  matiere1?: TypeMatiere;
  matiere2?: TypeMatiere;
  matiere3?: TypeMatiere;
  matiere4?: TypeMatiere;
  dateCreation?: Moment;
  dateModification?: Moment;
  dateArchivage?: Moment;
  utilisateurLogin?: string;
  utilisateurId?: number;
  contactId?: number;
  photoId?: number;
  etablissementNom?: string;
  etablissementId?: number;
}

export class Enseignant implements IEnseignant {
  constructor(
    public id?: number,
    public matiere1?: TypeMatiere,
    public matiere2?: TypeMatiere,
    public matiere3?: TypeMatiere,
    public matiere4?: TypeMatiere,
    public dateCreation?: Moment,
    public dateModification?: Moment,
    public dateArchivage?: Moment,
    public utilisateurLogin?: string,
    public utilisateurId?: number,
    public contactId?: number,
    public photoId?: number,
    public etablissementNom?: string,
    public etablissementId?: number
  ) {}
}
