import { Moment } from 'moment';
import { TypeStatusNotification } from 'app/shared/model/enumerations/type-status-notification.model';

export interface INotification {
  id?: number;
  statusNotification?: TypeStatusNotification;
  message?: string;
  dateCreation?: Moment;
  dateLecture?: Moment;
  dateArchivage?: Moment;
  destineALogin?: string;
  destineAId?: number;
  creeParLogin?: string;
  creeParId?: number;
}

export class Notification implements INotification {
  constructor(
    public id?: number,
    public statusNotification?: TypeStatusNotification,
    public message?: string,
    public dateCreation?: Moment,
    public dateLecture?: Moment,
    public dateArchivage?: Moment,
    public destineALogin?: string,
    public destineAId?: number,
    public creeParLogin?: string,
    public creeParId?: number
  ) {}
}
