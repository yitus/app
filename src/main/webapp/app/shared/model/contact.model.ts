export interface IContact {
  id?: number;
  badges?: string;
  ajoutContenuImageContentType?: string;
  ajoutContenuImage?: any;
  login?: string;
  nom?: string;
  prenom?: string;
  email?: string;
  telephone?: string;
  ville?: string;
  gravatar?: string;
  linkedin?: string;
  google?: string;
  facebook?: string;
  skype?: string;
  discord?: string;
  zoom?: string;
  whereby?: string;
  mumble?: string;
  tox?: string;
  matrix?: string;
}

export class Contact implements IContact {
  constructor(
    public id?: number,
    public badges?: string,
    public ajoutContenuImageContentType?: string,
    public ajoutContenuImage?: any,
    public login?: string,
    public nom?: string,
    public prenom?: string,
    public email?: string,
    public telephone?: string,
    public ville?: string,
    public gravatar?: string,
    public linkedin?: string,
    public google?: string,
    public facebook?: string,
    public skype?: string,
    public discord?: string,
    public zoom?: string,
    public whereby?: string,
    public mumble?: string,
    public tox?: string,
    public matrix?: string
  ) {}
}
