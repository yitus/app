import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ImageComponentsPage, ImageDeleteDialog, ImageUpdatePage } from './image.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Image e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let imageComponentsPage: ImageComponentsPage;
  let imageUpdatePage: ImageUpdatePage;
  let imageDeleteDialog: ImageDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Images', async () => {
    await navBarPage.goToEntity('image');
    imageComponentsPage = new ImageComponentsPage();
    await browser.wait(ec.visibilityOf(imageComponentsPage.title), 5000);
    expect(await imageComponentsPage.getTitle()).to.eq('yitusApp.image.home.title');
    await browser.wait(ec.or(ec.visibilityOf(imageComponentsPage.entities), ec.visibilityOf(imageComponentsPage.noResult)), 1000);
  });

  it('should load create Image page', async () => {
    await imageComponentsPage.clickOnCreateButton();
    imageUpdatePage = new ImageUpdatePage();
    expect(await imageUpdatePage.getPageTitle()).to.eq('yitusApp.image.home.createOrEditLabel');
    await imageUpdatePage.cancel();
  });

  it('should create and save Images', async () => {
    const nbButtonsBeforeCreate = await imageComponentsPage.countDeleteButtons();

    await imageComponentsPage.clickOnCreateButton();

    await promise.all([
      imageUpdatePage.setContenuInput(absolutePath),
      imageUpdatePage.setContenuSha1Input('E7883a3336E5dfA90e67CC9aEBBBE5a77413e6DD'),
      imageUpdatePage.setVignetteInput(absolutePath),
      imageUpdatePage.setVignetteSha1Input('dcaE5f7140aa3f1DE3C4dA948E0684fF7CA89Df8')
    ]);

    expect(await imageUpdatePage.getContenuInput()).to.endsWith(
      fileNameToUpload,
      'Expected Contenu value to be end with ' + fileNameToUpload
    );
    expect(await imageUpdatePage.getContenuSha1Input()).to.eq(
      'E7883a3336E5dfA90e67CC9aEBBBE5a77413e6DD',
      'Expected ContenuSha1 value to be equals to E7883a3336E5dfA90e67CC9aEBBBE5a77413e6DD'
    );
    expect(await imageUpdatePage.getVignetteInput()).to.endsWith(
      fileNameToUpload,
      'Expected Vignette value to be end with ' + fileNameToUpload
    );
    expect(await imageUpdatePage.getVignetteSha1Input()).to.eq(
      'dcaE5f7140aa3f1DE3C4dA948E0684fF7CA89Df8',
      'Expected VignetteSha1 value to be equals to dcaE5f7140aa3f1DE3C4dA948E0684fF7CA89Df8'
    );

    await imageUpdatePage.save();
    expect(await imageUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await imageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Image', async () => {
    const nbButtonsBeforeDelete = await imageComponentsPage.countDeleteButtons();
    await imageComponentsPage.clickOnLastDeleteButton();

    imageDeleteDialog = new ImageDeleteDialog();
    expect(await imageDeleteDialog.getDialogTitle()).to.eq('yitusApp.image.delete.question');
    await imageDeleteDialog.clickOnConfirmButton();

    expect(await imageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
