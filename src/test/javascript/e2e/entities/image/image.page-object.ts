import { element, by, ElementFinder } from 'protractor';

export class ImageComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-image div table .btn-danger'));
  title = element.all(by.css('jhi-image div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ImageUpdatePage {
  pageTitle = element(by.id('jhi-image-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  contenuInput = element(by.id('file_contenu'));
  contenuSha1Input = element(by.id('field_contenuSha1'));
  vignetteInput = element(by.id('file_vignette'));
  vignetteSha1Input = element(by.id('field_vignetteSha1'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setContenuInput(contenu: string): Promise<void> {
    await this.contenuInput.sendKeys(contenu);
  }

  async getContenuInput(): Promise<string> {
    return await this.contenuInput.getAttribute('value');
  }

  async setContenuSha1Input(contenuSha1: string): Promise<void> {
    await this.contenuSha1Input.sendKeys(contenuSha1);
  }

  async getContenuSha1Input(): Promise<string> {
    return await this.contenuSha1Input.getAttribute('value');
  }

  async setVignetteInput(vignette: string): Promise<void> {
    await this.vignetteInput.sendKeys(vignette);
  }

  async getVignetteInput(): Promise<string> {
    return await this.vignetteInput.getAttribute('value');
  }

  async setVignetteSha1Input(vignetteSha1: string): Promise<void> {
    await this.vignetteSha1Input.sendKeys(vignetteSha1);
  }

  async getVignetteSha1Input(): Promise<string> {
    return await this.vignetteSha1Input.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ImageDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-image-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-image'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
