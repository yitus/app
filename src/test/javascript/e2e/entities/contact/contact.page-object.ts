import { element, by, ElementFinder } from 'protractor';

export class ContactComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-contact div table .btn-danger'));
  title = element.all(by.css('jhi-contact div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ContactUpdatePage {
  pageTitle = element(by.id('jhi-contact-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  badgesInput = element(by.id('field_badges'));
  ajoutContenuImageInput = element(by.id('file_ajoutContenuImage'));
  loginInput = element(by.id('field_login'));
  nomInput = element(by.id('field_nom'));
  prenomInput = element(by.id('field_prenom'));
  emailInput = element(by.id('field_email'));
  telephoneInput = element(by.id('field_telephone'));
  villeInput = element(by.id('field_ville'));
  gravatarInput = element(by.id('field_gravatar'));
  linkedinInput = element(by.id('field_linkedin'));
  googleInput = element(by.id('field_google'));
  facebookInput = element(by.id('field_facebook'));
  skypeInput = element(by.id('field_skype'));
  discordInput = element(by.id('field_discord'));
  zoomInput = element(by.id('field_zoom'));
  wherebyInput = element(by.id('field_whereby'));
  mumbleInput = element(by.id('field_mumble'));
  toxInput = element(by.id('field_tox'));
  matrixInput = element(by.id('field_matrix'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setBadgesInput(badges: string): Promise<void> {
    await this.badgesInput.sendKeys(badges);
  }

  async getBadgesInput(): Promise<string> {
    return await this.badgesInput.getAttribute('value');
  }

  async setAjoutContenuImageInput(ajoutContenuImage: string): Promise<void> {
    await this.ajoutContenuImageInput.sendKeys(ajoutContenuImage);
  }

  async getAjoutContenuImageInput(): Promise<string> {
    return await this.ajoutContenuImageInput.getAttribute('value');
  }

  async setLoginInput(login: string): Promise<void> {
    await this.loginInput.sendKeys(login);
  }

  async getLoginInput(): Promise<string> {
    return await this.loginInput.getAttribute('value');
  }

  async setNomInput(nom: string): Promise<void> {
    await this.nomInput.sendKeys(nom);
  }

  async getNomInput(): Promise<string> {
    return await this.nomInput.getAttribute('value');
  }

  async setPrenomInput(prenom: string): Promise<void> {
    await this.prenomInput.sendKeys(prenom);
  }

  async getPrenomInput(): Promise<string> {
    return await this.prenomInput.getAttribute('value');
  }

  async setEmailInput(email: string): Promise<void> {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput(): Promise<string> {
    return await this.emailInput.getAttribute('value');
  }

  async setTelephoneInput(telephone: string): Promise<void> {
    await this.telephoneInput.sendKeys(telephone);
  }

  async getTelephoneInput(): Promise<string> {
    return await this.telephoneInput.getAttribute('value');
  }

  async setVilleInput(ville: string): Promise<void> {
    await this.villeInput.sendKeys(ville);
  }

  async getVilleInput(): Promise<string> {
    return await this.villeInput.getAttribute('value');
  }

  async setGravatarInput(gravatar: string): Promise<void> {
    await this.gravatarInput.sendKeys(gravatar);
  }

  async getGravatarInput(): Promise<string> {
    return await this.gravatarInput.getAttribute('value');
  }

  async setLinkedinInput(linkedin: string): Promise<void> {
    await this.linkedinInput.sendKeys(linkedin);
  }

  async getLinkedinInput(): Promise<string> {
    return await this.linkedinInput.getAttribute('value');
  }

  async setGoogleInput(google: string): Promise<void> {
    await this.googleInput.sendKeys(google);
  }

  async getGoogleInput(): Promise<string> {
    return await this.googleInput.getAttribute('value');
  }

  async setFacebookInput(facebook: string): Promise<void> {
    await this.facebookInput.sendKeys(facebook);
  }

  async getFacebookInput(): Promise<string> {
    return await this.facebookInput.getAttribute('value');
  }

  async setSkypeInput(skype: string): Promise<void> {
    await this.skypeInput.sendKeys(skype);
  }

  async getSkypeInput(): Promise<string> {
    return await this.skypeInput.getAttribute('value');
  }

  async setDiscordInput(discord: string): Promise<void> {
    await this.discordInput.sendKeys(discord);
  }

  async getDiscordInput(): Promise<string> {
    return await this.discordInput.getAttribute('value');
  }

  async setZoomInput(zoom: string): Promise<void> {
    await this.zoomInput.sendKeys(zoom);
  }

  async getZoomInput(): Promise<string> {
    return await this.zoomInput.getAttribute('value');
  }

  async setWherebyInput(whereby: string): Promise<void> {
    await this.wherebyInput.sendKeys(whereby);
  }

  async getWherebyInput(): Promise<string> {
    return await this.wherebyInput.getAttribute('value');
  }

  async setMumbleInput(mumble: string): Promise<void> {
    await this.mumbleInput.sendKeys(mumble);
  }

  async getMumbleInput(): Promise<string> {
    return await this.mumbleInput.getAttribute('value');
  }

  async setToxInput(tox: string): Promise<void> {
    await this.toxInput.sendKeys(tox);
  }

  async getToxInput(): Promise<string> {
    return await this.toxInput.getAttribute('value');
  }

  async setMatrixInput(matrix: string): Promise<void> {
    await this.matrixInput.sendKeys(matrix);
  }

  async getMatrixInput(): Promise<string> {
    return await this.matrixInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ContactDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-contact-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-contact'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
