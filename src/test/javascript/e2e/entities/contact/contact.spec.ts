import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ContactComponentsPage, ContactDeleteDialog, ContactUpdatePage } from './contact.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Contact e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactComponentsPage: ContactComponentsPage;
  let contactUpdatePage: ContactUpdatePage;
  let contactDeleteDialog: ContactDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Contacts', async () => {
    await navBarPage.goToEntity('contact');
    contactComponentsPage = new ContactComponentsPage();
    await browser.wait(ec.visibilityOf(contactComponentsPage.title), 5000);
    expect(await contactComponentsPage.getTitle()).to.eq('yitusApp.contact.home.title');
    await browser.wait(ec.or(ec.visibilityOf(contactComponentsPage.entities), ec.visibilityOf(contactComponentsPage.noResult)), 1000);
  });

  it('should load create Contact page', async () => {
    await contactComponentsPage.clickOnCreateButton();
    contactUpdatePage = new ContactUpdatePage();
    expect(await contactUpdatePage.getPageTitle()).to.eq('yitusApp.contact.home.createOrEditLabel');
    await contactUpdatePage.cancel();
  });

  it('should create and save Contacts', async () => {
    const nbButtonsBeforeCreate = await contactComponentsPage.countDeleteButtons();

    await contactComponentsPage.clickOnCreateButton();

    await promise.all([
      contactUpdatePage.setBadgesInput('badges'),
      contactUpdatePage.setAjoutContenuImageInput(absolutePath),
      contactUpdatePage.setLoginInput('login'),
      contactUpdatePage.setNomInput('nom'),
      contactUpdatePage.setPrenomInput('prenom'),
      contactUpdatePage.setEmailInput('email'),
      contactUpdatePage.setTelephoneInput('0094814856'),
      contactUpdatePage.setVilleInput('ville'),
      contactUpdatePage.setGravatarInput('gravatar'),
      contactUpdatePage.setLinkedinInput('linkedin'),
      contactUpdatePage.setGoogleInput('google'),
      contactUpdatePage.setFacebookInput('facebook'),
      contactUpdatePage.setSkypeInput('skype'),
      contactUpdatePage.setDiscordInput('discord'),
      contactUpdatePage.setZoomInput('zoom'),
      contactUpdatePage.setWherebyInput('whereby'),
      contactUpdatePage.setMumbleInput('mumble'),
      contactUpdatePage.setToxInput('tox'),
      contactUpdatePage.setMatrixInput('matrix')
    ]);

    expect(await contactUpdatePage.getBadgesInput()).to.eq('badges', 'Expected Badges value to be equals to badges');
    expect(await contactUpdatePage.getAjoutContenuImageInput()).to.endsWith(
      fileNameToUpload,
      'Expected AjoutContenuImage value to be end with ' + fileNameToUpload
    );
    expect(await contactUpdatePage.getLoginInput()).to.eq('login', 'Expected Login value to be equals to login');
    expect(await contactUpdatePage.getNomInput()).to.eq('nom', 'Expected Nom value to be equals to nom');
    expect(await contactUpdatePage.getPrenomInput()).to.eq('prenom', 'Expected Prenom value to be equals to prenom');
    expect(await contactUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await contactUpdatePage.getTelephoneInput()).to.eq('0094814856', 'Expected Telephone value to be equals to 0094814856');
    expect(await contactUpdatePage.getVilleInput()).to.eq('ville', 'Expected Ville value to be equals to ville');
    expect(await contactUpdatePage.getGravatarInput()).to.eq('gravatar', 'Expected Gravatar value to be equals to gravatar');
    expect(await contactUpdatePage.getLinkedinInput()).to.eq('linkedin', 'Expected Linkedin value to be equals to linkedin');
    expect(await contactUpdatePage.getGoogleInput()).to.eq('google', 'Expected Google value to be equals to google');
    expect(await contactUpdatePage.getFacebookInput()).to.eq('facebook', 'Expected Facebook value to be equals to facebook');
    expect(await contactUpdatePage.getSkypeInput()).to.eq('skype', 'Expected Skype value to be equals to skype');
    expect(await contactUpdatePage.getDiscordInput()).to.eq('discord', 'Expected Discord value to be equals to discord');
    expect(await contactUpdatePage.getZoomInput()).to.eq('zoom', 'Expected Zoom value to be equals to zoom');
    expect(await contactUpdatePage.getWherebyInput()).to.eq('whereby', 'Expected Whereby value to be equals to whereby');
    expect(await contactUpdatePage.getMumbleInput()).to.eq('mumble', 'Expected Mumble value to be equals to mumble');
    expect(await contactUpdatePage.getToxInput()).to.eq('tox', 'Expected Tox value to be equals to tox');
    expect(await contactUpdatePage.getMatrixInput()).to.eq('matrix', 'Expected Matrix value to be equals to matrix');

    await contactUpdatePage.save();
    expect(await contactUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await contactComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Contact', async () => {
    const nbButtonsBeforeDelete = await contactComponentsPage.countDeleteButtons();
    await contactComponentsPage.clickOnLastDeleteButton();

    contactDeleteDialog = new ContactDeleteDialog();
    expect(await contactDeleteDialog.getDialogTitle()).to.eq('yitusApp.contact.delete.question');
    await contactDeleteDialog.clickOnConfirmButton();

    expect(await contactComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
