import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AidantComponentsPage, AidantDeleteDialog, AidantUpdatePage } from './aidant.page-object';

const expect = chai.expect;

describe('Aidant e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let aidantComponentsPage: AidantComponentsPage;
  let aidantUpdatePage: AidantUpdatePage;
  let aidantDeleteDialog: AidantDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Aidants', async () => {
    await navBarPage.goToEntity('aidant');
    aidantComponentsPage = new AidantComponentsPage();
    await browser.wait(ec.visibilityOf(aidantComponentsPage.title), 5000);
    expect(await aidantComponentsPage.getTitle()).to.eq('yitusApp.aidant.home.title');
    await browser.wait(ec.or(ec.visibilityOf(aidantComponentsPage.entities), ec.visibilityOf(aidantComponentsPage.noResult)), 1000);
  });

  it('should load create Aidant page', async () => {
    await aidantComponentsPage.clickOnCreateButton();
    aidantUpdatePage = new AidantUpdatePage();
    expect(await aidantUpdatePage.getPageTitle()).to.eq('yitusApp.aidant.home.createOrEditLabel');
    await aidantUpdatePage.cancel();
  });

  it('should create and save Aidants', async () => {
    const nbButtonsBeforeCreate = await aidantComponentsPage.countDeleteButtons();

    await aidantComponentsPage.clickOnCreateButton();

    await promise.all([
      aidantUpdatePage.setCursusActuelInput('cursusActuel'),
      aidantUpdatePage.setNombreAnneesPostBACInput('5'),
      aidantUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      aidantUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      aidantUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      aidantUpdatePage.utilisateurSelectLastOption(),
      aidantUpdatePage.contactSelectLastOption(),
      aidantUpdatePage.photoSelectLastOption(),
      aidantUpdatePage.etablissementSelectLastOption()
    ]);

    expect(await aidantUpdatePage.getCursusActuelInput()).to.eq('cursusActuel', 'Expected CursusActuel value to be equals to cursusActuel');
    expect(await aidantUpdatePage.getNombreAnneesPostBACInput()).to.eq('5', 'Expected nombreAnneesPostBAC value to be equals to 5');
    expect(await aidantUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await aidantUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await aidantUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await aidantUpdatePage.save();
    expect(await aidantUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await aidantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Aidant', async () => {
    const nbButtonsBeforeDelete = await aidantComponentsPage.countDeleteButtons();
    await aidantComponentsPage.clickOnLastDeleteButton();

    aidantDeleteDialog = new AidantDeleteDialog();
    expect(await aidantDeleteDialog.getDialogTitle()).to.eq('yitusApp.aidant.delete.question');
    await aidantDeleteDialog.clickOnConfirmButton();

    expect(await aidantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
