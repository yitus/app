import { element, by, ElementFinder } from 'protractor';

export class EtablissementComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-etablissement div table .btn-danger'));
  title = element.all(by.css('jhi-etablissement div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EtablissementUpdatePage {
  pageTitle = element(by.id('jhi-etablissement-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  typeEtablissementSelect = element(by.id('field_typeEtablissement'));
  nomInput = element(by.id('field_nom'));
  sitewebInput = element(by.id('field_siteweb'));
  adresseInput = element(by.id('field_adresse'));
  villeInput = element(by.id('field_ville'));
  codePostalInput = element(by.id('field_codePostal'));
  dateCreationInput = element(by.id('field_dateCreation'));
  dateModificationInput = element(by.id('field_dateModification'));
  dateArchivageInput = element(by.id('field_dateArchivage'));

  chefSelect = element(by.id('field_chef'));
  photoSelect = element(by.id('field_photo'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTypeEtablissementSelect(typeEtablissement: string): Promise<void> {
    await this.typeEtablissementSelect.sendKeys(typeEtablissement);
  }

  async getTypeEtablissementSelect(): Promise<string> {
    return await this.typeEtablissementSelect.element(by.css('option:checked')).getText();
  }

  async typeEtablissementSelectLastOption(): Promise<void> {
    await this.typeEtablissementSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setNomInput(nom: string): Promise<void> {
    await this.nomInput.sendKeys(nom);
  }

  async getNomInput(): Promise<string> {
    return await this.nomInput.getAttribute('value');
  }

  async setSitewebInput(siteweb: string): Promise<void> {
    await this.sitewebInput.sendKeys(siteweb);
  }

  async getSitewebInput(): Promise<string> {
    return await this.sitewebInput.getAttribute('value');
  }

  async setAdresseInput(adresse: string): Promise<void> {
    await this.adresseInput.sendKeys(adresse);
  }

  async getAdresseInput(): Promise<string> {
    return await this.adresseInput.getAttribute('value');
  }

  async setVilleInput(ville: string): Promise<void> {
    await this.villeInput.sendKeys(ville);
  }

  async getVilleInput(): Promise<string> {
    return await this.villeInput.getAttribute('value');
  }

  async setCodePostalInput(codePostal: string): Promise<void> {
    await this.codePostalInput.sendKeys(codePostal);
  }

  async getCodePostalInput(): Promise<string> {
    return await this.codePostalInput.getAttribute('value');
  }

  async setDateCreationInput(dateCreation: string): Promise<void> {
    await this.dateCreationInput.sendKeys(dateCreation);
  }

  async getDateCreationInput(): Promise<string> {
    return await this.dateCreationInput.getAttribute('value');
  }

  async setDateModificationInput(dateModification: string): Promise<void> {
    await this.dateModificationInput.sendKeys(dateModification);
  }

  async getDateModificationInput(): Promise<string> {
    return await this.dateModificationInput.getAttribute('value');
  }

  async setDateArchivageInput(dateArchivage: string): Promise<void> {
    await this.dateArchivageInput.sendKeys(dateArchivage);
  }

  async getDateArchivageInput(): Promise<string> {
    return await this.dateArchivageInput.getAttribute('value');
  }

  async chefSelectLastOption(): Promise<void> {
    await this.chefSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async chefSelectOption(option: string): Promise<void> {
    await this.chefSelect.sendKeys(option);
  }

  getChefSelect(): ElementFinder {
    return this.chefSelect;
  }

  async getChefSelectedOption(): Promise<string> {
    return await this.chefSelect.element(by.css('option:checked')).getText();
  }

  async photoSelectLastOption(): Promise<void> {
    await this.photoSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async photoSelectOption(option: string): Promise<void> {
    await this.photoSelect.sendKeys(option);
  }

  getPhotoSelect(): ElementFinder {
    return this.photoSelect;
  }

  async getPhotoSelectedOption(): Promise<string> {
    return await this.photoSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EtablissementDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-etablissement-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-etablissement'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
