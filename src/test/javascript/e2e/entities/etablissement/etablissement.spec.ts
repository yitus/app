import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EtablissementComponentsPage, EtablissementDeleteDialog, EtablissementUpdatePage } from './etablissement.page-object';

const expect = chai.expect;

describe('Etablissement e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let etablissementComponentsPage: EtablissementComponentsPage;
  let etablissementUpdatePage: EtablissementUpdatePage;
  let etablissementDeleteDialog: EtablissementDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Etablissements', async () => {
    await navBarPage.goToEntity('etablissement');
    etablissementComponentsPage = new EtablissementComponentsPage();
    await browser.wait(ec.visibilityOf(etablissementComponentsPage.title), 5000);
    expect(await etablissementComponentsPage.getTitle()).to.eq('yitusApp.etablissement.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(etablissementComponentsPage.entities), ec.visibilityOf(etablissementComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Etablissement page', async () => {
    await etablissementComponentsPage.clickOnCreateButton();
    etablissementUpdatePage = new EtablissementUpdatePage();
    expect(await etablissementUpdatePage.getPageTitle()).to.eq('yitusApp.etablissement.home.createOrEditLabel');
    await etablissementUpdatePage.cancel();
  });

  it('should create and save Etablissements', async () => {
    const nbButtonsBeforeCreate = await etablissementComponentsPage.countDeleteButtons();

    await etablissementComponentsPage.clickOnCreateButton();

    await promise.all([
      etablissementUpdatePage.typeEtablissementSelectLastOption(),
      etablissementUpdatePage.setNomInput('nom'),
      etablissementUpdatePage.setSitewebInput('siteweb'),
      etablissementUpdatePage.setAdresseInput('adresse'),
      etablissementUpdatePage.setVilleInput('ville'),
      etablissementUpdatePage.setCodePostalInput('codePostal'),
      etablissementUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      etablissementUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      etablissementUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      etablissementUpdatePage.chefSelectLastOption(),
      etablissementUpdatePage.photoSelectLastOption()
    ]);

    expect(await etablissementUpdatePage.getNomInput()).to.eq('nom', 'Expected Nom value to be equals to nom');
    expect(await etablissementUpdatePage.getSitewebInput()).to.eq('siteweb', 'Expected Siteweb value to be equals to siteweb');
    expect(await etablissementUpdatePage.getAdresseInput()).to.eq('adresse', 'Expected Adresse value to be equals to adresse');
    expect(await etablissementUpdatePage.getVilleInput()).to.eq('ville', 'Expected Ville value to be equals to ville');
    expect(await etablissementUpdatePage.getCodePostalInput()).to.eq('codePostal', 'Expected CodePostal value to be equals to codePostal');
    expect(await etablissementUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await etablissementUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await etablissementUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await etablissementUpdatePage.save();
    expect(await etablissementUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await etablissementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Etablissement', async () => {
    const nbButtonsBeforeDelete = await etablissementComponentsPage.countDeleteButtons();
    await etablissementComponentsPage.clickOnLastDeleteButton();

    etablissementDeleteDialog = new EtablissementDeleteDialog();
    expect(await etablissementDeleteDialog.getDialogTitle()).to.eq('yitusApp.etablissement.delete.question');
    await etablissementDeleteDialog.clickOnConfirmButton();

    expect(await etablissementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
