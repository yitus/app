import { element, by, ElementFinder } from 'protractor';

export class ChefEtablissementComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-chef-etablissement div table .btn-danger'));
  title = element.all(by.css('jhi-chef-etablissement div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ChefEtablissementUpdatePage {
  pageTitle = element(by.id('jhi-chef-etablissement-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  dateCreationInput = element(by.id('field_dateCreation'));
  dateModificationInput = element(by.id('field_dateModification'));
  dateArchivageInput = element(by.id('field_dateArchivage'));

  utilisateurSelect = element(by.id('field_utilisateur'));
  photoSelect = element(by.id('field_photo'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateCreationInput(dateCreation: string): Promise<void> {
    await this.dateCreationInput.sendKeys(dateCreation);
  }

  async getDateCreationInput(): Promise<string> {
    return await this.dateCreationInput.getAttribute('value');
  }

  async setDateModificationInput(dateModification: string): Promise<void> {
    await this.dateModificationInput.sendKeys(dateModification);
  }

  async getDateModificationInput(): Promise<string> {
    return await this.dateModificationInput.getAttribute('value');
  }

  async setDateArchivageInput(dateArchivage: string): Promise<void> {
    await this.dateArchivageInput.sendKeys(dateArchivage);
  }

  async getDateArchivageInput(): Promise<string> {
    return await this.dateArchivageInput.getAttribute('value');
  }

  async utilisateurSelectLastOption(): Promise<void> {
    await this.utilisateurSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async utilisateurSelectOption(option: string): Promise<void> {
    await this.utilisateurSelect.sendKeys(option);
  }

  getUtilisateurSelect(): ElementFinder {
    return this.utilisateurSelect;
  }

  async getUtilisateurSelectedOption(): Promise<string> {
    return await this.utilisateurSelect.element(by.css('option:checked')).getText();
  }

  async photoSelectLastOption(): Promise<void> {
    await this.photoSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async photoSelectOption(option: string): Promise<void> {
    await this.photoSelect.sendKeys(option);
  }

  getPhotoSelect(): ElementFinder {
    return this.photoSelect;
  }

  async getPhotoSelectedOption(): Promise<string> {
    return await this.photoSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ChefEtablissementDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-chefEtablissement-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-chefEtablissement'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
