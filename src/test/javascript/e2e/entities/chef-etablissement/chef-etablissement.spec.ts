import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ChefEtablissementComponentsPage,
  ChefEtablissementDeleteDialog,
  ChefEtablissementUpdatePage
} from './chef-etablissement.page-object';

const expect = chai.expect;

describe('ChefEtablissement e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let chefEtablissementComponentsPage: ChefEtablissementComponentsPage;
  let chefEtablissementUpdatePage: ChefEtablissementUpdatePage;
  let chefEtablissementDeleteDialog: ChefEtablissementDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ChefEtablissements', async () => {
    await navBarPage.goToEntity('chef-etablissement');
    chefEtablissementComponentsPage = new ChefEtablissementComponentsPage();
    await browser.wait(ec.visibilityOf(chefEtablissementComponentsPage.title), 5000);
    expect(await chefEtablissementComponentsPage.getTitle()).to.eq('yitusApp.chefEtablissement.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(chefEtablissementComponentsPage.entities), ec.visibilityOf(chefEtablissementComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ChefEtablissement page', async () => {
    await chefEtablissementComponentsPage.clickOnCreateButton();
    chefEtablissementUpdatePage = new ChefEtablissementUpdatePage();
    expect(await chefEtablissementUpdatePage.getPageTitle()).to.eq('yitusApp.chefEtablissement.home.createOrEditLabel');
    await chefEtablissementUpdatePage.cancel();
  });

  it('should create and save ChefEtablissements', async () => {
    const nbButtonsBeforeCreate = await chefEtablissementComponentsPage.countDeleteButtons();

    await chefEtablissementComponentsPage.clickOnCreateButton();

    await promise.all([
      chefEtablissementUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      chefEtablissementUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      chefEtablissementUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      chefEtablissementUpdatePage.utilisateurSelectLastOption(),
      chefEtablissementUpdatePage.photoSelectLastOption()
    ]);

    expect(await chefEtablissementUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await chefEtablissementUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await chefEtablissementUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await chefEtablissementUpdatePage.save();
    expect(await chefEtablissementUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await chefEtablissementComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last ChefEtablissement', async () => {
    const nbButtonsBeforeDelete = await chefEtablissementComponentsPage.countDeleteButtons();
    await chefEtablissementComponentsPage.clickOnLastDeleteButton();

    chefEtablissementDeleteDialog = new ChefEtablissementDeleteDialog();
    expect(await chefEtablissementDeleteDialog.getDialogTitle()).to.eq('yitusApp.chefEtablissement.delete.question');
    await chefEtablissementDeleteDialog.clickOnConfirmButton();

    expect(await chefEtablissementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
