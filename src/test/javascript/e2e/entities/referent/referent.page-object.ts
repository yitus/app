import { element, by, ElementFinder } from 'protractor';

export class ReferentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-referent div table .btn-danger'));
  title = element.all(by.css('jhi-referent div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ReferentUpdatePage {
  pageTitle = element(by.id('jhi-referent-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  dateCreationInput = element(by.id('field_dateCreation'));
  dateModificationInput = element(by.id('field_dateModification'));
  dateArchivageInput = element(by.id('field_dateArchivage'));

  utilisateurSelect = element(by.id('field_utilisateur'));
  contactSelect = element(by.id('field_contact'));
  photoSelect = element(by.id('field_photo'));
  etablissementSelect = element(by.id('field_etablissement'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateCreationInput(dateCreation: string): Promise<void> {
    await this.dateCreationInput.sendKeys(dateCreation);
  }

  async getDateCreationInput(): Promise<string> {
    return await this.dateCreationInput.getAttribute('value');
  }

  async setDateModificationInput(dateModification: string): Promise<void> {
    await this.dateModificationInput.sendKeys(dateModification);
  }

  async getDateModificationInput(): Promise<string> {
    return await this.dateModificationInput.getAttribute('value');
  }

  async setDateArchivageInput(dateArchivage: string): Promise<void> {
    await this.dateArchivageInput.sendKeys(dateArchivage);
  }

  async getDateArchivageInput(): Promise<string> {
    return await this.dateArchivageInput.getAttribute('value');
  }

  async utilisateurSelectLastOption(): Promise<void> {
    await this.utilisateurSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async utilisateurSelectOption(option: string): Promise<void> {
    await this.utilisateurSelect.sendKeys(option);
  }

  getUtilisateurSelect(): ElementFinder {
    return this.utilisateurSelect;
  }

  async getUtilisateurSelectedOption(): Promise<string> {
    return await this.utilisateurSelect.element(by.css('option:checked')).getText();
  }

  async contactSelectLastOption(): Promise<void> {
    await this.contactSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async contactSelectOption(option: string): Promise<void> {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect(): ElementFinder {
    return this.contactSelect;
  }

  async getContactSelectedOption(): Promise<string> {
    return await this.contactSelect.element(by.css('option:checked')).getText();
  }

  async photoSelectLastOption(): Promise<void> {
    await this.photoSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async photoSelectOption(option: string): Promise<void> {
    await this.photoSelect.sendKeys(option);
  }

  getPhotoSelect(): ElementFinder {
    return this.photoSelect;
  }

  async getPhotoSelectedOption(): Promise<string> {
    return await this.photoSelect.element(by.css('option:checked')).getText();
  }

  async etablissementSelectLastOption(): Promise<void> {
    await this.etablissementSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async etablissementSelectOption(option: string): Promise<void> {
    await this.etablissementSelect.sendKeys(option);
  }

  getEtablissementSelect(): ElementFinder {
    return this.etablissementSelect;
  }

  async getEtablissementSelectedOption(): Promise<string> {
    return await this.etablissementSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ReferentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-referent-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-referent'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
