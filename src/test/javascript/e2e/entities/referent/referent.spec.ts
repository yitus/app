import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ReferentComponentsPage, ReferentDeleteDialog, ReferentUpdatePage } from './referent.page-object';

const expect = chai.expect;

describe('Referent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let referentComponentsPage: ReferentComponentsPage;
  let referentUpdatePage: ReferentUpdatePage;
  let referentDeleteDialog: ReferentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Referents', async () => {
    await navBarPage.goToEntity('referent');
    referentComponentsPage = new ReferentComponentsPage();
    await browser.wait(ec.visibilityOf(referentComponentsPage.title), 5000);
    expect(await referentComponentsPage.getTitle()).to.eq('yitusApp.referent.home.title');
    await browser.wait(ec.or(ec.visibilityOf(referentComponentsPage.entities), ec.visibilityOf(referentComponentsPage.noResult)), 1000);
  });

  it('should load create Referent page', async () => {
    await referentComponentsPage.clickOnCreateButton();
    referentUpdatePage = new ReferentUpdatePage();
    expect(await referentUpdatePage.getPageTitle()).to.eq('yitusApp.referent.home.createOrEditLabel');
    await referentUpdatePage.cancel();
  });

  it('should create and save Referents', async () => {
    const nbButtonsBeforeCreate = await referentComponentsPage.countDeleteButtons();

    await referentComponentsPage.clickOnCreateButton();

    await promise.all([
      referentUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      referentUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      referentUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      referentUpdatePage.utilisateurSelectLastOption(),
      referentUpdatePage.contactSelectLastOption(),
      referentUpdatePage.photoSelectLastOption(),
      referentUpdatePage.etablissementSelectLastOption()
    ]);

    expect(await referentUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await referentUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await referentUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await referentUpdatePage.save();
    expect(await referentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await referentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Referent', async () => {
    const nbButtonsBeforeDelete = await referentComponentsPage.countDeleteButtons();
    await referentComponentsPage.clickOnLastDeleteButton();

    referentDeleteDialog = new ReferentDeleteDialog();
    expect(await referentDeleteDialog.getDialogTitle()).to.eq('yitusApp.referent.delete.question');
    await referentDeleteDialog.clickOnConfirmButton();

    expect(await referentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
