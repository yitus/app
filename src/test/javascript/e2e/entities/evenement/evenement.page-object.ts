import { element, by, ElementFinder } from 'protractor';

export class EvenementComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-evenement div table .btn-danger'));
  title = element.all(by.css('jhi-evenement div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EvenementUpdatePage {
  pageTitle = element(by.id('jhi-evenement-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  statusEvenementSelect = element(by.id('field_statusEvenement'));
  dateEvenementInput = element(by.id('field_dateEvenement'));
  operationSelect = element(by.id('field_operation'));
  entiteSelect = element(by.id('field_entite'));
  idEntiteInput = element(by.id('field_idEntite'));
  messageInput = element(by.id('field_message'));

  creeParSelect = element(by.id('field_creePar'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setStatusEvenementSelect(statusEvenement: string): Promise<void> {
    await this.statusEvenementSelect.sendKeys(statusEvenement);
  }

  async getStatusEvenementSelect(): Promise<string> {
    return await this.statusEvenementSelect.element(by.css('option:checked')).getText();
  }

  async statusEvenementSelectLastOption(): Promise<void> {
    await this.statusEvenementSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setDateEvenementInput(dateEvenement: string): Promise<void> {
    await this.dateEvenementInput.sendKeys(dateEvenement);
  }

  async getDateEvenementInput(): Promise<string> {
    return await this.dateEvenementInput.getAttribute('value');
  }

  async setOperationSelect(operation: string): Promise<void> {
    await this.operationSelect.sendKeys(operation);
  }

  async getOperationSelect(): Promise<string> {
    return await this.operationSelect.element(by.css('option:checked')).getText();
  }

  async operationSelectLastOption(): Promise<void> {
    await this.operationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setEntiteSelect(entite: string): Promise<void> {
    await this.entiteSelect.sendKeys(entite);
  }

  async getEntiteSelect(): Promise<string> {
    return await this.entiteSelect.element(by.css('option:checked')).getText();
  }

  async entiteSelectLastOption(): Promise<void> {
    await this.entiteSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setIdEntiteInput(idEntite: string): Promise<void> {
    await this.idEntiteInput.sendKeys(idEntite);
  }

  async getIdEntiteInput(): Promise<string> {
    return await this.idEntiteInput.getAttribute('value');
  }

  async setMessageInput(message: string): Promise<void> {
    await this.messageInput.sendKeys(message);
  }

  async getMessageInput(): Promise<string> {
    return await this.messageInput.getAttribute('value');
  }

  async creeParSelectLastOption(): Promise<void> {
    await this.creeParSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async creeParSelectOption(option: string): Promise<void> {
    await this.creeParSelect.sendKeys(option);
  }

  getCreeParSelect(): ElementFinder {
    return this.creeParSelect;
  }

  async getCreeParSelectedOption(): Promise<string> {
    return await this.creeParSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EvenementDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-evenement-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-evenement'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
