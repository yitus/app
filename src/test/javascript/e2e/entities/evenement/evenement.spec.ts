import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EvenementComponentsPage, EvenementDeleteDialog, EvenementUpdatePage } from './evenement.page-object';

const expect = chai.expect;

describe('Evenement e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let evenementComponentsPage: EvenementComponentsPage;
  let evenementUpdatePage: EvenementUpdatePage;
  let evenementDeleteDialog: EvenementDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Evenements', async () => {
    await navBarPage.goToEntity('evenement');
    evenementComponentsPage = new EvenementComponentsPage();
    await browser.wait(ec.visibilityOf(evenementComponentsPage.title), 5000);
    expect(await evenementComponentsPage.getTitle()).to.eq('yitusApp.evenement.home.title');
    await browser.wait(ec.or(ec.visibilityOf(evenementComponentsPage.entities), ec.visibilityOf(evenementComponentsPage.noResult)), 1000);
  });

  it('should load create Evenement page', async () => {
    await evenementComponentsPage.clickOnCreateButton();
    evenementUpdatePage = new EvenementUpdatePage();
    expect(await evenementUpdatePage.getPageTitle()).to.eq('yitusApp.evenement.home.createOrEditLabel');
    await evenementUpdatePage.cancel();
  });

  it('should create and save Evenements', async () => {
    const nbButtonsBeforeCreate = await evenementComponentsPage.countDeleteButtons();

    await evenementComponentsPage.clickOnCreateButton();

    await promise.all([
      evenementUpdatePage.statusEvenementSelectLastOption(),
      evenementUpdatePage.setDateEvenementInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      evenementUpdatePage.operationSelectLastOption(),
      evenementUpdatePage.entiteSelectLastOption(),
      evenementUpdatePage.setIdEntiteInput('5'),
      evenementUpdatePage.setMessageInput('message'),
      evenementUpdatePage.creeParSelectLastOption()
    ]);

    expect(await evenementUpdatePage.getDateEvenementInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateEvenement value to be equals to 2000-12-31'
    );
    expect(await evenementUpdatePage.getIdEntiteInput()).to.eq('5', 'Expected idEntite value to be equals to 5');
    expect(await evenementUpdatePage.getMessageInput()).to.eq('message', 'Expected Message value to be equals to message');

    await evenementUpdatePage.save();
    expect(await evenementUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await evenementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Evenement', async () => {
    const nbButtonsBeforeDelete = await evenementComponentsPage.countDeleteButtons();
    await evenementComponentsPage.clickOnLastDeleteButton();

    evenementDeleteDialog = new EvenementDeleteDialog();
    expect(await evenementDeleteDialog.getDialogTitle()).to.eq('yitusApp.evenement.delete.question');
    await evenementDeleteDialog.clickOnConfirmButton();

    expect(await evenementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
