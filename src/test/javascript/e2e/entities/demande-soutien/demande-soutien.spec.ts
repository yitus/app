import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DemandeSoutienComponentsPage, DemandeSoutienDeleteDialog, DemandeSoutienUpdatePage } from './demande-soutien.page-object';

const expect = chai.expect;

describe('DemandeSoutien e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let demandeSoutienComponentsPage: DemandeSoutienComponentsPage;
  let demandeSoutienUpdatePage: DemandeSoutienUpdatePage;
  let demandeSoutienDeleteDialog: DemandeSoutienDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load DemandeSoutiens', async () => {
    await navBarPage.goToEntity('demande-soutien');
    demandeSoutienComponentsPage = new DemandeSoutienComponentsPage();
    await browser.wait(ec.visibilityOf(demandeSoutienComponentsPage.title), 5000);
    expect(await demandeSoutienComponentsPage.getTitle()).to.eq('yitusApp.demandeSoutien.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(demandeSoutienComponentsPage.entities), ec.visibilityOf(demandeSoutienComponentsPage.noResult)),
      1000
    );
  });

  it('should load create DemandeSoutien page', async () => {
    await demandeSoutienComponentsPage.clickOnCreateButton();
    demandeSoutienUpdatePage = new DemandeSoutienUpdatePage();
    expect(await demandeSoutienUpdatePage.getPageTitle()).to.eq('yitusApp.demandeSoutien.home.createOrEditLabel');
    await demandeSoutienUpdatePage.cancel();
  });

  it('should create and save DemandeSoutiens', async () => {
    const nbButtonsBeforeCreate = await demandeSoutienComponentsPage.countDeleteButtons();

    await demandeSoutienComponentsPage.clickOnCreateButton();

    await promise.all([
      demandeSoutienUpdatePage.statutSelectLastOption(),
      demandeSoutienUpdatePage.setEtiquettesInput('etiquettes'),
      demandeSoutienUpdatePage.niveauSelectLastOption(),
      demandeSoutienUpdatePage.matiereSelectLastOption(),
      demandeSoutienUpdatePage.setDetailInput('detail'),
      demandeSoutienUpdatePage.setHorairesDisponibilitesInput('horairesDisponibilites'),
      demandeSoutienUpdatePage.setDateRendezVousInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      demandeSoutienUpdatePage.setNoteApprenantInput('5'),
      demandeSoutienUpdatePage.setNoteAidantInput('5'),
      demandeSoutienUpdatePage.setCommentaireApprenantInput('commentaireApprenant'),
      demandeSoutienUpdatePage.setCommentaireAidantInput('commentaireAidant'),
      demandeSoutienUpdatePage.setCommentaireEnseignantInput('commentaireEnseignant'),
      demandeSoutienUpdatePage.setCommentaireReferentInput('commentaireReferent'),
      demandeSoutienUpdatePage.setDateLimiteInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      demandeSoutienUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      demandeSoutienUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      demandeSoutienUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      demandeSoutienUpdatePage.apprenantSelectLastOption(),
      demandeSoutienUpdatePage.aidantSelectLastOption(),
      demandeSoutienUpdatePage.enseignantSelectLastOption(),
      demandeSoutienUpdatePage.referentSelectLastOption()
    ]);

    expect(await demandeSoutienUpdatePage.getEtiquettesInput()).to.eq('etiquettes', 'Expected Etiquettes value to be equals to etiquettes');
    expect(await demandeSoutienUpdatePage.getDetailInput()).to.eq('detail', 'Expected Detail value to be equals to detail');
    expect(await demandeSoutienUpdatePage.getHorairesDisponibilitesInput()).to.eq(
      'horairesDisponibilites',
      'Expected HorairesDisponibilites value to be equals to horairesDisponibilites'
    );
    expect(await demandeSoutienUpdatePage.getDateRendezVousInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateRendezVous value to be equals to 2000-12-31'
    );
    expect(await demandeSoutienUpdatePage.getNoteApprenantInput()).to.eq('5', 'Expected noteApprenant value to be equals to 5');
    expect(await demandeSoutienUpdatePage.getNoteAidantInput()).to.eq('5', 'Expected noteAidant value to be equals to 5');
    expect(await demandeSoutienUpdatePage.getCommentaireApprenantInput()).to.eq(
      'commentaireApprenant',
      'Expected CommentaireApprenant value to be equals to commentaireApprenant'
    );
    expect(await demandeSoutienUpdatePage.getCommentaireAidantInput()).to.eq(
      'commentaireAidant',
      'Expected CommentaireAidant value to be equals to commentaireAidant'
    );
    expect(await demandeSoutienUpdatePage.getCommentaireEnseignantInput()).to.eq(
      'commentaireEnseignant',
      'Expected CommentaireEnseignant value to be equals to commentaireEnseignant'
    );
    expect(await demandeSoutienUpdatePage.getCommentaireReferentInput()).to.eq(
      'commentaireReferent',
      'Expected CommentaireReferent value to be equals to commentaireReferent'
    );
    expect(await demandeSoutienUpdatePage.getDateLimiteInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateLimite value to be equals to 2000-12-31'
    );
    expect(await demandeSoutienUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await demandeSoutienUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await demandeSoutienUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await demandeSoutienUpdatePage.save();
    expect(await demandeSoutienUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await demandeSoutienComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last DemandeSoutien', async () => {
    const nbButtonsBeforeDelete = await demandeSoutienComponentsPage.countDeleteButtons();
    await demandeSoutienComponentsPage.clickOnLastDeleteButton();

    demandeSoutienDeleteDialog = new DemandeSoutienDeleteDialog();
    expect(await demandeSoutienDeleteDialog.getDialogTitle()).to.eq('yitusApp.demandeSoutien.delete.question');
    await demandeSoutienDeleteDialog.clickOnConfirmButton();

    expect(await demandeSoutienComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
