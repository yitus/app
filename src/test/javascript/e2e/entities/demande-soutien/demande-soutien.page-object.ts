import { element, by, ElementFinder } from 'protractor';

export class DemandeSoutienComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-demande-soutien div table .btn-danger'));
  title = element.all(by.css('jhi-demande-soutien div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class DemandeSoutienUpdatePage {
  pageTitle = element(by.id('jhi-demande-soutien-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  statutSelect = element(by.id('field_statut'));
  etiquettesInput = element(by.id('field_etiquettes'));
  niveauSelect = element(by.id('field_niveau'));
  matiereSelect = element(by.id('field_matiere'));
  detailInput = element(by.id('field_detail'));
  horairesDisponibilitesInput = element(by.id('field_horairesDisponibilites'));
  dateRendezVousInput = element(by.id('field_dateRendezVous'));
  noteApprenantInput = element(by.id('field_noteApprenant'));
  noteAidantInput = element(by.id('field_noteAidant'));
  commentaireApprenantInput = element(by.id('field_commentaireApprenant'));
  commentaireAidantInput = element(by.id('field_commentaireAidant'));
  commentaireEnseignantInput = element(by.id('field_commentaireEnseignant'));
  commentaireReferentInput = element(by.id('field_commentaireReferent'));
  dateLimiteInput = element(by.id('field_dateLimite'));
  dateCreationInput = element(by.id('field_dateCreation'));
  dateModificationInput = element(by.id('field_dateModification'));
  dateArchivageInput = element(by.id('field_dateArchivage'));

  apprenantSelect = element(by.id('field_apprenant'));
  aidantSelect = element(by.id('field_aidant'));
  enseignantSelect = element(by.id('field_enseignant'));
  referentSelect = element(by.id('field_referent'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setStatutSelect(statut: string): Promise<void> {
    await this.statutSelect.sendKeys(statut);
  }

  async getStatutSelect(): Promise<string> {
    return await this.statutSelect.element(by.css('option:checked')).getText();
  }

  async statutSelectLastOption(): Promise<void> {
    await this.statutSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setEtiquettesInput(etiquettes: string): Promise<void> {
    await this.etiquettesInput.sendKeys(etiquettes);
  }

  async getEtiquettesInput(): Promise<string> {
    return await this.etiquettesInput.getAttribute('value');
  }

  async setNiveauSelect(niveau: string): Promise<void> {
    await this.niveauSelect.sendKeys(niveau);
  }

  async getNiveauSelect(): Promise<string> {
    return await this.niveauSelect.element(by.css('option:checked')).getText();
  }

  async niveauSelectLastOption(): Promise<void> {
    await this.niveauSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMatiereSelect(matiere: string): Promise<void> {
    await this.matiereSelect.sendKeys(matiere);
  }

  async getMatiereSelect(): Promise<string> {
    return await this.matiereSelect.element(by.css('option:checked')).getText();
  }

  async matiereSelectLastOption(): Promise<void> {
    await this.matiereSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setDetailInput(detail: string): Promise<void> {
    await this.detailInput.sendKeys(detail);
  }

  async getDetailInput(): Promise<string> {
    return await this.detailInput.getAttribute('value');
  }

  async setHorairesDisponibilitesInput(horairesDisponibilites: string): Promise<void> {
    await this.horairesDisponibilitesInput.sendKeys(horairesDisponibilites);
  }

  async getHorairesDisponibilitesInput(): Promise<string> {
    return await this.horairesDisponibilitesInput.getAttribute('value');
  }

  async setDateRendezVousInput(dateRendezVous: string): Promise<void> {
    await this.dateRendezVousInput.sendKeys(dateRendezVous);
  }

  async getDateRendezVousInput(): Promise<string> {
    return await this.dateRendezVousInput.getAttribute('value');
  }

  async setNoteApprenantInput(noteApprenant: string): Promise<void> {
    await this.noteApprenantInput.sendKeys(noteApprenant);
  }

  async getNoteApprenantInput(): Promise<string> {
    return await this.noteApprenantInput.getAttribute('value');
  }

  async setNoteAidantInput(noteAidant: string): Promise<void> {
    await this.noteAidantInput.sendKeys(noteAidant);
  }

  async getNoteAidantInput(): Promise<string> {
    return await this.noteAidantInput.getAttribute('value');
  }

  async setCommentaireApprenantInput(commentaireApprenant: string): Promise<void> {
    await this.commentaireApprenantInput.sendKeys(commentaireApprenant);
  }

  async getCommentaireApprenantInput(): Promise<string> {
    return await this.commentaireApprenantInput.getAttribute('value');
  }

  async setCommentaireAidantInput(commentaireAidant: string): Promise<void> {
    await this.commentaireAidantInput.sendKeys(commentaireAidant);
  }

  async getCommentaireAidantInput(): Promise<string> {
    return await this.commentaireAidantInput.getAttribute('value');
  }

  async setCommentaireEnseignantInput(commentaireEnseignant: string): Promise<void> {
    await this.commentaireEnseignantInput.sendKeys(commentaireEnseignant);
  }

  async getCommentaireEnseignantInput(): Promise<string> {
    return await this.commentaireEnseignantInput.getAttribute('value');
  }

  async setCommentaireReferentInput(commentaireReferent: string): Promise<void> {
    await this.commentaireReferentInput.sendKeys(commentaireReferent);
  }

  async getCommentaireReferentInput(): Promise<string> {
    return await this.commentaireReferentInput.getAttribute('value');
  }

  async setDateLimiteInput(dateLimite: string): Promise<void> {
    await this.dateLimiteInput.sendKeys(dateLimite);
  }

  async getDateLimiteInput(): Promise<string> {
    return await this.dateLimiteInput.getAttribute('value');
  }

  async setDateCreationInput(dateCreation: string): Promise<void> {
    await this.dateCreationInput.sendKeys(dateCreation);
  }

  async getDateCreationInput(): Promise<string> {
    return await this.dateCreationInput.getAttribute('value');
  }

  async setDateModificationInput(dateModification: string): Promise<void> {
    await this.dateModificationInput.sendKeys(dateModification);
  }

  async getDateModificationInput(): Promise<string> {
    return await this.dateModificationInput.getAttribute('value');
  }

  async setDateArchivageInput(dateArchivage: string): Promise<void> {
    await this.dateArchivageInput.sendKeys(dateArchivage);
  }

  async getDateArchivageInput(): Promise<string> {
    return await this.dateArchivageInput.getAttribute('value');
  }

  async apprenantSelectLastOption(): Promise<void> {
    await this.apprenantSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async apprenantSelectOption(option: string): Promise<void> {
    await this.apprenantSelect.sendKeys(option);
  }

  getApprenantSelect(): ElementFinder {
    return this.apprenantSelect;
  }

  async getApprenantSelectedOption(): Promise<string> {
    return await this.apprenantSelect.element(by.css('option:checked')).getText();
  }

  async aidantSelectLastOption(): Promise<void> {
    await this.aidantSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async aidantSelectOption(option: string): Promise<void> {
    await this.aidantSelect.sendKeys(option);
  }

  getAidantSelect(): ElementFinder {
    return this.aidantSelect;
  }

  async getAidantSelectedOption(): Promise<string> {
    return await this.aidantSelect.element(by.css('option:checked')).getText();
  }

  async enseignantSelectLastOption(): Promise<void> {
    await this.enseignantSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async enseignantSelectOption(option: string): Promise<void> {
    await this.enseignantSelect.sendKeys(option);
  }

  getEnseignantSelect(): ElementFinder {
    return this.enseignantSelect;
  }

  async getEnseignantSelectedOption(): Promise<string> {
    return await this.enseignantSelect.element(by.css('option:checked')).getText();
  }

  async referentSelectLastOption(): Promise<void> {
    await this.referentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async referentSelectOption(option: string): Promise<void> {
    await this.referentSelect.sendKeys(option);
  }

  getReferentSelect(): ElementFinder {
    return this.referentSelect;
  }

  async getReferentSelectedOption(): Promise<string> {
    return await this.referentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class DemandeSoutienDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-demandeSoutien-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-demandeSoutien'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
