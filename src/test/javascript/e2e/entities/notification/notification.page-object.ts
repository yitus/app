import { element, by, ElementFinder } from 'protractor';

export class NotificationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-notification div table .btn-danger'));
  title = element.all(by.css('jhi-notification div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class NotificationUpdatePage {
  pageTitle = element(by.id('jhi-notification-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  statusNotificationSelect = element(by.id('field_statusNotification'));
  messageInput = element(by.id('field_message'));
  dateCreationInput = element(by.id('field_dateCreation'));
  dateLectureInput = element(by.id('field_dateLecture'));
  dateArchivageInput = element(by.id('field_dateArchivage'));

  destineASelect = element(by.id('field_destineA'));
  creeParSelect = element(by.id('field_creePar'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setStatusNotificationSelect(statusNotification: string): Promise<void> {
    await this.statusNotificationSelect.sendKeys(statusNotification);
  }

  async getStatusNotificationSelect(): Promise<string> {
    return await this.statusNotificationSelect.element(by.css('option:checked')).getText();
  }

  async statusNotificationSelectLastOption(): Promise<void> {
    await this.statusNotificationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMessageInput(message: string): Promise<void> {
    await this.messageInput.sendKeys(message);
  }

  async getMessageInput(): Promise<string> {
    return await this.messageInput.getAttribute('value');
  }

  async setDateCreationInput(dateCreation: string): Promise<void> {
    await this.dateCreationInput.sendKeys(dateCreation);
  }

  async getDateCreationInput(): Promise<string> {
    return await this.dateCreationInput.getAttribute('value');
  }

  async setDateLectureInput(dateLecture: string): Promise<void> {
    await this.dateLectureInput.sendKeys(dateLecture);
  }

  async getDateLectureInput(): Promise<string> {
    return await this.dateLectureInput.getAttribute('value');
  }

  async setDateArchivageInput(dateArchivage: string): Promise<void> {
    await this.dateArchivageInput.sendKeys(dateArchivage);
  }

  async getDateArchivageInput(): Promise<string> {
    return await this.dateArchivageInput.getAttribute('value');
  }

  async destineASelectLastOption(): Promise<void> {
    await this.destineASelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async destineASelectOption(option: string): Promise<void> {
    await this.destineASelect.sendKeys(option);
  }

  getDestineASelect(): ElementFinder {
    return this.destineASelect;
  }

  async getDestineASelectedOption(): Promise<string> {
    return await this.destineASelect.element(by.css('option:checked')).getText();
  }

  async creeParSelectLastOption(): Promise<void> {
    await this.creeParSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async creeParSelectOption(option: string): Promise<void> {
    await this.creeParSelect.sendKeys(option);
  }

  getCreeParSelect(): ElementFinder {
    return this.creeParSelect;
  }

  async getCreeParSelectedOption(): Promise<string> {
    return await this.creeParSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class NotificationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-notification-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-notification'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
