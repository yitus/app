import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ApprenantComponentsPage, ApprenantDeleteDialog, ApprenantUpdatePage } from './apprenant.page-object';

const expect = chai.expect;

describe('Apprenant e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let apprenantComponentsPage: ApprenantComponentsPage;
  let apprenantUpdatePage: ApprenantUpdatePage;
  let apprenantDeleteDialog: ApprenantDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Apprenants', async () => {
    await navBarPage.goToEntity('apprenant');
    apprenantComponentsPage = new ApprenantComponentsPage();
    await browser.wait(ec.visibilityOf(apprenantComponentsPage.title), 5000);
    expect(await apprenantComponentsPage.getTitle()).to.eq('yitusApp.apprenant.home.title');
    await browser.wait(ec.or(ec.visibilityOf(apprenantComponentsPage.entities), ec.visibilityOf(apprenantComponentsPage.noResult)), 1000);
  });

  it('should load create Apprenant page', async () => {
    await apprenantComponentsPage.clickOnCreateButton();
    apprenantUpdatePage = new ApprenantUpdatePage();
    expect(await apprenantUpdatePage.getPageTitle()).to.eq('yitusApp.apprenant.home.createOrEditLabel');
    await apprenantUpdatePage.cancel();
  });

  it('should create and save Apprenants', async () => {
    const nbButtonsBeforeCreate = await apprenantComponentsPage.countDeleteButtons();

    await apprenantComponentsPage.clickOnCreateButton();

    await promise.all([
      apprenantUpdatePage.niveauSelectLastOption(),
      apprenantUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      apprenantUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      apprenantUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      apprenantUpdatePage.utilisateurSelectLastOption(),
      apprenantUpdatePage.contactSelectLastOption(),
      apprenantUpdatePage.photoSelectLastOption(),
      apprenantUpdatePage.etablissementSelectLastOption()
    ]);

    expect(await apprenantUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await apprenantUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await apprenantUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await apprenantUpdatePage.save();
    expect(await apprenantUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await apprenantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Apprenant', async () => {
    const nbButtonsBeforeDelete = await apprenantComponentsPage.countDeleteButtons();
    await apprenantComponentsPage.clickOnLastDeleteButton();

    apprenantDeleteDialog = new ApprenantDeleteDialog();
    expect(await apprenantDeleteDialog.getDialogTitle()).to.eq('yitusApp.apprenant.delete.question');
    await apprenantDeleteDialog.clickOnConfirmButton();

    expect(await apprenantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
