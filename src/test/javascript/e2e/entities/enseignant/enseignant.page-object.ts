import { element, by, ElementFinder } from 'protractor';

export class EnseignantComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-enseignant div table .btn-danger'));
  title = element.all(by.css('jhi-enseignant div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class EnseignantUpdatePage {
  pageTitle = element(by.id('jhi-enseignant-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  matiere1Select = element(by.id('field_matiere1'));
  matiere2Select = element(by.id('field_matiere2'));
  matiere3Select = element(by.id('field_matiere3'));
  matiere4Select = element(by.id('field_matiere4'));
  dateCreationInput = element(by.id('field_dateCreation'));
  dateModificationInput = element(by.id('field_dateModification'));
  dateArchivageInput = element(by.id('field_dateArchivage'));

  utilisateurSelect = element(by.id('field_utilisateur'));
  contactSelect = element(by.id('field_contact'));
  photoSelect = element(by.id('field_photo'));
  etablissementSelect = element(by.id('field_etablissement'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setMatiere1Select(matiere1: string): Promise<void> {
    await this.matiere1Select.sendKeys(matiere1);
  }

  async getMatiere1Select(): Promise<string> {
    return await this.matiere1Select.element(by.css('option:checked')).getText();
  }

  async matiere1SelectLastOption(): Promise<void> {
    await this.matiere1Select
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMatiere2Select(matiere2: string): Promise<void> {
    await this.matiere2Select.sendKeys(matiere2);
  }

  async getMatiere2Select(): Promise<string> {
    return await this.matiere2Select.element(by.css('option:checked')).getText();
  }

  async matiere2SelectLastOption(): Promise<void> {
    await this.matiere2Select
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMatiere3Select(matiere3: string): Promise<void> {
    await this.matiere3Select.sendKeys(matiere3);
  }

  async getMatiere3Select(): Promise<string> {
    return await this.matiere3Select.element(by.css('option:checked')).getText();
  }

  async matiere3SelectLastOption(): Promise<void> {
    await this.matiere3Select
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMatiere4Select(matiere4: string): Promise<void> {
    await this.matiere4Select.sendKeys(matiere4);
  }

  async getMatiere4Select(): Promise<string> {
    return await this.matiere4Select.element(by.css('option:checked')).getText();
  }

  async matiere4SelectLastOption(): Promise<void> {
    await this.matiere4Select
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setDateCreationInput(dateCreation: string): Promise<void> {
    await this.dateCreationInput.sendKeys(dateCreation);
  }

  async getDateCreationInput(): Promise<string> {
    return await this.dateCreationInput.getAttribute('value');
  }

  async setDateModificationInput(dateModification: string): Promise<void> {
    await this.dateModificationInput.sendKeys(dateModification);
  }

  async getDateModificationInput(): Promise<string> {
    return await this.dateModificationInput.getAttribute('value');
  }

  async setDateArchivageInput(dateArchivage: string): Promise<void> {
    await this.dateArchivageInput.sendKeys(dateArchivage);
  }

  async getDateArchivageInput(): Promise<string> {
    return await this.dateArchivageInput.getAttribute('value');
  }

  async utilisateurSelectLastOption(): Promise<void> {
    await this.utilisateurSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async utilisateurSelectOption(option: string): Promise<void> {
    await this.utilisateurSelect.sendKeys(option);
  }

  getUtilisateurSelect(): ElementFinder {
    return this.utilisateurSelect;
  }

  async getUtilisateurSelectedOption(): Promise<string> {
    return await this.utilisateurSelect.element(by.css('option:checked')).getText();
  }

  async contactSelectLastOption(): Promise<void> {
    await this.contactSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async contactSelectOption(option: string): Promise<void> {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect(): ElementFinder {
    return this.contactSelect;
  }

  async getContactSelectedOption(): Promise<string> {
    return await this.contactSelect.element(by.css('option:checked')).getText();
  }

  async photoSelectLastOption(): Promise<void> {
    await this.photoSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async photoSelectOption(option: string): Promise<void> {
    await this.photoSelect.sendKeys(option);
  }

  getPhotoSelect(): ElementFinder {
    return this.photoSelect;
  }

  async getPhotoSelectedOption(): Promise<string> {
    return await this.photoSelect.element(by.css('option:checked')).getText();
  }

  async etablissementSelectLastOption(): Promise<void> {
    await this.etablissementSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async etablissementSelectOption(option: string): Promise<void> {
    await this.etablissementSelect.sendKeys(option);
  }

  getEtablissementSelect(): ElementFinder {
    return this.etablissementSelect;
  }

  async getEtablissementSelectedOption(): Promise<string> {
    return await this.etablissementSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EnseignantDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-enseignant-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-enseignant'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
