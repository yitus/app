import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EnseignantComponentsPage, EnseignantDeleteDialog, EnseignantUpdatePage } from './enseignant.page-object';

const expect = chai.expect;

describe('Enseignant e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let enseignantComponentsPage: EnseignantComponentsPage;
  let enseignantUpdatePage: EnseignantUpdatePage;
  let enseignantDeleteDialog: EnseignantDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Enseignants', async () => {
    await navBarPage.goToEntity('enseignant');
    enseignantComponentsPage = new EnseignantComponentsPage();
    await browser.wait(ec.visibilityOf(enseignantComponentsPage.title), 5000);
    expect(await enseignantComponentsPage.getTitle()).to.eq('yitusApp.enseignant.home.title');
    await browser.wait(ec.or(ec.visibilityOf(enseignantComponentsPage.entities), ec.visibilityOf(enseignantComponentsPage.noResult)), 1000);
  });

  it('should load create Enseignant page', async () => {
    await enseignantComponentsPage.clickOnCreateButton();
    enseignantUpdatePage = new EnseignantUpdatePage();
    expect(await enseignantUpdatePage.getPageTitle()).to.eq('yitusApp.enseignant.home.createOrEditLabel');
    await enseignantUpdatePage.cancel();
  });

  it('should create and save Enseignants', async () => {
    const nbButtonsBeforeCreate = await enseignantComponentsPage.countDeleteButtons();

    await enseignantComponentsPage.clickOnCreateButton();

    await promise.all([
      enseignantUpdatePage.matiere1SelectLastOption(),
      enseignantUpdatePage.matiere2SelectLastOption(),
      enseignantUpdatePage.matiere3SelectLastOption(),
      enseignantUpdatePage.matiere4SelectLastOption(),
      enseignantUpdatePage.setDateCreationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      enseignantUpdatePage.setDateModificationInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      enseignantUpdatePage.setDateArchivageInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      enseignantUpdatePage.utilisateurSelectLastOption(),
      enseignantUpdatePage.contactSelectLastOption(),
      enseignantUpdatePage.photoSelectLastOption(),
      enseignantUpdatePage.etablissementSelectLastOption()
    ]);

    expect(await enseignantUpdatePage.getDateCreationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateCreation value to be equals to 2000-12-31'
    );
    expect(await enseignantUpdatePage.getDateModificationInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateModification value to be equals to 2000-12-31'
    );
    expect(await enseignantUpdatePage.getDateArchivageInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateArchivage value to be equals to 2000-12-31'
    );

    await enseignantUpdatePage.save();
    expect(await enseignantUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await enseignantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Enseignant', async () => {
    const nbButtonsBeforeDelete = await enseignantComponentsPage.countDeleteButtons();
    await enseignantComponentsPage.clickOnLastDeleteButton();

    enseignantDeleteDialog = new EnseignantDeleteDialog();
    expect(await enseignantDeleteDialog.getDialogTitle()).to.eq('yitusApp.enseignant.delete.question');
    await enseignantDeleteDialog.clickOnConfirmButton();

    expect(await enseignantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
