import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { DemandeSoutienService } from 'app/entities/demande-soutien/demande-soutien.service';
import { IDemandeSoutien, DemandeSoutien } from 'app/shared/model/demande-soutien.model';
import { TypeStatut } from 'app/shared/model/enumerations/type-statut.model';
import { TypeNiveau } from 'app/shared/model/enumerations/type-niveau.model';
import { TypeMatiere } from 'app/shared/model/enumerations/type-matiere.model';

describe('Service Tests', () => {
  describe('DemandeSoutien Service', () => {
    let injector: TestBed;
    let service: DemandeSoutienService;
    let httpMock: HttpTestingController;
    let elemDefault: IDemandeSoutien;
    let expectedResult: IDemandeSoutien | IDemandeSoutien[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(DemandeSoutienService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new DemandeSoutien(
        0,
        TypeStatut.EN_COURS,
        'AAAAAAA',
        TypeNiveau.SIXIEME,
        TypeMatiere.MATHS,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        currentDate,
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateRendezVous: currentDate.format(DATE_TIME_FORMAT),
            dateLimite: currentDate.format(DATE_TIME_FORMAT),
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DemandeSoutien', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateRendezVous: currentDate.format(DATE_TIME_FORMAT),
            dateLimite: currentDate.format(DATE_TIME_FORMAT),
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateRendezVous: currentDate,
            dateLimite: currentDate,
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.create(new DemandeSoutien()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DemandeSoutien', () => {
        const returnedFromService = Object.assign(
          {
            statut: 'BBBBBB',
            etiquettes: 'BBBBBB',
            niveau: 'BBBBBB',
            matiere: 'BBBBBB',
            detail: 'BBBBBB',
            horairesDisponibilites: 'BBBBBB',
            dateRendezVous: currentDate.format(DATE_TIME_FORMAT),
            noteApprenant: 1,
            noteAidant: 1,
            commentaireApprenant: 'BBBBBB',
            commentaireAidant: 'BBBBBB',
            commentaireEnseignant: 'BBBBBB',
            commentaireReferent: 'BBBBBB',
            dateLimite: currentDate.format(DATE_TIME_FORMAT),
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateRendezVous: currentDate,
            dateLimite: currentDate,
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DemandeSoutien', () => {
        const returnedFromService = Object.assign(
          {
            statut: 'BBBBBB',
            etiquettes: 'BBBBBB',
            niveau: 'BBBBBB',
            matiere: 'BBBBBB',
            detail: 'BBBBBB',
            horairesDisponibilites: 'BBBBBB',
            dateRendezVous: currentDate.format(DATE_TIME_FORMAT),
            noteApprenant: 1,
            noteAidant: 1,
            commentaireApprenant: 'BBBBBB',
            commentaireAidant: 'BBBBBB',
            commentaireEnseignant: 'BBBBBB',
            commentaireReferent: 'BBBBBB',
            dateLimite: currentDate.format(DATE_TIME_FORMAT),
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateRendezVous: currentDate,
            dateLimite: currentDate,
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DemandeSoutien', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
