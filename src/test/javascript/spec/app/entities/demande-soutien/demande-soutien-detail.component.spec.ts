import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { YitusTestModule } from '../../../test.module';
import { DemandeSoutienDetailComponent } from 'app/entities/demande-soutien/demande-soutien-detail.component';
import { DemandeSoutien } from 'app/shared/model/demande-soutien.model';

describe('Component Tests', () => {
  describe('DemandeSoutien Management Detail Component', () => {
    let comp: DemandeSoutienDetailComponent;
    let fixture: ComponentFixture<DemandeSoutienDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ demandeSoutien: new DemandeSoutien(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [DemandeSoutienDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DemandeSoutienDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DemandeSoutienDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load demandeSoutien on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.demandeSoutien).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
