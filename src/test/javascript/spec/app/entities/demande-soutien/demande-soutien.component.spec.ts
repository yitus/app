import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { YitusTestModule } from '../../../test.module';
import { DemandeSoutienComponent } from 'app/entities/demande-soutien/demande-soutien.component';
import { DemandeSoutienService } from 'app/entities/demande-soutien/demande-soutien.service';
import { DemandeSoutien } from 'app/shared/model/demande-soutien.model';

describe('Component Tests', () => {
  describe('DemandeSoutien Management Component', () => {
    let comp: DemandeSoutienComponent;
    let fixture: ComponentFixture<DemandeSoutienComponent>;
    let service: DemandeSoutienService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [DemandeSoutienComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(DemandeSoutienComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DemandeSoutienComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DemandeSoutienService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DemandeSoutien(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.demandeSoutiens && comp.demandeSoutiens[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DemandeSoutien(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.demandeSoutiens && comp.demandeSoutiens[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
