import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { YitusTestModule } from '../../../test.module';
import { DemandeSoutienUpdateComponent } from 'app/entities/demande-soutien/demande-soutien-update.component';
import { DemandeSoutienService } from 'app/entities/demande-soutien/demande-soutien.service';
import { DemandeSoutien } from 'app/shared/model/demande-soutien.model';

describe('Component Tests', () => {
  describe('DemandeSoutien Management Update Component', () => {
    let comp: DemandeSoutienUpdateComponent;
    let fixture: ComponentFixture<DemandeSoutienUpdateComponent>;
    let service: DemandeSoutienService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [DemandeSoutienUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DemandeSoutienUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DemandeSoutienUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DemandeSoutienService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DemandeSoutien(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DemandeSoutien();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
