import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { YitusTestModule } from '../../../test.module';
import { AidantDetailComponent } from 'app/entities/aidant/aidant-detail.component';
import { Aidant } from 'app/shared/model/aidant.model';

describe('Component Tests', () => {
  describe('Aidant Management Detail Component', () => {
    let comp: AidantDetailComponent;
    let fixture: ComponentFixture<AidantDetailComponent>;
    const route = ({ data: of({ aidant: new Aidant(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [AidantDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AidantDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AidantDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load aidant on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.aidant).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
