import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { YitusTestModule } from '../../../test.module';
import { AidantUpdateComponent } from 'app/entities/aidant/aidant-update.component';
import { AidantService } from 'app/entities/aidant/aidant.service';
import { Aidant } from 'app/shared/model/aidant.model';

describe('Component Tests', () => {
  describe('Aidant Management Update Component', () => {
    let comp: AidantUpdateComponent;
    let fixture: ComponentFixture<AidantUpdateComponent>;
    let service: AidantService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [AidantUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AidantUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AidantUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AidantService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Aidant(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Aidant();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
