import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { EtablissementService } from 'app/entities/etablissement/etablissement.service';
import { IEtablissement, Etablissement } from 'app/shared/model/etablissement.model';
import { TypeEtablissement } from 'app/shared/model/enumerations/type-etablissement.model';

describe('Service Tests', () => {
  describe('Etablissement Service', () => {
    let injector: TestBed;
    let service: EtablissementService;
    let httpMock: HttpTestingController;
    let elemDefault: IEtablissement;
    let expectedResult: IEtablissement | IEtablissement[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(EtablissementService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Etablissement(
        0,
        TypeEtablissement.COLLEGE,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Etablissement', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.create(new Etablissement()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Etablissement', () => {
        const returnedFromService = Object.assign(
          {
            typeEtablissement: 'BBBBBB',
            nom: 'BBBBBB',
            siteweb: 'BBBBBB',
            adresse: 'BBBBBB',
            ville: 'BBBBBB',
            codePostal: 'BBBBBB',
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Etablissement', () => {
        const returnedFromService = Object.assign(
          {
            typeEtablissement: 'BBBBBB',
            nom: 'BBBBBB',
            siteweb: 'BBBBBB',
            adresse: 'BBBBBB',
            ville: 'BBBBBB',
            codePostal: 'BBBBBB',
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Etablissement', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
