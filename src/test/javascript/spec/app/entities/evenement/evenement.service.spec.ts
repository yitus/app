import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { EvenementService } from 'app/entities/evenement/evenement.service';
import { IEvenement, Evenement } from 'app/shared/model/evenement.model';
import { TypeStatusEvenement } from 'app/shared/model/enumerations/type-status-evenement.model';
import { TypeOperation } from 'app/shared/model/enumerations/type-operation.model';
import { TypeEntite } from 'app/shared/model/enumerations/type-entite.model';

describe('Service Tests', () => {
  describe('Evenement Service', () => {
    let injector: TestBed;
    let service: EvenementService;
    let httpMock: HttpTestingController;
    let elemDefault: IEvenement;
    let expectedResult: IEvenement | IEvenement[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(EvenementService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Evenement(0, TypeStatusEvenement.INFO, currentDate, TypeOperation.CREATION, TypeEntite.AIDANT, 0, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateEvenement: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Evenement', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateEvenement: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateEvenement: currentDate
          },
          returnedFromService
        );

        service.create(new Evenement()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Evenement', () => {
        const returnedFromService = Object.assign(
          {
            statusEvenement: 'BBBBBB',
            dateEvenement: currentDate.format(DATE_TIME_FORMAT),
            operation: 'BBBBBB',
            entite: 'BBBBBB',
            idEntite: 1,
            message: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateEvenement: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Evenement', () => {
        const returnedFromService = Object.assign(
          {
            statusEvenement: 'BBBBBB',
            dateEvenement: currentDate.format(DATE_TIME_FORMAT),
            operation: 'BBBBBB',
            entite: 'BBBBBB',
            idEntite: 1,
            message: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateEvenement: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Evenement', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
