import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ApprenantService } from 'app/entities/apprenant/apprenant.service';
import { IApprenant, Apprenant } from 'app/shared/model/apprenant.model';
import { TypeNiveau } from 'app/shared/model/enumerations/type-niveau.model';

describe('Service Tests', () => {
  describe('Apprenant Service', () => {
    let injector: TestBed;
    let service: ApprenantService;
    let httpMock: HttpTestingController;
    let elemDefault: IApprenant;
    let expectedResult: IApprenant | IApprenant[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ApprenantService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Apprenant(0, TypeNiveau.SIXIEME, currentDate, currentDate, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Apprenant', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.create(new Apprenant()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Apprenant', () => {
        const returnedFromService = Object.assign(
          {
            niveau: 'BBBBBB',
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Apprenant', () => {
        const returnedFromService = Object.assign(
          {
            niveau: 'BBBBBB',
            dateCreation: currentDate.format(DATE_TIME_FORMAT),
            dateModification: currentDate.format(DATE_TIME_FORMAT),
            dateArchivage: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreation: currentDate,
            dateModification: currentDate,
            dateArchivage: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Apprenant', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
