import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { YitusTestModule } from '../../../test.module';
import { ChefEtablissementDetailComponent } from 'app/entities/chef-etablissement/chef-etablissement-detail.component';
import { ChefEtablissement } from 'app/shared/model/chef-etablissement.model';

describe('Component Tests', () => {
  describe('ChefEtablissement Management Detail Component', () => {
    let comp: ChefEtablissementDetailComponent;
    let fixture: ComponentFixture<ChefEtablissementDetailComponent>;
    const route = ({ data: of({ chefEtablissement: new ChefEtablissement(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [ChefEtablissementDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ChefEtablissementDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChefEtablissementDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load chefEtablissement on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.chefEtablissement).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
