import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { YitusTestModule } from '../../../test.module';
import { ChefEtablissementUpdateComponent } from 'app/entities/chef-etablissement/chef-etablissement-update.component';
import { ChefEtablissementService } from 'app/entities/chef-etablissement/chef-etablissement.service';
import { ChefEtablissement } from 'app/shared/model/chef-etablissement.model';

describe('Component Tests', () => {
  describe('ChefEtablissement Management Update Component', () => {
    let comp: ChefEtablissementUpdateComponent;
    let fixture: ComponentFixture<ChefEtablissementUpdateComponent>;
    let service: ChefEtablissementService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [YitusTestModule],
        declarations: [ChefEtablissementUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ChefEtablissementUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChefEtablissementUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ChefEtablissementService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ChefEtablissement(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ChefEtablissement();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
