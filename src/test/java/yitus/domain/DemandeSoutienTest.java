package yitus.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import yitus.web.rest.TestUtil;

public class DemandeSoutienTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DemandeSoutien.class);
        DemandeSoutien demandeSoutien1 = new DemandeSoutien();
        demandeSoutien1.setId(1L);
        DemandeSoutien demandeSoutien2 = new DemandeSoutien();
        demandeSoutien2.setId(demandeSoutien1.getId());
        assertThat(demandeSoutien1).isEqualTo(demandeSoutien2);
        demandeSoutien2.setId(2L);
        assertThat(demandeSoutien1).isNotEqualTo(demandeSoutien2);
        demandeSoutien1.setId(null);
        assertThat(demandeSoutien1).isNotEqualTo(demandeSoutien2);
    }
}
