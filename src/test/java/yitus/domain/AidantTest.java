package yitus.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import yitus.web.rest.TestUtil;

public class AidantTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Aidant.class);
        Aidant aidant1 = new Aidant();
        aidant1.setId(1L);
        Aidant aidant2 = new Aidant();
        aidant2.setId(aidant1.getId());
        assertThat(aidant1).isEqualTo(aidant2);
        aidant2.setId(2L);
        assertThat(aidant1).isNotEqualTo(aidant2);
        aidant1.setId(null);
        assertThat(aidant1).isNotEqualTo(aidant2);
    }
}
