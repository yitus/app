package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Notification;
import yitus.domain.User;
import yitus.repository.NotificationRepository;
import yitus.service.NotificationService;
import yitus.service.dto.NotificationDTO;
import yitus.service.mapper.NotificationMapper;
import yitus.service.dto.NotificationCriteria;
import yitus.service.NotificationQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import yitus.domain.enumeration.TypeStatusNotification;
/**
 * Integration tests for the {@link NotificationResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class NotificationResourceIT {

    private static final TypeStatusNotification DEFAULT_STATUS_NOTIFICATION = TypeStatusNotification.INFO;
    private static final TypeStatusNotification UPDATED_STATUS_NOTIFICATION = TypeStatusNotification.ALERTE;

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_LECTURE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_LECTURE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private NotificationMapper notificationMapper;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private NotificationQueryService notificationQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNotificationMockMvc;

    private Notification notification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notification createEntity(EntityManager em) {
        Notification notification = new Notification()
            .statusNotification(DEFAULT_STATUS_NOTIFICATION)
            .message(DEFAULT_MESSAGE)
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateLecture(DEFAULT_DATE_LECTURE)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return notification;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notification createUpdatedEntity(EntityManager em) {
        Notification notification = new Notification()
            .statusNotification(UPDATED_STATUS_NOTIFICATION)
            .message(UPDATED_MESSAGE)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateLecture(UPDATED_DATE_LECTURE)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return notification;
    }

    @BeforeEach
    public void initTest() {
        notification = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotification() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);
        restNotificationMockMvc.perform(post("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(notificationDTO)))
            .andExpect(status().isCreated());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeCreate + 1);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getStatusNotification()).isEqualTo(DEFAULT_STATUS_NOTIFICATION);
        assertThat(testNotification.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testNotification.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testNotification.getDateLecture()).isEqualTo(DEFAULT_DATE_LECTURE);
        assertThat(testNotification.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createNotificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // Create the Notification with an existing ID
        notification.setId(1L);
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationMockMvc.perform(post("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(notificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStatusNotificationIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRepository.findAll().size();
        // set the field null
        notification.setStatusNotification(null);

        // Create the Notification, which fails.
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        restNotificationMockMvc.perform(post("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(notificationDTO)))
            .andExpect(status().isBadRequest());

        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateCreationIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRepository.findAll().size();
        // set the field null
        notification.setDateCreation(null);

        // Create the Notification, which fails.
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        restNotificationMockMvc.perform(post("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(notificationDTO)))
            .andExpect(status().isBadRequest());

        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotifications() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList
        restNotificationMockMvc.perform(get("/api/notifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
            .andExpect(jsonPath("$.[*].statusNotification").value(hasItem(DEFAULT_STATUS_NOTIFICATION.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateLecture").value(hasItem(DEFAULT_DATE_LECTURE.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", notification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(notification.getId().intValue()))
            .andExpect(jsonPath("$.statusNotification").value(DEFAULT_STATUS_NOTIFICATION.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateLecture").value(DEFAULT_DATE_LECTURE.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getNotificationsByIdFiltering() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        Long id = notification.getId();

        defaultNotificationShouldBeFound("id.equals=" + id);
        defaultNotificationShouldNotBeFound("id.notEquals=" + id);

        defaultNotificationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNotificationShouldNotBeFound("id.greaterThan=" + id);

        defaultNotificationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNotificationShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNotificationsByStatusNotificationIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where statusNotification equals to DEFAULT_STATUS_NOTIFICATION
        defaultNotificationShouldBeFound("statusNotification.equals=" + DEFAULT_STATUS_NOTIFICATION);

        // Get all the notificationList where statusNotification equals to UPDATED_STATUS_NOTIFICATION
        defaultNotificationShouldNotBeFound("statusNotification.equals=" + UPDATED_STATUS_NOTIFICATION);
    }

    @Test
    @Transactional
    public void getAllNotificationsByStatusNotificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where statusNotification not equals to DEFAULT_STATUS_NOTIFICATION
        defaultNotificationShouldNotBeFound("statusNotification.notEquals=" + DEFAULT_STATUS_NOTIFICATION);

        // Get all the notificationList where statusNotification not equals to UPDATED_STATUS_NOTIFICATION
        defaultNotificationShouldBeFound("statusNotification.notEquals=" + UPDATED_STATUS_NOTIFICATION);
    }

    @Test
    @Transactional
    public void getAllNotificationsByStatusNotificationIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where statusNotification in DEFAULT_STATUS_NOTIFICATION or UPDATED_STATUS_NOTIFICATION
        defaultNotificationShouldBeFound("statusNotification.in=" + DEFAULT_STATUS_NOTIFICATION + "," + UPDATED_STATUS_NOTIFICATION);

        // Get all the notificationList where statusNotification equals to UPDATED_STATUS_NOTIFICATION
        defaultNotificationShouldNotBeFound("statusNotification.in=" + UPDATED_STATUS_NOTIFICATION);
    }

    @Test
    @Transactional
    public void getAllNotificationsByStatusNotificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where statusNotification is not null
        defaultNotificationShouldBeFound("statusNotification.specified=true");

        // Get all the notificationList where statusNotification is null
        defaultNotificationShouldNotBeFound("statusNotification.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message equals to DEFAULT_MESSAGE
        defaultNotificationShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message equals to UPDATED_MESSAGE
        defaultNotificationShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message not equals to DEFAULT_MESSAGE
        defaultNotificationShouldNotBeFound("message.notEquals=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message not equals to UPDATED_MESSAGE
        defaultNotificationShouldBeFound("message.notEquals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultNotificationShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the notificationList where message equals to UPDATED_MESSAGE
        defaultNotificationShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message is not null
        defaultNotificationShouldBeFound("message.specified=true");

        // Get all the notificationList where message is null
        defaultNotificationShouldNotBeFound("message.specified=false");
    }
                @Test
    @Transactional
    public void getAllNotificationsByMessageContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message contains DEFAULT_MESSAGE
        defaultNotificationShouldBeFound("message.contains=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message contains UPDATED_MESSAGE
        defaultNotificationShouldNotBeFound("message.contains=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByMessageNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where message does not contain DEFAULT_MESSAGE
        defaultNotificationShouldNotBeFound("message.doesNotContain=" + DEFAULT_MESSAGE);

        // Get all the notificationList where message does not contain UPDATED_MESSAGE
        defaultNotificationShouldBeFound("message.doesNotContain=" + UPDATED_MESSAGE);
    }


    @Test
    @Transactional
    public void getAllNotificationsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultNotificationShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the notificationList where dateCreation equals to UPDATED_DATE_CREATION
        defaultNotificationShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultNotificationShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the notificationList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultNotificationShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultNotificationShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the notificationList where dateCreation equals to UPDATED_DATE_CREATION
        defaultNotificationShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateCreation is not null
        defaultNotificationShouldBeFound("dateCreation.specified=true");

        // Get all the notificationList where dateCreation is null
        defaultNotificationShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateLectureIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateLecture equals to DEFAULT_DATE_LECTURE
        defaultNotificationShouldBeFound("dateLecture.equals=" + DEFAULT_DATE_LECTURE);

        // Get all the notificationList where dateLecture equals to UPDATED_DATE_LECTURE
        defaultNotificationShouldNotBeFound("dateLecture.equals=" + UPDATED_DATE_LECTURE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateLectureIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateLecture not equals to DEFAULT_DATE_LECTURE
        defaultNotificationShouldNotBeFound("dateLecture.notEquals=" + DEFAULT_DATE_LECTURE);

        // Get all the notificationList where dateLecture not equals to UPDATED_DATE_LECTURE
        defaultNotificationShouldBeFound("dateLecture.notEquals=" + UPDATED_DATE_LECTURE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateLectureIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateLecture in DEFAULT_DATE_LECTURE or UPDATED_DATE_LECTURE
        defaultNotificationShouldBeFound("dateLecture.in=" + DEFAULT_DATE_LECTURE + "," + UPDATED_DATE_LECTURE);

        // Get all the notificationList where dateLecture equals to UPDATED_DATE_LECTURE
        defaultNotificationShouldNotBeFound("dateLecture.in=" + UPDATED_DATE_LECTURE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateLectureIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateLecture is not null
        defaultNotificationShouldBeFound("dateLecture.specified=true");

        // Get all the notificationList where dateLecture is null
        defaultNotificationShouldNotBeFound("dateLecture.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultNotificationShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the notificationList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultNotificationShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultNotificationShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the notificationList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultNotificationShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultNotificationShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the notificationList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultNotificationShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateArchivage is not null
        defaultNotificationShouldBeFound("dateArchivage.specified=true");

        // Get all the notificationList where dateArchivage is null
        defaultNotificationShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationsByDestineAIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);
        User destineA = UserResourceIT.createEntity(em);
        em.persist(destineA);
        em.flush();
        notification.setDestineA(destineA);
        notificationRepository.saveAndFlush(notification);
        Long destineAId = destineA.getId();

        // Get all the notificationList where destineA equals to destineAId
        defaultNotificationShouldBeFound("destineAId.equals=" + destineAId);

        // Get all the notificationList where destineA equals to destineAId + 1
        defaultNotificationShouldNotBeFound("destineAId.equals=" + (destineAId + 1));
    }


    @Test
    @Transactional
    public void getAllNotificationsByCreeParIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);
        User creePar = UserResourceIT.createEntity(em);
        em.persist(creePar);
        em.flush();
        notification.setCreePar(creePar);
        notificationRepository.saveAndFlush(notification);
        Long creeParId = creePar.getId();

        // Get all the notificationList where creePar equals to creeParId
        defaultNotificationShouldBeFound("creeParId.equals=" + creeParId);

        // Get all the notificationList where creePar equals to creeParId + 1
        defaultNotificationShouldNotBeFound("creeParId.equals=" + (creeParId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNotificationShouldBeFound(String filter) throws Exception {
        restNotificationMockMvc.perform(get("/api/notifications?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
            .andExpect(jsonPath("$.[*].statusNotification").value(hasItem(DEFAULT_STATUS_NOTIFICATION.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateLecture").value(hasItem(DEFAULT_DATE_LECTURE.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restNotificationMockMvc.perform(get("/api/notifications/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNotificationShouldNotBeFound(String filter) throws Exception {
        restNotificationMockMvc.perform(get("/api/notifications?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNotificationMockMvc.perform(get("/api/notifications/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNotification() throws Exception {
        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification
        Notification updatedNotification = notificationRepository.findById(notification.getId()).get();
        // Disconnect from session so that the updates on updatedNotification are not directly saved in db
        em.detach(updatedNotification);
        updatedNotification
            .statusNotification(UPDATED_STATUS_NOTIFICATION)
            .message(UPDATED_MESSAGE)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateLecture(UPDATED_DATE_LECTURE)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        NotificationDTO notificationDTO = notificationMapper.toDto(updatedNotification);

        restNotificationMockMvc.perform(put("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(notificationDTO)))
            .andExpect(status().isOk());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getStatusNotification()).isEqualTo(UPDATED_STATUS_NOTIFICATION);
        assertThat(testNotification.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testNotification.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testNotification.getDateLecture()).isEqualTo(UPDATED_DATE_LECTURE);
        assertThat(testNotification.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationMockMvc.perform(put("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(notificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeDelete = notificationRepository.findAll().size();

        // Delete the notification
        restNotificationMockMvc.perform(delete("/api/notifications/{id}", notification.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
