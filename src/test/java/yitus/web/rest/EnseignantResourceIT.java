package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Enseignant;
import yitus.domain.User;
import yitus.domain.Contact;
import yitus.domain.Image;
import yitus.domain.Etablissement;
import yitus.repository.EnseignantRepository;
import yitus.service.EnseignantService;
import yitus.service.dto.EnseignantDTO;
import yitus.service.mapper.EnseignantMapper;
import yitus.service.dto.EnseignantCriteria;
import yitus.service.EnseignantQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import yitus.domain.enumeration.TypeMatiere;
import yitus.domain.enumeration.TypeMatiere;
import yitus.domain.enumeration.TypeMatiere;
import yitus.domain.enumeration.TypeMatiere;
/**
 * Integration tests for the {@link EnseignantResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class EnseignantResourceIT {

    private static final TypeMatiere DEFAULT_MATIERE_1 = TypeMatiere.MATHS;
    private static final TypeMatiere UPDATED_MATIERE_1 = TypeMatiere.FRANCAIS;

    private static final TypeMatiere DEFAULT_MATIERE_2 = TypeMatiere.MATHS;
    private static final TypeMatiere UPDATED_MATIERE_2 = TypeMatiere.FRANCAIS;

    private static final TypeMatiere DEFAULT_MATIERE_3 = TypeMatiere.MATHS;
    private static final TypeMatiere UPDATED_MATIERE_3 = TypeMatiere.FRANCAIS;

    private static final TypeMatiere DEFAULT_MATIERE_4 = TypeMatiere.MATHS;
    private static final TypeMatiere UPDATED_MATIERE_4 = TypeMatiere.FRANCAIS;

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private EnseignantRepository enseignantRepository;

    @Autowired
    private EnseignantMapper enseignantMapper;

    @Autowired
    private EnseignantService enseignantService;

    @Autowired
    private EnseignantQueryService enseignantQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEnseignantMockMvc;

    private Enseignant enseignant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Enseignant createEntity(EntityManager em) {
        Enseignant enseignant = new Enseignant()
            .matiere1(DEFAULT_MATIERE_1)
            .matiere2(DEFAULT_MATIERE_2)
            .matiere3(DEFAULT_MATIERE_3)
            .matiere4(DEFAULT_MATIERE_4)
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return enseignant;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Enseignant createUpdatedEntity(EntityManager em) {
        Enseignant enseignant = new Enseignant()
            .matiere1(UPDATED_MATIERE_1)
            .matiere2(UPDATED_MATIERE_2)
            .matiere3(UPDATED_MATIERE_3)
            .matiere4(UPDATED_MATIERE_4)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return enseignant;
    }

    @BeforeEach
    public void initTest() {
        enseignant = createEntity(em);
    }

    @Test
    @Transactional
    public void createEnseignant() throws Exception {
        int databaseSizeBeforeCreate = enseignantRepository.findAll().size();

        // Create the Enseignant
        EnseignantDTO enseignantDTO = enseignantMapper.toDto(enseignant);
        restEnseignantMockMvc.perform(post("/api/enseignants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(enseignantDTO)))
            .andExpect(status().isCreated());

        // Validate the Enseignant in the database
        List<Enseignant> enseignantList = enseignantRepository.findAll();
        assertThat(enseignantList).hasSize(databaseSizeBeforeCreate + 1);
        Enseignant testEnseignant = enseignantList.get(enseignantList.size() - 1);
        assertThat(testEnseignant.getMatiere1()).isEqualTo(DEFAULT_MATIERE_1);
        assertThat(testEnseignant.getMatiere2()).isEqualTo(DEFAULT_MATIERE_2);
        assertThat(testEnseignant.getMatiere3()).isEqualTo(DEFAULT_MATIERE_3);
        assertThat(testEnseignant.getMatiere4()).isEqualTo(DEFAULT_MATIERE_4);
        assertThat(testEnseignant.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testEnseignant.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testEnseignant.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createEnseignantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = enseignantRepository.findAll().size();

        // Create the Enseignant with an existing ID
        enseignant.setId(1L);
        EnseignantDTO enseignantDTO = enseignantMapper.toDto(enseignant);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEnseignantMockMvc.perform(post("/api/enseignants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(enseignantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Enseignant in the database
        List<Enseignant> enseignantList = enseignantRepository.findAll();
        assertThat(enseignantList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMatiere1IsRequired() throws Exception {
        int databaseSizeBeforeTest = enseignantRepository.findAll().size();
        // set the field null
        enseignant.setMatiere1(null);

        // Create the Enseignant, which fails.
        EnseignantDTO enseignantDTO = enseignantMapper.toDto(enseignant);

        restEnseignantMockMvc.perform(post("/api/enseignants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(enseignantDTO)))
            .andExpect(status().isBadRequest());

        List<Enseignant> enseignantList = enseignantRepository.findAll();
        assertThat(enseignantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEnseignants() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList
        restEnseignantMockMvc.perform(get("/api/enseignants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(enseignant.getId().intValue())))
            .andExpect(jsonPath("$.[*].matiere1").value(hasItem(DEFAULT_MATIERE_1.toString())))
            .andExpect(jsonPath("$.[*].matiere2").value(hasItem(DEFAULT_MATIERE_2.toString())))
            .andExpect(jsonPath("$.[*].matiere3").value(hasItem(DEFAULT_MATIERE_3.toString())))
            .andExpect(jsonPath("$.[*].matiere4").value(hasItem(DEFAULT_MATIERE_4.toString())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getEnseignant() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get the enseignant
        restEnseignantMockMvc.perform(get("/api/enseignants/{id}", enseignant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(enseignant.getId().intValue()))
            .andExpect(jsonPath("$.matiere1").value(DEFAULT_MATIERE_1.toString()))
            .andExpect(jsonPath("$.matiere2").value(DEFAULT_MATIERE_2.toString()))
            .andExpect(jsonPath("$.matiere3").value(DEFAULT_MATIERE_3.toString()))
            .andExpect(jsonPath("$.matiere4").value(DEFAULT_MATIERE_4.toString()))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getEnseignantsByIdFiltering() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        Long id = enseignant.getId();

        defaultEnseignantShouldBeFound("id.equals=" + id);
        defaultEnseignantShouldNotBeFound("id.notEquals=" + id);

        defaultEnseignantShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEnseignantShouldNotBeFound("id.greaterThan=" + id);

        defaultEnseignantShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEnseignantShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllEnseignantsByMatiere1IsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere1 equals to DEFAULT_MATIERE_1
        defaultEnseignantShouldBeFound("matiere1.equals=" + DEFAULT_MATIERE_1);

        // Get all the enseignantList where matiere1 equals to UPDATED_MATIERE_1
        defaultEnseignantShouldNotBeFound("matiere1.equals=" + UPDATED_MATIERE_1);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere1 not equals to DEFAULT_MATIERE_1
        defaultEnseignantShouldNotBeFound("matiere1.notEquals=" + DEFAULT_MATIERE_1);

        // Get all the enseignantList where matiere1 not equals to UPDATED_MATIERE_1
        defaultEnseignantShouldBeFound("matiere1.notEquals=" + UPDATED_MATIERE_1);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere1IsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere1 in DEFAULT_MATIERE_1 or UPDATED_MATIERE_1
        defaultEnseignantShouldBeFound("matiere1.in=" + DEFAULT_MATIERE_1 + "," + UPDATED_MATIERE_1);

        // Get all the enseignantList where matiere1 equals to UPDATED_MATIERE_1
        defaultEnseignantShouldNotBeFound("matiere1.in=" + UPDATED_MATIERE_1);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere1IsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere1 is not null
        defaultEnseignantShouldBeFound("matiere1.specified=true");

        // Get all the enseignantList where matiere1 is null
        defaultEnseignantShouldNotBeFound("matiere1.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere2IsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere2 equals to DEFAULT_MATIERE_2
        defaultEnseignantShouldBeFound("matiere2.equals=" + DEFAULT_MATIERE_2);

        // Get all the enseignantList where matiere2 equals to UPDATED_MATIERE_2
        defaultEnseignantShouldNotBeFound("matiere2.equals=" + UPDATED_MATIERE_2);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere2 not equals to DEFAULT_MATIERE_2
        defaultEnseignantShouldNotBeFound("matiere2.notEquals=" + DEFAULT_MATIERE_2);

        // Get all the enseignantList where matiere2 not equals to UPDATED_MATIERE_2
        defaultEnseignantShouldBeFound("matiere2.notEquals=" + UPDATED_MATIERE_2);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere2IsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere2 in DEFAULT_MATIERE_2 or UPDATED_MATIERE_2
        defaultEnseignantShouldBeFound("matiere2.in=" + DEFAULT_MATIERE_2 + "," + UPDATED_MATIERE_2);

        // Get all the enseignantList where matiere2 equals to UPDATED_MATIERE_2
        defaultEnseignantShouldNotBeFound("matiere2.in=" + UPDATED_MATIERE_2);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere2IsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere2 is not null
        defaultEnseignantShouldBeFound("matiere2.specified=true");

        // Get all the enseignantList where matiere2 is null
        defaultEnseignantShouldNotBeFound("matiere2.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere3IsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere3 equals to DEFAULT_MATIERE_3
        defaultEnseignantShouldBeFound("matiere3.equals=" + DEFAULT_MATIERE_3);

        // Get all the enseignantList where matiere3 equals to UPDATED_MATIERE_3
        defaultEnseignantShouldNotBeFound("matiere3.equals=" + UPDATED_MATIERE_3);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere3 not equals to DEFAULT_MATIERE_3
        defaultEnseignantShouldNotBeFound("matiere3.notEquals=" + DEFAULT_MATIERE_3);

        // Get all the enseignantList where matiere3 not equals to UPDATED_MATIERE_3
        defaultEnseignantShouldBeFound("matiere3.notEquals=" + UPDATED_MATIERE_3);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere3IsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere3 in DEFAULT_MATIERE_3 or UPDATED_MATIERE_3
        defaultEnseignantShouldBeFound("matiere3.in=" + DEFAULT_MATIERE_3 + "," + UPDATED_MATIERE_3);

        // Get all the enseignantList where matiere3 equals to UPDATED_MATIERE_3
        defaultEnseignantShouldNotBeFound("matiere3.in=" + UPDATED_MATIERE_3);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere3IsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere3 is not null
        defaultEnseignantShouldBeFound("matiere3.specified=true");

        // Get all the enseignantList where matiere3 is null
        defaultEnseignantShouldNotBeFound("matiere3.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere4IsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere4 equals to DEFAULT_MATIERE_4
        defaultEnseignantShouldBeFound("matiere4.equals=" + DEFAULT_MATIERE_4);

        // Get all the enseignantList where matiere4 equals to UPDATED_MATIERE_4
        defaultEnseignantShouldNotBeFound("matiere4.equals=" + UPDATED_MATIERE_4);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere4IsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere4 not equals to DEFAULT_MATIERE_4
        defaultEnseignantShouldNotBeFound("matiere4.notEquals=" + DEFAULT_MATIERE_4);

        // Get all the enseignantList where matiere4 not equals to UPDATED_MATIERE_4
        defaultEnseignantShouldBeFound("matiere4.notEquals=" + UPDATED_MATIERE_4);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere4IsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere4 in DEFAULT_MATIERE_4 or UPDATED_MATIERE_4
        defaultEnseignantShouldBeFound("matiere4.in=" + DEFAULT_MATIERE_4 + "," + UPDATED_MATIERE_4);

        // Get all the enseignantList where matiere4 equals to UPDATED_MATIERE_4
        defaultEnseignantShouldNotBeFound("matiere4.in=" + UPDATED_MATIERE_4);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByMatiere4IsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where matiere4 is not null
        defaultEnseignantShouldBeFound("matiere4.specified=true");

        // Get all the enseignantList where matiere4 is null
        defaultEnseignantShouldNotBeFound("matiere4.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultEnseignantShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the enseignantList where dateCreation equals to UPDATED_DATE_CREATION
        defaultEnseignantShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultEnseignantShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the enseignantList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultEnseignantShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultEnseignantShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the enseignantList where dateCreation equals to UPDATED_DATE_CREATION
        defaultEnseignantShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateCreation is not null
        defaultEnseignantShouldBeFound("dateCreation.specified=true");

        // Get all the enseignantList where dateCreation is null
        defaultEnseignantShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultEnseignantShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the enseignantList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultEnseignantShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultEnseignantShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the enseignantList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultEnseignantShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultEnseignantShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the enseignantList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultEnseignantShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateModification is not null
        defaultEnseignantShouldBeFound("dateModification.specified=true");

        // Get all the enseignantList where dateModification is null
        defaultEnseignantShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultEnseignantShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the enseignantList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultEnseignantShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultEnseignantShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the enseignantList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultEnseignantShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultEnseignantShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the enseignantList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultEnseignantShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllEnseignantsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        // Get all the enseignantList where dateArchivage is not null
        defaultEnseignantShouldBeFound("dateArchivage.specified=true");

        // Get all the enseignantList where dateArchivage is null
        defaultEnseignantShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllEnseignantsByUtilisateurIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);
        User utilisateur = UserResourceIT.createEntity(em);
        em.persist(utilisateur);
        em.flush();
        enseignant.setUtilisateur(utilisateur);
        enseignantRepository.saveAndFlush(enseignant);
        Long utilisateurId = utilisateur.getId();

        // Get all the enseignantList where utilisateur equals to utilisateurId
        defaultEnseignantShouldBeFound("utilisateurId.equals=" + utilisateurId);

        // Get all the enseignantList where utilisateur equals to utilisateurId + 1
        defaultEnseignantShouldNotBeFound("utilisateurId.equals=" + (utilisateurId + 1));
    }


    @Test
    @Transactional
    public void getAllEnseignantsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        enseignant.setContact(contact);
        enseignantRepository.saveAndFlush(enseignant);
        Long contactId = contact.getId();

        // Get all the enseignantList where contact equals to contactId
        defaultEnseignantShouldBeFound("contactId.equals=" + contactId);

        // Get all the enseignantList where contact equals to contactId + 1
        defaultEnseignantShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }


    @Test
    @Transactional
    public void getAllEnseignantsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);
        Image photo = ImageResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        enseignant.setPhoto(photo);
        enseignantRepository.saveAndFlush(enseignant);
        Long photoId = photo.getId();

        // Get all the enseignantList where photo equals to photoId
        defaultEnseignantShouldBeFound("photoId.equals=" + photoId);

        // Get all the enseignantList where photo equals to photoId + 1
        defaultEnseignantShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }


    @Test
    @Transactional
    public void getAllEnseignantsByEtablissementIsEqualToSomething() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);
        Etablissement etablissement = EtablissementResourceIT.createEntity(em);
        em.persist(etablissement);
        em.flush();
        enseignant.setEtablissement(etablissement);
        enseignantRepository.saveAndFlush(enseignant);
        Long etablissementId = etablissement.getId();

        // Get all the enseignantList where etablissement equals to etablissementId
        defaultEnseignantShouldBeFound("etablissementId.equals=" + etablissementId);

        // Get all the enseignantList where etablissement equals to etablissementId + 1
        defaultEnseignantShouldNotBeFound("etablissementId.equals=" + (etablissementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEnseignantShouldBeFound(String filter) throws Exception {
        restEnseignantMockMvc.perform(get("/api/enseignants?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(enseignant.getId().intValue())))
            .andExpect(jsonPath("$.[*].matiere1").value(hasItem(DEFAULT_MATIERE_1.toString())))
            .andExpect(jsonPath("$.[*].matiere2").value(hasItem(DEFAULT_MATIERE_2.toString())))
            .andExpect(jsonPath("$.[*].matiere3").value(hasItem(DEFAULT_MATIERE_3.toString())))
            .andExpect(jsonPath("$.[*].matiere4").value(hasItem(DEFAULT_MATIERE_4.toString())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restEnseignantMockMvc.perform(get("/api/enseignants/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEnseignantShouldNotBeFound(String filter) throws Exception {
        restEnseignantMockMvc.perform(get("/api/enseignants?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEnseignantMockMvc.perform(get("/api/enseignants/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEnseignant() throws Exception {
        // Get the enseignant
        restEnseignantMockMvc.perform(get("/api/enseignants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEnseignant() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        int databaseSizeBeforeUpdate = enseignantRepository.findAll().size();

        // Update the enseignant
        Enseignant updatedEnseignant = enseignantRepository.findById(enseignant.getId()).get();
        // Disconnect from session so that the updates on updatedEnseignant are not directly saved in db
        em.detach(updatedEnseignant);
        updatedEnseignant
            .matiere1(UPDATED_MATIERE_1)
            .matiere2(UPDATED_MATIERE_2)
            .matiere3(UPDATED_MATIERE_3)
            .matiere4(UPDATED_MATIERE_4)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        EnseignantDTO enseignantDTO = enseignantMapper.toDto(updatedEnseignant);

        restEnseignantMockMvc.perform(put("/api/enseignants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(enseignantDTO)))
            .andExpect(status().isOk());

        // Validate the Enseignant in the database
        List<Enseignant> enseignantList = enseignantRepository.findAll();
        assertThat(enseignantList).hasSize(databaseSizeBeforeUpdate);
        Enseignant testEnseignant = enseignantList.get(enseignantList.size() - 1);
        assertThat(testEnseignant.getMatiere1()).isEqualTo(UPDATED_MATIERE_1);
        assertThat(testEnseignant.getMatiere2()).isEqualTo(UPDATED_MATIERE_2);
        assertThat(testEnseignant.getMatiere3()).isEqualTo(UPDATED_MATIERE_3);
        assertThat(testEnseignant.getMatiere4()).isEqualTo(UPDATED_MATIERE_4);
        assertThat(testEnseignant.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testEnseignant.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testEnseignant.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingEnseignant() throws Exception {
        int databaseSizeBeforeUpdate = enseignantRepository.findAll().size();

        // Create the Enseignant
        EnseignantDTO enseignantDTO = enseignantMapper.toDto(enseignant);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEnseignantMockMvc.perform(put("/api/enseignants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(enseignantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Enseignant in the database
        List<Enseignant> enseignantList = enseignantRepository.findAll();
        assertThat(enseignantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEnseignant() throws Exception {
        // Initialize the database
        enseignantRepository.saveAndFlush(enseignant);

        int databaseSizeBeforeDelete = enseignantRepository.findAll().size();

        // Delete the enseignant
        restEnseignantMockMvc.perform(delete("/api/enseignants/{id}", enseignant.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Enseignant> enseignantList = enseignantRepository.findAll();
        assertThat(enseignantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
