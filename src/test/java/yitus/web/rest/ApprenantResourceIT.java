package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Apprenant;
import yitus.domain.User;
import yitus.domain.Contact;
import yitus.domain.Image;
import yitus.domain.Etablissement;
import yitus.repository.ApprenantRepository;
import yitus.service.ApprenantService;
import yitus.service.dto.ApprenantDTO;
import yitus.service.mapper.ApprenantMapper;
import yitus.service.dto.ApprenantCriteria;
import yitus.service.ApprenantQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import yitus.domain.enumeration.TypeNiveau;
/**
 * Integration tests for the {@link ApprenantResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ApprenantResourceIT {

    private static final TypeNiveau DEFAULT_NIVEAU = TypeNiveau.SIXIEME;
    private static final TypeNiveau UPDATED_NIVEAU = TypeNiveau.CINQUIEME;

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ApprenantRepository apprenantRepository;

    @Autowired
    private ApprenantMapper apprenantMapper;

    @Autowired
    private ApprenantService apprenantService;

    @Autowired
    private ApprenantQueryService apprenantQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restApprenantMockMvc;

    private Apprenant apprenant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Apprenant createEntity(EntityManager em) {
        Apprenant apprenant = new Apprenant()
            .niveau(DEFAULT_NIVEAU)
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return apprenant;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Apprenant createUpdatedEntity(EntityManager em) {
        Apprenant apprenant = new Apprenant()
            .niveau(UPDATED_NIVEAU)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return apprenant;
    }

    @BeforeEach
    public void initTest() {
        apprenant = createEntity(em);
    }

    @Test
    @Transactional
    public void createApprenant() throws Exception {
        int databaseSizeBeforeCreate = apprenantRepository.findAll().size();

        // Create the Apprenant
        ApprenantDTO apprenantDTO = apprenantMapper.toDto(apprenant);
        restApprenantMockMvc.perform(post("/api/apprenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(apprenantDTO)))
            .andExpect(status().isCreated());

        // Validate the Apprenant in the database
        List<Apprenant> apprenantList = apprenantRepository.findAll();
        assertThat(apprenantList).hasSize(databaseSizeBeforeCreate + 1);
        Apprenant testApprenant = apprenantList.get(apprenantList.size() - 1);
        assertThat(testApprenant.getNiveau()).isEqualTo(DEFAULT_NIVEAU);
        assertThat(testApprenant.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testApprenant.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testApprenant.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createApprenantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = apprenantRepository.findAll().size();

        // Create the Apprenant with an existing ID
        apprenant.setId(1L);
        ApprenantDTO apprenantDTO = apprenantMapper.toDto(apprenant);

        // An entity with an existing ID cannot be created, so this API call must fail
        restApprenantMockMvc.perform(post("/api/apprenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(apprenantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Apprenant in the database
        List<Apprenant> apprenantList = apprenantRepository.findAll();
        assertThat(apprenantList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNiveauIsRequired() throws Exception {
        int databaseSizeBeforeTest = apprenantRepository.findAll().size();
        // set the field null
        apprenant.setNiveau(null);

        // Create the Apprenant, which fails.
        ApprenantDTO apprenantDTO = apprenantMapper.toDto(apprenant);

        restApprenantMockMvc.perform(post("/api/apprenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(apprenantDTO)))
            .andExpect(status().isBadRequest());

        List<Apprenant> apprenantList = apprenantRepository.findAll();
        assertThat(apprenantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllApprenants() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList
        restApprenantMockMvc.perform(get("/api/apprenants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(apprenant.getId().intValue())))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU.toString())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getApprenant() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get the apprenant
        restApprenantMockMvc.perform(get("/api/apprenants/{id}", apprenant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(apprenant.getId().intValue()))
            .andExpect(jsonPath("$.niveau").value(DEFAULT_NIVEAU.toString()))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getApprenantsByIdFiltering() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        Long id = apprenant.getId();

        defaultApprenantShouldBeFound("id.equals=" + id);
        defaultApprenantShouldNotBeFound("id.notEquals=" + id);

        defaultApprenantShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultApprenantShouldNotBeFound("id.greaterThan=" + id);

        defaultApprenantShouldBeFound("id.lessThanOrEqual=" + id);
        defaultApprenantShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllApprenantsByNiveauIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where niveau equals to DEFAULT_NIVEAU
        defaultApprenantShouldBeFound("niveau.equals=" + DEFAULT_NIVEAU);

        // Get all the apprenantList where niveau equals to UPDATED_NIVEAU
        defaultApprenantShouldNotBeFound("niveau.equals=" + UPDATED_NIVEAU);
    }

    @Test
    @Transactional
    public void getAllApprenantsByNiveauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where niveau not equals to DEFAULT_NIVEAU
        defaultApprenantShouldNotBeFound("niveau.notEquals=" + DEFAULT_NIVEAU);

        // Get all the apprenantList where niveau not equals to UPDATED_NIVEAU
        defaultApprenantShouldBeFound("niveau.notEquals=" + UPDATED_NIVEAU);
    }

    @Test
    @Transactional
    public void getAllApprenantsByNiveauIsInShouldWork() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where niveau in DEFAULT_NIVEAU or UPDATED_NIVEAU
        defaultApprenantShouldBeFound("niveau.in=" + DEFAULT_NIVEAU + "," + UPDATED_NIVEAU);

        // Get all the apprenantList where niveau equals to UPDATED_NIVEAU
        defaultApprenantShouldNotBeFound("niveau.in=" + UPDATED_NIVEAU);
    }

    @Test
    @Transactional
    public void getAllApprenantsByNiveauIsNullOrNotNull() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where niveau is not null
        defaultApprenantShouldBeFound("niveau.specified=true");

        // Get all the apprenantList where niveau is null
        defaultApprenantShouldNotBeFound("niveau.specified=false");
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultApprenantShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the apprenantList where dateCreation equals to UPDATED_DATE_CREATION
        defaultApprenantShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultApprenantShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the apprenantList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultApprenantShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultApprenantShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the apprenantList where dateCreation equals to UPDATED_DATE_CREATION
        defaultApprenantShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateCreation is not null
        defaultApprenantShouldBeFound("dateCreation.specified=true");

        // Get all the apprenantList where dateCreation is null
        defaultApprenantShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultApprenantShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the apprenantList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultApprenantShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultApprenantShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the apprenantList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultApprenantShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultApprenantShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the apprenantList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultApprenantShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateModification is not null
        defaultApprenantShouldBeFound("dateModification.specified=true");

        // Get all the apprenantList where dateModification is null
        defaultApprenantShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultApprenantShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the apprenantList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultApprenantShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultApprenantShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the apprenantList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultApprenantShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultApprenantShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the apprenantList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultApprenantShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllApprenantsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        // Get all the apprenantList where dateArchivage is not null
        defaultApprenantShouldBeFound("dateArchivage.specified=true");

        // Get all the apprenantList where dateArchivage is null
        defaultApprenantShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllApprenantsByUtilisateurIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);
        User utilisateur = UserResourceIT.createEntity(em);
        em.persist(utilisateur);
        em.flush();
        apprenant.setUtilisateur(utilisateur);
        apprenantRepository.saveAndFlush(apprenant);
        Long utilisateurId = utilisateur.getId();

        // Get all the apprenantList where utilisateur equals to utilisateurId
        defaultApprenantShouldBeFound("utilisateurId.equals=" + utilisateurId);

        // Get all the apprenantList where utilisateur equals to utilisateurId + 1
        defaultApprenantShouldNotBeFound("utilisateurId.equals=" + (utilisateurId + 1));
    }


    @Test
    @Transactional
    public void getAllApprenantsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        apprenant.setContact(contact);
        apprenantRepository.saveAndFlush(apprenant);
        Long contactId = contact.getId();

        // Get all the apprenantList where contact equals to contactId
        defaultApprenantShouldBeFound("contactId.equals=" + contactId);

        // Get all the apprenantList where contact equals to contactId + 1
        defaultApprenantShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }


    @Test
    @Transactional
    public void getAllApprenantsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);
        Image photo = ImageResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        apprenant.setPhoto(photo);
        apprenantRepository.saveAndFlush(apprenant);
        Long photoId = photo.getId();

        // Get all the apprenantList where photo equals to photoId
        defaultApprenantShouldBeFound("photoId.equals=" + photoId);

        // Get all the apprenantList where photo equals to photoId + 1
        defaultApprenantShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }


    @Test
    @Transactional
    public void getAllApprenantsByEtablissementIsEqualToSomething() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);
        Etablissement etablissement = EtablissementResourceIT.createEntity(em);
        em.persist(etablissement);
        em.flush();
        apprenant.setEtablissement(etablissement);
        apprenantRepository.saveAndFlush(apprenant);
        Long etablissementId = etablissement.getId();

        // Get all the apprenantList where etablissement equals to etablissementId
        defaultApprenantShouldBeFound("etablissementId.equals=" + etablissementId);

        // Get all the apprenantList where etablissement equals to etablissementId + 1
        defaultApprenantShouldNotBeFound("etablissementId.equals=" + (etablissementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultApprenantShouldBeFound(String filter) throws Exception {
        restApprenantMockMvc.perform(get("/api/apprenants?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(apprenant.getId().intValue())))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU.toString())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restApprenantMockMvc.perform(get("/api/apprenants/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultApprenantShouldNotBeFound(String filter) throws Exception {
        restApprenantMockMvc.perform(get("/api/apprenants?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restApprenantMockMvc.perform(get("/api/apprenants/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingApprenant() throws Exception {
        // Get the apprenant
        restApprenantMockMvc.perform(get("/api/apprenants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateApprenant() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        int databaseSizeBeforeUpdate = apprenantRepository.findAll().size();

        // Update the apprenant
        Apprenant updatedApprenant = apprenantRepository.findById(apprenant.getId()).get();
        // Disconnect from session so that the updates on updatedApprenant are not directly saved in db
        em.detach(updatedApprenant);
        updatedApprenant
            .niveau(UPDATED_NIVEAU)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        ApprenantDTO apprenantDTO = apprenantMapper.toDto(updatedApprenant);

        restApprenantMockMvc.perform(put("/api/apprenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(apprenantDTO)))
            .andExpect(status().isOk());

        // Validate the Apprenant in the database
        List<Apprenant> apprenantList = apprenantRepository.findAll();
        assertThat(apprenantList).hasSize(databaseSizeBeforeUpdate);
        Apprenant testApprenant = apprenantList.get(apprenantList.size() - 1);
        assertThat(testApprenant.getNiveau()).isEqualTo(UPDATED_NIVEAU);
        assertThat(testApprenant.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testApprenant.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testApprenant.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingApprenant() throws Exception {
        int databaseSizeBeforeUpdate = apprenantRepository.findAll().size();

        // Create the Apprenant
        ApprenantDTO apprenantDTO = apprenantMapper.toDto(apprenant);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApprenantMockMvc.perform(put("/api/apprenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(apprenantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Apprenant in the database
        List<Apprenant> apprenantList = apprenantRepository.findAll();
        assertThat(apprenantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteApprenant() throws Exception {
        // Initialize the database
        apprenantRepository.saveAndFlush(apprenant);

        int databaseSizeBeforeDelete = apprenantRepository.findAll().size();

        // Delete the apprenant
        restApprenantMockMvc.perform(delete("/api/apprenants/{id}", apprenant.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Apprenant> apprenantList = apprenantRepository.findAll();
        assertThat(apprenantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
