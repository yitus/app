package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Aidant;
import yitus.domain.User;
import yitus.domain.Contact;
import yitus.domain.Image;
import yitus.domain.Etablissement;
import yitus.repository.AidantRepository;
import yitus.service.AidantService;
import yitus.service.dto.AidantDTO;
import yitus.service.mapper.AidantMapper;
import yitus.service.dto.AidantCriteria;
import yitus.service.AidantQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AidantResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AidantResourceIT {

    private static final String DEFAULT_CURSUS_ACTUEL = "AAAAAAAAAA";
    private static final String UPDATED_CURSUS_ACTUEL = "BBBBBBBBBB";

    private static final Integer DEFAULT_NOMBRE_ANNEES_POST_BAC = 1;
    private static final Integer UPDATED_NOMBRE_ANNEES_POST_BAC = 2;
    private static final Integer SMALLER_NOMBRE_ANNEES_POST_BAC = 1 - 1;

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private AidantRepository aidantRepository;

    @Autowired
    private AidantMapper aidantMapper;

    @Autowired
    private AidantService aidantService;

    @Autowired
    private AidantQueryService aidantQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAidantMockMvc;

    private Aidant aidant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Aidant createEntity(EntityManager em) {
        Aidant aidant = new Aidant()
            .cursusActuel(DEFAULT_CURSUS_ACTUEL)
            .nombreAnneesPostBAC(DEFAULT_NOMBRE_ANNEES_POST_BAC)
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return aidant;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Aidant createUpdatedEntity(EntityManager em) {
        Aidant aidant = new Aidant()
            .cursusActuel(UPDATED_CURSUS_ACTUEL)
            .nombreAnneesPostBAC(UPDATED_NOMBRE_ANNEES_POST_BAC)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return aidant;
    }

    @BeforeEach
    public void initTest() {
        aidant = createEntity(em);
    }

    @Test
    @Transactional
    public void createAidant() throws Exception {
        int databaseSizeBeforeCreate = aidantRepository.findAll().size();

        // Create the Aidant
        AidantDTO aidantDTO = aidantMapper.toDto(aidant);
        restAidantMockMvc.perform(post("/api/aidants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aidantDTO)))
            .andExpect(status().isCreated());

        // Validate the Aidant in the database
        List<Aidant> aidantList = aidantRepository.findAll();
        assertThat(aidantList).hasSize(databaseSizeBeforeCreate + 1);
        Aidant testAidant = aidantList.get(aidantList.size() - 1);
        assertThat(testAidant.getCursusActuel()).isEqualTo(DEFAULT_CURSUS_ACTUEL);
        assertThat(testAidant.getNombreAnneesPostBAC()).isEqualTo(DEFAULT_NOMBRE_ANNEES_POST_BAC);
        assertThat(testAidant.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testAidant.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testAidant.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createAidantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aidantRepository.findAll().size();

        // Create the Aidant with an existing ID
        aidant.setId(1L);
        AidantDTO aidantDTO = aidantMapper.toDto(aidant);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAidantMockMvc.perform(post("/api/aidants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aidantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Aidant in the database
        List<Aidant> aidantList = aidantRepository.findAll();
        assertThat(aidantList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCursusActuelIsRequired() throws Exception {
        int databaseSizeBeforeTest = aidantRepository.findAll().size();
        // set the field null
        aidant.setCursusActuel(null);

        // Create the Aidant, which fails.
        AidantDTO aidantDTO = aidantMapper.toDto(aidant);

        restAidantMockMvc.perform(post("/api/aidants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aidantDTO)))
            .andExpect(status().isBadRequest());

        List<Aidant> aidantList = aidantRepository.findAll();
        assertThat(aidantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAidants() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList
        restAidantMockMvc.perform(get("/api/aidants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aidant.getId().intValue())))
            .andExpect(jsonPath("$.[*].cursusActuel").value(hasItem(DEFAULT_CURSUS_ACTUEL)))
            .andExpect(jsonPath("$.[*].nombreAnneesPostBAC").value(hasItem(DEFAULT_NOMBRE_ANNEES_POST_BAC)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getAidant() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get the aidant
        restAidantMockMvc.perform(get("/api/aidants/{id}", aidant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(aidant.getId().intValue()))
            .andExpect(jsonPath("$.cursusActuel").value(DEFAULT_CURSUS_ACTUEL))
            .andExpect(jsonPath("$.nombreAnneesPostBAC").value(DEFAULT_NOMBRE_ANNEES_POST_BAC))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getAidantsByIdFiltering() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        Long id = aidant.getId();

        defaultAidantShouldBeFound("id.equals=" + id);
        defaultAidantShouldNotBeFound("id.notEquals=" + id);

        defaultAidantShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAidantShouldNotBeFound("id.greaterThan=" + id);

        defaultAidantShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAidantShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAidantsByCursusActuelIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where cursusActuel equals to DEFAULT_CURSUS_ACTUEL
        defaultAidantShouldBeFound("cursusActuel.equals=" + DEFAULT_CURSUS_ACTUEL);

        // Get all the aidantList where cursusActuel equals to UPDATED_CURSUS_ACTUEL
        defaultAidantShouldNotBeFound("cursusActuel.equals=" + UPDATED_CURSUS_ACTUEL);
    }

    @Test
    @Transactional
    public void getAllAidantsByCursusActuelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where cursusActuel not equals to DEFAULT_CURSUS_ACTUEL
        defaultAidantShouldNotBeFound("cursusActuel.notEquals=" + DEFAULT_CURSUS_ACTUEL);

        // Get all the aidantList where cursusActuel not equals to UPDATED_CURSUS_ACTUEL
        defaultAidantShouldBeFound("cursusActuel.notEquals=" + UPDATED_CURSUS_ACTUEL);
    }

    @Test
    @Transactional
    public void getAllAidantsByCursusActuelIsInShouldWork() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where cursusActuel in DEFAULT_CURSUS_ACTUEL or UPDATED_CURSUS_ACTUEL
        defaultAidantShouldBeFound("cursusActuel.in=" + DEFAULT_CURSUS_ACTUEL + "," + UPDATED_CURSUS_ACTUEL);

        // Get all the aidantList where cursusActuel equals to UPDATED_CURSUS_ACTUEL
        defaultAidantShouldNotBeFound("cursusActuel.in=" + UPDATED_CURSUS_ACTUEL);
    }

    @Test
    @Transactional
    public void getAllAidantsByCursusActuelIsNullOrNotNull() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where cursusActuel is not null
        defaultAidantShouldBeFound("cursusActuel.specified=true");

        // Get all the aidantList where cursusActuel is null
        defaultAidantShouldNotBeFound("cursusActuel.specified=false");
    }
                @Test
    @Transactional
    public void getAllAidantsByCursusActuelContainsSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where cursusActuel contains DEFAULT_CURSUS_ACTUEL
        defaultAidantShouldBeFound("cursusActuel.contains=" + DEFAULT_CURSUS_ACTUEL);

        // Get all the aidantList where cursusActuel contains UPDATED_CURSUS_ACTUEL
        defaultAidantShouldNotBeFound("cursusActuel.contains=" + UPDATED_CURSUS_ACTUEL);
    }

    @Test
    @Transactional
    public void getAllAidantsByCursusActuelNotContainsSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where cursusActuel does not contain DEFAULT_CURSUS_ACTUEL
        defaultAidantShouldNotBeFound("cursusActuel.doesNotContain=" + DEFAULT_CURSUS_ACTUEL);

        // Get all the aidantList where cursusActuel does not contain UPDATED_CURSUS_ACTUEL
        defaultAidantShouldBeFound("cursusActuel.doesNotContain=" + UPDATED_CURSUS_ACTUEL);
    }


    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC equals to DEFAULT_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldBeFound("nombreAnneesPostBAC.equals=" + DEFAULT_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC equals to UPDATED_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.equals=" + UPDATED_NOMBRE_ANNEES_POST_BAC);
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC not equals to DEFAULT_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.notEquals=" + DEFAULT_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC not equals to UPDATED_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldBeFound("nombreAnneesPostBAC.notEquals=" + UPDATED_NOMBRE_ANNEES_POST_BAC);
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsInShouldWork() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC in DEFAULT_NOMBRE_ANNEES_POST_BAC or UPDATED_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldBeFound("nombreAnneesPostBAC.in=" + DEFAULT_NOMBRE_ANNEES_POST_BAC + "," + UPDATED_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC equals to UPDATED_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.in=" + UPDATED_NOMBRE_ANNEES_POST_BAC);
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsNullOrNotNull() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC is not null
        defaultAidantShouldBeFound("nombreAnneesPostBAC.specified=true");

        // Get all the aidantList where nombreAnneesPostBAC is null
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.specified=false");
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC is greater than or equal to DEFAULT_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldBeFound("nombreAnneesPostBAC.greaterThanOrEqual=" + DEFAULT_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC is greater than or equal to (DEFAULT_NOMBRE_ANNEES_POST_BAC + 1)
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.greaterThanOrEqual=" + (DEFAULT_NOMBRE_ANNEES_POST_BAC + 1));
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC is less than or equal to DEFAULT_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldBeFound("nombreAnneesPostBAC.lessThanOrEqual=" + DEFAULT_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC is less than or equal to SMALLER_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.lessThanOrEqual=" + SMALLER_NOMBRE_ANNEES_POST_BAC);
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsLessThanSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC is less than DEFAULT_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.lessThan=" + DEFAULT_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC is less than (DEFAULT_NOMBRE_ANNEES_POST_BAC + 1)
        defaultAidantShouldBeFound("nombreAnneesPostBAC.lessThan=" + (DEFAULT_NOMBRE_ANNEES_POST_BAC + 1));
    }

    @Test
    @Transactional
    public void getAllAidantsByNombreAnneesPostBACIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where nombreAnneesPostBAC is greater than DEFAULT_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldNotBeFound("nombreAnneesPostBAC.greaterThan=" + DEFAULT_NOMBRE_ANNEES_POST_BAC);

        // Get all the aidantList where nombreAnneesPostBAC is greater than SMALLER_NOMBRE_ANNEES_POST_BAC
        defaultAidantShouldBeFound("nombreAnneesPostBAC.greaterThan=" + SMALLER_NOMBRE_ANNEES_POST_BAC);
    }


    @Test
    @Transactional
    public void getAllAidantsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultAidantShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the aidantList where dateCreation equals to UPDATED_DATE_CREATION
        defaultAidantShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultAidantShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the aidantList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultAidantShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultAidantShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the aidantList where dateCreation equals to UPDATED_DATE_CREATION
        defaultAidantShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateCreation is not null
        defaultAidantShouldBeFound("dateCreation.specified=true");

        // Get all the aidantList where dateCreation is null
        defaultAidantShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllAidantsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultAidantShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the aidantList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultAidantShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultAidantShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the aidantList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultAidantShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultAidantShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the aidantList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultAidantShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateModification is not null
        defaultAidantShouldBeFound("dateModification.specified=true");

        // Get all the aidantList where dateModification is null
        defaultAidantShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllAidantsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultAidantShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the aidantList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultAidantShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultAidantShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the aidantList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultAidantShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultAidantShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the aidantList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultAidantShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllAidantsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        // Get all the aidantList where dateArchivage is not null
        defaultAidantShouldBeFound("dateArchivage.specified=true");

        // Get all the aidantList where dateArchivage is null
        defaultAidantShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllAidantsByUtilisateurIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);
        User utilisateur = UserResourceIT.createEntity(em);
        em.persist(utilisateur);
        em.flush();
        aidant.setUtilisateur(utilisateur);
        aidantRepository.saveAndFlush(aidant);
        Long utilisateurId = utilisateur.getId();

        // Get all the aidantList where utilisateur equals to utilisateurId
        defaultAidantShouldBeFound("utilisateurId.equals=" + utilisateurId);

        // Get all the aidantList where utilisateur equals to utilisateurId + 1
        defaultAidantShouldNotBeFound("utilisateurId.equals=" + (utilisateurId + 1));
    }


    @Test
    @Transactional
    public void getAllAidantsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        aidant.setContact(contact);
        aidantRepository.saveAndFlush(aidant);
        Long contactId = contact.getId();

        // Get all the aidantList where contact equals to contactId
        defaultAidantShouldBeFound("contactId.equals=" + contactId);

        // Get all the aidantList where contact equals to contactId + 1
        defaultAidantShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }


    @Test
    @Transactional
    public void getAllAidantsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);
        Image photo = ImageResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        aidant.setPhoto(photo);
        aidantRepository.saveAndFlush(aidant);
        Long photoId = photo.getId();

        // Get all the aidantList where photo equals to photoId
        defaultAidantShouldBeFound("photoId.equals=" + photoId);

        // Get all the aidantList where photo equals to photoId + 1
        defaultAidantShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }


    @Test
    @Transactional
    public void getAllAidantsByEtablissementIsEqualToSomething() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);
        Etablissement etablissement = EtablissementResourceIT.createEntity(em);
        em.persist(etablissement);
        em.flush();
        aidant.setEtablissement(etablissement);
        aidantRepository.saveAndFlush(aidant);
        Long etablissementId = etablissement.getId();

        // Get all the aidantList where etablissement equals to etablissementId
        defaultAidantShouldBeFound("etablissementId.equals=" + etablissementId);

        // Get all the aidantList where etablissement equals to etablissementId + 1
        defaultAidantShouldNotBeFound("etablissementId.equals=" + (etablissementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAidantShouldBeFound(String filter) throws Exception {
        restAidantMockMvc.perform(get("/api/aidants?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aidant.getId().intValue())))
            .andExpect(jsonPath("$.[*].cursusActuel").value(hasItem(DEFAULT_CURSUS_ACTUEL)))
            .andExpect(jsonPath("$.[*].nombreAnneesPostBAC").value(hasItem(DEFAULT_NOMBRE_ANNEES_POST_BAC)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restAidantMockMvc.perform(get("/api/aidants/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAidantShouldNotBeFound(String filter) throws Exception {
        restAidantMockMvc.perform(get("/api/aidants?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAidantMockMvc.perform(get("/api/aidants/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAidant() throws Exception {
        // Get the aidant
        restAidantMockMvc.perform(get("/api/aidants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAidant() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        int databaseSizeBeforeUpdate = aidantRepository.findAll().size();

        // Update the aidant
        Aidant updatedAidant = aidantRepository.findById(aidant.getId()).get();
        // Disconnect from session so that the updates on updatedAidant are not directly saved in db
        em.detach(updatedAidant);
        updatedAidant
            .cursusActuel(UPDATED_CURSUS_ACTUEL)
            .nombreAnneesPostBAC(UPDATED_NOMBRE_ANNEES_POST_BAC)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        AidantDTO aidantDTO = aidantMapper.toDto(updatedAidant);

        restAidantMockMvc.perform(put("/api/aidants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aidantDTO)))
            .andExpect(status().isOk());

        // Validate the Aidant in the database
        List<Aidant> aidantList = aidantRepository.findAll();
        assertThat(aidantList).hasSize(databaseSizeBeforeUpdate);
        Aidant testAidant = aidantList.get(aidantList.size() - 1);
        assertThat(testAidant.getCursusActuel()).isEqualTo(UPDATED_CURSUS_ACTUEL);
        assertThat(testAidant.getNombreAnneesPostBAC()).isEqualTo(UPDATED_NOMBRE_ANNEES_POST_BAC);
        assertThat(testAidant.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testAidant.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testAidant.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingAidant() throws Exception {
        int databaseSizeBeforeUpdate = aidantRepository.findAll().size();

        // Create the Aidant
        AidantDTO aidantDTO = aidantMapper.toDto(aidant);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAidantMockMvc.perform(put("/api/aidants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aidantDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Aidant in the database
        List<Aidant> aidantList = aidantRepository.findAll();
        assertThat(aidantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAidant() throws Exception {
        // Initialize the database
        aidantRepository.saveAndFlush(aidant);

        int databaseSizeBeforeDelete = aidantRepository.findAll().size();

        // Delete the aidant
        restAidantMockMvc.perform(delete("/api/aidants/{id}", aidant.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Aidant> aidantList = aidantRepository.findAll();
        assertThat(aidantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
