package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.DemandeSoutien;
import yitus.domain.Apprenant;
import yitus.domain.Aidant;
import yitus.domain.Enseignant;
import yitus.domain.Referent;
import yitus.repository.DemandeSoutienRepository;
import yitus.service.DemandeSoutienService;
import yitus.service.dto.DemandeSoutienDTO;
import yitus.service.mapper.DemandeSoutienMapper;
import yitus.service.dto.DemandeSoutienCriteria;
import yitus.service.DemandeSoutienQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import yitus.domain.enumeration.TypeStatut;
import yitus.domain.enumeration.TypeNiveau;
import yitus.domain.enumeration.TypeMatiere;
/**
 * Integration tests for the {@link DemandeSoutienResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class DemandeSoutienResourceIT {

    private static final TypeStatut DEFAULT_STATUT = TypeStatut.EN_COURS;
    private static final TypeStatut UPDATED_STATUT = TypeStatut.RDV_PROPOSE;

    private static final String DEFAULT_ETIQUETTES = "bajvmi,irkw";
    private static final String UPDATED_ETIQUETTES = "";

    private static final TypeNiveau DEFAULT_NIVEAU = TypeNiveau.SIXIEME;
    private static final TypeNiveau UPDATED_NIVEAU = TypeNiveau.CINQUIEME;

    private static final TypeMatiere DEFAULT_MATIERE = TypeMatiere.MATHS;
    private static final TypeMatiere UPDATED_MATIERE = TypeMatiere.FRANCAIS;

    private static final String DEFAULT_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_DETAIL = "BBBBBBBBBB";

    private static final String DEFAULT_HORAIRES_DISPONIBILITES = "AAAAAAAAAA";
    private static final String UPDATED_HORAIRES_DISPONIBILITES = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_RENDEZ_VOUS = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_RENDEZ_VOUS = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NOTE_APPRENANT = 1;
    private static final Integer UPDATED_NOTE_APPRENANT = 2;
    private static final Integer SMALLER_NOTE_APPRENANT = 1 - 1;

    private static final Integer DEFAULT_NOTE_AIDANT = 1;
    private static final Integer UPDATED_NOTE_AIDANT = 2;
    private static final Integer SMALLER_NOTE_AIDANT = 1 - 1;

    private static final String DEFAULT_COMMENTAIRE_APPRENANT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTAIRE_APPRENANT = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTAIRE_AIDANT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTAIRE_AIDANT = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTAIRE_ENSEIGNANT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTAIRE_ENSEIGNANT = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTAIRE_REFERENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTAIRE_REFERENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_LIMITE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_LIMITE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DemandeSoutienRepository demandeSoutienRepository;

    @Autowired
    private DemandeSoutienMapper demandeSoutienMapper;

    @Autowired
    private DemandeSoutienService demandeSoutienService;

    @Autowired
    private DemandeSoutienQueryService demandeSoutienQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDemandeSoutienMockMvc;

    private DemandeSoutien demandeSoutien;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DemandeSoutien createEntity(EntityManager em) {
        DemandeSoutien demandeSoutien = new DemandeSoutien()
            .statut(DEFAULT_STATUT)
            .etiquettes(DEFAULT_ETIQUETTES)
            .niveau(DEFAULT_NIVEAU)
            .matiere(DEFAULT_MATIERE)
            .detail(DEFAULT_DETAIL)
            .horairesDisponibilites(DEFAULT_HORAIRES_DISPONIBILITES)
            .dateRendezVous(DEFAULT_DATE_RENDEZ_VOUS)
            .noteApprenant(DEFAULT_NOTE_APPRENANT)
            .noteAidant(DEFAULT_NOTE_AIDANT)
            .commentaireApprenant(DEFAULT_COMMENTAIRE_APPRENANT)
            .commentaireAidant(DEFAULT_COMMENTAIRE_AIDANT)
            .commentaireEnseignant(DEFAULT_COMMENTAIRE_ENSEIGNANT)
            .commentaireReferent(DEFAULT_COMMENTAIRE_REFERENT)
            .dateLimite(DEFAULT_DATE_LIMITE)
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return demandeSoutien;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DemandeSoutien createUpdatedEntity(EntityManager em) {
        DemandeSoutien demandeSoutien = new DemandeSoutien()
            .statut(UPDATED_STATUT)
            .etiquettes(UPDATED_ETIQUETTES)
            .niveau(UPDATED_NIVEAU)
            .matiere(UPDATED_MATIERE)
            .detail(UPDATED_DETAIL)
            .horairesDisponibilites(UPDATED_HORAIRES_DISPONIBILITES)
            .dateRendezVous(UPDATED_DATE_RENDEZ_VOUS)
            .noteApprenant(UPDATED_NOTE_APPRENANT)
            .noteAidant(UPDATED_NOTE_AIDANT)
            .commentaireApprenant(UPDATED_COMMENTAIRE_APPRENANT)
            .commentaireAidant(UPDATED_COMMENTAIRE_AIDANT)
            .commentaireEnseignant(UPDATED_COMMENTAIRE_ENSEIGNANT)
            .commentaireReferent(UPDATED_COMMENTAIRE_REFERENT)
            .dateLimite(UPDATED_DATE_LIMITE)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return demandeSoutien;
    }

    @BeforeEach
    public void initTest() {
        demandeSoutien = createEntity(em);
    }

    @Test
    @Transactional
    public void createDemandeSoutien() throws Exception {
        int databaseSizeBeforeCreate = demandeSoutienRepository.findAll().size();

        // Create the DemandeSoutien
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);
        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isCreated());

        // Validate the DemandeSoutien in the database
        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeCreate + 1);
        DemandeSoutien testDemandeSoutien = demandeSoutienList.get(demandeSoutienList.size() - 1);
        assertThat(testDemandeSoutien.getStatut()).isEqualTo(DEFAULT_STATUT);
        assertThat(testDemandeSoutien.getEtiquettes()).isEqualTo(DEFAULT_ETIQUETTES);
        assertThat(testDemandeSoutien.getNiveau()).isEqualTo(DEFAULT_NIVEAU);
        assertThat(testDemandeSoutien.getMatiere()).isEqualTo(DEFAULT_MATIERE);
        assertThat(testDemandeSoutien.getDetail()).isEqualTo(DEFAULT_DETAIL);
        assertThat(testDemandeSoutien.getHorairesDisponibilites()).isEqualTo(DEFAULT_HORAIRES_DISPONIBILITES);
        assertThat(testDemandeSoutien.getDateRendezVous()).isEqualTo(DEFAULT_DATE_RENDEZ_VOUS);
        assertThat(testDemandeSoutien.getNoteApprenant()).isEqualTo(DEFAULT_NOTE_APPRENANT);
        assertThat(testDemandeSoutien.getNoteAidant()).isEqualTo(DEFAULT_NOTE_AIDANT);
        assertThat(testDemandeSoutien.getCommentaireApprenant()).isEqualTo(DEFAULT_COMMENTAIRE_APPRENANT);
        assertThat(testDemandeSoutien.getCommentaireAidant()).isEqualTo(DEFAULT_COMMENTAIRE_AIDANT);
        assertThat(testDemandeSoutien.getCommentaireEnseignant()).isEqualTo(DEFAULT_COMMENTAIRE_ENSEIGNANT);
        assertThat(testDemandeSoutien.getCommentaireReferent()).isEqualTo(DEFAULT_COMMENTAIRE_REFERENT);
        assertThat(testDemandeSoutien.getDateLimite()).isEqualTo(DEFAULT_DATE_LIMITE);
        assertThat(testDemandeSoutien.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testDemandeSoutien.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testDemandeSoutien.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createDemandeSoutienWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = demandeSoutienRepository.findAll().size();

        // Create the DemandeSoutien with an existing ID
        demandeSoutien.setId(1L);
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DemandeSoutien in the database
        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStatutIsRequired() throws Exception {
        int databaseSizeBeforeTest = demandeSoutienRepository.findAll().size();
        // set the field null
        demandeSoutien.setStatut(null);

        // Create the DemandeSoutien, which fails.
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNiveauIsRequired() throws Exception {
        int databaseSizeBeforeTest = demandeSoutienRepository.findAll().size();
        // set the field null
        demandeSoutien.setNiveau(null);

        // Create the DemandeSoutien, which fails.
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMatiereIsRequired() throws Exception {
        int databaseSizeBeforeTest = demandeSoutienRepository.findAll().size();
        // set the field null
        demandeSoutien.setMatiere(null);

        // Create the DemandeSoutien, which fails.
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHorairesDisponibilitesIsRequired() throws Exception {
        int databaseSizeBeforeTest = demandeSoutienRepository.findAll().size();
        // set the field null
        demandeSoutien.setHorairesDisponibilites(null);

        // Create the DemandeSoutien, which fails.
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateLimiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = demandeSoutienRepository.findAll().size();
        // set the field null
        demandeSoutien.setDateLimite(null);

        // Create the DemandeSoutien, which fails.
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        restDemandeSoutienMockMvc.perform(post("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiens() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(demandeSoutien.getId().intValue())))
            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
            .andExpect(jsonPath("$.[*].etiquettes").value(hasItem(DEFAULT_ETIQUETTES)))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU.toString())))
            .andExpect(jsonPath("$.[*].matiere").value(hasItem(DEFAULT_MATIERE.toString())))
            .andExpect(jsonPath("$.[*].detail").value(hasItem(DEFAULT_DETAIL.toString())))
            .andExpect(jsonPath("$.[*].horairesDisponibilites").value(hasItem(DEFAULT_HORAIRES_DISPONIBILITES)))
            .andExpect(jsonPath("$.[*].dateRendezVous").value(hasItem(DEFAULT_DATE_RENDEZ_VOUS.toString())))
            .andExpect(jsonPath("$.[*].noteApprenant").value(hasItem(DEFAULT_NOTE_APPRENANT)))
            .andExpect(jsonPath("$.[*].noteAidant").value(hasItem(DEFAULT_NOTE_AIDANT)))
            .andExpect(jsonPath("$.[*].commentaireApprenant").value(hasItem(DEFAULT_COMMENTAIRE_APPRENANT.toString())))
            .andExpect(jsonPath("$.[*].commentaireAidant").value(hasItem(DEFAULT_COMMENTAIRE_AIDANT.toString())))
            .andExpect(jsonPath("$.[*].commentaireEnseignant").value(hasItem(DEFAULT_COMMENTAIRE_ENSEIGNANT.toString())))
            .andExpect(jsonPath("$.[*].commentaireReferent").value(hasItem(DEFAULT_COMMENTAIRE_REFERENT.toString())))
            .andExpect(jsonPath("$.[*].dateLimite").value(hasItem(DEFAULT_DATE_LIMITE.toString())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getDemandeSoutien() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get the demandeSoutien
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens/{id}", demandeSoutien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(demandeSoutien.getId().intValue()))
            .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.toString()))
            .andExpect(jsonPath("$.etiquettes").value(DEFAULT_ETIQUETTES))
            .andExpect(jsonPath("$.niveau").value(DEFAULT_NIVEAU.toString()))
            .andExpect(jsonPath("$.matiere").value(DEFAULT_MATIERE.toString()))
            .andExpect(jsonPath("$.detail").value(DEFAULT_DETAIL.toString()))
            .andExpect(jsonPath("$.horairesDisponibilites").value(DEFAULT_HORAIRES_DISPONIBILITES))
            .andExpect(jsonPath("$.dateRendezVous").value(DEFAULT_DATE_RENDEZ_VOUS.toString()))
            .andExpect(jsonPath("$.noteApprenant").value(DEFAULT_NOTE_APPRENANT))
            .andExpect(jsonPath("$.noteAidant").value(DEFAULT_NOTE_AIDANT))
            .andExpect(jsonPath("$.commentaireApprenant").value(DEFAULT_COMMENTAIRE_APPRENANT.toString()))
            .andExpect(jsonPath("$.commentaireAidant").value(DEFAULT_COMMENTAIRE_AIDANT.toString()))
            .andExpect(jsonPath("$.commentaireEnseignant").value(DEFAULT_COMMENTAIRE_ENSEIGNANT.toString()))
            .andExpect(jsonPath("$.commentaireReferent").value(DEFAULT_COMMENTAIRE_REFERENT.toString()))
            .andExpect(jsonPath("$.dateLimite").value(DEFAULT_DATE_LIMITE.toString()))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getDemandeSoutiensByIdFiltering() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        Long id = demandeSoutien.getId();

        defaultDemandeSoutienShouldBeFound("id.equals=" + id);
        defaultDemandeSoutienShouldNotBeFound("id.notEquals=" + id);

        defaultDemandeSoutienShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDemandeSoutienShouldNotBeFound("id.greaterThan=" + id);

        defaultDemandeSoutienShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDemandeSoutienShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByStatutIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where statut equals to DEFAULT_STATUT
        defaultDemandeSoutienShouldBeFound("statut.equals=" + DEFAULT_STATUT);

        // Get all the demandeSoutienList where statut equals to UPDATED_STATUT
        defaultDemandeSoutienShouldNotBeFound("statut.equals=" + UPDATED_STATUT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByStatutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where statut not equals to DEFAULT_STATUT
        defaultDemandeSoutienShouldNotBeFound("statut.notEquals=" + DEFAULT_STATUT);

        // Get all the demandeSoutienList where statut not equals to UPDATED_STATUT
        defaultDemandeSoutienShouldBeFound("statut.notEquals=" + UPDATED_STATUT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByStatutIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where statut in DEFAULT_STATUT or UPDATED_STATUT
        defaultDemandeSoutienShouldBeFound("statut.in=" + DEFAULT_STATUT + "," + UPDATED_STATUT);

        // Get all the demandeSoutienList where statut equals to UPDATED_STATUT
        defaultDemandeSoutienShouldNotBeFound("statut.in=" + UPDATED_STATUT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByStatutIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where statut is not null
        defaultDemandeSoutienShouldBeFound("statut.specified=true");

        // Get all the demandeSoutienList where statut is null
        defaultDemandeSoutienShouldNotBeFound("statut.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByEtiquettesIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where etiquettes equals to DEFAULT_ETIQUETTES
        defaultDemandeSoutienShouldBeFound("etiquettes.equals=" + DEFAULT_ETIQUETTES);

        // Get all the demandeSoutienList where etiquettes equals to UPDATED_ETIQUETTES
        defaultDemandeSoutienShouldNotBeFound("etiquettes.equals=" + UPDATED_ETIQUETTES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByEtiquettesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where etiquettes not equals to DEFAULT_ETIQUETTES
        defaultDemandeSoutienShouldNotBeFound("etiquettes.notEquals=" + DEFAULT_ETIQUETTES);

        // Get all the demandeSoutienList where etiquettes not equals to UPDATED_ETIQUETTES
        defaultDemandeSoutienShouldBeFound("etiquettes.notEquals=" + UPDATED_ETIQUETTES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByEtiquettesIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where etiquettes in DEFAULT_ETIQUETTES or UPDATED_ETIQUETTES
        defaultDemandeSoutienShouldBeFound("etiquettes.in=" + DEFAULT_ETIQUETTES + "," + UPDATED_ETIQUETTES);

        // Get all the demandeSoutienList where etiquettes equals to UPDATED_ETIQUETTES
        defaultDemandeSoutienShouldNotBeFound("etiquettes.in=" + UPDATED_ETIQUETTES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByEtiquettesIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where etiquettes is not null
        defaultDemandeSoutienShouldBeFound("etiquettes.specified=true");

        // Get all the demandeSoutienList where etiquettes is null
        defaultDemandeSoutienShouldNotBeFound("etiquettes.specified=false");
    }
                @Test
    @Transactional
    public void getAllDemandeSoutiensByEtiquettesContainsSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where etiquettes contains DEFAULT_ETIQUETTES
        defaultDemandeSoutienShouldBeFound("etiquettes.contains=" + DEFAULT_ETIQUETTES);

        // Get all the demandeSoutienList where etiquettes contains UPDATED_ETIQUETTES
        defaultDemandeSoutienShouldNotBeFound("etiquettes.contains=" + UPDATED_ETIQUETTES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByEtiquettesNotContainsSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where etiquettes does not contain DEFAULT_ETIQUETTES
        defaultDemandeSoutienShouldNotBeFound("etiquettes.doesNotContain=" + DEFAULT_ETIQUETTES);

        // Get all the demandeSoutienList where etiquettes does not contain UPDATED_ETIQUETTES
        defaultDemandeSoutienShouldBeFound("etiquettes.doesNotContain=" + UPDATED_ETIQUETTES);
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByNiveauIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where niveau equals to DEFAULT_NIVEAU
        defaultDemandeSoutienShouldBeFound("niveau.equals=" + DEFAULT_NIVEAU);

        // Get all the demandeSoutienList where niveau equals to UPDATED_NIVEAU
        defaultDemandeSoutienShouldNotBeFound("niveau.equals=" + UPDATED_NIVEAU);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNiveauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where niveau not equals to DEFAULT_NIVEAU
        defaultDemandeSoutienShouldNotBeFound("niveau.notEquals=" + DEFAULT_NIVEAU);

        // Get all the demandeSoutienList where niveau not equals to UPDATED_NIVEAU
        defaultDemandeSoutienShouldBeFound("niveau.notEquals=" + UPDATED_NIVEAU);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNiveauIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where niveau in DEFAULT_NIVEAU or UPDATED_NIVEAU
        defaultDemandeSoutienShouldBeFound("niveau.in=" + DEFAULT_NIVEAU + "," + UPDATED_NIVEAU);

        // Get all the demandeSoutienList where niveau equals to UPDATED_NIVEAU
        defaultDemandeSoutienShouldNotBeFound("niveau.in=" + UPDATED_NIVEAU);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNiveauIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where niveau is not null
        defaultDemandeSoutienShouldBeFound("niveau.specified=true");

        // Get all the demandeSoutienList where niveau is null
        defaultDemandeSoutienShouldNotBeFound("niveau.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByMatiereIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where matiere equals to DEFAULT_MATIERE
        defaultDemandeSoutienShouldBeFound("matiere.equals=" + DEFAULT_MATIERE);

        // Get all the demandeSoutienList where matiere equals to UPDATED_MATIERE
        defaultDemandeSoutienShouldNotBeFound("matiere.equals=" + UPDATED_MATIERE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByMatiereIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where matiere not equals to DEFAULT_MATIERE
        defaultDemandeSoutienShouldNotBeFound("matiere.notEquals=" + DEFAULT_MATIERE);

        // Get all the demandeSoutienList where matiere not equals to UPDATED_MATIERE
        defaultDemandeSoutienShouldBeFound("matiere.notEquals=" + UPDATED_MATIERE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByMatiereIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where matiere in DEFAULT_MATIERE or UPDATED_MATIERE
        defaultDemandeSoutienShouldBeFound("matiere.in=" + DEFAULT_MATIERE + "," + UPDATED_MATIERE);

        // Get all the demandeSoutienList where matiere equals to UPDATED_MATIERE
        defaultDemandeSoutienShouldNotBeFound("matiere.in=" + UPDATED_MATIERE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByMatiereIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where matiere is not null
        defaultDemandeSoutienShouldBeFound("matiere.specified=true");

        // Get all the demandeSoutienList where matiere is null
        defaultDemandeSoutienShouldNotBeFound("matiere.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByHorairesDisponibilitesIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where horairesDisponibilites equals to DEFAULT_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldBeFound("horairesDisponibilites.equals=" + DEFAULT_HORAIRES_DISPONIBILITES);

        // Get all the demandeSoutienList where horairesDisponibilites equals to UPDATED_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldNotBeFound("horairesDisponibilites.equals=" + UPDATED_HORAIRES_DISPONIBILITES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByHorairesDisponibilitesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where horairesDisponibilites not equals to DEFAULT_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldNotBeFound("horairesDisponibilites.notEquals=" + DEFAULT_HORAIRES_DISPONIBILITES);

        // Get all the demandeSoutienList where horairesDisponibilites not equals to UPDATED_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldBeFound("horairesDisponibilites.notEquals=" + UPDATED_HORAIRES_DISPONIBILITES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByHorairesDisponibilitesIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where horairesDisponibilites in DEFAULT_HORAIRES_DISPONIBILITES or UPDATED_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldBeFound("horairesDisponibilites.in=" + DEFAULT_HORAIRES_DISPONIBILITES + "," + UPDATED_HORAIRES_DISPONIBILITES);

        // Get all the demandeSoutienList where horairesDisponibilites equals to UPDATED_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldNotBeFound("horairesDisponibilites.in=" + UPDATED_HORAIRES_DISPONIBILITES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByHorairesDisponibilitesIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where horairesDisponibilites is not null
        defaultDemandeSoutienShouldBeFound("horairesDisponibilites.specified=true");

        // Get all the demandeSoutienList where horairesDisponibilites is null
        defaultDemandeSoutienShouldNotBeFound("horairesDisponibilites.specified=false");
    }
                @Test
    @Transactional
    public void getAllDemandeSoutiensByHorairesDisponibilitesContainsSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where horairesDisponibilites contains DEFAULT_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldBeFound("horairesDisponibilites.contains=" + DEFAULT_HORAIRES_DISPONIBILITES);

        // Get all the demandeSoutienList where horairesDisponibilites contains UPDATED_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldNotBeFound("horairesDisponibilites.contains=" + UPDATED_HORAIRES_DISPONIBILITES);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByHorairesDisponibilitesNotContainsSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where horairesDisponibilites does not contain DEFAULT_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldNotBeFound("horairesDisponibilites.doesNotContain=" + DEFAULT_HORAIRES_DISPONIBILITES);

        // Get all the demandeSoutienList where horairesDisponibilites does not contain UPDATED_HORAIRES_DISPONIBILITES
        defaultDemandeSoutienShouldBeFound("horairesDisponibilites.doesNotContain=" + UPDATED_HORAIRES_DISPONIBILITES);
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateRendezVousIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateRendezVous equals to DEFAULT_DATE_RENDEZ_VOUS
        defaultDemandeSoutienShouldBeFound("dateRendezVous.equals=" + DEFAULT_DATE_RENDEZ_VOUS);

        // Get all the demandeSoutienList where dateRendezVous equals to UPDATED_DATE_RENDEZ_VOUS
        defaultDemandeSoutienShouldNotBeFound("dateRendezVous.equals=" + UPDATED_DATE_RENDEZ_VOUS);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateRendezVousIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateRendezVous not equals to DEFAULT_DATE_RENDEZ_VOUS
        defaultDemandeSoutienShouldNotBeFound("dateRendezVous.notEquals=" + DEFAULT_DATE_RENDEZ_VOUS);

        // Get all the demandeSoutienList where dateRendezVous not equals to UPDATED_DATE_RENDEZ_VOUS
        defaultDemandeSoutienShouldBeFound("dateRendezVous.notEquals=" + UPDATED_DATE_RENDEZ_VOUS);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateRendezVousIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateRendezVous in DEFAULT_DATE_RENDEZ_VOUS or UPDATED_DATE_RENDEZ_VOUS
        defaultDemandeSoutienShouldBeFound("dateRendezVous.in=" + DEFAULT_DATE_RENDEZ_VOUS + "," + UPDATED_DATE_RENDEZ_VOUS);

        // Get all the demandeSoutienList where dateRendezVous equals to UPDATED_DATE_RENDEZ_VOUS
        defaultDemandeSoutienShouldNotBeFound("dateRendezVous.in=" + UPDATED_DATE_RENDEZ_VOUS);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateRendezVousIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateRendezVous is not null
        defaultDemandeSoutienShouldBeFound("dateRendezVous.specified=true");

        // Get all the demandeSoutienList where dateRendezVous is null
        defaultDemandeSoutienShouldNotBeFound("dateRendezVous.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant equals to DEFAULT_NOTE_APPRENANT
        defaultDemandeSoutienShouldBeFound("noteApprenant.equals=" + DEFAULT_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant equals to UPDATED_NOTE_APPRENANT
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.equals=" + UPDATED_NOTE_APPRENANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant not equals to DEFAULT_NOTE_APPRENANT
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.notEquals=" + DEFAULT_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant not equals to UPDATED_NOTE_APPRENANT
        defaultDemandeSoutienShouldBeFound("noteApprenant.notEquals=" + UPDATED_NOTE_APPRENANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant in DEFAULT_NOTE_APPRENANT or UPDATED_NOTE_APPRENANT
        defaultDemandeSoutienShouldBeFound("noteApprenant.in=" + DEFAULT_NOTE_APPRENANT + "," + UPDATED_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant equals to UPDATED_NOTE_APPRENANT
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.in=" + UPDATED_NOTE_APPRENANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant is not null
        defaultDemandeSoutienShouldBeFound("noteApprenant.specified=true");

        // Get all the demandeSoutienList where noteApprenant is null
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant is greater than or equal to DEFAULT_NOTE_APPRENANT
        defaultDemandeSoutienShouldBeFound("noteApprenant.greaterThanOrEqual=" + DEFAULT_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant is greater than or equal to (DEFAULT_NOTE_APPRENANT + 1)
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.greaterThanOrEqual=" + (DEFAULT_NOTE_APPRENANT + 1));
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant is less than or equal to DEFAULT_NOTE_APPRENANT
        defaultDemandeSoutienShouldBeFound("noteApprenant.lessThanOrEqual=" + DEFAULT_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant is less than or equal to SMALLER_NOTE_APPRENANT
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.lessThanOrEqual=" + SMALLER_NOTE_APPRENANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsLessThanSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant is less than DEFAULT_NOTE_APPRENANT
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.lessThan=" + DEFAULT_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant is less than (DEFAULT_NOTE_APPRENANT + 1)
        defaultDemandeSoutienShouldBeFound("noteApprenant.lessThan=" + (DEFAULT_NOTE_APPRENANT + 1));
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteApprenantIsGreaterThanSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteApprenant is greater than DEFAULT_NOTE_APPRENANT
        defaultDemandeSoutienShouldNotBeFound("noteApprenant.greaterThan=" + DEFAULT_NOTE_APPRENANT);

        // Get all the demandeSoutienList where noteApprenant is greater than SMALLER_NOTE_APPRENANT
        defaultDemandeSoutienShouldBeFound("noteApprenant.greaterThan=" + SMALLER_NOTE_APPRENANT);
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant equals to DEFAULT_NOTE_AIDANT
        defaultDemandeSoutienShouldBeFound("noteAidant.equals=" + DEFAULT_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant equals to UPDATED_NOTE_AIDANT
        defaultDemandeSoutienShouldNotBeFound("noteAidant.equals=" + UPDATED_NOTE_AIDANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant not equals to DEFAULT_NOTE_AIDANT
        defaultDemandeSoutienShouldNotBeFound("noteAidant.notEquals=" + DEFAULT_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant not equals to UPDATED_NOTE_AIDANT
        defaultDemandeSoutienShouldBeFound("noteAidant.notEquals=" + UPDATED_NOTE_AIDANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant in DEFAULT_NOTE_AIDANT or UPDATED_NOTE_AIDANT
        defaultDemandeSoutienShouldBeFound("noteAidant.in=" + DEFAULT_NOTE_AIDANT + "," + UPDATED_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant equals to UPDATED_NOTE_AIDANT
        defaultDemandeSoutienShouldNotBeFound("noteAidant.in=" + UPDATED_NOTE_AIDANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant is not null
        defaultDemandeSoutienShouldBeFound("noteAidant.specified=true");

        // Get all the demandeSoutienList where noteAidant is null
        defaultDemandeSoutienShouldNotBeFound("noteAidant.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant is greater than or equal to DEFAULT_NOTE_AIDANT
        defaultDemandeSoutienShouldBeFound("noteAidant.greaterThanOrEqual=" + DEFAULT_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant is greater than or equal to (DEFAULT_NOTE_AIDANT + 1)
        defaultDemandeSoutienShouldNotBeFound("noteAidant.greaterThanOrEqual=" + (DEFAULT_NOTE_AIDANT + 1));
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant is less than or equal to DEFAULT_NOTE_AIDANT
        defaultDemandeSoutienShouldBeFound("noteAidant.lessThanOrEqual=" + DEFAULT_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant is less than or equal to SMALLER_NOTE_AIDANT
        defaultDemandeSoutienShouldNotBeFound("noteAidant.lessThanOrEqual=" + SMALLER_NOTE_AIDANT);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsLessThanSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant is less than DEFAULT_NOTE_AIDANT
        defaultDemandeSoutienShouldNotBeFound("noteAidant.lessThan=" + DEFAULT_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant is less than (DEFAULT_NOTE_AIDANT + 1)
        defaultDemandeSoutienShouldBeFound("noteAidant.lessThan=" + (DEFAULT_NOTE_AIDANT + 1));
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByNoteAidantIsGreaterThanSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where noteAidant is greater than DEFAULT_NOTE_AIDANT
        defaultDemandeSoutienShouldNotBeFound("noteAidant.greaterThan=" + DEFAULT_NOTE_AIDANT);

        // Get all the demandeSoutienList where noteAidant is greater than SMALLER_NOTE_AIDANT
        defaultDemandeSoutienShouldBeFound("noteAidant.greaterThan=" + SMALLER_NOTE_AIDANT);
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateLimiteIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateLimite equals to DEFAULT_DATE_LIMITE
        defaultDemandeSoutienShouldBeFound("dateLimite.equals=" + DEFAULT_DATE_LIMITE);

        // Get all the demandeSoutienList where dateLimite equals to UPDATED_DATE_LIMITE
        defaultDemandeSoutienShouldNotBeFound("dateLimite.equals=" + UPDATED_DATE_LIMITE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateLimiteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateLimite not equals to DEFAULT_DATE_LIMITE
        defaultDemandeSoutienShouldNotBeFound("dateLimite.notEquals=" + DEFAULT_DATE_LIMITE);

        // Get all the demandeSoutienList where dateLimite not equals to UPDATED_DATE_LIMITE
        defaultDemandeSoutienShouldBeFound("dateLimite.notEquals=" + UPDATED_DATE_LIMITE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateLimiteIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateLimite in DEFAULT_DATE_LIMITE or UPDATED_DATE_LIMITE
        defaultDemandeSoutienShouldBeFound("dateLimite.in=" + DEFAULT_DATE_LIMITE + "," + UPDATED_DATE_LIMITE);

        // Get all the demandeSoutienList where dateLimite equals to UPDATED_DATE_LIMITE
        defaultDemandeSoutienShouldNotBeFound("dateLimite.in=" + UPDATED_DATE_LIMITE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateLimiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateLimite is not null
        defaultDemandeSoutienShouldBeFound("dateLimite.specified=true");

        // Get all the demandeSoutienList where dateLimite is null
        defaultDemandeSoutienShouldNotBeFound("dateLimite.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultDemandeSoutienShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the demandeSoutienList where dateCreation equals to UPDATED_DATE_CREATION
        defaultDemandeSoutienShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultDemandeSoutienShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the demandeSoutienList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultDemandeSoutienShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultDemandeSoutienShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the demandeSoutienList where dateCreation equals to UPDATED_DATE_CREATION
        defaultDemandeSoutienShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateCreation is not null
        defaultDemandeSoutienShouldBeFound("dateCreation.specified=true");

        // Get all the demandeSoutienList where dateCreation is null
        defaultDemandeSoutienShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultDemandeSoutienShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the demandeSoutienList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultDemandeSoutienShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultDemandeSoutienShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the demandeSoutienList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultDemandeSoutienShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultDemandeSoutienShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the demandeSoutienList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultDemandeSoutienShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateModification is not null
        defaultDemandeSoutienShouldBeFound("dateModification.specified=true");

        // Get all the demandeSoutienList where dateModification is null
        defaultDemandeSoutienShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultDemandeSoutienShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the demandeSoutienList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultDemandeSoutienShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultDemandeSoutienShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the demandeSoutienList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultDemandeSoutienShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultDemandeSoutienShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the demandeSoutienList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultDemandeSoutienShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        // Get all the demandeSoutienList where dateArchivage is not null
        defaultDemandeSoutienShouldBeFound("dateArchivage.specified=true");

        // Get all the demandeSoutienList where dateArchivage is null
        defaultDemandeSoutienShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllDemandeSoutiensByApprenantIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Apprenant apprenant = ApprenantResourceIT.createEntity(em);
        em.persist(apprenant);
        em.flush();
        demandeSoutien.setApprenant(apprenant);
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Long apprenantId = apprenant.getId();

        // Get all the demandeSoutienList where apprenant equals to apprenantId
        defaultDemandeSoutienShouldBeFound("apprenantId.equals=" + apprenantId);

        // Get all the demandeSoutienList where apprenant equals to apprenantId + 1
        defaultDemandeSoutienShouldNotBeFound("apprenantId.equals=" + (apprenantId + 1));
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByAidantIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Aidant aidant = AidantResourceIT.createEntity(em);
        em.persist(aidant);
        em.flush();
        demandeSoutien.setAidant(aidant);
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Long aidantId = aidant.getId();

        // Get all the demandeSoutienList where aidant equals to aidantId
        defaultDemandeSoutienShouldBeFound("aidantId.equals=" + aidantId);

        // Get all the demandeSoutienList where aidant equals to aidantId + 1
        defaultDemandeSoutienShouldNotBeFound("aidantId.equals=" + (aidantId + 1));
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByEnseignantIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Enseignant enseignant = EnseignantResourceIT.createEntity(em);
        em.persist(enseignant);
        em.flush();
        demandeSoutien.setEnseignant(enseignant);
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Long enseignantId = enseignant.getId();

        // Get all the demandeSoutienList where enseignant equals to enseignantId
        defaultDemandeSoutienShouldBeFound("enseignantId.equals=" + enseignantId);

        // Get all the demandeSoutienList where enseignant equals to enseignantId + 1
        defaultDemandeSoutienShouldNotBeFound("enseignantId.equals=" + (enseignantId + 1));
    }


    @Test
    @Transactional
    public void getAllDemandeSoutiensByReferentIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Referent referent = ReferentResourceIT.createEntity(em);
        em.persist(referent);
        em.flush();
        demandeSoutien.setReferent(referent);
        demandeSoutienRepository.saveAndFlush(demandeSoutien);
        Long referentId = referent.getId();

        // Get all the demandeSoutienList where referent equals to referentId
        defaultDemandeSoutienShouldBeFound("referentId.equals=" + referentId);

        // Get all the demandeSoutienList where referent equals to referentId + 1
        defaultDemandeSoutienShouldNotBeFound("referentId.equals=" + (referentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDemandeSoutienShouldBeFound(String filter) throws Exception {
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(demandeSoutien.getId().intValue())))
            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
            .andExpect(jsonPath("$.[*].etiquettes").value(hasItem(DEFAULT_ETIQUETTES)))
            .andExpect(jsonPath("$.[*].niveau").value(hasItem(DEFAULT_NIVEAU.toString())))
            .andExpect(jsonPath("$.[*].matiere").value(hasItem(DEFAULT_MATIERE.toString())))
            .andExpect(jsonPath("$.[*].detail").value(hasItem(DEFAULT_DETAIL.toString())))
            .andExpect(jsonPath("$.[*].horairesDisponibilites").value(hasItem(DEFAULT_HORAIRES_DISPONIBILITES)))
            .andExpect(jsonPath("$.[*].dateRendezVous").value(hasItem(DEFAULT_DATE_RENDEZ_VOUS.toString())))
            .andExpect(jsonPath("$.[*].noteApprenant").value(hasItem(DEFAULT_NOTE_APPRENANT)))
            .andExpect(jsonPath("$.[*].noteAidant").value(hasItem(DEFAULT_NOTE_AIDANT)))
            .andExpect(jsonPath("$.[*].commentaireApprenant").value(hasItem(DEFAULT_COMMENTAIRE_APPRENANT.toString())))
            .andExpect(jsonPath("$.[*].commentaireAidant").value(hasItem(DEFAULT_COMMENTAIRE_AIDANT.toString())))
            .andExpect(jsonPath("$.[*].commentaireEnseignant").value(hasItem(DEFAULT_COMMENTAIRE_ENSEIGNANT.toString())))
            .andExpect(jsonPath("$.[*].commentaireReferent").value(hasItem(DEFAULT_COMMENTAIRE_REFERENT.toString())))
            .andExpect(jsonPath("$.[*].dateLimite").value(hasItem(DEFAULT_DATE_LIMITE.toString())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDemandeSoutienShouldNotBeFound(String filter) throws Exception {
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDemandeSoutien() throws Exception {
        // Get the demandeSoutien
        restDemandeSoutienMockMvc.perform(get("/api/demande-soutiens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDemandeSoutien() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        int databaseSizeBeforeUpdate = demandeSoutienRepository.findAll().size();

        // Update the demandeSoutien
        DemandeSoutien updatedDemandeSoutien = demandeSoutienRepository.findById(demandeSoutien.getId()).get();
        // Disconnect from session so that the updates on updatedDemandeSoutien are not directly saved in db
        em.detach(updatedDemandeSoutien);
        updatedDemandeSoutien
            .statut(UPDATED_STATUT)
            .etiquettes(UPDATED_ETIQUETTES)
            .niveau(UPDATED_NIVEAU)
            .matiere(UPDATED_MATIERE)
            .detail(UPDATED_DETAIL)
            .horairesDisponibilites(UPDATED_HORAIRES_DISPONIBILITES)
            .dateRendezVous(UPDATED_DATE_RENDEZ_VOUS)
            .noteApprenant(UPDATED_NOTE_APPRENANT)
            .noteAidant(UPDATED_NOTE_AIDANT)
            .commentaireApprenant(UPDATED_COMMENTAIRE_APPRENANT)
            .commentaireAidant(UPDATED_COMMENTAIRE_AIDANT)
            .commentaireEnseignant(UPDATED_COMMENTAIRE_ENSEIGNANT)
            .commentaireReferent(UPDATED_COMMENTAIRE_REFERENT)
            .dateLimite(UPDATED_DATE_LIMITE)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(updatedDemandeSoutien);

        restDemandeSoutienMockMvc.perform(put("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isOk());

        // Validate the DemandeSoutien in the database
        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeUpdate);
        DemandeSoutien testDemandeSoutien = demandeSoutienList.get(demandeSoutienList.size() - 1);
        assertThat(testDemandeSoutien.getStatut()).isEqualTo(UPDATED_STATUT);
        assertThat(testDemandeSoutien.getEtiquettes()).isEqualTo(UPDATED_ETIQUETTES);
        assertThat(testDemandeSoutien.getNiveau()).isEqualTo(UPDATED_NIVEAU);
        assertThat(testDemandeSoutien.getMatiere()).isEqualTo(UPDATED_MATIERE);
        assertThat(testDemandeSoutien.getDetail()).isEqualTo(UPDATED_DETAIL);
        assertThat(testDemandeSoutien.getHorairesDisponibilites()).isEqualTo(UPDATED_HORAIRES_DISPONIBILITES);
        assertThat(testDemandeSoutien.getDateRendezVous()).isEqualTo(UPDATED_DATE_RENDEZ_VOUS);
        assertThat(testDemandeSoutien.getNoteApprenant()).isEqualTo(UPDATED_NOTE_APPRENANT);
        assertThat(testDemandeSoutien.getNoteAidant()).isEqualTo(UPDATED_NOTE_AIDANT);
        assertThat(testDemandeSoutien.getCommentaireApprenant()).isEqualTo(UPDATED_COMMENTAIRE_APPRENANT);
        assertThat(testDemandeSoutien.getCommentaireAidant()).isEqualTo(UPDATED_COMMENTAIRE_AIDANT);
        assertThat(testDemandeSoutien.getCommentaireEnseignant()).isEqualTo(UPDATED_COMMENTAIRE_ENSEIGNANT);
        assertThat(testDemandeSoutien.getCommentaireReferent()).isEqualTo(UPDATED_COMMENTAIRE_REFERENT);
        assertThat(testDemandeSoutien.getDateLimite()).isEqualTo(UPDATED_DATE_LIMITE);
        assertThat(testDemandeSoutien.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testDemandeSoutien.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testDemandeSoutien.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingDemandeSoutien() throws Exception {
        int databaseSizeBeforeUpdate = demandeSoutienRepository.findAll().size();

        // Create the DemandeSoutien
        DemandeSoutienDTO demandeSoutienDTO = demandeSoutienMapper.toDto(demandeSoutien);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDemandeSoutienMockMvc.perform(put("/api/demande-soutiens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeSoutienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DemandeSoutien in the database
        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDemandeSoutien() throws Exception {
        // Initialize the database
        demandeSoutienRepository.saveAndFlush(demandeSoutien);

        int databaseSizeBeforeDelete = demandeSoutienRepository.findAll().size();

        // Delete the demandeSoutien
        restDemandeSoutienMockMvc.perform(delete("/api/demande-soutiens/{id}", demandeSoutien.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DemandeSoutien> demandeSoutienList = demandeSoutienRepository.findAll();
        assertThat(demandeSoutienList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
