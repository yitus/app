package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Evenement;
import yitus.domain.User;
import yitus.repository.EvenementRepository;
import yitus.service.EvenementService;
import yitus.service.dto.EvenementDTO;
import yitus.service.mapper.EvenementMapper;
import yitus.service.dto.EvenementCriteria;
import yitus.service.EvenementQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import yitus.domain.enumeration.TypeStatusEvenement;
import yitus.domain.enumeration.TypeOperation;
import yitus.domain.enumeration.TypeEntite;
/**
 * Integration tests for the {@link EvenementResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class EvenementResourceIT {

    private static final TypeStatusEvenement DEFAULT_STATUS_EVENEMENT = TypeStatusEvenement.INFO;
    private static final TypeStatusEvenement UPDATED_STATUS_EVENEMENT = TypeStatusEvenement.SUCCES;

    private static final Instant DEFAULT_DATE_EVENEMENT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_EVENEMENT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final TypeOperation DEFAULT_OPERATION = TypeOperation.CREATION;
    private static final TypeOperation UPDATED_OPERATION = TypeOperation.MODIFICATION;

    private static final TypeEntite DEFAULT_ENTITE = TypeEntite.AIDANT;
    private static final TypeEntite UPDATED_ENTITE = TypeEntite.APPRENANT;

    private static final Integer DEFAULT_ID_ENTITE = 1;
    private static final Integer UPDATED_ID_ENTITE = 2;
    private static final Integer SMALLER_ID_ENTITE = 1 - 1;

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private EvenementRepository evenementRepository;

    @Autowired
    private EvenementMapper evenementMapper;

    @Autowired
    private EvenementService evenementService;

    @Autowired
    private EvenementQueryService evenementQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEvenementMockMvc;

    private Evenement evenement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evenement createEntity(EntityManager em) {
        Evenement evenement = new Evenement()
            .statusEvenement(DEFAULT_STATUS_EVENEMENT)
            .dateEvenement(DEFAULT_DATE_EVENEMENT)
            .operation(DEFAULT_OPERATION)
            .entite(DEFAULT_ENTITE)
            .idEntite(DEFAULT_ID_ENTITE)
            .message(DEFAULT_MESSAGE);
        return evenement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evenement createUpdatedEntity(EntityManager em) {
        Evenement evenement = new Evenement()
            .statusEvenement(UPDATED_STATUS_EVENEMENT)
            .dateEvenement(UPDATED_DATE_EVENEMENT)
            .operation(UPDATED_OPERATION)
            .entite(UPDATED_ENTITE)
            .idEntite(UPDATED_ID_ENTITE)
            .message(UPDATED_MESSAGE);
        return evenement;
    }

    @BeforeEach
    public void initTest() {
        evenement = createEntity(em);
    }

    @Test
    @Transactional
    public void createEvenement() throws Exception {
        int databaseSizeBeforeCreate = evenementRepository.findAll().size();

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);
        restEvenementMockMvc.perform(post("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isCreated());

        // Validate the Evenement in the database
        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeCreate + 1);
        Evenement testEvenement = evenementList.get(evenementList.size() - 1);
        assertThat(testEvenement.getStatusEvenement()).isEqualTo(DEFAULT_STATUS_EVENEMENT);
        assertThat(testEvenement.getDateEvenement()).isEqualTo(DEFAULT_DATE_EVENEMENT);
        assertThat(testEvenement.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testEvenement.getEntite()).isEqualTo(DEFAULT_ENTITE);
        assertThat(testEvenement.getIdEntite()).isEqualTo(DEFAULT_ID_ENTITE);
        assertThat(testEvenement.getMessage()).isEqualTo(DEFAULT_MESSAGE);
    }

    @Test
    @Transactional
    public void createEvenementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = evenementRepository.findAll().size();

        // Create the Evenement with an existing ID
        evenement.setId(1L);
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEvenementMockMvc.perform(post("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Evenement in the database
        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStatusEvenementIsRequired() throws Exception {
        int databaseSizeBeforeTest = evenementRepository.findAll().size();
        // set the field null
        evenement.setStatusEvenement(null);

        // Create the Evenement, which fails.
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        restEvenementMockMvc.perform(post("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isBadRequest());

        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateEvenementIsRequired() throws Exception {
        int databaseSizeBeforeTest = evenementRepository.findAll().size();
        // set the field null
        evenement.setDateEvenement(null);

        // Create the Evenement, which fails.
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        restEvenementMockMvc.perform(post("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isBadRequest());

        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOperationIsRequired() throws Exception {
        int databaseSizeBeforeTest = evenementRepository.findAll().size();
        // set the field null
        evenement.setOperation(null);

        // Create the Evenement, which fails.
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        restEvenementMockMvc.perform(post("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isBadRequest());

        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEntiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = evenementRepository.findAll().size();
        // set the field null
        evenement.setEntite(null);

        // Create the Evenement, which fails.
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        restEvenementMockMvc.perform(post("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isBadRequest());

        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEvenements() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList
        restEvenementMockMvc.perform(get("/api/evenements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evenement.getId().intValue())))
            .andExpect(jsonPath("$.[*].statusEvenement").value(hasItem(DEFAULT_STATUS_EVENEMENT.toString())))
            .andExpect(jsonPath("$.[*].dateEvenement").value(hasItem(DEFAULT_DATE_EVENEMENT.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION.toString())))
            .andExpect(jsonPath("$.[*].entite").value(hasItem(DEFAULT_ENTITE.toString())))
            .andExpect(jsonPath("$.[*].idEntite").value(hasItem(DEFAULT_ID_ENTITE)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));
    }
    
    @Test
    @Transactional
    public void getEvenement() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get the evenement
        restEvenementMockMvc.perform(get("/api/evenements/{id}", evenement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(evenement.getId().intValue()))
            .andExpect(jsonPath("$.statusEvenement").value(DEFAULT_STATUS_EVENEMENT.toString()))
            .andExpect(jsonPath("$.dateEvenement").value(DEFAULT_DATE_EVENEMENT.toString()))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION.toString()))
            .andExpect(jsonPath("$.entite").value(DEFAULT_ENTITE.toString()))
            .andExpect(jsonPath("$.idEntite").value(DEFAULT_ID_ENTITE))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE));
    }


    @Test
    @Transactional
    public void getEvenementsByIdFiltering() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        Long id = evenement.getId();

        defaultEvenementShouldBeFound("id.equals=" + id);
        defaultEvenementShouldNotBeFound("id.notEquals=" + id);

        defaultEvenementShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEvenementShouldNotBeFound("id.greaterThan=" + id);

        defaultEvenementShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEvenementShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllEvenementsByStatusEvenementIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where statusEvenement equals to DEFAULT_STATUS_EVENEMENT
        defaultEvenementShouldBeFound("statusEvenement.equals=" + DEFAULT_STATUS_EVENEMENT);

        // Get all the evenementList where statusEvenement equals to UPDATED_STATUS_EVENEMENT
        defaultEvenementShouldNotBeFound("statusEvenement.equals=" + UPDATED_STATUS_EVENEMENT);
    }

    @Test
    @Transactional
    public void getAllEvenementsByStatusEvenementIsNotEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where statusEvenement not equals to DEFAULT_STATUS_EVENEMENT
        defaultEvenementShouldNotBeFound("statusEvenement.notEquals=" + DEFAULT_STATUS_EVENEMENT);

        // Get all the evenementList where statusEvenement not equals to UPDATED_STATUS_EVENEMENT
        defaultEvenementShouldBeFound("statusEvenement.notEquals=" + UPDATED_STATUS_EVENEMENT);
    }

    @Test
    @Transactional
    public void getAllEvenementsByStatusEvenementIsInShouldWork() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where statusEvenement in DEFAULT_STATUS_EVENEMENT or UPDATED_STATUS_EVENEMENT
        defaultEvenementShouldBeFound("statusEvenement.in=" + DEFAULT_STATUS_EVENEMENT + "," + UPDATED_STATUS_EVENEMENT);

        // Get all the evenementList where statusEvenement equals to UPDATED_STATUS_EVENEMENT
        defaultEvenementShouldNotBeFound("statusEvenement.in=" + UPDATED_STATUS_EVENEMENT);
    }

    @Test
    @Transactional
    public void getAllEvenementsByStatusEvenementIsNullOrNotNull() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where statusEvenement is not null
        defaultEvenementShouldBeFound("statusEvenement.specified=true");

        // Get all the evenementList where statusEvenement is null
        defaultEvenementShouldNotBeFound("statusEvenement.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvenementsByDateEvenementIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where dateEvenement equals to DEFAULT_DATE_EVENEMENT
        defaultEvenementShouldBeFound("dateEvenement.equals=" + DEFAULT_DATE_EVENEMENT);

        // Get all the evenementList where dateEvenement equals to UPDATED_DATE_EVENEMENT
        defaultEvenementShouldNotBeFound("dateEvenement.equals=" + UPDATED_DATE_EVENEMENT);
    }

    @Test
    @Transactional
    public void getAllEvenementsByDateEvenementIsNotEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where dateEvenement not equals to DEFAULT_DATE_EVENEMENT
        defaultEvenementShouldNotBeFound("dateEvenement.notEquals=" + DEFAULT_DATE_EVENEMENT);

        // Get all the evenementList where dateEvenement not equals to UPDATED_DATE_EVENEMENT
        defaultEvenementShouldBeFound("dateEvenement.notEquals=" + UPDATED_DATE_EVENEMENT);
    }

    @Test
    @Transactional
    public void getAllEvenementsByDateEvenementIsInShouldWork() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where dateEvenement in DEFAULT_DATE_EVENEMENT or UPDATED_DATE_EVENEMENT
        defaultEvenementShouldBeFound("dateEvenement.in=" + DEFAULT_DATE_EVENEMENT + "," + UPDATED_DATE_EVENEMENT);

        // Get all the evenementList where dateEvenement equals to UPDATED_DATE_EVENEMENT
        defaultEvenementShouldNotBeFound("dateEvenement.in=" + UPDATED_DATE_EVENEMENT);
    }

    @Test
    @Transactional
    public void getAllEvenementsByDateEvenementIsNullOrNotNull() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where dateEvenement is not null
        defaultEvenementShouldBeFound("dateEvenement.specified=true");

        // Get all the evenementList where dateEvenement is null
        defaultEvenementShouldNotBeFound("dateEvenement.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvenementsByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where operation equals to DEFAULT_OPERATION
        defaultEvenementShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the evenementList where operation equals to UPDATED_OPERATION
        defaultEvenementShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllEvenementsByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where operation not equals to DEFAULT_OPERATION
        defaultEvenementShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the evenementList where operation not equals to UPDATED_OPERATION
        defaultEvenementShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllEvenementsByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultEvenementShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the evenementList where operation equals to UPDATED_OPERATION
        defaultEvenementShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllEvenementsByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where operation is not null
        defaultEvenementShouldBeFound("operation.specified=true");

        // Get all the evenementList where operation is null
        defaultEvenementShouldNotBeFound("operation.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvenementsByEntiteIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where entite equals to DEFAULT_ENTITE
        defaultEvenementShouldBeFound("entite.equals=" + DEFAULT_ENTITE);

        // Get all the evenementList where entite equals to UPDATED_ENTITE
        defaultEvenementShouldNotBeFound("entite.equals=" + UPDATED_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByEntiteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where entite not equals to DEFAULT_ENTITE
        defaultEvenementShouldNotBeFound("entite.notEquals=" + DEFAULT_ENTITE);

        // Get all the evenementList where entite not equals to UPDATED_ENTITE
        defaultEvenementShouldBeFound("entite.notEquals=" + UPDATED_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByEntiteIsInShouldWork() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where entite in DEFAULT_ENTITE or UPDATED_ENTITE
        defaultEvenementShouldBeFound("entite.in=" + DEFAULT_ENTITE + "," + UPDATED_ENTITE);

        // Get all the evenementList where entite equals to UPDATED_ENTITE
        defaultEvenementShouldNotBeFound("entite.in=" + UPDATED_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByEntiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where entite is not null
        defaultEvenementShouldBeFound("entite.specified=true");

        // Get all the evenementList where entite is null
        defaultEvenementShouldNotBeFound("entite.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite equals to DEFAULT_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.equals=" + DEFAULT_ID_ENTITE);

        // Get all the evenementList where idEntite equals to UPDATED_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.equals=" + UPDATED_ID_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite not equals to DEFAULT_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.notEquals=" + DEFAULT_ID_ENTITE);

        // Get all the evenementList where idEntite not equals to UPDATED_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.notEquals=" + UPDATED_ID_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsInShouldWork() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite in DEFAULT_ID_ENTITE or UPDATED_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.in=" + DEFAULT_ID_ENTITE + "," + UPDATED_ID_ENTITE);

        // Get all the evenementList where idEntite equals to UPDATED_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.in=" + UPDATED_ID_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite is not null
        defaultEvenementShouldBeFound("idEntite.specified=true");

        // Get all the evenementList where idEntite is null
        defaultEvenementShouldNotBeFound("idEntite.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite is greater than or equal to DEFAULT_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.greaterThanOrEqual=" + DEFAULT_ID_ENTITE);

        // Get all the evenementList where idEntite is greater than or equal to UPDATED_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.greaterThanOrEqual=" + UPDATED_ID_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite is less than or equal to DEFAULT_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.lessThanOrEqual=" + DEFAULT_ID_ENTITE);

        // Get all the evenementList where idEntite is less than or equal to SMALLER_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.lessThanOrEqual=" + SMALLER_ID_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsLessThanSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite is less than DEFAULT_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.lessThan=" + DEFAULT_ID_ENTITE);

        // Get all the evenementList where idEntite is less than UPDATED_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.lessThan=" + UPDATED_ID_ENTITE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByIdEntiteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where idEntite is greater than DEFAULT_ID_ENTITE
        defaultEvenementShouldNotBeFound("idEntite.greaterThan=" + DEFAULT_ID_ENTITE);

        // Get all the evenementList where idEntite is greater than SMALLER_ID_ENTITE
        defaultEvenementShouldBeFound("idEntite.greaterThan=" + SMALLER_ID_ENTITE);
    }


    @Test
    @Transactional
    public void getAllEvenementsByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where message equals to DEFAULT_MESSAGE
        defaultEvenementShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the evenementList where message equals to UPDATED_MESSAGE
        defaultEvenementShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where message not equals to DEFAULT_MESSAGE
        defaultEvenementShouldNotBeFound("message.notEquals=" + DEFAULT_MESSAGE);

        // Get all the evenementList where message not equals to UPDATED_MESSAGE
        defaultEvenementShouldBeFound("message.notEquals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultEvenementShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the evenementList where message equals to UPDATED_MESSAGE
        defaultEvenementShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where message is not null
        defaultEvenementShouldBeFound("message.specified=true");

        // Get all the evenementList where message is null
        defaultEvenementShouldNotBeFound("message.specified=false");
    }
                @Test
    @Transactional
    public void getAllEvenementsByMessageContainsSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where message contains DEFAULT_MESSAGE
        defaultEvenementShouldBeFound("message.contains=" + DEFAULT_MESSAGE);

        // Get all the evenementList where message contains UPDATED_MESSAGE
        defaultEvenementShouldNotBeFound("message.contains=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllEvenementsByMessageNotContainsSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        // Get all the evenementList where message does not contain DEFAULT_MESSAGE
        defaultEvenementShouldNotBeFound("message.doesNotContain=" + DEFAULT_MESSAGE);

        // Get all the evenementList where message does not contain UPDATED_MESSAGE
        defaultEvenementShouldBeFound("message.doesNotContain=" + UPDATED_MESSAGE);
    }


    @Test
    @Transactional
    public void getAllEvenementsByCreeParIsEqualToSomething() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);
        User creePar = UserResourceIT.createEntity(em);
        em.persist(creePar);
        em.flush();
        evenement.setCreePar(creePar);
        evenementRepository.saveAndFlush(evenement);
        Long creeParId = creePar.getId();

        // Get all the evenementList where creePar equals to creeParId
        defaultEvenementShouldBeFound("creeParId.equals=" + creeParId);

        // Get all the evenementList where creePar equals to creeParId + 1
        defaultEvenementShouldNotBeFound("creeParId.equals=" + (creeParId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEvenementShouldBeFound(String filter) throws Exception {
        restEvenementMockMvc.perform(get("/api/evenements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evenement.getId().intValue())))
            .andExpect(jsonPath("$.[*].statusEvenement").value(hasItem(DEFAULT_STATUS_EVENEMENT.toString())))
            .andExpect(jsonPath("$.[*].dateEvenement").value(hasItem(DEFAULT_DATE_EVENEMENT.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION.toString())))
            .andExpect(jsonPath("$.[*].entite").value(hasItem(DEFAULT_ENTITE.toString())))
            .andExpect(jsonPath("$.[*].idEntite").value(hasItem(DEFAULT_ID_ENTITE)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));

        // Check, that the count call also returns 1
        restEvenementMockMvc.perform(get("/api/evenements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEvenementShouldNotBeFound(String filter) throws Exception {
        restEvenementMockMvc.perform(get("/api/evenements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEvenementMockMvc.perform(get("/api/evenements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEvenement() throws Exception {
        // Get the evenement
        restEvenementMockMvc.perform(get("/api/evenements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEvenement() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        int databaseSizeBeforeUpdate = evenementRepository.findAll().size();

        // Update the evenement
        Evenement updatedEvenement = evenementRepository.findById(evenement.getId()).get();
        // Disconnect from session so that the updates on updatedEvenement are not directly saved in db
        em.detach(updatedEvenement);
        updatedEvenement
            .statusEvenement(UPDATED_STATUS_EVENEMENT)
            .dateEvenement(UPDATED_DATE_EVENEMENT)
            .operation(UPDATED_OPERATION)
            .entite(UPDATED_ENTITE)
            .idEntite(UPDATED_ID_ENTITE)
            .message(UPDATED_MESSAGE);
        EvenementDTO evenementDTO = evenementMapper.toDto(updatedEvenement);

        restEvenementMockMvc.perform(put("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isOk());

        // Validate the Evenement in the database
        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeUpdate);
        Evenement testEvenement = evenementList.get(evenementList.size() - 1);
        assertThat(testEvenement.getStatusEvenement()).isEqualTo(UPDATED_STATUS_EVENEMENT);
        assertThat(testEvenement.getDateEvenement()).isEqualTo(UPDATED_DATE_EVENEMENT);
        assertThat(testEvenement.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testEvenement.getEntite()).isEqualTo(UPDATED_ENTITE);
        assertThat(testEvenement.getIdEntite()).isEqualTo(UPDATED_ID_ENTITE);
        assertThat(testEvenement.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingEvenement() throws Exception {
        int databaseSizeBeforeUpdate = evenementRepository.findAll().size();

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEvenementMockMvc.perform(put("/api/evenements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(evenementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Evenement in the database
        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEvenement() throws Exception {
        // Initialize the database
        evenementRepository.saveAndFlush(evenement);

        int databaseSizeBeforeDelete = evenementRepository.findAll().size();

        // Delete the evenement
        restEvenementMockMvc.perform(delete("/api/evenements/{id}", evenement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Evenement> evenementList = evenementRepository.findAll();
        assertThat(evenementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
