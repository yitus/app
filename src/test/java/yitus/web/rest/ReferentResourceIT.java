package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Referent;
import yitus.domain.User;
import yitus.domain.Contact;
import yitus.domain.Image;
import yitus.domain.Etablissement;
import yitus.repository.ReferentRepository;
import yitus.service.ReferentService;
import yitus.service.dto.ReferentDTO;
import yitus.service.mapper.ReferentMapper;
import yitus.service.dto.ReferentCriteria;
import yitus.service.ReferentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ReferentResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ReferentResourceIT {

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ReferentRepository referentRepository;

    @Autowired
    private ReferentMapper referentMapper;

    @Autowired
    private ReferentService referentService;

    @Autowired
    private ReferentQueryService referentQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restReferentMockMvc;

    private Referent referent;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Referent createEntity(EntityManager em) {
        Referent referent = new Referent()
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return referent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Referent createUpdatedEntity(EntityManager em) {
        Referent referent = new Referent()
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return referent;
    }

    @BeforeEach
    public void initTest() {
        referent = createEntity(em);
    }

    @Test
    @Transactional
    public void createReferent() throws Exception {
        int databaseSizeBeforeCreate = referentRepository.findAll().size();

        // Create the Referent
        ReferentDTO referentDTO = referentMapper.toDto(referent);
        restReferentMockMvc.perform(post("/api/referents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(referentDTO)))
            .andExpect(status().isCreated());

        // Validate the Referent in the database
        List<Referent> referentList = referentRepository.findAll();
        assertThat(referentList).hasSize(databaseSizeBeforeCreate + 1);
        Referent testReferent = referentList.get(referentList.size() - 1);
        assertThat(testReferent.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testReferent.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testReferent.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createReferentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = referentRepository.findAll().size();

        // Create the Referent with an existing ID
        referent.setId(1L);
        ReferentDTO referentDTO = referentMapper.toDto(referent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReferentMockMvc.perform(post("/api/referents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(referentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Referent in the database
        List<Referent> referentList = referentRepository.findAll();
        assertThat(referentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllReferents() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList
        restReferentMockMvc.perform(get("/api/referents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(referent.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getReferent() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get the referent
        restReferentMockMvc.perform(get("/api/referents/{id}", referent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(referent.getId().intValue()))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getReferentsByIdFiltering() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        Long id = referent.getId();

        defaultReferentShouldBeFound("id.equals=" + id);
        defaultReferentShouldNotBeFound("id.notEquals=" + id);

        defaultReferentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultReferentShouldNotBeFound("id.greaterThan=" + id);

        defaultReferentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultReferentShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllReferentsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultReferentShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the referentList where dateCreation equals to UPDATED_DATE_CREATION
        defaultReferentShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultReferentShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the referentList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultReferentShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultReferentShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the referentList where dateCreation equals to UPDATED_DATE_CREATION
        defaultReferentShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateCreation is not null
        defaultReferentShouldBeFound("dateCreation.specified=true");

        // Get all the referentList where dateCreation is null
        defaultReferentShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllReferentsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultReferentShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the referentList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultReferentShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultReferentShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the referentList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultReferentShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultReferentShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the referentList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultReferentShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateModification is not null
        defaultReferentShouldBeFound("dateModification.specified=true");

        // Get all the referentList where dateModification is null
        defaultReferentShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllReferentsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultReferentShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the referentList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultReferentShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultReferentShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the referentList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultReferentShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultReferentShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the referentList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultReferentShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllReferentsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        // Get all the referentList where dateArchivage is not null
        defaultReferentShouldBeFound("dateArchivage.specified=true");

        // Get all the referentList where dateArchivage is null
        defaultReferentShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllReferentsByUtilisateurIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);
        User utilisateur = UserResourceIT.createEntity(em);
        em.persist(utilisateur);
        em.flush();
        referent.setUtilisateur(utilisateur);
        referentRepository.saveAndFlush(referent);
        Long utilisateurId = utilisateur.getId();

        // Get all the referentList where utilisateur equals to utilisateurId
        defaultReferentShouldBeFound("utilisateurId.equals=" + utilisateurId);

        // Get all the referentList where utilisateur equals to utilisateurId + 1
        defaultReferentShouldNotBeFound("utilisateurId.equals=" + (utilisateurId + 1));
    }


    @Test
    @Transactional
    public void getAllReferentsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        referent.setContact(contact);
        referentRepository.saveAndFlush(referent);
        Long contactId = contact.getId();

        // Get all the referentList where contact equals to contactId
        defaultReferentShouldBeFound("contactId.equals=" + contactId);

        // Get all the referentList where contact equals to contactId + 1
        defaultReferentShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }


    @Test
    @Transactional
    public void getAllReferentsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);
        Image photo = ImageResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        referent.setPhoto(photo);
        referentRepository.saveAndFlush(referent);
        Long photoId = photo.getId();

        // Get all the referentList where photo equals to photoId
        defaultReferentShouldBeFound("photoId.equals=" + photoId);

        // Get all the referentList where photo equals to photoId + 1
        defaultReferentShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }


    @Test
    @Transactional
    public void getAllReferentsByEtablissementIsEqualToSomething() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);
        Etablissement etablissement = EtablissementResourceIT.createEntity(em);
        em.persist(etablissement);
        em.flush();
        referent.setEtablissement(etablissement);
        referentRepository.saveAndFlush(referent);
        Long etablissementId = etablissement.getId();

        // Get all the referentList where etablissement equals to etablissementId
        defaultReferentShouldBeFound("etablissementId.equals=" + etablissementId);

        // Get all the referentList where etablissement equals to etablissementId + 1
        defaultReferentShouldNotBeFound("etablissementId.equals=" + (etablissementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultReferentShouldBeFound(String filter) throws Exception {
        restReferentMockMvc.perform(get("/api/referents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(referent.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restReferentMockMvc.perform(get("/api/referents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultReferentShouldNotBeFound(String filter) throws Exception {
        restReferentMockMvc.perform(get("/api/referents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restReferentMockMvc.perform(get("/api/referents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingReferent() throws Exception {
        // Get the referent
        restReferentMockMvc.perform(get("/api/referents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReferent() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        int databaseSizeBeforeUpdate = referentRepository.findAll().size();

        // Update the referent
        Referent updatedReferent = referentRepository.findById(referent.getId()).get();
        // Disconnect from session so that the updates on updatedReferent are not directly saved in db
        em.detach(updatedReferent);
        updatedReferent
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        ReferentDTO referentDTO = referentMapper.toDto(updatedReferent);

        restReferentMockMvc.perform(put("/api/referents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(referentDTO)))
            .andExpect(status().isOk());

        // Validate the Referent in the database
        List<Referent> referentList = referentRepository.findAll();
        assertThat(referentList).hasSize(databaseSizeBeforeUpdate);
        Referent testReferent = referentList.get(referentList.size() - 1);
        assertThat(testReferent.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testReferent.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testReferent.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingReferent() throws Exception {
        int databaseSizeBeforeUpdate = referentRepository.findAll().size();

        // Create the Referent
        ReferentDTO referentDTO = referentMapper.toDto(referent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReferentMockMvc.perform(put("/api/referents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(referentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Referent in the database
        List<Referent> referentList = referentRepository.findAll();
        assertThat(referentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReferent() throws Exception {
        // Initialize the database
        referentRepository.saveAndFlush(referent);

        int databaseSizeBeforeDelete = referentRepository.findAll().size();

        // Delete the referent
        restReferentMockMvc.perform(delete("/api/referents/{id}", referent.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Referent> referentList = referentRepository.findAll();
        assertThat(referentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
