package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Etablissement;
import yitus.domain.ChefEtablissement;
import yitus.domain.Image;
import yitus.repository.EtablissementRepository;
import yitus.service.EtablissementService;
import yitus.service.dto.EtablissementDTO;
import yitus.service.mapper.EtablissementMapper;
import yitus.service.dto.EtablissementCriteria;
import yitus.service.EtablissementQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import yitus.domain.enumeration.TypeEtablissement;
/**
 * Integration tests for the {@link EtablissementResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class EtablissementResourceIT {

    private static final TypeEtablissement DEFAULT_TYPE_ETABLISSEMENT = TypeEtablissement.COLLEGE;
    private static final TypeEtablissement UPDATED_TYPE_ETABLISSEMENT = TypeEtablissement.LYCEE;

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_SITEWEB = "AAAAAAAAAA";
    private static final String UPDATED_SITEWEB = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_VILLE = "AAAAAAAAAA";
    private static final String UPDATED_VILLE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_POSTAL = "AAAAAAAAAA";
    private static final String UPDATED_CODE_POSTAL = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private EtablissementRepository etablissementRepository;

    @Autowired
    private EtablissementMapper etablissementMapper;

    @Autowired
    private EtablissementService etablissementService;

    @Autowired
    private EtablissementQueryService etablissementQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEtablissementMockMvc;

    private Etablissement etablissement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etablissement createEntity(EntityManager em) {
        Etablissement etablissement = new Etablissement()
            .typeEtablissement(DEFAULT_TYPE_ETABLISSEMENT)
            .nom(DEFAULT_NOM)
            .siteweb(DEFAULT_SITEWEB)
            .adresse(DEFAULT_ADRESSE)
            .ville(DEFAULT_VILLE)
            .codePostal(DEFAULT_CODE_POSTAL)
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return etablissement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etablissement createUpdatedEntity(EntityManager em) {
        Etablissement etablissement = new Etablissement()
            .typeEtablissement(UPDATED_TYPE_ETABLISSEMENT)
            .nom(UPDATED_NOM)
            .siteweb(UPDATED_SITEWEB)
            .adresse(UPDATED_ADRESSE)
            .ville(UPDATED_VILLE)
            .codePostal(UPDATED_CODE_POSTAL)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return etablissement;
    }

    @BeforeEach
    public void initTest() {
        etablissement = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtablissement() throws Exception {
        int databaseSizeBeforeCreate = etablissementRepository.findAll().size();

        // Create the Etablissement
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);
        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isCreated());

        // Validate the Etablissement in the database
        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeCreate + 1);
        Etablissement testEtablissement = etablissementList.get(etablissementList.size() - 1);
        assertThat(testEtablissement.getTypeEtablissement()).isEqualTo(DEFAULT_TYPE_ETABLISSEMENT);
        assertThat(testEtablissement.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testEtablissement.getSiteweb()).isEqualTo(DEFAULT_SITEWEB);
        assertThat(testEtablissement.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testEtablissement.getVille()).isEqualTo(DEFAULT_VILLE);
        assertThat(testEtablissement.getCodePostal()).isEqualTo(DEFAULT_CODE_POSTAL);
        assertThat(testEtablissement.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testEtablissement.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testEtablissement.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createEtablissementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etablissementRepository.findAll().size();

        // Create the Etablissement with an existing ID
        etablissement.setId(1L);
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Etablissement in the database
        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTypeEtablissementIsRequired() throws Exception {
        int databaseSizeBeforeTest = etablissementRepository.findAll().size();
        // set the field null
        etablissement.setTypeEtablissement(null);

        // Create the Etablissement, which fails.
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = etablissementRepository.findAll().size();
        // set the field null
        etablissement.setNom(null);

        // Create the Etablissement, which fails.
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSitewebIsRequired() throws Exception {
        int databaseSizeBeforeTest = etablissementRepository.findAll().size();
        // set the field null
        etablissement.setSiteweb(null);

        // Create the Etablissement, which fails.
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAdresseIsRequired() throws Exception {
        int databaseSizeBeforeTest = etablissementRepository.findAll().size();
        // set the field null
        etablissement.setAdresse(null);

        // Create the Etablissement, which fails.
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVilleIsRequired() throws Exception {
        int databaseSizeBeforeTest = etablissementRepository.findAll().size();
        // set the field null
        etablissement.setVille(null);

        // Create the Etablissement, which fails.
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodePostalIsRequired() throws Exception {
        int databaseSizeBeforeTest = etablissementRepository.findAll().size();
        // set the field null
        etablissement.setCodePostal(null);

        // Create the Etablissement, which fails.
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        restEtablissementMockMvc.perform(post("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEtablissements() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList
        restEtablissementMockMvc.perform(get("/api/etablissements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etablissement.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeEtablissement").value(hasItem(DEFAULT_TYPE_ETABLISSEMENT.toString())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].siteweb").value(hasItem(DEFAULT_SITEWEB)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)))
            .andExpect(jsonPath("$.[*].codePostal").value(hasItem(DEFAULT_CODE_POSTAL)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getEtablissement() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get the etablissement
        restEtablissementMockMvc.perform(get("/api/etablissements/{id}", etablissement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(etablissement.getId().intValue()))
            .andExpect(jsonPath("$.typeEtablissement").value(DEFAULT_TYPE_ETABLISSEMENT.toString()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.siteweb").value(DEFAULT_SITEWEB))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.ville").value(DEFAULT_VILLE))
            .andExpect(jsonPath("$.codePostal").value(DEFAULT_CODE_POSTAL))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getEtablissementsByIdFiltering() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        Long id = etablissement.getId();

        defaultEtablissementShouldBeFound("id.equals=" + id);
        defaultEtablissementShouldNotBeFound("id.notEquals=" + id);

        defaultEtablissementShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEtablissementShouldNotBeFound("id.greaterThan=" + id);

        defaultEtablissementShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEtablissementShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllEtablissementsByTypeEtablissementIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where typeEtablissement equals to DEFAULT_TYPE_ETABLISSEMENT
        defaultEtablissementShouldBeFound("typeEtablissement.equals=" + DEFAULT_TYPE_ETABLISSEMENT);

        // Get all the etablissementList where typeEtablissement equals to UPDATED_TYPE_ETABLISSEMENT
        defaultEtablissementShouldNotBeFound("typeEtablissement.equals=" + UPDATED_TYPE_ETABLISSEMENT);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByTypeEtablissementIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where typeEtablissement not equals to DEFAULT_TYPE_ETABLISSEMENT
        defaultEtablissementShouldNotBeFound("typeEtablissement.notEquals=" + DEFAULT_TYPE_ETABLISSEMENT);

        // Get all the etablissementList where typeEtablissement not equals to UPDATED_TYPE_ETABLISSEMENT
        defaultEtablissementShouldBeFound("typeEtablissement.notEquals=" + UPDATED_TYPE_ETABLISSEMENT);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByTypeEtablissementIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where typeEtablissement in DEFAULT_TYPE_ETABLISSEMENT or UPDATED_TYPE_ETABLISSEMENT
        defaultEtablissementShouldBeFound("typeEtablissement.in=" + DEFAULT_TYPE_ETABLISSEMENT + "," + UPDATED_TYPE_ETABLISSEMENT);

        // Get all the etablissementList where typeEtablissement equals to UPDATED_TYPE_ETABLISSEMENT
        defaultEtablissementShouldNotBeFound("typeEtablissement.in=" + UPDATED_TYPE_ETABLISSEMENT);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByTypeEtablissementIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where typeEtablissement is not null
        defaultEtablissementShouldBeFound("typeEtablissement.specified=true");

        // Get all the etablissementList where typeEtablissement is null
        defaultEtablissementShouldNotBeFound("typeEtablissement.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtablissementsByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where nom equals to DEFAULT_NOM
        defaultEtablissementShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the etablissementList where nom equals to UPDATED_NOM
        defaultEtablissementShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where nom not equals to DEFAULT_NOM
        defaultEtablissementShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the etablissementList where nom not equals to UPDATED_NOM
        defaultEtablissementShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByNomIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultEtablissementShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the etablissementList where nom equals to UPDATED_NOM
        defaultEtablissementShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where nom is not null
        defaultEtablissementShouldBeFound("nom.specified=true");

        // Get all the etablissementList where nom is null
        defaultEtablissementShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllEtablissementsByNomContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where nom contains DEFAULT_NOM
        defaultEtablissementShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the etablissementList where nom contains UPDATED_NOM
        defaultEtablissementShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByNomNotContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where nom does not contain DEFAULT_NOM
        defaultEtablissementShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the etablissementList where nom does not contain UPDATED_NOM
        defaultEtablissementShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllEtablissementsBySitewebIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where siteweb equals to DEFAULT_SITEWEB
        defaultEtablissementShouldBeFound("siteweb.equals=" + DEFAULT_SITEWEB);

        // Get all the etablissementList where siteweb equals to UPDATED_SITEWEB
        defaultEtablissementShouldNotBeFound("siteweb.equals=" + UPDATED_SITEWEB);
    }

    @Test
    @Transactional
    public void getAllEtablissementsBySitewebIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where siteweb not equals to DEFAULT_SITEWEB
        defaultEtablissementShouldNotBeFound("siteweb.notEquals=" + DEFAULT_SITEWEB);

        // Get all the etablissementList where siteweb not equals to UPDATED_SITEWEB
        defaultEtablissementShouldBeFound("siteweb.notEquals=" + UPDATED_SITEWEB);
    }

    @Test
    @Transactional
    public void getAllEtablissementsBySitewebIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where siteweb in DEFAULT_SITEWEB or UPDATED_SITEWEB
        defaultEtablissementShouldBeFound("siteweb.in=" + DEFAULT_SITEWEB + "," + UPDATED_SITEWEB);

        // Get all the etablissementList where siteweb equals to UPDATED_SITEWEB
        defaultEtablissementShouldNotBeFound("siteweb.in=" + UPDATED_SITEWEB);
    }

    @Test
    @Transactional
    public void getAllEtablissementsBySitewebIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where siteweb is not null
        defaultEtablissementShouldBeFound("siteweb.specified=true");

        // Get all the etablissementList where siteweb is null
        defaultEtablissementShouldNotBeFound("siteweb.specified=false");
    }
                @Test
    @Transactional
    public void getAllEtablissementsBySitewebContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where siteweb contains DEFAULT_SITEWEB
        defaultEtablissementShouldBeFound("siteweb.contains=" + DEFAULT_SITEWEB);

        // Get all the etablissementList where siteweb contains UPDATED_SITEWEB
        defaultEtablissementShouldNotBeFound("siteweb.contains=" + UPDATED_SITEWEB);
    }

    @Test
    @Transactional
    public void getAllEtablissementsBySitewebNotContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where siteweb does not contain DEFAULT_SITEWEB
        defaultEtablissementShouldNotBeFound("siteweb.doesNotContain=" + DEFAULT_SITEWEB);

        // Get all the etablissementList where siteweb does not contain UPDATED_SITEWEB
        defaultEtablissementShouldBeFound("siteweb.doesNotContain=" + UPDATED_SITEWEB);
    }


    @Test
    @Transactional
    public void getAllEtablissementsByAdresseIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where adresse equals to DEFAULT_ADRESSE
        defaultEtablissementShouldBeFound("adresse.equals=" + DEFAULT_ADRESSE);

        // Get all the etablissementList where adresse equals to UPDATED_ADRESSE
        defaultEtablissementShouldNotBeFound("adresse.equals=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByAdresseIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where adresse not equals to DEFAULT_ADRESSE
        defaultEtablissementShouldNotBeFound("adresse.notEquals=" + DEFAULT_ADRESSE);

        // Get all the etablissementList where adresse not equals to UPDATED_ADRESSE
        defaultEtablissementShouldBeFound("adresse.notEquals=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByAdresseIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where adresse in DEFAULT_ADRESSE or UPDATED_ADRESSE
        defaultEtablissementShouldBeFound("adresse.in=" + DEFAULT_ADRESSE + "," + UPDATED_ADRESSE);

        // Get all the etablissementList where adresse equals to UPDATED_ADRESSE
        defaultEtablissementShouldNotBeFound("adresse.in=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByAdresseIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where adresse is not null
        defaultEtablissementShouldBeFound("adresse.specified=true");

        // Get all the etablissementList where adresse is null
        defaultEtablissementShouldNotBeFound("adresse.specified=false");
    }
                @Test
    @Transactional
    public void getAllEtablissementsByAdresseContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where adresse contains DEFAULT_ADRESSE
        defaultEtablissementShouldBeFound("adresse.contains=" + DEFAULT_ADRESSE);

        // Get all the etablissementList where adresse contains UPDATED_ADRESSE
        defaultEtablissementShouldNotBeFound("adresse.contains=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByAdresseNotContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where adresse does not contain DEFAULT_ADRESSE
        defaultEtablissementShouldNotBeFound("adresse.doesNotContain=" + DEFAULT_ADRESSE);

        // Get all the etablissementList where adresse does not contain UPDATED_ADRESSE
        defaultEtablissementShouldBeFound("adresse.doesNotContain=" + UPDATED_ADRESSE);
    }


    @Test
    @Transactional
    public void getAllEtablissementsByVilleIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where ville equals to DEFAULT_VILLE
        defaultEtablissementShouldBeFound("ville.equals=" + DEFAULT_VILLE);

        // Get all the etablissementList where ville equals to UPDATED_VILLE
        defaultEtablissementShouldNotBeFound("ville.equals=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByVilleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where ville not equals to DEFAULT_VILLE
        defaultEtablissementShouldNotBeFound("ville.notEquals=" + DEFAULT_VILLE);

        // Get all the etablissementList where ville not equals to UPDATED_VILLE
        defaultEtablissementShouldBeFound("ville.notEquals=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByVilleIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where ville in DEFAULT_VILLE or UPDATED_VILLE
        defaultEtablissementShouldBeFound("ville.in=" + DEFAULT_VILLE + "," + UPDATED_VILLE);

        // Get all the etablissementList where ville equals to UPDATED_VILLE
        defaultEtablissementShouldNotBeFound("ville.in=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByVilleIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where ville is not null
        defaultEtablissementShouldBeFound("ville.specified=true");

        // Get all the etablissementList where ville is null
        defaultEtablissementShouldNotBeFound("ville.specified=false");
    }
                @Test
    @Transactional
    public void getAllEtablissementsByVilleContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where ville contains DEFAULT_VILLE
        defaultEtablissementShouldBeFound("ville.contains=" + DEFAULT_VILLE);

        // Get all the etablissementList where ville contains UPDATED_VILLE
        defaultEtablissementShouldNotBeFound("ville.contains=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByVilleNotContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where ville does not contain DEFAULT_VILLE
        defaultEtablissementShouldNotBeFound("ville.doesNotContain=" + DEFAULT_VILLE);

        // Get all the etablissementList where ville does not contain UPDATED_VILLE
        defaultEtablissementShouldBeFound("ville.doesNotContain=" + UPDATED_VILLE);
    }


    @Test
    @Transactional
    public void getAllEtablissementsByCodePostalIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where codePostal equals to DEFAULT_CODE_POSTAL
        defaultEtablissementShouldBeFound("codePostal.equals=" + DEFAULT_CODE_POSTAL);

        // Get all the etablissementList where codePostal equals to UPDATED_CODE_POSTAL
        defaultEtablissementShouldNotBeFound("codePostal.equals=" + UPDATED_CODE_POSTAL);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByCodePostalIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where codePostal not equals to DEFAULT_CODE_POSTAL
        defaultEtablissementShouldNotBeFound("codePostal.notEquals=" + DEFAULT_CODE_POSTAL);

        // Get all the etablissementList where codePostal not equals to UPDATED_CODE_POSTAL
        defaultEtablissementShouldBeFound("codePostal.notEquals=" + UPDATED_CODE_POSTAL);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByCodePostalIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where codePostal in DEFAULT_CODE_POSTAL or UPDATED_CODE_POSTAL
        defaultEtablissementShouldBeFound("codePostal.in=" + DEFAULT_CODE_POSTAL + "," + UPDATED_CODE_POSTAL);

        // Get all the etablissementList where codePostal equals to UPDATED_CODE_POSTAL
        defaultEtablissementShouldNotBeFound("codePostal.in=" + UPDATED_CODE_POSTAL);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByCodePostalIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where codePostal is not null
        defaultEtablissementShouldBeFound("codePostal.specified=true");

        // Get all the etablissementList where codePostal is null
        defaultEtablissementShouldNotBeFound("codePostal.specified=false");
    }
                @Test
    @Transactional
    public void getAllEtablissementsByCodePostalContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where codePostal contains DEFAULT_CODE_POSTAL
        defaultEtablissementShouldBeFound("codePostal.contains=" + DEFAULT_CODE_POSTAL);

        // Get all the etablissementList where codePostal contains UPDATED_CODE_POSTAL
        defaultEtablissementShouldNotBeFound("codePostal.contains=" + UPDATED_CODE_POSTAL);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByCodePostalNotContainsSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where codePostal does not contain DEFAULT_CODE_POSTAL
        defaultEtablissementShouldNotBeFound("codePostal.doesNotContain=" + DEFAULT_CODE_POSTAL);

        // Get all the etablissementList where codePostal does not contain UPDATED_CODE_POSTAL
        defaultEtablissementShouldBeFound("codePostal.doesNotContain=" + UPDATED_CODE_POSTAL);
    }


    @Test
    @Transactional
    public void getAllEtablissementsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultEtablissementShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the etablissementList where dateCreation equals to UPDATED_DATE_CREATION
        defaultEtablissementShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultEtablissementShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the etablissementList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultEtablissementShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultEtablissementShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the etablissementList where dateCreation equals to UPDATED_DATE_CREATION
        defaultEtablissementShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateCreation is not null
        defaultEtablissementShouldBeFound("dateCreation.specified=true");

        // Get all the etablissementList where dateCreation is null
        defaultEtablissementShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultEtablissementShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the etablissementList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultEtablissementShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultEtablissementShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the etablissementList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultEtablissementShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultEtablissementShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the etablissementList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultEtablissementShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateModification is not null
        defaultEtablissementShouldBeFound("dateModification.specified=true");

        // Get all the etablissementList where dateModification is null
        defaultEtablissementShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultEtablissementShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the etablissementList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultEtablissementShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultEtablissementShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the etablissementList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultEtablissementShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultEtablissementShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the etablissementList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultEtablissementShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllEtablissementsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        // Get all the etablissementList where dateArchivage is not null
        defaultEtablissementShouldBeFound("dateArchivage.specified=true");

        // Get all the etablissementList where dateArchivage is null
        defaultEtablissementShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllEtablissementsByChefIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);
        ChefEtablissement chef = ChefEtablissementResourceIT.createEntity(em);
        em.persist(chef);
        em.flush();
        etablissement.setChef(chef);
        etablissementRepository.saveAndFlush(etablissement);
        Long chefId = chef.getId();

        // Get all the etablissementList where chef equals to chefId
        defaultEtablissementShouldBeFound("chefId.equals=" + chefId);

        // Get all the etablissementList where chef equals to chefId + 1
        defaultEtablissementShouldNotBeFound("chefId.equals=" + (chefId + 1));
    }


    @Test
    @Transactional
    public void getAllEtablissementsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);
        Image photo = ImageResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        etablissement.setPhoto(photo);
        etablissementRepository.saveAndFlush(etablissement);
        Long photoId = photo.getId();

        // Get all the etablissementList where photo equals to photoId
        defaultEtablissementShouldBeFound("photoId.equals=" + photoId);

        // Get all the etablissementList where photo equals to photoId + 1
        defaultEtablissementShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtablissementShouldBeFound(String filter) throws Exception {
        restEtablissementMockMvc.perform(get("/api/etablissements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etablissement.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeEtablissement").value(hasItem(DEFAULT_TYPE_ETABLISSEMENT.toString())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].siteweb").value(hasItem(DEFAULT_SITEWEB)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)))
            .andExpect(jsonPath("$.[*].codePostal").value(hasItem(DEFAULT_CODE_POSTAL)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restEtablissementMockMvc.perform(get("/api/etablissements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtablissementShouldNotBeFound(String filter) throws Exception {
        restEtablissementMockMvc.perform(get("/api/etablissements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtablissementMockMvc.perform(get("/api/etablissements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEtablissement() throws Exception {
        // Get the etablissement
        restEtablissementMockMvc.perform(get("/api/etablissements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtablissement() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        int databaseSizeBeforeUpdate = etablissementRepository.findAll().size();

        // Update the etablissement
        Etablissement updatedEtablissement = etablissementRepository.findById(etablissement.getId()).get();
        // Disconnect from session so that the updates on updatedEtablissement are not directly saved in db
        em.detach(updatedEtablissement);
        updatedEtablissement
            .typeEtablissement(UPDATED_TYPE_ETABLISSEMENT)
            .nom(UPDATED_NOM)
            .siteweb(UPDATED_SITEWEB)
            .adresse(UPDATED_ADRESSE)
            .ville(UPDATED_VILLE)
            .codePostal(UPDATED_CODE_POSTAL)
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(updatedEtablissement);

        restEtablissementMockMvc.perform(put("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isOk());

        // Validate the Etablissement in the database
        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeUpdate);
        Etablissement testEtablissement = etablissementList.get(etablissementList.size() - 1);
        assertThat(testEtablissement.getTypeEtablissement()).isEqualTo(UPDATED_TYPE_ETABLISSEMENT);
        assertThat(testEtablissement.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testEtablissement.getSiteweb()).isEqualTo(UPDATED_SITEWEB);
        assertThat(testEtablissement.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testEtablissement.getVille()).isEqualTo(UPDATED_VILLE);
        assertThat(testEtablissement.getCodePostal()).isEqualTo(UPDATED_CODE_POSTAL);
        assertThat(testEtablissement.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testEtablissement.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testEtablissement.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingEtablissement() throws Exception {
        int databaseSizeBeforeUpdate = etablissementRepository.findAll().size();

        // Create the Etablissement
        EtablissementDTO etablissementDTO = etablissementMapper.toDto(etablissement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtablissementMockMvc.perform(put("/api/etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etablissementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Etablissement in the database
        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtablissement() throws Exception {
        // Initialize the database
        etablissementRepository.saveAndFlush(etablissement);

        int databaseSizeBeforeDelete = etablissementRepository.findAll().size();

        // Delete the etablissement
        restEtablissementMockMvc.perform(delete("/api/etablissements/{id}", etablissement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Etablissement> etablissementList = etablissementRepository.findAll();
        assertThat(etablissementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
