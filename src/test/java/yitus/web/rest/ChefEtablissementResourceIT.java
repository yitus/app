package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.ChefEtablissement;
import yitus.domain.User;
import yitus.domain.Image;
import yitus.domain.Etablissement;
import yitus.repository.ChefEtablissementRepository;
import yitus.service.ChefEtablissementService;
import yitus.service.dto.ChefEtablissementDTO;
import yitus.service.mapper.ChefEtablissementMapper;
import yitus.service.dto.ChefEtablissementCriteria;
import yitus.service.ChefEtablissementQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChefEtablissementResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ChefEtablissementResourceIT {

    private static final Instant DEFAULT_DATE_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_MODIFICATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_MODIFICATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATE_ARCHIVAGE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ARCHIVAGE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ChefEtablissementRepository chefEtablissementRepository;

    @Autowired
    private ChefEtablissementMapper chefEtablissementMapper;

    @Autowired
    private ChefEtablissementService chefEtablissementService;

    @Autowired
    private ChefEtablissementQueryService chefEtablissementQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChefEtablissementMockMvc;

    private ChefEtablissement chefEtablissement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChefEtablissement createEntity(EntityManager em) {
        ChefEtablissement chefEtablissement = new ChefEtablissement()
            .dateCreation(DEFAULT_DATE_CREATION)
            .dateModification(DEFAULT_DATE_MODIFICATION)
            .dateArchivage(DEFAULT_DATE_ARCHIVAGE);
        return chefEtablissement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChefEtablissement createUpdatedEntity(EntityManager em) {
        ChefEtablissement chefEtablissement = new ChefEtablissement()
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        return chefEtablissement;
    }

    @BeforeEach
    public void initTest() {
        chefEtablissement = createEntity(em);
    }

    @Test
    @Transactional
    public void createChefEtablissement() throws Exception {
        int databaseSizeBeforeCreate = chefEtablissementRepository.findAll().size();

        // Create the ChefEtablissement
        ChefEtablissementDTO chefEtablissementDTO = chefEtablissementMapper.toDto(chefEtablissement);
        restChefEtablissementMockMvc.perform(post("/api/chef-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chefEtablissementDTO)))
            .andExpect(status().isCreated());

        // Validate the ChefEtablissement in the database
        List<ChefEtablissement> chefEtablissementList = chefEtablissementRepository.findAll();
        assertThat(chefEtablissementList).hasSize(databaseSizeBeforeCreate + 1);
        ChefEtablissement testChefEtablissement = chefEtablissementList.get(chefEtablissementList.size() - 1);
        assertThat(testChefEtablissement.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testChefEtablissement.getDateModification()).isEqualTo(DEFAULT_DATE_MODIFICATION);
        assertThat(testChefEtablissement.getDateArchivage()).isEqualTo(DEFAULT_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void createChefEtablissementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chefEtablissementRepository.findAll().size();

        // Create the ChefEtablissement with an existing ID
        chefEtablissement.setId(1L);
        ChefEtablissementDTO chefEtablissementDTO = chefEtablissementMapper.toDto(chefEtablissement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChefEtablissementMockMvc.perform(post("/api/chef-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chefEtablissementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChefEtablissement in the database
        List<ChefEtablissement> chefEtablissementList = chefEtablissementRepository.findAll();
        assertThat(chefEtablissementList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllChefEtablissements() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chefEtablissement.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getChefEtablissement() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get the chefEtablissement
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements/{id}", chefEtablissement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chefEtablissement.getId().intValue()))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.dateModification").value(DEFAULT_DATE_MODIFICATION.toString()))
            .andExpect(jsonPath("$.dateArchivage").value(DEFAULT_DATE_ARCHIVAGE.toString()));
    }


    @Test
    @Transactional
    public void getChefEtablissementsByIdFiltering() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        Long id = chefEtablissement.getId();

        defaultChefEtablissementShouldBeFound("id.equals=" + id);
        defaultChefEtablissementShouldNotBeFound("id.notEquals=" + id);

        defaultChefEtablissementShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChefEtablissementShouldNotBeFound("id.greaterThan=" + id);

        defaultChefEtablissementShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChefEtablissementShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChefEtablissementsByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultChefEtablissementShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the chefEtablissementList where dateCreation equals to UPDATED_DATE_CREATION
        defaultChefEtablissementShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultChefEtablissementShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the chefEtablissementList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultChefEtablissementShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultChefEtablissementShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the chefEtablissementList where dateCreation equals to UPDATED_DATE_CREATION
        defaultChefEtablissementShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateCreation is not null
        defaultChefEtablissementShouldBeFound("dateCreation.specified=true");

        // Get all the chefEtablissementList where dateCreation is null
        defaultChefEtablissementShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateModificationIsEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateModification equals to DEFAULT_DATE_MODIFICATION
        defaultChefEtablissementShouldBeFound("dateModification.equals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the chefEtablissementList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultChefEtablissementShouldNotBeFound("dateModification.equals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateModificationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateModification not equals to DEFAULT_DATE_MODIFICATION
        defaultChefEtablissementShouldNotBeFound("dateModification.notEquals=" + DEFAULT_DATE_MODIFICATION);

        // Get all the chefEtablissementList where dateModification not equals to UPDATED_DATE_MODIFICATION
        defaultChefEtablissementShouldBeFound("dateModification.notEquals=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateModificationIsInShouldWork() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateModification in DEFAULT_DATE_MODIFICATION or UPDATED_DATE_MODIFICATION
        defaultChefEtablissementShouldBeFound("dateModification.in=" + DEFAULT_DATE_MODIFICATION + "," + UPDATED_DATE_MODIFICATION);

        // Get all the chefEtablissementList where dateModification equals to UPDATED_DATE_MODIFICATION
        defaultChefEtablissementShouldNotBeFound("dateModification.in=" + UPDATED_DATE_MODIFICATION);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateModificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateModification is not null
        defaultChefEtablissementShouldBeFound("dateModification.specified=true");

        // Get all the chefEtablissementList where dateModification is null
        defaultChefEtablissementShouldNotBeFound("dateModification.specified=false");
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateArchivageIsEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateArchivage equals to DEFAULT_DATE_ARCHIVAGE
        defaultChefEtablissementShouldBeFound("dateArchivage.equals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the chefEtablissementList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultChefEtablissementShouldNotBeFound("dateArchivage.equals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateArchivageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateArchivage not equals to DEFAULT_DATE_ARCHIVAGE
        defaultChefEtablissementShouldNotBeFound("dateArchivage.notEquals=" + DEFAULT_DATE_ARCHIVAGE);

        // Get all the chefEtablissementList where dateArchivage not equals to UPDATED_DATE_ARCHIVAGE
        defaultChefEtablissementShouldBeFound("dateArchivage.notEquals=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateArchivageIsInShouldWork() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateArchivage in DEFAULT_DATE_ARCHIVAGE or UPDATED_DATE_ARCHIVAGE
        defaultChefEtablissementShouldBeFound("dateArchivage.in=" + DEFAULT_DATE_ARCHIVAGE + "," + UPDATED_DATE_ARCHIVAGE);

        // Get all the chefEtablissementList where dateArchivage equals to UPDATED_DATE_ARCHIVAGE
        defaultChefEtablissementShouldNotBeFound("dateArchivage.in=" + UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByDateArchivageIsNullOrNotNull() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        // Get all the chefEtablissementList where dateArchivage is not null
        defaultChefEtablissementShouldBeFound("dateArchivage.specified=true");

        // Get all the chefEtablissementList where dateArchivage is null
        defaultChefEtablissementShouldNotBeFound("dateArchivage.specified=false");
    }

    @Test
    @Transactional
    public void getAllChefEtablissementsByUtilisateurIsEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);
        User utilisateur = UserResourceIT.createEntity(em);
        em.persist(utilisateur);
        em.flush();
        chefEtablissement.setUtilisateur(utilisateur);
        chefEtablissementRepository.saveAndFlush(chefEtablissement);
        Long utilisateurId = utilisateur.getId();

        // Get all the chefEtablissementList where utilisateur equals to utilisateurId
        defaultChefEtablissementShouldBeFound("utilisateurId.equals=" + utilisateurId);

        // Get all the chefEtablissementList where utilisateur equals to utilisateurId + 1
        defaultChefEtablissementShouldNotBeFound("utilisateurId.equals=" + (utilisateurId + 1));
    }


    @Test
    @Transactional
    public void getAllChefEtablissementsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);
        Image photo = ImageResourceIT.createEntity(em);
        em.persist(photo);
        em.flush();
        chefEtablissement.setPhoto(photo);
        chefEtablissementRepository.saveAndFlush(chefEtablissement);
        Long photoId = photo.getId();

        // Get all the chefEtablissementList where photo equals to photoId
        defaultChefEtablissementShouldBeFound("photoId.equals=" + photoId);

        // Get all the chefEtablissementList where photo equals to photoId + 1
        defaultChefEtablissementShouldNotBeFound("photoId.equals=" + (photoId + 1));
    }


    @Test
    @Transactional
    public void getAllChefEtablissementsByEtablissementIsEqualToSomething() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);
        Etablissement etablissement = EtablissementResourceIT.createEntity(em);
        em.persist(etablissement);
        em.flush();
        chefEtablissement.setEtablissement(etablissement);
        etablissement.setChef(chefEtablissement);
        chefEtablissementRepository.saveAndFlush(chefEtablissement);
        Long etablissementId = etablissement.getId();

        // Get all the chefEtablissementList where etablissement equals to etablissementId
        defaultChefEtablissementShouldBeFound("etablissementId.equals=" + etablissementId);

        // Get all the chefEtablissementList where etablissement equals to etablissementId + 1
        defaultChefEtablissementShouldNotBeFound("etablissementId.equals=" + (etablissementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChefEtablissementShouldBeFound(String filter) throws Exception {
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chefEtablissement.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].dateModification").value(hasItem(DEFAULT_DATE_MODIFICATION.toString())))
            .andExpect(jsonPath("$.[*].dateArchivage").value(hasItem(DEFAULT_DATE_ARCHIVAGE.toString())));

        // Check, that the count call also returns 1
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChefEtablissementShouldNotBeFound(String filter) throws Exception {
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChefEtablissement() throws Exception {
        // Get the chefEtablissement
        restChefEtablissementMockMvc.perform(get("/api/chef-etablissements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChefEtablissement() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        int databaseSizeBeforeUpdate = chefEtablissementRepository.findAll().size();

        // Update the chefEtablissement
        ChefEtablissement updatedChefEtablissement = chefEtablissementRepository.findById(chefEtablissement.getId()).get();
        // Disconnect from session so that the updates on updatedChefEtablissement are not directly saved in db
        em.detach(updatedChefEtablissement);
        updatedChefEtablissement
            .dateCreation(UPDATED_DATE_CREATION)
            .dateModification(UPDATED_DATE_MODIFICATION)
            .dateArchivage(UPDATED_DATE_ARCHIVAGE);
        ChefEtablissementDTO chefEtablissementDTO = chefEtablissementMapper.toDto(updatedChefEtablissement);

        restChefEtablissementMockMvc.perform(put("/api/chef-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chefEtablissementDTO)))
            .andExpect(status().isOk());

        // Validate the ChefEtablissement in the database
        List<ChefEtablissement> chefEtablissementList = chefEtablissementRepository.findAll();
        assertThat(chefEtablissementList).hasSize(databaseSizeBeforeUpdate);
        ChefEtablissement testChefEtablissement = chefEtablissementList.get(chefEtablissementList.size() - 1);
        assertThat(testChefEtablissement.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testChefEtablissement.getDateModification()).isEqualTo(UPDATED_DATE_MODIFICATION);
        assertThat(testChefEtablissement.getDateArchivage()).isEqualTo(UPDATED_DATE_ARCHIVAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingChefEtablissement() throws Exception {
        int databaseSizeBeforeUpdate = chefEtablissementRepository.findAll().size();

        // Create the ChefEtablissement
        ChefEtablissementDTO chefEtablissementDTO = chefEtablissementMapper.toDto(chefEtablissement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChefEtablissementMockMvc.perform(put("/api/chef-etablissements")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chefEtablissementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChefEtablissement in the database
        List<ChefEtablissement> chefEtablissementList = chefEtablissementRepository.findAll();
        assertThat(chefEtablissementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChefEtablissement() throws Exception {
        // Initialize the database
        chefEtablissementRepository.saveAndFlush(chefEtablissement);

        int databaseSizeBeforeDelete = chefEtablissementRepository.findAll().size();

        // Delete the chefEtablissement
        restChefEtablissementMockMvc.perform(delete("/api/chef-etablissements/{id}", chefEtablissement.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChefEtablissement> chefEtablissementList = chefEtablissementRepository.findAll();
        assertThat(chefEtablissementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
