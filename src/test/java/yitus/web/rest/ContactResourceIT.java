package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Contact;
import yitus.repository.ContactRepository;
import yitus.service.ContactService;
import yitus.service.dto.ContactDTO;
import yitus.service.mapper.ContactMapper;
import yitus.service.dto.ContactCriteria;
import yitus.service.ContactQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ContactResourceIT {

    private static final String DEFAULT_BADGES = "";
    private static final String UPDATED_BADGES = "B";

    private static final byte[] DEFAULT_AJOUT_CONTENU_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_AJOUT_CONTENU_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_AJOUT_CONTENU_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_AJOUT_CONTENU_IMAGE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "0031804392";
    private static final String UPDATED_TELEPHONE = "0116153316";

    private static final String DEFAULT_VILLE = "AAAAAAAAAA";
    private static final String UPDATED_VILLE = "BBBBBBBBBB";

    private static final String DEFAULT_GRAVATAR = "AAAAAAAAAA";
    private static final String UPDATED_GRAVATAR = "BBBBBBBBBB";

    private static final String DEFAULT_LINKEDIN = "AAAAAAAAAA";
    private static final String UPDATED_LINKEDIN = "BBBBBBBBBB";

    private static final String DEFAULT_GOOGLE = "AAAAAAAAAA";
    private static final String UPDATED_GOOGLE = "BBBBBBBBBB";

    private static final String DEFAULT_FACEBOOK = "AAAAAAAAAA";
    private static final String UPDATED_FACEBOOK = "BBBBBBBBBB";

    private static final String DEFAULT_SKYPE = "AAAAAAAAAA";
    private static final String UPDATED_SKYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DISCORD = "AAAAAAAAAA";
    private static final String UPDATED_DISCORD = "BBBBBBBBBB";

    private static final String DEFAULT_ZOOM = "AAAAAAAAAA";
    private static final String UPDATED_ZOOM = "BBBBBBBBBB";

    private static final String DEFAULT_WHEREBY = "AAAAAAAAAA";
    private static final String UPDATED_WHEREBY = "BBBBBBBBBB";

    private static final String DEFAULT_MUMBLE = "AAAAAAAAAA";
    private static final String UPDATED_MUMBLE = "BBBBBBBBBB";

    private static final String DEFAULT_TOX = "AAAAAAAAAA";
    private static final String UPDATED_TOX = "BBBBBBBBBB";

    private static final String DEFAULT_MATRIX = "AAAAAAAAAA";
    private static final String UPDATED_MATRIX = "BBBBBBBBBB";

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactMapper contactMapper;

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactQueryService contactQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactMockMvc;

    private Contact contact;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contact createEntity(EntityManager em) {
        Contact contact = new Contact()
            .badges(DEFAULT_BADGES)
            .ajoutContenuImage(DEFAULT_AJOUT_CONTENU_IMAGE)
            .ajoutContenuImageContentType(DEFAULT_AJOUT_CONTENU_IMAGE_CONTENT_TYPE)
            .login(DEFAULT_LOGIN)
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .email(DEFAULT_EMAIL)
            .telephone(DEFAULT_TELEPHONE)
            .ville(DEFAULT_VILLE)
            .gravatar(DEFAULT_GRAVATAR)
            .linkedin(DEFAULT_LINKEDIN)
            .google(DEFAULT_GOOGLE)
            .facebook(DEFAULT_FACEBOOK)
            .skype(DEFAULT_SKYPE)
            .discord(DEFAULT_DISCORD)
            .zoom(DEFAULT_ZOOM)
            .whereby(DEFAULT_WHEREBY)
            .mumble(DEFAULT_MUMBLE)
            .tox(DEFAULT_TOX)
            .matrix(DEFAULT_MATRIX);
        return contact;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contact createUpdatedEntity(EntityManager em) {
        Contact contact = new Contact()
            .badges(UPDATED_BADGES)
            .ajoutContenuImage(UPDATED_AJOUT_CONTENU_IMAGE)
            .ajoutContenuImageContentType(UPDATED_AJOUT_CONTENU_IMAGE_CONTENT_TYPE)
            .login(UPDATED_LOGIN)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .email(UPDATED_EMAIL)
            .telephone(UPDATED_TELEPHONE)
            .ville(UPDATED_VILLE)
            .gravatar(UPDATED_GRAVATAR)
            .linkedin(UPDATED_LINKEDIN)
            .google(UPDATED_GOOGLE)
            .facebook(UPDATED_FACEBOOK)
            .skype(UPDATED_SKYPE)
            .discord(UPDATED_DISCORD)
            .zoom(UPDATED_ZOOM)
            .whereby(UPDATED_WHEREBY)
            .mumble(UPDATED_MUMBLE)
            .tox(UPDATED_TOX)
            .matrix(UPDATED_MATRIX);
        return contact;
    }

    @BeforeEach
    public void initTest() {
        contact = createEntity(em);
    }

    @Test
    @Transactional
    public void createContact() throws Exception {
        int databaseSizeBeforeCreate = contactRepository.findAll().size();

        // Create the Contact
        ContactDTO contactDTO = contactMapper.toDto(contact);
        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isCreated());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeCreate + 1);
        Contact testContact = contactList.get(contactList.size() - 1);
        assertThat(testContact.getBadges()).isEqualTo(DEFAULT_BADGES);
        assertThat(testContact.getAjoutContenuImage()).isEqualTo(DEFAULT_AJOUT_CONTENU_IMAGE);
        assertThat(testContact.getAjoutContenuImageContentType()).isEqualTo(DEFAULT_AJOUT_CONTENU_IMAGE_CONTENT_TYPE);
        assertThat(testContact.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testContact.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testContact.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testContact.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testContact.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testContact.getVille()).isEqualTo(DEFAULT_VILLE);
        assertThat(testContact.getGravatar()).isEqualTo(DEFAULT_GRAVATAR);
        assertThat(testContact.getLinkedin()).isEqualTo(DEFAULT_LINKEDIN);
        assertThat(testContact.getGoogle()).isEqualTo(DEFAULT_GOOGLE);
        assertThat(testContact.getFacebook()).isEqualTo(DEFAULT_FACEBOOK);
        assertThat(testContact.getSkype()).isEqualTo(DEFAULT_SKYPE);
        assertThat(testContact.getDiscord()).isEqualTo(DEFAULT_DISCORD);
        assertThat(testContact.getZoom()).isEqualTo(DEFAULT_ZOOM);
        assertThat(testContact.getWhereby()).isEqualTo(DEFAULT_WHEREBY);
        assertThat(testContact.getMumble()).isEqualTo(DEFAULT_MUMBLE);
        assertThat(testContact.getTox()).isEqualTo(DEFAULT_TOX);
        assertThat(testContact.getMatrix()).isEqualTo(DEFAULT_MATRIX);
    }

    @Test
    @Transactional
    public void createContactWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactRepository.findAll().size();

        // Create the Contact with an existing ID
        contact.setId(1L);
        ContactDTO contactDTO = contactMapper.toDto(contact);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setEmail(null);

        // Create the Contact, which fails.
        ContactDTO contactDTO = contactMapper.toDto(contact);

        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContacts() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contact.getId().intValue())))
            .andExpect(jsonPath("$.[*].badges").value(hasItem(DEFAULT_BADGES)))
            .andExpect(jsonPath("$.[*].ajoutContenuImageContentType").value(hasItem(DEFAULT_AJOUT_CONTENU_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].ajoutContenuImage").value(hasItem(Base64Utils.encodeToString(DEFAULT_AJOUT_CONTENU_IMAGE))))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)))
            .andExpect(jsonPath("$.[*].gravatar").value(hasItem(DEFAULT_GRAVATAR)))
            .andExpect(jsonPath("$.[*].linkedin").value(hasItem(DEFAULT_LINKEDIN)))
            .andExpect(jsonPath("$.[*].google").value(hasItem(DEFAULT_GOOGLE)))
            .andExpect(jsonPath("$.[*].facebook").value(hasItem(DEFAULT_FACEBOOK)))
            .andExpect(jsonPath("$.[*].skype").value(hasItem(DEFAULT_SKYPE)))
            .andExpect(jsonPath("$.[*].discord").value(hasItem(DEFAULT_DISCORD)))
            .andExpect(jsonPath("$.[*].zoom").value(hasItem(DEFAULT_ZOOM)))
            .andExpect(jsonPath("$.[*].whereby").value(hasItem(DEFAULT_WHEREBY)))
            .andExpect(jsonPath("$.[*].mumble").value(hasItem(DEFAULT_MUMBLE)))
            .andExpect(jsonPath("$.[*].tox").value(hasItem(DEFAULT_TOX)))
            .andExpect(jsonPath("$.[*].matrix").value(hasItem(DEFAULT_MATRIX)));
    }
    
    @Test
    @Transactional
    public void getContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get the contact
        restContactMockMvc.perform(get("/api/contacts/{id}", contact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contact.getId().intValue()))
            .andExpect(jsonPath("$.badges").value(DEFAULT_BADGES))
            .andExpect(jsonPath("$.ajoutContenuImageContentType").value(DEFAULT_AJOUT_CONTENU_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.ajoutContenuImage").value(Base64Utils.encodeToString(DEFAULT_AJOUT_CONTENU_IMAGE)))
            .andExpect(jsonPath("$.login").value(DEFAULT_LOGIN))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.ville").value(DEFAULT_VILLE))
            .andExpect(jsonPath("$.gravatar").value(DEFAULT_GRAVATAR))
            .andExpect(jsonPath("$.linkedin").value(DEFAULT_LINKEDIN))
            .andExpect(jsonPath("$.google").value(DEFAULT_GOOGLE))
            .andExpect(jsonPath("$.facebook").value(DEFAULT_FACEBOOK))
            .andExpect(jsonPath("$.skype").value(DEFAULT_SKYPE))
            .andExpect(jsonPath("$.discord").value(DEFAULT_DISCORD))
            .andExpect(jsonPath("$.zoom").value(DEFAULT_ZOOM))
            .andExpect(jsonPath("$.whereby").value(DEFAULT_WHEREBY))
            .andExpect(jsonPath("$.mumble").value(DEFAULT_MUMBLE))
            .andExpect(jsonPath("$.tox").value(DEFAULT_TOX))
            .andExpect(jsonPath("$.matrix").value(DEFAULT_MATRIX));
    }


    @Test
    @Transactional
    public void getContactsByIdFiltering() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        Long id = contact.getId();

        defaultContactShouldBeFound("id.equals=" + id);
        defaultContactShouldNotBeFound("id.notEquals=" + id);

        defaultContactShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactShouldNotBeFound("id.greaterThan=" + id);

        defaultContactShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactsByBadgesIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where badges equals to DEFAULT_BADGES
        defaultContactShouldBeFound("badges.equals=" + DEFAULT_BADGES);

        // Get all the contactList where badges equals to UPDATED_BADGES
        defaultContactShouldNotBeFound("badges.equals=" + UPDATED_BADGES);
    }

    @Test
    @Transactional
    public void getAllContactsByBadgesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where badges not equals to DEFAULT_BADGES
        defaultContactShouldNotBeFound("badges.notEquals=" + DEFAULT_BADGES);

        // Get all the contactList where badges not equals to UPDATED_BADGES
        defaultContactShouldBeFound("badges.notEquals=" + UPDATED_BADGES);
    }

    @Test
    @Transactional
    public void getAllContactsByBadgesIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where badges in DEFAULT_BADGES or UPDATED_BADGES
        defaultContactShouldBeFound("badges.in=" + DEFAULT_BADGES + "," + UPDATED_BADGES);

        // Get all the contactList where badges equals to UPDATED_BADGES
        defaultContactShouldNotBeFound("badges.in=" + UPDATED_BADGES);
    }

    @Test
    @Transactional
    public void getAllContactsByBadgesIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where badges is not null
        defaultContactShouldBeFound("badges.specified=true");

        // Get all the contactList where badges is null
        defaultContactShouldNotBeFound("badges.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByBadgesContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where badges contains DEFAULT_BADGES
        defaultContactShouldBeFound("badges.contains=" + DEFAULT_BADGES);

        // Get all the contactList where badges contains UPDATED_BADGES
        defaultContactShouldNotBeFound("badges.contains=" + UPDATED_BADGES);
    }

    @Test
    @Transactional
    public void getAllContactsByBadgesNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where badges does not contain DEFAULT_BADGES
        defaultContactShouldNotBeFound("badges.doesNotContain=" + DEFAULT_BADGES);

        // Get all the contactList where badges does not contain UPDATED_BADGES
        defaultContactShouldBeFound("badges.doesNotContain=" + UPDATED_BADGES);
    }


    @Test
    @Transactional
    public void getAllContactsByLoginIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where login equals to DEFAULT_LOGIN
        defaultContactShouldBeFound("login.equals=" + DEFAULT_LOGIN);

        // Get all the contactList where login equals to UPDATED_LOGIN
        defaultContactShouldNotBeFound("login.equals=" + UPDATED_LOGIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLoginIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where login not equals to DEFAULT_LOGIN
        defaultContactShouldNotBeFound("login.notEquals=" + DEFAULT_LOGIN);

        // Get all the contactList where login not equals to UPDATED_LOGIN
        defaultContactShouldBeFound("login.notEquals=" + UPDATED_LOGIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLoginIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where login in DEFAULT_LOGIN or UPDATED_LOGIN
        defaultContactShouldBeFound("login.in=" + DEFAULT_LOGIN + "," + UPDATED_LOGIN);

        // Get all the contactList where login equals to UPDATED_LOGIN
        defaultContactShouldNotBeFound("login.in=" + UPDATED_LOGIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLoginIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where login is not null
        defaultContactShouldBeFound("login.specified=true");

        // Get all the contactList where login is null
        defaultContactShouldNotBeFound("login.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByLoginContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where login contains DEFAULT_LOGIN
        defaultContactShouldBeFound("login.contains=" + DEFAULT_LOGIN);

        // Get all the contactList where login contains UPDATED_LOGIN
        defaultContactShouldNotBeFound("login.contains=" + UPDATED_LOGIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLoginNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where login does not contain DEFAULT_LOGIN
        defaultContactShouldNotBeFound("login.doesNotContain=" + DEFAULT_LOGIN);

        // Get all the contactList where login does not contain UPDATED_LOGIN
        defaultContactShouldBeFound("login.doesNotContain=" + UPDATED_LOGIN);
    }


    @Test
    @Transactional
    public void getAllContactsByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nom equals to DEFAULT_NOM
        defaultContactShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the contactList where nom equals to UPDATED_NOM
        defaultContactShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllContactsByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nom not equals to DEFAULT_NOM
        defaultContactShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the contactList where nom not equals to UPDATED_NOM
        defaultContactShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllContactsByNomIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultContactShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the contactList where nom equals to UPDATED_NOM
        defaultContactShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllContactsByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nom is not null
        defaultContactShouldBeFound("nom.specified=true");

        // Get all the contactList where nom is null
        defaultContactShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByNomContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nom contains DEFAULT_NOM
        defaultContactShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the contactList where nom contains UPDATED_NOM
        defaultContactShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllContactsByNomNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nom does not contain DEFAULT_NOM
        defaultContactShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the contactList where nom does not contain UPDATED_NOM
        defaultContactShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllContactsByPrenomIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prenom equals to DEFAULT_PRENOM
        defaultContactShouldBeFound("prenom.equals=" + DEFAULT_PRENOM);

        // Get all the contactList where prenom equals to UPDATED_PRENOM
        defaultContactShouldNotBeFound("prenom.equals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllContactsByPrenomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prenom not equals to DEFAULT_PRENOM
        defaultContactShouldNotBeFound("prenom.notEquals=" + DEFAULT_PRENOM);

        // Get all the contactList where prenom not equals to UPDATED_PRENOM
        defaultContactShouldBeFound("prenom.notEquals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllContactsByPrenomIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prenom in DEFAULT_PRENOM or UPDATED_PRENOM
        defaultContactShouldBeFound("prenom.in=" + DEFAULT_PRENOM + "," + UPDATED_PRENOM);

        // Get all the contactList where prenom equals to UPDATED_PRENOM
        defaultContactShouldNotBeFound("prenom.in=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllContactsByPrenomIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prenom is not null
        defaultContactShouldBeFound("prenom.specified=true");

        // Get all the contactList where prenom is null
        defaultContactShouldNotBeFound("prenom.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByPrenomContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prenom contains DEFAULT_PRENOM
        defaultContactShouldBeFound("prenom.contains=" + DEFAULT_PRENOM);

        // Get all the contactList where prenom contains UPDATED_PRENOM
        defaultContactShouldNotBeFound("prenom.contains=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllContactsByPrenomNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prenom does not contain DEFAULT_PRENOM
        defaultContactShouldNotBeFound("prenom.doesNotContain=" + DEFAULT_PRENOM);

        // Get all the contactList where prenom does not contain UPDATED_PRENOM
        defaultContactShouldBeFound("prenom.doesNotContain=" + UPDATED_PRENOM);
    }


    @Test
    @Transactional
    public void getAllContactsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where email equals to DEFAULT_EMAIL
        defaultContactShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the contactList where email equals to UPDATED_EMAIL
        defaultContactShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactsByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where email not equals to DEFAULT_EMAIL
        defaultContactShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the contactList where email not equals to UPDATED_EMAIL
        defaultContactShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultContactShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the contactList where email equals to UPDATED_EMAIL
        defaultContactShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where email is not null
        defaultContactShouldBeFound("email.specified=true");

        // Get all the contactList where email is null
        defaultContactShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByEmailContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where email contains DEFAULT_EMAIL
        defaultContactShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the contactList where email contains UPDATED_EMAIL
        defaultContactShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where email does not contain DEFAULT_EMAIL
        defaultContactShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the contactList where email does not contain UPDATED_EMAIL
        defaultContactShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllContactsByTelephoneIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where telephone equals to DEFAULT_TELEPHONE
        defaultContactShouldBeFound("telephone.equals=" + DEFAULT_TELEPHONE);

        // Get all the contactList where telephone equals to UPDATED_TELEPHONE
        defaultContactShouldNotBeFound("telephone.equals=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllContactsByTelephoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where telephone not equals to DEFAULT_TELEPHONE
        defaultContactShouldNotBeFound("telephone.notEquals=" + DEFAULT_TELEPHONE);

        // Get all the contactList where telephone not equals to UPDATED_TELEPHONE
        defaultContactShouldBeFound("telephone.notEquals=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllContactsByTelephoneIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where telephone in DEFAULT_TELEPHONE or UPDATED_TELEPHONE
        defaultContactShouldBeFound("telephone.in=" + DEFAULT_TELEPHONE + "," + UPDATED_TELEPHONE);

        // Get all the contactList where telephone equals to UPDATED_TELEPHONE
        defaultContactShouldNotBeFound("telephone.in=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllContactsByTelephoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where telephone is not null
        defaultContactShouldBeFound("telephone.specified=true");

        // Get all the contactList where telephone is null
        defaultContactShouldNotBeFound("telephone.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByTelephoneContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where telephone contains DEFAULT_TELEPHONE
        defaultContactShouldBeFound("telephone.contains=" + DEFAULT_TELEPHONE);

        // Get all the contactList where telephone contains UPDATED_TELEPHONE
        defaultContactShouldNotBeFound("telephone.contains=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllContactsByTelephoneNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where telephone does not contain DEFAULT_TELEPHONE
        defaultContactShouldNotBeFound("telephone.doesNotContain=" + DEFAULT_TELEPHONE);

        // Get all the contactList where telephone does not contain UPDATED_TELEPHONE
        defaultContactShouldBeFound("telephone.doesNotContain=" + UPDATED_TELEPHONE);
    }


    @Test
    @Transactional
    public void getAllContactsByVilleIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where ville equals to DEFAULT_VILLE
        defaultContactShouldBeFound("ville.equals=" + DEFAULT_VILLE);

        // Get all the contactList where ville equals to UPDATED_VILLE
        defaultContactShouldNotBeFound("ville.equals=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllContactsByVilleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where ville not equals to DEFAULT_VILLE
        defaultContactShouldNotBeFound("ville.notEquals=" + DEFAULT_VILLE);

        // Get all the contactList where ville not equals to UPDATED_VILLE
        defaultContactShouldBeFound("ville.notEquals=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllContactsByVilleIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where ville in DEFAULT_VILLE or UPDATED_VILLE
        defaultContactShouldBeFound("ville.in=" + DEFAULT_VILLE + "," + UPDATED_VILLE);

        // Get all the contactList where ville equals to UPDATED_VILLE
        defaultContactShouldNotBeFound("ville.in=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllContactsByVilleIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where ville is not null
        defaultContactShouldBeFound("ville.specified=true");

        // Get all the contactList where ville is null
        defaultContactShouldNotBeFound("ville.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByVilleContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where ville contains DEFAULT_VILLE
        defaultContactShouldBeFound("ville.contains=" + DEFAULT_VILLE);

        // Get all the contactList where ville contains UPDATED_VILLE
        defaultContactShouldNotBeFound("ville.contains=" + UPDATED_VILLE);
    }

    @Test
    @Transactional
    public void getAllContactsByVilleNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where ville does not contain DEFAULT_VILLE
        defaultContactShouldNotBeFound("ville.doesNotContain=" + DEFAULT_VILLE);

        // Get all the contactList where ville does not contain UPDATED_VILLE
        defaultContactShouldBeFound("ville.doesNotContain=" + UPDATED_VILLE);
    }


    @Test
    @Transactional
    public void getAllContactsByGravatarIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where gravatar equals to DEFAULT_GRAVATAR
        defaultContactShouldBeFound("gravatar.equals=" + DEFAULT_GRAVATAR);

        // Get all the contactList where gravatar equals to UPDATED_GRAVATAR
        defaultContactShouldNotBeFound("gravatar.equals=" + UPDATED_GRAVATAR);
    }

    @Test
    @Transactional
    public void getAllContactsByGravatarIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where gravatar not equals to DEFAULT_GRAVATAR
        defaultContactShouldNotBeFound("gravatar.notEquals=" + DEFAULT_GRAVATAR);

        // Get all the contactList where gravatar not equals to UPDATED_GRAVATAR
        defaultContactShouldBeFound("gravatar.notEquals=" + UPDATED_GRAVATAR);
    }

    @Test
    @Transactional
    public void getAllContactsByGravatarIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where gravatar in DEFAULT_GRAVATAR or UPDATED_GRAVATAR
        defaultContactShouldBeFound("gravatar.in=" + DEFAULT_GRAVATAR + "," + UPDATED_GRAVATAR);

        // Get all the contactList where gravatar equals to UPDATED_GRAVATAR
        defaultContactShouldNotBeFound("gravatar.in=" + UPDATED_GRAVATAR);
    }

    @Test
    @Transactional
    public void getAllContactsByGravatarIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where gravatar is not null
        defaultContactShouldBeFound("gravatar.specified=true");

        // Get all the contactList where gravatar is null
        defaultContactShouldNotBeFound("gravatar.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByGravatarContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where gravatar contains DEFAULT_GRAVATAR
        defaultContactShouldBeFound("gravatar.contains=" + DEFAULT_GRAVATAR);

        // Get all the contactList where gravatar contains UPDATED_GRAVATAR
        defaultContactShouldNotBeFound("gravatar.contains=" + UPDATED_GRAVATAR);
    }

    @Test
    @Transactional
    public void getAllContactsByGravatarNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where gravatar does not contain DEFAULT_GRAVATAR
        defaultContactShouldNotBeFound("gravatar.doesNotContain=" + DEFAULT_GRAVATAR);

        // Get all the contactList where gravatar does not contain UPDATED_GRAVATAR
        defaultContactShouldBeFound("gravatar.doesNotContain=" + UPDATED_GRAVATAR);
    }


    @Test
    @Transactional
    public void getAllContactsByLinkedinIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where linkedin equals to DEFAULT_LINKEDIN
        defaultContactShouldBeFound("linkedin.equals=" + DEFAULT_LINKEDIN);

        // Get all the contactList where linkedin equals to UPDATED_LINKEDIN
        defaultContactShouldNotBeFound("linkedin.equals=" + UPDATED_LINKEDIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLinkedinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where linkedin not equals to DEFAULT_LINKEDIN
        defaultContactShouldNotBeFound("linkedin.notEquals=" + DEFAULT_LINKEDIN);

        // Get all the contactList where linkedin not equals to UPDATED_LINKEDIN
        defaultContactShouldBeFound("linkedin.notEquals=" + UPDATED_LINKEDIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLinkedinIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where linkedin in DEFAULT_LINKEDIN or UPDATED_LINKEDIN
        defaultContactShouldBeFound("linkedin.in=" + DEFAULT_LINKEDIN + "," + UPDATED_LINKEDIN);

        // Get all the contactList where linkedin equals to UPDATED_LINKEDIN
        defaultContactShouldNotBeFound("linkedin.in=" + UPDATED_LINKEDIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLinkedinIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where linkedin is not null
        defaultContactShouldBeFound("linkedin.specified=true");

        // Get all the contactList where linkedin is null
        defaultContactShouldNotBeFound("linkedin.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByLinkedinContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where linkedin contains DEFAULT_LINKEDIN
        defaultContactShouldBeFound("linkedin.contains=" + DEFAULT_LINKEDIN);

        // Get all the contactList where linkedin contains UPDATED_LINKEDIN
        defaultContactShouldNotBeFound("linkedin.contains=" + UPDATED_LINKEDIN);
    }

    @Test
    @Transactional
    public void getAllContactsByLinkedinNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where linkedin does not contain DEFAULT_LINKEDIN
        defaultContactShouldNotBeFound("linkedin.doesNotContain=" + DEFAULT_LINKEDIN);

        // Get all the contactList where linkedin does not contain UPDATED_LINKEDIN
        defaultContactShouldBeFound("linkedin.doesNotContain=" + UPDATED_LINKEDIN);
    }


    @Test
    @Transactional
    public void getAllContactsByGoogleIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where google equals to DEFAULT_GOOGLE
        defaultContactShouldBeFound("google.equals=" + DEFAULT_GOOGLE);

        // Get all the contactList where google equals to UPDATED_GOOGLE
        defaultContactShouldNotBeFound("google.equals=" + UPDATED_GOOGLE);
    }

    @Test
    @Transactional
    public void getAllContactsByGoogleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where google not equals to DEFAULT_GOOGLE
        defaultContactShouldNotBeFound("google.notEquals=" + DEFAULT_GOOGLE);

        // Get all the contactList where google not equals to UPDATED_GOOGLE
        defaultContactShouldBeFound("google.notEquals=" + UPDATED_GOOGLE);
    }

    @Test
    @Transactional
    public void getAllContactsByGoogleIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where google in DEFAULT_GOOGLE or UPDATED_GOOGLE
        defaultContactShouldBeFound("google.in=" + DEFAULT_GOOGLE + "," + UPDATED_GOOGLE);

        // Get all the contactList where google equals to UPDATED_GOOGLE
        defaultContactShouldNotBeFound("google.in=" + UPDATED_GOOGLE);
    }

    @Test
    @Transactional
    public void getAllContactsByGoogleIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where google is not null
        defaultContactShouldBeFound("google.specified=true");

        // Get all the contactList where google is null
        defaultContactShouldNotBeFound("google.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByGoogleContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where google contains DEFAULT_GOOGLE
        defaultContactShouldBeFound("google.contains=" + DEFAULT_GOOGLE);

        // Get all the contactList where google contains UPDATED_GOOGLE
        defaultContactShouldNotBeFound("google.contains=" + UPDATED_GOOGLE);
    }

    @Test
    @Transactional
    public void getAllContactsByGoogleNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where google does not contain DEFAULT_GOOGLE
        defaultContactShouldNotBeFound("google.doesNotContain=" + DEFAULT_GOOGLE);

        // Get all the contactList where google does not contain UPDATED_GOOGLE
        defaultContactShouldBeFound("google.doesNotContain=" + UPDATED_GOOGLE);
    }


    @Test
    @Transactional
    public void getAllContactsByFacebookIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where facebook equals to DEFAULT_FACEBOOK
        defaultContactShouldBeFound("facebook.equals=" + DEFAULT_FACEBOOK);

        // Get all the contactList where facebook equals to UPDATED_FACEBOOK
        defaultContactShouldNotBeFound("facebook.equals=" + UPDATED_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllContactsByFacebookIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where facebook not equals to DEFAULT_FACEBOOK
        defaultContactShouldNotBeFound("facebook.notEquals=" + DEFAULT_FACEBOOK);

        // Get all the contactList where facebook not equals to UPDATED_FACEBOOK
        defaultContactShouldBeFound("facebook.notEquals=" + UPDATED_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllContactsByFacebookIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where facebook in DEFAULT_FACEBOOK or UPDATED_FACEBOOK
        defaultContactShouldBeFound("facebook.in=" + DEFAULT_FACEBOOK + "," + UPDATED_FACEBOOK);

        // Get all the contactList where facebook equals to UPDATED_FACEBOOK
        defaultContactShouldNotBeFound("facebook.in=" + UPDATED_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllContactsByFacebookIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where facebook is not null
        defaultContactShouldBeFound("facebook.specified=true");

        // Get all the contactList where facebook is null
        defaultContactShouldNotBeFound("facebook.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByFacebookContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where facebook contains DEFAULT_FACEBOOK
        defaultContactShouldBeFound("facebook.contains=" + DEFAULT_FACEBOOK);

        // Get all the contactList where facebook contains UPDATED_FACEBOOK
        defaultContactShouldNotBeFound("facebook.contains=" + UPDATED_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllContactsByFacebookNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where facebook does not contain DEFAULT_FACEBOOK
        defaultContactShouldNotBeFound("facebook.doesNotContain=" + DEFAULT_FACEBOOK);

        // Get all the contactList where facebook does not contain UPDATED_FACEBOOK
        defaultContactShouldBeFound("facebook.doesNotContain=" + UPDATED_FACEBOOK);
    }


    @Test
    @Transactional
    public void getAllContactsBySkypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where skype equals to DEFAULT_SKYPE
        defaultContactShouldBeFound("skype.equals=" + DEFAULT_SKYPE);

        // Get all the contactList where skype equals to UPDATED_SKYPE
        defaultContactShouldNotBeFound("skype.equals=" + UPDATED_SKYPE);
    }

    @Test
    @Transactional
    public void getAllContactsBySkypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where skype not equals to DEFAULT_SKYPE
        defaultContactShouldNotBeFound("skype.notEquals=" + DEFAULT_SKYPE);

        // Get all the contactList where skype not equals to UPDATED_SKYPE
        defaultContactShouldBeFound("skype.notEquals=" + UPDATED_SKYPE);
    }

    @Test
    @Transactional
    public void getAllContactsBySkypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where skype in DEFAULT_SKYPE or UPDATED_SKYPE
        defaultContactShouldBeFound("skype.in=" + DEFAULT_SKYPE + "," + UPDATED_SKYPE);

        // Get all the contactList where skype equals to UPDATED_SKYPE
        defaultContactShouldNotBeFound("skype.in=" + UPDATED_SKYPE);
    }

    @Test
    @Transactional
    public void getAllContactsBySkypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where skype is not null
        defaultContactShouldBeFound("skype.specified=true");

        // Get all the contactList where skype is null
        defaultContactShouldNotBeFound("skype.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsBySkypeContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where skype contains DEFAULT_SKYPE
        defaultContactShouldBeFound("skype.contains=" + DEFAULT_SKYPE);

        // Get all the contactList where skype contains UPDATED_SKYPE
        defaultContactShouldNotBeFound("skype.contains=" + UPDATED_SKYPE);
    }

    @Test
    @Transactional
    public void getAllContactsBySkypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where skype does not contain DEFAULT_SKYPE
        defaultContactShouldNotBeFound("skype.doesNotContain=" + DEFAULT_SKYPE);

        // Get all the contactList where skype does not contain UPDATED_SKYPE
        defaultContactShouldBeFound("skype.doesNotContain=" + UPDATED_SKYPE);
    }


    @Test
    @Transactional
    public void getAllContactsByDiscordIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where discord equals to DEFAULT_DISCORD
        defaultContactShouldBeFound("discord.equals=" + DEFAULT_DISCORD);

        // Get all the contactList where discord equals to UPDATED_DISCORD
        defaultContactShouldNotBeFound("discord.equals=" + UPDATED_DISCORD);
    }

    @Test
    @Transactional
    public void getAllContactsByDiscordIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where discord not equals to DEFAULT_DISCORD
        defaultContactShouldNotBeFound("discord.notEquals=" + DEFAULT_DISCORD);

        // Get all the contactList where discord not equals to UPDATED_DISCORD
        defaultContactShouldBeFound("discord.notEquals=" + UPDATED_DISCORD);
    }

    @Test
    @Transactional
    public void getAllContactsByDiscordIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where discord in DEFAULT_DISCORD or UPDATED_DISCORD
        defaultContactShouldBeFound("discord.in=" + DEFAULT_DISCORD + "," + UPDATED_DISCORD);

        // Get all the contactList where discord equals to UPDATED_DISCORD
        defaultContactShouldNotBeFound("discord.in=" + UPDATED_DISCORD);
    }

    @Test
    @Transactional
    public void getAllContactsByDiscordIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where discord is not null
        defaultContactShouldBeFound("discord.specified=true");

        // Get all the contactList where discord is null
        defaultContactShouldNotBeFound("discord.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByDiscordContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where discord contains DEFAULT_DISCORD
        defaultContactShouldBeFound("discord.contains=" + DEFAULT_DISCORD);

        // Get all the contactList where discord contains UPDATED_DISCORD
        defaultContactShouldNotBeFound("discord.contains=" + UPDATED_DISCORD);
    }

    @Test
    @Transactional
    public void getAllContactsByDiscordNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where discord does not contain DEFAULT_DISCORD
        defaultContactShouldNotBeFound("discord.doesNotContain=" + DEFAULT_DISCORD);

        // Get all the contactList where discord does not contain UPDATED_DISCORD
        defaultContactShouldBeFound("discord.doesNotContain=" + UPDATED_DISCORD);
    }


    @Test
    @Transactional
    public void getAllContactsByZoomIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where zoom equals to DEFAULT_ZOOM
        defaultContactShouldBeFound("zoom.equals=" + DEFAULT_ZOOM);

        // Get all the contactList where zoom equals to UPDATED_ZOOM
        defaultContactShouldNotBeFound("zoom.equals=" + UPDATED_ZOOM);
    }

    @Test
    @Transactional
    public void getAllContactsByZoomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where zoom not equals to DEFAULT_ZOOM
        defaultContactShouldNotBeFound("zoom.notEquals=" + DEFAULT_ZOOM);

        // Get all the contactList where zoom not equals to UPDATED_ZOOM
        defaultContactShouldBeFound("zoom.notEquals=" + UPDATED_ZOOM);
    }

    @Test
    @Transactional
    public void getAllContactsByZoomIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where zoom in DEFAULT_ZOOM or UPDATED_ZOOM
        defaultContactShouldBeFound("zoom.in=" + DEFAULT_ZOOM + "," + UPDATED_ZOOM);

        // Get all the contactList where zoom equals to UPDATED_ZOOM
        defaultContactShouldNotBeFound("zoom.in=" + UPDATED_ZOOM);
    }

    @Test
    @Transactional
    public void getAllContactsByZoomIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where zoom is not null
        defaultContactShouldBeFound("zoom.specified=true");

        // Get all the contactList where zoom is null
        defaultContactShouldNotBeFound("zoom.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByZoomContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where zoom contains DEFAULT_ZOOM
        defaultContactShouldBeFound("zoom.contains=" + DEFAULT_ZOOM);

        // Get all the contactList where zoom contains UPDATED_ZOOM
        defaultContactShouldNotBeFound("zoom.contains=" + UPDATED_ZOOM);
    }

    @Test
    @Transactional
    public void getAllContactsByZoomNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where zoom does not contain DEFAULT_ZOOM
        defaultContactShouldNotBeFound("zoom.doesNotContain=" + DEFAULT_ZOOM);

        // Get all the contactList where zoom does not contain UPDATED_ZOOM
        defaultContactShouldBeFound("zoom.doesNotContain=" + UPDATED_ZOOM);
    }


    @Test
    @Transactional
    public void getAllContactsByWherebyIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where whereby equals to DEFAULT_WHEREBY
        defaultContactShouldBeFound("whereby.equals=" + DEFAULT_WHEREBY);

        // Get all the contactList where whereby equals to UPDATED_WHEREBY
        defaultContactShouldNotBeFound("whereby.equals=" + UPDATED_WHEREBY);
    }

    @Test
    @Transactional
    public void getAllContactsByWherebyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where whereby not equals to DEFAULT_WHEREBY
        defaultContactShouldNotBeFound("whereby.notEquals=" + DEFAULT_WHEREBY);

        // Get all the contactList where whereby not equals to UPDATED_WHEREBY
        defaultContactShouldBeFound("whereby.notEquals=" + UPDATED_WHEREBY);
    }

    @Test
    @Transactional
    public void getAllContactsByWherebyIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where whereby in DEFAULT_WHEREBY or UPDATED_WHEREBY
        defaultContactShouldBeFound("whereby.in=" + DEFAULT_WHEREBY + "," + UPDATED_WHEREBY);

        // Get all the contactList where whereby equals to UPDATED_WHEREBY
        defaultContactShouldNotBeFound("whereby.in=" + UPDATED_WHEREBY);
    }

    @Test
    @Transactional
    public void getAllContactsByWherebyIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where whereby is not null
        defaultContactShouldBeFound("whereby.specified=true");

        // Get all the contactList where whereby is null
        defaultContactShouldNotBeFound("whereby.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByWherebyContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where whereby contains DEFAULT_WHEREBY
        defaultContactShouldBeFound("whereby.contains=" + DEFAULT_WHEREBY);

        // Get all the contactList where whereby contains UPDATED_WHEREBY
        defaultContactShouldNotBeFound("whereby.contains=" + UPDATED_WHEREBY);
    }

    @Test
    @Transactional
    public void getAllContactsByWherebyNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where whereby does not contain DEFAULT_WHEREBY
        defaultContactShouldNotBeFound("whereby.doesNotContain=" + DEFAULT_WHEREBY);

        // Get all the contactList where whereby does not contain UPDATED_WHEREBY
        defaultContactShouldBeFound("whereby.doesNotContain=" + UPDATED_WHEREBY);
    }


    @Test
    @Transactional
    public void getAllContactsByMumbleIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mumble equals to DEFAULT_MUMBLE
        defaultContactShouldBeFound("mumble.equals=" + DEFAULT_MUMBLE);

        // Get all the contactList where mumble equals to UPDATED_MUMBLE
        defaultContactShouldNotBeFound("mumble.equals=" + UPDATED_MUMBLE);
    }

    @Test
    @Transactional
    public void getAllContactsByMumbleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mumble not equals to DEFAULT_MUMBLE
        defaultContactShouldNotBeFound("mumble.notEquals=" + DEFAULT_MUMBLE);

        // Get all the contactList where mumble not equals to UPDATED_MUMBLE
        defaultContactShouldBeFound("mumble.notEquals=" + UPDATED_MUMBLE);
    }

    @Test
    @Transactional
    public void getAllContactsByMumbleIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mumble in DEFAULT_MUMBLE or UPDATED_MUMBLE
        defaultContactShouldBeFound("mumble.in=" + DEFAULT_MUMBLE + "," + UPDATED_MUMBLE);

        // Get all the contactList where mumble equals to UPDATED_MUMBLE
        defaultContactShouldNotBeFound("mumble.in=" + UPDATED_MUMBLE);
    }

    @Test
    @Transactional
    public void getAllContactsByMumbleIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mumble is not null
        defaultContactShouldBeFound("mumble.specified=true");

        // Get all the contactList where mumble is null
        defaultContactShouldNotBeFound("mumble.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByMumbleContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mumble contains DEFAULT_MUMBLE
        defaultContactShouldBeFound("mumble.contains=" + DEFAULT_MUMBLE);

        // Get all the contactList where mumble contains UPDATED_MUMBLE
        defaultContactShouldNotBeFound("mumble.contains=" + UPDATED_MUMBLE);
    }

    @Test
    @Transactional
    public void getAllContactsByMumbleNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mumble does not contain DEFAULT_MUMBLE
        defaultContactShouldNotBeFound("mumble.doesNotContain=" + DEFAULT_MUMBLE);

        // Get all the contactList where mumble does not contain UPDATED_MUMBLE
        defaultContactShouldBeFound("mumble.doesNotContain=" + UPDATED_MUMBLE);
    }


    @Test
    @Transactional
    public void getAllContactsByToxIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tox equals to DEFAULT_TOX
        defaultContactShouldBeFound("tox.equals=" + DEFAULT_TOX);

        // Get all the contactList where tox equals to UPDATED_TOX
        defaultContactShouldNotBeFound("tox.equals=" + UPDATED_TOX);
    }

    @Test
    @Transactional
    public void getAllContactsByToxIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tox not equals to DEFAULT_TOX
        defaultContactShouldNotBeFound("tox.notEquals=" + DEFAULT_TOX);

        // Get all the contactList where tox not equals to UPDATED_TOX
        defaultContactShouldBeFound("tox.notEquals=" + UPDATED_TOX);
    }

    @Test
    @Transactional
    public void getAllContactsByToxIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tox in DEFAULT_TOX or UPDATED_TOX
        defaultContactShouldBeFound("tox.in=" + DEFAULT_TOX + "," + UPDATED_TOX);

        // Get all the contactList where tox equals to UPDATED_TOX
        defaultContactShouldNotBeFound("tox.in=" + UPDATED_TOX);
    }

    @Test
    @Transactional
    public void getAllContactsByToxIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tox is not null
        defaultContactShouldBeFound("tox.specified=true");

        // Get all the contactList where tox is null
        defaultContactShouldNotBeFound("tox.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByToxContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tox contains DEFAULT_TOX
        defaultContactShouldBeFound("tox.contains=" + DEFAULT_TOX);

        // Get all the contactList where tox contains UPDATED_TOX
        defaultContactShouldNotBeFound("tox.contains=" + UPDATED_TOX);
    }

    @Test
    @Transactional
    public void getAllContactsByToxNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tox does not contain DEFAULT_TOX
        defaultContactShouldNotBeFound("tox.doesNotContain=" + DEFAULT_TOX);

        // Get all the contactList where tox does not contain UPDATED_TOX
        defaultContactShouldBeFound("tox.doesNotContain=" + UPDATED_TOX);
    }


    @Test
    @Transactional
    public void getAllContactsByMatrixIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where matrix equals to DEFAULT_MATRIX
        defaultContactShouldBeFound("matrix.equals=" + DEFAULT_MATRIX);

        // Get all the contactList where matrix equals to UPDATED_MATRIX
        defaultContactShouldNotBeFound("matrix.equals=" + UPDATED_MATRIX);
    }

    @Test
    @Transactional
    public void getAllContactsByMatrixIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where matrix not equals to DEFAULT_MATRIX
        defaultContactShouldNotBeFound("matrix.notEquals=" + DEFAULT_MATRIX);

        // Get all the contactList where matrix not equals to UPDATED_MATRIX
        defaultContactShouldBeFound("matrix.notEquals=" + UPDATED_MATRIX);
    }

    @Test
    @Transactional
    public void getAllContactsByMatrixIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where matrix in DEFAULT_MATRIX or UPDATED_MATRIX
        defaultContactShouldBeFound("matrix.in=" + DEFAULT_MATRIX + "," + UPDATED_MATRIX);

        // Get all the contactList where matrix equals to UPDATED_MATRIX
        defaultContactShouldNotBeFound("matrix.in=" + UPDATED_MATRIX);
    }

    @Test
    @Transactional
    public void getAllContactsByMatrixIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where matrix is not null
        defaultContactShouldBeFound("matrix.specified=true");

        // Get all the contactList where matrix is null
        defaultContactShouldNotBeFound("matrix.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByMatrixContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where matrix contains DEFAULT_MATRIX
        defaultContactShouldBeFound("matrix.contains=" + DEFAULT_MATRIX);

        // Get all the contactList where matrix contains UPDATED_MATRIX
        defaultContactShouldNotBeFound("matrix.contains=" + UPDATED_MATRIX);
    }

    @Test
    @Transactional
    public void getAllContactsByMatrixNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where matrix does not contain DEFAULT_MATRIX
        defaultContactShouldNotBeFound("matrix.doesNotContain=" + DEFAULT_MATRIX);

        // Get all the contactList where matrix does not contain UPDATED_MATRIX
        defaultContactShouldBeFound("matrix.doesNotContain=" + UPDATED_MATRIX);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactShouldBeFound(String filter) throws Exception {
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contact.getId().intValue())))
            .andExpect(jsonPath("$.[*].badges").value(hasItem(DEFAULT_BADGES)))
            .andExpect(jsonPath("$.[*].ajoutContenuImageContentType").value(hasItem(DEFAULT_AJOUT_CONTENU_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].ajoutContenuImage").value(hasItem(Base64Utils.encodeToString(DEFAULT_AJOUT_CONTENU_IMAGE))))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE)))
            .andExpect(jsonPath("$.[*].gravatar").value(hasItem(DEFAULT_GRAVATAR)))
            .andExpect(jsonPath("$.[*].linkedin").value(hasItem(DEFAULT_LINKEDIN)))
            .andExpect(jsonPath("$.[*].google").value(hasItem(DEFAULT_GOOGLE)))
            .andExpect(jsonPath("$.[*].facebook").value(hasItem(DEFAULT_FACEBOOK)))
            .andExpect(jsonPath("$.[*].skype").value(hasItem(DEFAULT_SKYPE)))
            .andExpect(jsonPath("$.[*].discord").value(hasItem(DEFAULT_DISCORD)))
            .andExpect(jsonPath("$.[*].zoom").value(hasItem(DEFAULT_ZOOM)))
            .andExpect(jsonPath("$.[*].whereby").value(hasItem(DEFAULT_WHEREBY)))
            .andExpect(jsonPath("$.[*].mumble").value(hasItem(DEFAULT_MUMBLE)))
            .andExpect(jsonPath("$.[*].tox").value(hasItem(DEFAULT_TOX)))
            .andExpect(jsonPath("$.[*].matrix").value(hasItem(DEFAULT_MATRIX)));

        // Check, that the count call also returns 1
        restContactMockMvc.perform(get("/api/contacts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactShouldNotBeFound(String filter) throws Exception {
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactMockMvc.perform(get("/api/contacts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingContact() throws Exception {
        // Get the contact
        restContactMockMvc.perform(get("/api/contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        int databaseSizeBeforeUpdate = contactRepository.findAll().size();

        // Update the contact
        Contact updatedContact = contactRepository.findById(contact.getId()).get();
        // Disconnect from session so that the updates on updatedContact are not directly saved in db
        em.detach(updatedContact);
        updatedContact
            .badges(UPDATED_BADGES)
            .ajoutContenuImage(UPDATED_AJOUT_CONTENU_IMAGE)
            .ajoutContenuImageContentType(UPDATED_AJOUT_CONTENU_IMAGE_CONTENT_TYPE)
            .login(UPDATED_LOGIN)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .email(UPDATED_EMAIL)
            .telephone(UPDATED_TELEPHONE)
            .ville(UPDATED_VILLE)
            .gravatar(UPDATED_GRAVATAR)
            .linkedin(UPDATED_LINKEDIN)
            .google(UPDATED_GOOGLE)
            .facebook(UPDATED_FACEBOOK)
            .skype(UPDATED_SKYPE)
            .discord(UPDATED_DISCORD)
            .zoom(UPDATED_ZOOM)
            .whereby(UPDATED_WHEREBY)
            .mumble(UPDATED_MUMBLE)
            .tox(UPDATED_TOX)
            .matrix(UPDATED_MATRIX);
        ContactDTO contactDTO = contactMapper.toDto(updatedContact);

        restContactMockMvc.perform(put("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isOk());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeUpdate);
        Contact testContact = contactList.get(contactList.size() - 1);
        assertThat(testContact.getBadges()).isEqualTo(UPDATED_BADGES);
        assertThat(testContact.getAjoutContenuImage()).isEqualTo(UPDATED_AJOUT_CONTENU_IMAGE);
        assertThat(testContact.getAjoutContenuImageContentType()).isEqualTo(UPDATED_AJOUT_CONTENU_IMAGE_CONTENT_TYPE);
        assertThat(testContact.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testContact.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testContact.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testContact.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testContact.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testContact.getVille()).isEqualTo(UPDATED_VILLE);
        assertThat(testContact.getGravatar()).isEqualTo(UPDATED_GRAVATAR);
        assertThat(testContact.getLinkedin()).isEqualTo(UPDATED_LINKEDIN);
        assertThat(testContact.getGoogle()).isEqualTo(UPDATED_GOOGLE);
        assertThat(testContact.getFacebook()).isEqualTo(UPDATED_FACEBOOK);
        assertThat(testContact.getSkype()).isEqualTo(UPDATED_SKYPE);
        assertThat(testContact.getDiscord()).isEqualTo(UPDATED_DISCORD);
        assertThat(testContact.getZoom()).isEqualTo(UPDATED_ZOOM);
        assertThat(testContact.getWhereby()).isEqualTo(UPDATED_WHEREBY);
        assertThat(testContact.getMumble()).isEqualTo(UPDATED_MUMBLE);
        assertThat(testContact.getTox()).isEqualTo(UPDATED_TOX);
        assertThat(testContact.getMatrix()).isEqualTo(UPDATED_MATRIX);
    }

    @Test
    @Transactional
    public void updateNonExistingContact() throws Exception {
        int databaseSizeBeforeUpdate = contactRepository.findAll().size();

        // Create the Contact
        ContactDTO contactDTO = contactMapper.toDto(contact);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactMockMvc.perform(put("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        int databaseSizeBeforeDelete = contactRepository.findAll().size();

        // Delete the contact
        restContactMockMvc.perform(delete("/api/contacts/{id}", contact.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
