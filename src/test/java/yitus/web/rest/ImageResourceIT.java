package yitus.web.rest;

import yitus.YitusApp;
import yitus.domain.Image;
import yitus.repository.ImageRepository;
import yitus.service.ImageService;
import yitus.service.dto.ImageDTO;
import yitus.service.mapper.ImageMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImageResource} REST controller.
 */
@SpringBootTest(classes = YitusApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ImageResourceIT {

    private static final byte[] DEFAULT_CONTENU = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_CONTENU = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_CONTENU_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_CONTENU_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_CONTENU_SHA_1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_CONTENU_SHA_1 = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final byte[] DEFAULT_VIGNETTE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_VIGNETTE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_VIGNETTE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_VIGNETTE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_VIGNETTE_SHA_1 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_VIGNETTE_SHA_1 = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ImageMapper imageMapper;

    @Autowired
    private ImageService imageService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImageMockMvc;

    private Image image;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Image createEntity(EntityManager em) {
        Image image = new Image()
            .contenu(DEFAULT_CONTENU)
            .contenuContentType(DEFAULT_CONTENU_CONTENT_TYPE)
            .contenuSha1(DEFAULT_CONTENU_SHA_1)
            .vignette(DEFAULT_VIGNETTE)
            .vignetteContentType(DEFAULT_VIGNETTE_CONTENT_TYPE)
            .vignetteSha1(DEFAULT_VIGNETTE_SHA_1);
        return image;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Image createUpdatedEntity(EntityManager em) {
        Image image = new Image()
            .contenu(UPDATED_CONTENU)
            .contenuContentType(UPDATED_CONTENU_CONTENT_TYPE)
            .contenuSha1(UPDATED_CONTENU_SHA_1)
            .vignette(UPDATED_VIGNETTE)
            .vignetteContentType(UPDATED_VIGNETTE_CONTENT_TYPE)
            .vignetteSha1(UPDATED_VIGNETTE_SHA_1);
        return image;
    }

    @BeforeEach
    public void initTest() {
        image = createEntity(em);
    }

    @Test
    @Transactional
    public void createImage() throws Exception {
        int databaseSizeBeforeCreate = imageRepository.findAll().size();

        // Create the Image
        ImageDTO imageDTO = imageMapper.toDto(image);
        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isCreated());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeCreate + 1);
        Image testImage = imageList.get(imageList.size() - 1);
        assertThat(testImage.getContenu()).isEqualTo(DEFAULT_CONTENU);
        assertThat(testImage.getContenuContentType()).isEqualTo(DEFAULT_CONTENU_CONTENT_TYPE);
        assertThat(testImage.getContenuSha1()).isEqualTo(DEFAULT_CONTENU_SHA_1);
        assertThat(testImage.getVignette()).isEqualTo(DEFAULT_VIGNETTE);
        assertThat(testImage.getVignetteContentType()).isEqualTo(DEFAULT_VIGNETTE_CONTENT_TYPE);
        assertThat(testImage.getVignetteSha1()).isEqualTo(DEFAULT_VIGNETTE_SHA_1);
    }

    @Test
    @Transactional
    public void createImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageRepository.findAll().size();

        // Create the Image with an existing ID
        image.setId(1L);
        ImageDTO imageDTO = imageMapper.toDto(image);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllImages() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList
        restImageMockMvc.perform(get("/api/images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(image.getId().intValue())))
            .andExpect(jsonPath("$.[*].contenuContentType").value(hasItem(DEFAULT_CONTENU_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].contenu").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTENU))))
            .andExpect(jsonPath("$.[*].contenuSha1").value(hasItem(DEFAULT_CONTENU_SHA_1)))
            .andExpect(jsonPath("$.[*].vignetteContentType").value(hasItem(DEFAULT_VIGNETTE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].vignette").value(hasItem(Base64Utils.encodeToString(DEFAULT_VIGNETTE))))
            .andExpect(jsonPath("$.[*].vignetteSha1").value(hasItem(DEFAULT_VIGNETTE_SHA_1)));
    }
    
    @Test
    @Transactional
    public void getImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get the image
        restImageMockMvc.perform(get("/api/images/{id}", image.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(image.getId().intValue()))
            .andExpect(jsonPath("$.contenuContentType").value(DEFAULT_CONTENU_CONTENT_TYPE))
            .andExpect(jsonPath("$.contenu").value(Base64Utils.encodeToString(DEFAULT_CONTENU)))
            .andExpect(jsonPath("$.contenuSha1").value(DEFAULT_CONTENU_SHA_1))
            .andExpect(jsonPath("$.vignetteContentType").value(DEFAULT_VIGNETTE_CONTENT_TYPE))
            .andExpect(jsonPath("$.vignette").value(Base64Utils.encodeToString(DEFAULT_VIGNETTE)))
            .andExpect(jsonPath("$.vignetteSha1").value(DEFAULT_VIGNETTE_SHA_1));
    }

    @Test
    @Transactional
    public void getNonExistingImage() throws Exception {
        // Get the image
        restImageMockMvc.perform(get("/api/images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        int databaseSizeBeforeUpdate = imageRepository.findAll().size();

        // Update the image
        Image updatedImage = imageRepository.findById(image.getId()).get();
        // Disconnect from session so that the updates on updatedImage are not directly saved in db
        em.detach(updatedImage);
        updatedImage
            .contenu(UPDATED_CONTENU)
            .contenuContentType(UPDATED_CONTENU_CONTENT_TYPE)
            .contenuSha1(UPDATED_CONTENU_SHA_1)
            .vignette(UPDATED_VIGNETTE)
            .vignetteContentType(UPDATED_VIGNETTE_CONTENT_TYPE)
            .vignetteSha1(UPDATED_VIGNETTE_SHA_1);
        ImageDTO imageDTO = imageMapper.toDto(updatedImage);

        restImageMockMvc.perform(put("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isOk());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
        Image testImage = imageList.get(imageList.size() - 1);
        assertThat(testImage.getContenu()).isEqualTo(UPDATED_CONTENU);
        assertThat(testImage.getContenuContentType()).isEqualTo(UPDATED_CONTENU_CONTENT_TYPE);
        assertThat(testImage.getContenuSha1()).isEqualTo(UPDATED_CONTENU_SHA_1);
        assertThat(testImage.getVignette()).isEqualTo(UPDATED_VIGNETTE);
        assertThat(testImage.getVignetteContentType()).isEqualTo(UPDATED_VIGNETTE_CONTENT_TYPE);
        assertThat(testImage.getVignetteSha1()).isEqualTo(UPDATED_VIGNETTE_SHA_1);
    }

    @Test
    @Transactional
    public void updateNonExistingImage() throws Exception {
        int databaseSizeBeforeUpdate = imageRepository.findAll().size();

        // Create the Image
        ImageDTO imageDTO = imageMapper.toDto(image);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageMockMvc.perform(put("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        int databaseSizeBeforeDelete = imageRepository.findAll().size();

        // Delete the image
        restImageMockMvc.perform(delete("/api/images/{id}", image.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
