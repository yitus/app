package yitus.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import yitus.web.rest.TestUtil;

public class DemandeSoutienDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DemandeSoutienDTO.class);
        DemandeSoutienDTO demandeSoutienDTO1 = new DemandeSoutienDTO();
        demandeSoutienDTO1.setId(1L);
        DemandeSoutienDTO demandeSoutienDTO2 = new DemandeSoutienDTO();
        assertThat(demandeSoutienDTO1).isNotEqualTo(demandeSoutienDTO2);
        demandeSoutienDTO2.setId(demandeSoutienDTO1.getId());
        assertThat(demandeSoutienDTO1).isEqualTo(demandeSoutienDTO2);
        demandeSoutienDTO2.setId(2L);
        assertThat(demandeSoutienDTO1).isNotEqualTo(demandeSoutienDTO2);
        demandeSoutienDTO1.setId(null);
        assertThat(demandeSoutienDTO1).isNotEqualTo(demandeSoutienDTO2);
    }
}
