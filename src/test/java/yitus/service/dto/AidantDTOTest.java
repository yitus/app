package yitus.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import yitus.web.rest.TestUtil;

public class AidantDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AidantDTO.class);
        AidantDTO aidantDTO1 = new AidantDTO();
        aidantDTO1.setId(1L);
        AidantDTO aidantDTO2 = new AidantDTO();
        assertThat(aidantDTO1).isNotEqualTo(aidantDTO2);
        aidantDTO2.setId(aidantDTO1.getId());
        assertThat(aidantDTO1).isEqualTo(aidantDTO2);
        aidantDTO2.setId(2L);
        assertThat(aidantDTO1).isNotEqualTo(aidantDTO2);
        aidantDTO1.setId(null);
        assertThat(aidantDTO1).isNotEqualTo(aidantDTO2);
    }
}
