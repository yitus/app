package yitus.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import yitus.web.rest.TestUtil;

public class ReferentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReferentDTO.class);
        ReferentDTO referentDTO1 = new ReferentDTO();
        referentDTO1.setId(1L);
        ReferentDTO referentDTO2 = new ReferentDTO();
        assertThat(referentDTO1).isNotEqualTo(referentDTO2);
        referentDTO2.setId(referentDTO1.getId());
        assertThat(referentDTO1).isEqualTo(referentDTO2);
        referentDTO2.setId(2L);
        assertThat(referentDTO1).isNotEqualTo(referentDTO2);
        referentDTO1.setId(null);
        assertThat(referentDTO1).isNotEqualTo(referentDTO2);
    }
}
