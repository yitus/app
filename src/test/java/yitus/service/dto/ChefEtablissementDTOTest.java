package yitus.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import yitus.web.rest.TestUtil;

public class ChefEtablissementDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChefEtablissementDTO.class);
        ChefEtablissementDTO chefEtablissementDTO1 = new ChefEtablissementDTO();
        chefEtablissementDTO1.setId(1L);
        ChefEtablissementDTO chefEtablissementDTO2 = new ChefEtablissementDTO();
        assertThat(chefEtablissementDTO1).isNotEqualTo(chefEtablissementDTO2);
        chefEtablissementDTO2.setId(chefEtablissementDTO1.getId());
        assertThat(chefEtablissementDTO1).isEqualTo(chefEtablissementDTO2);
        chefEtablissementDTO2.setId(2L);
        assertThat(chefEtablissementDTO1).isNotEqualTo(chefEtablissementDTO2);
        chefEtablissementDTO1.setId(null);
        assertThat(chefEtablissementDTO1).isNotEqualTo(chefEtablissementDTO2);
    }
}
