package yitus.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ReferentMapperTest {

    private ReferentMapper referentMapper;

    @BeforeEach
    public void setUp() {
        referentMapper = new ReferentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(referentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(referentMapper.fromId(null)).isNull();
    }
}
