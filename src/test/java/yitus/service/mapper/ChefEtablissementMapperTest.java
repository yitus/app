package yitus.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChefEtablissementMapperTest {

    private ChefEtablissementMapper chefEtablissementMapper;

    @BeforeEach
    public void setUp() {
        chefEtablissementMapper = new ChefEtablissementMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chefEtablissementMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chefEtablissementMapper.fromId(null)).isNull();
    }
}
