package yitus.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AidantMapperTest {

    private AidantMapper aidantMapper;

    @BeforeEach
    public void setUp() {
        aidantMapper = new AidantMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(aidantMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(aidantMapper.fromId(null)).isNull();
    }
}
