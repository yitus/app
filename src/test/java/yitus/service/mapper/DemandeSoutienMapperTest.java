package yitus.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DemandeSoutienMapperTest {

    private DemandeSoutienMapper demandeSoutienMapper;

    @BeforeEach
    public void setUp() {
        demandeSoutienMapper = new DemandeSoutienMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(demandeSoutienMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(demandeSoutienMapper.fromId(null)).isNull();
    }
}
